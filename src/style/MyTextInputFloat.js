import React from 'react';
import PropTypes from 'prop-types';
import { Animated, TextInput, TouchableWithoutFeedback, View, StyleSheet, Platform } from 'react-native';

import { SCALE_RATIO_HEIGHT_BASIS, SCALE_RATIO_WIDTH_BASIS, FS } from '../constants/Constants';

import BaseInput from './BaseInput';
import style from '../constants/style';

const LABEL_HEIGHT = 20;
const PADDING = 16;

export default class MyTextInputFloat extends BaseInput {
  static propTypes = {
    height: PropTypes.number
  };

  static defaultProps = {
    height: 35 * SCALE_RATIO_HEIGHT_BASIS,
    animationDuration: 300
  };

  render() {
    const { label, height: inputHeight, inputStyle, labelStyle, styleContent } = this.props;
    const { width, focusedAnim, value } = this.state;

    return (
      <View
        style={[
          {
            height: inputHeight + PADDING,
            borderBottomWidth: 1 * SCALE_RATIO_HEIGHT_BASIS,
            borderBottomColor: '#E2D9C8',
            marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS
          },
          styleContent
        ]}
        onLayout={this._onLayout}
      >
        <TouchableWithoutFeedback onPress={this.focus}>
          <Animated.View
            style={{
              position: 'absolute',
              bottom: focusedAnim.interpolate({
                inputRange: [0, 1],
                outputRange:
                  Platform.OS === 'ios'
                    ? [LABEL_HEIGHT - 5 * SCALE_RATIO_HEIGHT_BASIS, LABEL_HEIGHT + 12 * SCALE_RATIO_HEIGHT_BASIS]
                    : [LABEL_HEIGHT - 10 * SCALE_RATIO_HEIGHT_BASIS, LABEL_HEIGHT + 10 * SCALE_RATIO_HEIGHT_BASIS]
              })
            }}
          >
            <Animated.Text
              style={[
                styles.label,
                style.textCaption,
                {
                  paddingBottom: 5 * SCALE_RATIO_HEIGHT_BASIS,
                  paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
                  fontSize: focusedAnim.interpolate({
                    inputRange: [0, 1],
                    outputRange: [14, 10]
                  })
                },
                labelStyle
              ]}
            >
              {label}
            </Animated.Text>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TextInput
          selectionColor={'#AFA99A'}
          ref="input"
          {...this.props}
          style={[
            styles.textInput,
            style.textInput,
            {
              width,
              height: inputHeight
            },
            inputStyle
          ]}
          value={value}
          onBlur={this._onBlur}
          onChange={this._onChange}
          onFocus={this._onFocus}
          underlineColorAndroid={'transparent'}
          autoCapitalize="none"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden'
  },
  label: {
    backgroundColor: '#fff',
    fontFamily: 'Helvetica Neue',
    color: '#AFA99A',
    fontSize: FS(10)
  },
  textInput: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    paddingTop: -5 * SCALE_RATIO_HEIGHT_BASIS,
    paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
    color: '#AFA99A',
    fontSize: FS(10)
  }
});
