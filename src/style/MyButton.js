import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  ActivityIndicator,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  TouchableOpacity,
  View
} from 'react-native';
import Feather from 'react-native-vector-icons/dist/Feather';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import SimpleLineIcons from 'react-native-vector-icons/dist/SimpleLineIcons';
import { DEVICE_HEIGHT, FS, SCALE_RATIO_HEIGHT_BASIS, SCALE_RATIO_WIDTH_BASIS } from '../constants/Constants';
import style, { APP_COLOR, FONT } from '../constants/style';

export default class MyButton extends Component {
  static propTypes = {
    textStyle: Text.propTypes.style,
    disabledStyle: Text.propTypes.style,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node, PropTypes.element]),
    testID: PropTypes.string,
    accessibilityLabel: PropTypes.string,
    activeOpacity: PropTypes.number,
    allowFontScaling: PropTypes.bool,
    isLoading: PropTypes.bool,
    isDisabled: PropTypes.bool,
    activityIndicatorColor: PropTypes.string,
    delayLongPress: PropTypes.number,
    delayPressIn: PropTypes.number,
    delayPressOut: PropTypes.number,
    onPress: PropTypes.func,
    onLongPress: PropTypes.func,
    onPressIn: PropTypes.func,
    onPressOut: PropTypes.func,
    background: TouchableNativeFeedback.propTypes ? TouchableNativeFeedback.propTypes.background : PropTypes.any
  };

  static isAndroid = Platform.OS === 'android';

  // shouldComponentUpdate(nextProps, nextState) {
  //   if (!isEqual(nextProps, this.props)) {
  //     return true;
  //   }
  //   return false;
  // }

  _renderChildren() {
    const childElements = [];
    React.Children.forEach(this.props.children, item => {
      if (typeof item === 'string' || typeof item === 'number') {
        const element = (
          <Text
            style={[style.textButton, { color: this.props.outline ? APP_COLOR : '#fff' }, this.props.textStyle]}
            allowFontScaling={this.props.allowFontScaling}
            key={item}
          >
            {item}
          </Text>
        );
        childElements.push(element);
      } else if (React.isValidElement(item)) {
        childElements.push(item);
      }
    });
    return childElements;
  }

  _renderInnerText() {
    if (this.props.isLoading) {
      return (
        <ActivityIndicator
          animating
          size="small"
          style={{ alignSelf: 'center' }}
          color={this.props.outline ? APP_COLOR : '#fff'}
        />
      );
    }
    return this._renderChildren();
  }

  renderIcon(type, icon, styles) {
    if (type === 'Ionicons') {
      return (
        <Ionicons
          name={icon}
          size={(styles && styles.width) || FS(16)}
          style={styles}
          color={this.props.outline ? APP_COLOR : '#fff'}
        />
      );
    }
    if (type === 'MaterialIcons') {
      return (
        <MaterialIcons
          name={icon}
          size={(styles && styles.width) || FS(16)}
          style={styles}
          color={this.props.outline ? APP_COLOR : '#fff'}
        />
      );
    }
    if (type === 'MaterialCommunityIcons') {
      return (
        <MaterialCommunityIcons
          name={icon}
          size={(styles && styles.width) || FS(16)}
          style={styles}
          color={this.props.outline ? APP_COLOR : '#fff'}
        />
      );
    }
    if (type === 'Feather') {
      return (
        <Feather
          name={icon}
          size={(styles && styles.width) || FS(16)}
          style={styles}
          color={this.props.outline ? APP_COLOR : '#fff'}
        />
      );
    }
    if (type === 'SimpleLineIcons') {
      return (
        <SimpleLineIcons
          name={icon}
          size={(styles && styles.width) || FS(16)}
          style={styles}
          color={this.props.outline ? APP_COLOR : '#fff'}
        />
      );
    }
    if (type === 'Image') {
      return (
        <Image
          source={icon}
          resizeMode="contain"
          style={[{ width: 16 * SCALE_RATIO_WIDTH_BASIS, height: 16 * SCALE_RATIO_WIDTH_BASIS }, styles]}
        />
      );
    }
    return <View style={{ width: 16 * SCALE_RATIO_WIDTH_BASIS, height: 16 * SCALE_RATIO_WIDTH_BASIS }} />;
  }
  render() {
    // Extract Touchable props
    let touchableProps = {
      testID: this.props.testID,
      accessibilityLabel: this.props.accessibilityLabel,
      onPress: this.props.onPress,
      onPressIn: this.props.onPressIn,
      onPressOut: this.props.onPressOut,
      onLongPress: this.props.onLongPress,
      activeOpacity: this.props.activeOpacity,
      delayLongPress: this.props.delayLongPress,
      delayPressIn: this.props.delayPressIn,
      delayPressOut: this.props.delayPressOut
    };
    //Icon
    const {
      leftIcon,
      leftIconType,
      leftIconStyle,

      rightIcon,
      rightIconType,
      rightIconStyle,

      outline,
      width,
      styleContent
    } = this.props;

    const styles = StyleSheet.create({
      button: {
        width,
        height: 40 * SCALE_RATIO_HEIGHT_BASIS,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch',
        justifyContent: 'center',
        borderRadius: (40 * SCALE_RATIO_HEIGHT_BASIS) / 2,
        paddingVertical: 5 * SCALE_RATIO_HEIGHT_BASIS,
        paddingHorizontal: 20 * SCALE_RATIO_HEIGHT_BASIS,
        backgroundColor: outline ? '#fff' : APP_COLOR,
        borderWidth: outline ? 1 : 0,
        borderColor: outline ? APP_COLOR : '#fff'
      },
      textButton: {
        fontSize: FS(14),
        textAlign: 'center',
        fontFamily: FONT.Medium,
        color: outline ? APP_COLOR : '#fff',
        backgroundColor: 'transparent'
      },
      opacity: {
        opacity: 0.5
      }
    });

    if (this.props.isDisabled === true || this.props.isLoading === true) {
      return (
        <View style={[styles.button, this.props.style, styleContent, this.props.disabledStyle || styles.opacity]}>
          {leftIcon ? (
            <View style={{ marginRight: 8 * SCALE_RATIO_WIDTH_BASIS }}>
              {this.renderIcon(leftIconType, leftIcon, leftIconStyle)}
            </View>
          ) : null}
          {this._renderInnerText()}
          {rightIcon ? (
            <View style={{ marginLeft: 8 * SCALE_RATIO_WIDTH_BASIS }}>
              {this.renderIcon(rightIconType, rightIcon, rightIconStyle)}
            </View>
          ) : null}
        </View>
      );
    }

    if (MyButton.isAndroid) {
      touchableProps = Object.assign(touchableProps, {
        background: this.props.background || TouchableNativeFeedback.SelectableBackground()
      });
      return (
        <TouchableNativeFeedback {...touchableProps}>
          <View style={[styles.button, this.props.shadow ? style.shadow : {}, this.props.style, styleContent]}>
            {leftIcon ? (
              <View style={{ marginRight: 8 * SCALE_RATIO_WIDTH_BASIS }}>
                {this.renderIcon(leftIconType, leftIcon, leftIconStyle)}
              </View>
            ) : null}
            {this._renderInnerText()}
            {rightIcon ? (
              <View style={{ marginLeft: 8 * SCALE_RATIO_WIDTH_BASIS }}>
                {this.renderIcon(rightIconType, rightIcon, rightIconStyle)}
              </View>
            ) : null}
          </View>
        </TouchableNativeFeedback>
      );
    }
    return (
      <TouchableOpacity
        {...touchableProps}
        style={[styles.button, this.props.shadow ? style.shadow : {}, this.props.style, styleContent]}
      >
        {leftIcon ? (
          <View style={{ marginRight: 8 * SCALE_RATIO_WIDTH_BASIS }}>
            {this.renderIcon(leftIconType, leftIcon, leftIconStyle)}
          </View>
        ) : null}
        {this._renderInnerText()}
        {rightIcon ? (
          <View style={{ marginLeft: 8 * SCALE_RATIO_WIDTH_BASIS }}>
            {this.renderIcon(rightIconType, rightIcon, rightIconStyle)}
          </View>
        ) : null}
      </TouchableOpacity>
    );
  }
}
