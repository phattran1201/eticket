import style, { APP_COLOR } from '../../constants/style';

const React = require('react');
const { ViewPropTypes } = (ReactNative = require('react-native'));
const PropTypes = require('prop-types');
const createReactClass = require('create-react-class');
const { StyleSheet, Text, View, Animated } = ReactNative;
const Button = require('../../../node_modules/react-native-scrollable-tab-view/Button');

const CustomTabBar = createReactClass({
  propTypes: {
    goToPage: PropTypes.func,
    activeTab: PropTypes.number,
    tabs: PropTypes.array,
    backgroundColor: PropTypes.string,
    activeTextColor: PropTypes.string,
    inactiveTextColor: PropTypes.string,
    textStyle: Text.propTypes.style,
    tabStyle: ViewPropTypes.style,
    renderTab: PropTypes.func,
    underlineStyle: ViewPropTypes.style
  },

  getDefaultProps() {
    return {
      activeTextColor: '#fff',
      inactiveTextColor: '#929292',
      activeTabColor: APP_COLOR,
      inactiveTabColor: '#fff',
      backgroundColor: null
    };
  },

  renderTabOption(name, page) {},

  renderTab(name, page, isTabActive, onPressHandler) {
    const { activeTextColor, inactiveTextColor, textStyle, activeTabColor, inactiveTabColor } = this.props;
    const textColor = isTabActive ? activeTextColor : inactiveTextColor;
    const tabColor = isTabActive ? activeTabColor : inactiveTabColor;
    const fontWeight = isTabActive ? 'bold' : 'normal';

    return (
      <Button
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          overflow: 'visible',
          zIndex: isTabActive ? 1 : 0
        }}
        key={name}
        accessible
        accessibilityLabel={name}
        accessibilityTraits="button"
        onPress={() => onPressHandler(page)}
      >
        <View
          style={[
            styles.tab,
            {
              overflow: 'visible',
              shadowColor: APP_COLOR,
              shadowOffset: {
                width: 0,
                height: 2
              },
              shadowOpacity: 0.5,
              shadowRadius: 2,
              elevation: 2,
              backgroundColor: tabColor,
              height: 30,
              borderRadius: 18,
              marginRight: page % 2 === 0 ? -14 : 0,
              marginLeft: page % 2 === 0 ? 0 : -14,
              zIndex: isTabActive ? 1 : 0,
              paddingHorizontal: 36
            },
            this.props.tabStyle
          ]}
        >
          <Text
            style={[
              {
                color: textColor,
                fontWeight
              },
              textStyle
            ]}
          >
            {name}
          </Text>
        </View>
      </Button>
    );
  },

  render() {
    const containerWidth = this.props.containerWidth;
    const numberOfTabs = this.props.tabs.length;
    const tabUnderlineStyle = {
      position: 'absolute',
      width: containerWidth / numberOfTabs,
      height: 4,
      backgroundColor: APP_COLOR,
      bottom: 0
    };

    const translateX = this.props.scrollValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, containerWidth / numberOfTabs]
    });
    return (
      <Animated.View
        style={[
          styles.tabs,
          { backgroundColor: this.props.backgroundColor, justifyContent: 'center' },
          this.props.style
        ]}
      >
        {this.props.tabs.map((name, page) => {
          const isTabActive = this.props.activeTab === page;
          const renderTab = this.props.renderTab || this.renderTab;
          return renderTab(name, page, isTabActive, this.props.goToPage);
        })}
        {/* <Animated.View
          style={[
            tabUnderlineStyle,
            {
              transform: [{ translateX }]
            },
            this.props.underlineStyle
          ]}
        /> */}
      </Animated.View>
    );
  }
});

const styles = StyleSheet.create({
  tab: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabs: {
    height: 36,
    flexDirection: 'row',
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0
  }
});

export default CustomTabBar;
