/* eslint-disable max-line-length */
import React, { Component } from 'react';
import { Dimensions, Image, StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import { APP_COLOR } from '../../constants/style';

export default class TabBarCenter extends Component {
  render() {
    const style = this.props.setBtn === 0 ? styles.centerView : styles.centerViewBtn;
    return (
      <TouchableOpacity
        style={[style, styles.eqView, {}, this.props.style]}
        onPress={this.props.onPress}
        activeOpacity={this.props.setBtn === 0 ? 0.2 : 1}
      >
        <View />
        <Image
          style={{
            width: 100,
            height: 100
          }}
          resizeMode="contain"
          source={require('../../assets/imgs/tabbar/milkshake_circle.png')}
        />

        {this.props.setBtn === 0 ? null : (
          <Text style={[this.props.styleText, { position: 'absolute', bottom: 0 }]}>{this.props.title}</Text>
        )}
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  eqView: {
    width: 100,
    height: 100,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
    // backgroundColor: APP_COLOR,
    // borderRadius: 80,
    // overflow: 'visible',
    // borderColor: APP_COLOR,
    // shadowColor: APP_COLOR,
    // shadowOffset: {
    //   width: 0,
    //   height: 0
    // },
    // shadowRadius: 5,
    // shadowOpacity: 0.8,
    // elevation: 3
  },
  centerView: {
    position: 'absolute',
    left: Dimensions.get('window').width / 2,
    bottom: 8,
    right: 0,
    marginLeft: -35
  },
  centerViewBtn: {
    position: 'absolute',
    bottom: 8
  },
  centerViewText: {
    color: '#fff',
    fontSize: 34
  }
});
