export function getLocationDelta(lat, lon, distance) {
  const MAGIC_NUMBER = 0.4;
  distance = (MAGIC_NUMBER * distance) / 2; //eslint-disable-line
  const circumference = 40075;
  const oneDegreeOfLatitudeInMeters = 111.32 * 1000;
  const angularDistance = distance / circumference;

  const latitudeDelta = distance / oneDegreeOfLatitudeInMeters;
  const longitudeDelta = Math.abs(
    Math.atan2(
      Math.sin(angularDistance) * Math.cos(lat),
      Math.cos(angularDistance) - Math.sin(lat) * Math.sin(lat)
    )
  );

  return {
    latitudeDelta,
    longitudeDelta
  };
}
