/* eslint-disable max-line-length */
import React from 'react';
import { Image } from 'react-native';
import { SCALE_RATIO_HEIGHT_BASIS, SCALE_RATIO_WIDTH_BASIS } from '../constants/Constants';

const getGenderImage = (gender, size = 1) => {
  if (gender === 'MALE') {
    return (
      <Image
        source={require('../assets/imgs/gender/male.png')}
        style={{
          width: size * 10 * SCALE_RATIO_WIDTH_BASIS,
          height: size * 14.4 * SCALE_RATIO_HEIGHT_BASIS,
          marginRight: size * 3 * SCALE_RATIO_WIDTH_BASIS
        }}
        resizeMode="contain"
      />
    );
  }
  if (gender === 'FEMALE') {
    return (
      <Image
        source={require('../assets/imgs/gender/female.png')}
        style={{
          width: size * 10 * SCALE_RATIO_WIDTH_BASIS,
          height: size * 14.4 * SCALE_RATIO_HEIGHT_BASIS,
          marginRight: size * 3 * SCALE_RATIO_WIDTH_BASIS
        }}
        resizeMode="contain"
      />
    );
  }
  return (
    <Image
      source={require('../assets/imgs/gender/otherGender.png')}
      style={{
        width: size * 15 * SCALE_RATIO_WIDTH_BASIS,
        height: size * 14 * SCALE_RATIO_HEIGHT_BASIS,
        marginRight: size * 3 * SCALE_RATIO_WIDTH_BASIS
      }}
      resizeMode="contain"
    />
  );
};

export default getGenderImage;
