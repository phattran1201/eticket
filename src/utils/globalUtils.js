const global = {
    isChatScreenForeground: false,
    chattingConversationId: '',
    isCallingFromBackground: false,
    callingFromBackgroundUrl: '',
    cancelCallMySelf: true,
    showSpinnerOnMain: true,
    isBackAndroidButtonPressed: false,
    isAcceptCallFromBackground: false,
};

export default global;
