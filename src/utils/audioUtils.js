import Sound from 'react-native-sound';

export const playMessageReceivedSound = () => {
    playSound('facebook_message.mp3');
};

export const playSound = (soundName) => {
    const sound = new Sound(soundName, Sound.MAIN_BUNDLE, (error) => {
        if (!error) {
            sound.play();
        }
    });
};
