const vietnamFlag = require('../assets/imgs/flag/vietnam_flag.png');
const koreaFlag = require('../assets/imgs/flag/korea_flag.png');
const unitedFlag = require('../assets/imgs/flag/united_flag.png');
const usaFlag = require('../assets/imgs/flag/us_flag.png');
const getFlag = flag => {
  switch (flag) {
    case 'VN' || 'vi':
      return vietnamFlag;
    case 'ko' || 'KR':
      return koreaFlag;
    case 'US' || 'us':
      return usaFlag;
    default:
      return unitedFlag;
  }
};

export default getFlag;
