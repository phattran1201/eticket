import { Dimensions, Platform, StyleSheet } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import {
  DEVICE_HEIGHT,
  FS,
  IS_IOS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  DEVICE_WIDTH
} from './Constants';

const height = DEVICE_HEIGHT;
const width = Dimensions.get('window').width;
export const fixHeaderTranslucent =
  Platform.OS === 'android' && DeviceInfo.getSystemVersion() < '4.5' ? 0 : getStatusBarHeight();
export const headerHeight = 60 + fixHeaderTranslucent;

export const APP_COLOR = '#FF4D00';
export const APP_COLOR_2 = '#FFA555';
export const APP_COLOR_TEXT = '#333333';
export const APP_COLOR_TEXT_GRAY = '#707070';
export const APP_COLOR_TEXT_GRAY_2 = '#70707030';
export const APP_COLOR_BLUE = '#799dd8';
export const APP_COLOR_BLUE_2 = '#4B6783';

export const FONT = {
  Black: 'Barlow-Black',
  Bold: 'Barlow-Bold',
  ExtraBold: 'Barlow-ExtraBold',
  ExtraLight: 'Barlow-ExtraLight',
  Italic: 'Barlow-Italic',
  Light: 'Barlow-Light',
  Medium: 'Barlow-Medium',
  Regular: 'Barlow-Regular',
  SemiBold: 'Barlow-SemiBold',
  Thin: 'Barlow-Thin'
};

const style = StyleSheet.create({
  text: {
    fontSize: FS(14),
    fontFamily: FONT.Regular,
    color: APP_COLOR_TEXT,
    backgroundColor: 'transparent'
  },
  textCaption: {
    fontSize: FS(12),
    fontFamily: FONT.Medium,
    color: APP_COLOR,
    backgroundColor: 'transparent'
  },
  textHeader: {
    fontSize: FS(16),
    fontFamily: FONT.Black,
    color: APP_COLOR_TEXT,
    backgroundColor: 'transparent'
    // textAlign: 'center'
  },
  //Button
  button: {
    height: 33 * SCALE_RATIO_HEIGHT_BASIS,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: (41 * SCALE_RATIO_HEIGHT_BASIS) / 2,
    paddingVertical: 8 * SCALE_RATIO_HEIGHT_BASIS,
    paddingHorizontal: 25 * SCALE_RATIO_WIDTH_BASIS,
    shadowRadius: 4
  },
  buttonBottom: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    height: 48 * SCALE_RATIO_HEIGHT_BASIS,
    bottom: 0,
    width,
    backgroundColor: APP_COLOR
  },
  //textInput
  textInput: {
    fontSize: FS(18),
    fontFamily: FONT.Medium,
    color: APP_COLOR_TEXT,
    backgroundColor: 'transparent',
    marginBottom: Platform.OS === 'ios' ? 0 : -5
  },
  textButton: {
    fontSize: FS(14),
    fontFamily: FONT.Medium,
    backgroundColor: 'transparent'
  },
  viewInput: {
    flexDirection: 'row',
    borderColor: Platform.OS === 'ios' ? '#C7AE6D30' : '#70707010',
    borderWidth: 1,
    backgroundColor: '#fff',
    height: 43 * SCALE_RATIO_HEIGHT_BASIS,
    borderRadius: (43 * SCALE_RATIO_HEIGHT_BASIS) / 2,
    paddingVertical: 8 * SCALE_RATIO_HEIGHT_BASIS,
    paddingHorizontal: 25 * SCALE_RATIO_WIDTH_BASIS,
    shadowColor: APP_COLOR,
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 3
  },
  //shadow
  shadow: {
    overflow: 'visible',
    shadowColor: APP_COLOR,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 3
  },
  //header
  titleHeader: {
    fontSize: FS(18),
    fontFamily: FONT.SemiBold,
    color: APP_COLOR_TEXT,
    backgroundColor: 'transparent',
    textAlign: 'center'
  },
  header: {
    // paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
    borderTopWidth: 0,
    height: headerHeight,
    width: DEVICE_WIDTH,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#fff',
    paddingTop: Platform.OS === 'android' && DeviceInfo.getSystemVersion() < '4.5' ? 0 : getStatusBarHeight(),
    borderBottomWidth: 1,
    marginBottom: IS_IOS ? 5 : 0,
    borderColor: Platform.OS === 'ios' ? '#C7AE6D30' : 'transparent',
    shadowColor: APP_COLOR,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 3,
    overflow: 'visible'
  },
  iconHeader: {
    width: 20 * SCALE_RATIO_WIDTH_BASIS,
    height: 20 * SCALE_RATIO_WIDTH_BASIS,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  //modal
  textModal: {
    fontSize: FS(16),
    fontFamily: FONT.Regular,
    color: APP_COLOR,
    backgroundColor: 'transparent',
    textAlign: 'center',
    alignSelf: 'center'
  },
  viewModal: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Platform.OS === 'ios' ? '#C7AE6D30' : '#70707010',
    borderWidth: 1,
    backgroundColor: '#fff',
    marginHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 15 * SCALE_RATIO_HEIGHT_BASIS,
    paddingVertical: 12 * SCALE_RATIO_HEIGHT_BASIS,
    paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
    shadowColor: APP_COLOR,
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 3
  },

  textNormal: {
    fontSize: FS(14),
    fontFamily: FONT.Regular,
    color: APP_COLOR_TEXT,
    backgroundColor: 'transparent'
  },
  //icon
  viewIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 42 * SCALE_RATIO_WIDTH_BASIS,
    width: 42 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: (42 * SCALE_RATIO_WIDTH_BASIS) / 2,
    backgroundColor: APP_COLOR
  },

  ///từ đây ko nên dùng
  content: {
    flex: 1,
    top: 0,
    left: 0
  },
  form: {
    alignItems: 'center'
  },
  // button: {
  //   width: 93 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
  //   height: 16 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
  //   marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
  //   alignSelf: 'center',
  //   justifyContent: 'center',
  //   backgroundColor: 'rgb(109,119,247)'
  // },
  element: {
    flexDirection: 'row',
    width: 80 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginBottom: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginTop: -5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    //textAlign: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  element2: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textBox: {
    textAlign: 'left',
    color: 'rgb(102,123,157)',
    borderColor: 'rgb(240,243,246)',
    borderRadius: 16 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  itemTextBox: {
    width: 93 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 16 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingLeft: 10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingRight: 10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginBottom: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  btnFacebook: {
    backgroundColor: '#365899'
  },
  imgLogo: {
    width,
    height,
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: -1
  },
  boxLogin: {
    marginTop: 150 * SCALE_RATIO_HEIGHT_BASIS - DEVICE_HEIGHT / 224,
    marginHorizontal: width / 6
  },
  boxEmailSignUp: {
    marginTop: 90 * SCALE_RATIO_HEIGHT_BASIS - DEVICE_HEIGHT / 224,
    marginHorizontal: width / 6
  },
  borberSignUp: {
    height: 15 * SCALE_RATIO_HEIGHT_BASIS - DEVICE_HEIGHT / 224,
    borderRadius: 7.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: APP_COLOR
  },
  borderemailsignup: {
    height: 60 * SCALE_RATIO_HEIGHT_BASIS - DEVICE_HEIGHT / 224,
    justifyContent: 'center',
    alignItems: 'center'
  },
  signin: {
    height: 48 * SCALE_RATIO_HEIGHT_BASIS,
    bottom: 0,
    width,
    backgroundColor: APP_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute'
  },
  signupNext: {
    height: 48 * SCALE_RATIO_HEIGHT_BASIS,
    bottom: 24 * SCALE_RATIO_HEIGHT_BASIS + getStatusBarHeight(true) / 2,
    width,
    backgroundColor: APP_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute'
  },
  modalOptionstoSignUp: {
    marginTop: 20 * SCALE_RATIO_HEIGHT_BASIS,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row'
  },
  modalPosition: {
    marginTop: height - 80 * SCALE_RATIO_HEIGHT_BASIS - DEVICE_HEIGHT / 224
  },
  btnNext: {
    height: 15 * SCALE_RATIO_HEIGHT_BASIS - DEVICE_HEIGHT / 224,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: APP_COLOR_TEXT
  }
});

export default style;
