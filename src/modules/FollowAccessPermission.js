/* eslint-disable max-line-length */
import React from 'react';
import { Image, Text, View, TouchableOpacity } from 'react-native';
import { Button, CheckBox, ListItem } from 'react-native-elements';
import Feather from 'react-native-vector-icons/Feather';
import { FS, SCALE_RATIO_WIDTH_BASIS } from '../constants/Constants';
import strings from '../constants/Strings';
import style from '../constants/style';
import { alert } from '../utils/alert';
import MyComponent from './view/MyComponent';

class FollowAccessPermission extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      followAccessPermission: true,
      checkedAll: false,
      checkedTerms: false,
      checkedPrivacy: false,
      checkedLocation: false,
      checkedCommunicationAccess: false
    };
  }

  render() {
    const list = [
      {
        title: 'Location',
        icon: 'location-on',
        subtitle: 'Required to load your near by members',
        necessary: true
      },
      {
        title: 'Photo',
        icon: 'photo-library',
        subtitle: 'Required to register profile photo and send photo during chat',
        necessary: false
      },
      {
        title: 'Camera',
        icon: 'camera-alt',
        subtitle: 'Required to register profile photo and send photo during chat',
        necessary: true
      },
      {
        title: 'Microphone',
        icon: 'mic',
        subtitle: 'Required for voice recognition function and voice call during video call',
        necessary: true
      }
    ];
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#fff'
        }}
      >
        {!this.state.followAccessPermission ? (
          <View
            style={{
              flex: 1,
              backgroundColor: '#fff',
              justifyContent: 'center',
              margin: 5 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <Image
              source={require('../assets/imgs/app_logo.jpeg')}
              style={{
                width: 150 * SCALE_RATIO_WIDTH_BASIS,
                height: 120 * SCALE_RATIO_WIDTH_BASIS,
                alignSelf: 'center'
              }}
              resizeMode="contain"
            />
            <View
              style={{
                margin: 5 * SCALE_RATIO_WIDTH_BASIS,
                borderWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
                borderColor: '#70707050',
                borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <CheckBox
                checkedColor="#6d77f7"
                containerStyle={{
                  borderWidth: 0,
                  width: '80%',
                  backgroundColor: '#fff'
                }}
                title="Agree to all the terms."
                checked={
                  !!(
                    this.state.checkedTerms &&
                    this.state.checkedPrivacy &&
                    this.state.checkedLocation &&
                    this.state.checkedCommunicationAccess
                  )
                }
                onPress={() =>
                  this.setState({
                    checkedAll: true,
                    checkedTerms: true,
                    checkedPrivacy: true,
                    checkedLocation: true,
                    checkedCommunicationAccess: true
                  })
                }
              />
              <Feather
                onPress={() => alert(strings.alert, 'test')}
                name="chevron-right"
                size={20 * SCALE_RATIO_WIDTH_BASIS}
                style={{ marginRight: 10 * SCALE_RATIO_WIDTH_BASIS }}
                color="#707070"
              />
            </View>
            <View
              style={{
                margin: 5 * SCALE_RATIO_WIDTH_BASIS,
                borderWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
                borderColor: '#70707050',
                borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <CheckBox
                checkedColor="#6d77f7"
                containerStyle={{
                  borderWidth: 0,
                  width: '80%',
                  backgroundColor: '#fff'
                }}
                title="Terms of Use"
                checked={this.state.checkedTerms}
                onPress={() => this.setState({ checkedTerms: !this.state.checkedTerms })}
              />
              <Feather
                onPress={() => alert(strings.alert, 'test')}
                name="chevron-right"
                size={20 * SCALE_RATIO_WIDTH_BASIS}
                style={{ marginRight: 10 * SCALE_RATIO_WIDTH_BASIS }}
                color="#707070"
              />
            </View>
            <View
              style={{
                margin: 5 * SCALE_RATIO_WIDTH_BASIS,
                borderWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
                borderColor: '#70707050',
                borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <CheckBox
                checkedColor="#6d77f7"
                containerStyle={{
                  borderWidth: 0,
                  width: '80%',
                  backgroundColor: '#fff'
                }}
                title="Privacy Policy"
                checked={this.state.checkedPrivacy}
                onPress={() => this.setState({ checkedPrivacy: !this.state.checkedPrivacy })}
              />
              <Feather
                onPress={() => alert(strings.alert, 'test')}
                name="chevron-right"
                size={20 * SCALE_RATIO_WIDTH_BASIS}
                style={{ marginRight: 10 * SCALE_RATIO_WIDTH_BASIS }}
                color="#707070"
              />
            </View>
            <View
              style={{
                margin: 5 * SCALE_RATIO_WIDTH_BASIS,
                borderWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
                borderColor: '#70707050',
                borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <CheckBox
                checkedColor="#6d77f7"
                containerStyle={{
                  borderWidth: 0,
                  width: '80%',
                  backgroundColor: '#fff'
                }}
                title="Location-Based Terms and Conditions"
                checked={this.state.checkedLocation}
                onPress={() => this.setState({ checkedLocation: !this.state.checkedLocation })}
              />
              <Feather
                onPress={() => alert(strings.alert, 'test')}
                name="chevron-right"
                size={20 * SCALE_RATIO_WIDTH_BASIS}
                style={{ marginRight: 10 * SCALE_RATIO_WIDTH_BASIS }}
                color="#707070"
              />
            </View>
            <View
              style={{
                margin: 5 * SCALE_RATIO_WIDTH_BASIS,
                borderWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
                borderColor: '#70707050',
                borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <CheckBox
                checkedColor="#6d77f7"
                containerStyle={{
                  borderWidth: 0,
                  width: '80%',
                  backgroundColor: '#fff'
                }}
                title="Mobile communication access permission"
                checked={this.state.checkedCommunicationAccess}
                onPress={() =>
                  this.setState({
                    checkedCommunicationAccess: !this.state.checkedCommunicationAccess
                  })
                }
              />
              <TouchableOpacity
                onPress={() => alert(strings.alert, 'test')}
                style={{ width: 30 * SCALE_RATIO_WIDTH_BASIS, justifyContent: 'center', alignItems: 'center' }}
              >
                <Feather

                  name="chevron-right"
                  size={20 * SCALE_RATIO_WIDTH_BASIS}
                  // style={{ marginRight: 10 * SCALE_RATIO_WIDTH_BASIS }}
                  color="#707070"
                />
              </TouchableOpacity>
            </View>
            <Button
              onPress={() =>
                this.setState({
                  followAccessPermission: !this.state.followAccessPermission
                })
              }
              title={strings.accept}
              disabled={
                !this.state.checkedTerms ||
                !this.state.checkedPrivacy ||
                !this.state.checkedLocation ||
                !this.state.checkedCommunicationAccess
              }
              buttonStyle={{
                backgroundColor: '#6d77f7',
                marginTop: 10 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS
              }}
            />
          </View>
        ) : (
            <View
              style={{
                flex: 1,
                backgroundColor: '#fff',
                margin: 5 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <Text
                style={[
                  style.textCaption,
                  { fontSize: FS(12), textAlign: 'center', alignSelf: 'center', margin: 15 * SCALE_RATIO_WIDTH_BASIS, marginTop: 25 * SCALE_RATIO_WIDTH_BASIS }
                ]}
              >
                Please allow the following access permissions required to properly use this app.
            </Text>
              <View>
                {list.map((item, i) => (
                  <ListItem
                    key={i}
                    title={
                      <View style={{ flexDirection: 'row' }}>
                        <Text style={[style.text, { color: '#234' }]}>{item.title}</Text>
                        <View
                          style={{
                            borderColor: item.necessary ? 'red' : '#707070',
                            borderWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
                            paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
                            marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
                            borderRadius: 20 * SCALE_RATIO_WIDTH_BASIS,
                            alignItems: 'center',
                            justifyContent: 'center'
                          }}
                        >
                          <Text
                            style={[
                              style.textCaption,
                              {
                                fontSize: FS(10),
                                color: item.necessary ? 'red' : '#707070'
                              }
                            ]}
                          >
                            {item.necessary ? 'necessary' : 'optional'}
                          </Text>
                        </View>
                      </View>
                    }
                    subtitle={
                      <View style={{ flexDirection: 'row' }}>
                        <Text style={[style.textCaption, { color: '#707070' }]}>{item.subtitle}</Text>
                      </View>
                    }
                    leftIcon={{ name: item.icon }}
                    hideChevron
                  />
                ))}
                <Button
                  onPress={() => {
                    this.props.onDoneFunc();
                  }}
                  title={strings.accept}
                  buttonStyle={{
                    backgroundColor: '#6d77f7',
                    marginTop: 10 * SCALE_RATIO_WIDTH_BASIS,
                    borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS
                  }}
                />
              </View>
            </View>
          )}
      </View>
    );
  }
}

export default FollowAccessPermission;
