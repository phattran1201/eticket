import React from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import DetailViewableImage from './DetailViewableImage';
import MyComponent from './MyComponent';

export default class ImageSlideShow extends MyComponent {
  render() {
    const { sources, defaultSource, width = Dimensions.get('window').width } = this.props;
    if (!sources) return null;
    if (!sources.length) return null;
    if (sources.length === 0) return null;

    return (
      <View style={styles.container}>
        {sources.length === 1 && (
          <DetailViewableImage
            source={sources[0]}
            style={{
              width,
              height: width
            }}
            defaultSource={defaultSource}
          />
        )}
        {sources.length === 2 && (
          <View style={{ flexDirection: 'row' }}>
            <DetailViewableImage
              source={sources[0]}
              style={{
                width: width / 2 - MARGIN,
                height: width,
                marginRight: MARGIN
              }}
              defaultSource={defaultSource}
            />
            <DetailViewableImage
              source={sources[1]}
              style={{
                width: width / 2,
                height: width
              }}
              defaultSource={defaultSource}
            />
          </View>
        )}
        {sources.length === 3 && (
          <View style={{ flexDirection: 'row' }}>
            <DetailViewableImage
              source={sources[0]}
              style={{
                width: (width * 2) / 3 - MARGIN,
                height: width,
                marginRight: MARGIN
              }}
              defaultSource={defaultSource}
            />
            <View>
              <DetailViewableImage
                source={sources[1]}
                style={{
                  width: width / 3,
                  height: width / 2 - MARGIN,
                  marginBottom: MARGIN
                }}
                defaultSource={defaultSource}
              />
              <DetailViewableImage
                source={sources[2]}
                style={{
                  width: width / 3,
                  height: width / 2
                }}
                defaultSource={defaultSource}
              />
            </View>
          </View>
        )}
        {sources.length === 4 && (
          <View style={{ flexDirection: 'row' }}>
            <DetailViewableImage
              source={sources[0]}
              style={{
                width: (width * 2) / 3 - MARGIN,
                height: width,
                marginRight: MARGIN
              }}
              defaultSource={defaultSource}
            />
            <View>
              <DetailViewableImage
                source={sources[1]}
                style={{
                  width: width / 3,
                  height: width / 3 - MARGIN,
                  marginBottom: MARGIN
                }}
                defaultSource={defaultSource}
              />
              <DetailViewableImage
                source={sources[2]}
                style={{
                  width: width / 3,
                  height: width / 3 - MARGIN,
                  marginBottom: MARGIN
                }}
                defaultSource={defaultSource}
              />
              <DetailViewableImage
                source={sources[3]}
                style={{
                  width: width / 3,
                  height: width / 3
                }}
                defaultSource={defaultSource}
              />
            </View>
          </View>
        )}
        {sources.length === 5 && (
          <View>
            <View style={{ flexDirection: 'row' }}>
              <DetailViewableImage
                source={sources[0]}
                style={{
                  width: width / 2 - MARGIN,
                  height: width / 2 - MARGIN,
                  marginRight: MARGIN,
                  marginBottom: MARGIN
                }}
                defaultSource={defaultSource}
              />
              <DetailViewableImage
                source={sources[1]}
                style={{
                  width: width / 2,
                  height: width / 2 - MARGIN,
                  marginBottom: MARGIN
                }}
                defaultSource={defaultSource}
              />
            </View>
            <View style={{ flexDirection: 'row' }}>
              <DetailViewableImage
                source={sources[2]}
                style={{
                  width: width / 3 - MARGIN,
                  height: width / 3,
                  marginRight: MARGIN
                }}
                defaultSource={defaultSource}
              />
              <DetailViewableImage
                source={sources[3]}
                style={{
                  width: width / 3 - MARGIN,
                  height: width / 3,
                  marginRight: MARGIN
                }}
                defaultSource={defaultSource}
              />
              <View>
                <DetailViewableImage
                  source={sources[4]}
                  style={{
                    width: width / 3,
                    height: width / 3
                  }}
                  defaultSource={defaultSource}
                />
              </View>
            </View>
          </View>
        )}
        {sources.length >= 6 && (
          <View>
            <View style={{ flexDirection: 'row' }}>
              <DetailViewableImage
                source={sources[0]}
                style={{
                  width: width / 2 - MARGIN,
                  height: width / 2 - MARGIN,
                  marginRight: MARGIN,
                  marginBottom: MARGIN
                }}
                defaultSource={defaultSource}
              />
              <DetailViewableImage
                source={sources[1]}
                style={{
                  width: width / 2,
                  height: width / 2 - MARGIN,
                  marginBottom: MARGIN
                }}
                defaultSource={defaultSource}
              />
            </View>
            <View style={{ flexDirection: 'row' }}>
              <DetailViewableImage
                source={sources[2]}
                style={{
                  width: width / 3 - MARGIN,
                  height: width / 3,
                  marginRight: MARGIN
                }}
                defaultSource={defaultSource}
              />
              <DetailViewableImage
                source={sources[3]}
                style={{
                  width: width / 3 - MARGIN,
                  height: width / 3,
                  marginRight: MARGIN
                }}
                defaultSource={defaultSource}
              />
              <View>
                <DetailViewableImage
                  sources={sources.slice(4, sources.length)}
                  style={{
                    width: width / 3,
                    height: width / 3
                  }}
                  defaultSource={defaultSource}
                />
              </View>
            </View>
          </View>
        )}
      </View>
    );
  }
}

const MARGIN = 1.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224);

const styles = StyleSheet.create({});
