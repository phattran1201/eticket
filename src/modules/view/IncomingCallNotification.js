import React from 'react';
import { Animated, Dimensions, Platform, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import DetectNavbar from 'react-native-detect-navbar-android';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import strings from '../../constants/Strings';
import MyComponent from './MyComponent';
import MyImage from './MyImage';

const { height, width } = Dimensions.get('window');

export default class IncomingCallNotification extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      anim: new Animated.Value(0),
      hasNavBar: false
    };
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      DetectNavbar.hasSoftKeys().then(bool => {
        this.setState({ hasNavBar: bool });
      });
    }
    setInterval(() => {
      Animated.sequence([
        Animated.timing(this.state.anim, {
          toValue: 1,
          duration: 400
        }),
        Animated.timing(this.state.anim, {
          toValue: 0,
          duration: 400
        })
      ]).start();
    }, 1000);
  }

  render() {
    console.log('IncomingCallNotification hihi', this.state.hasNavBar);

    console.log('IncomingCallNotification callInfo', this.props.userInfo);
    return (
      <View style={{ flex: 1 }}>
        <StatusBar hidden />
        <MyImage
          source={
            this.props.userInfo && this.props.userInfo.avatar
              ? { uri: this.props.userInfo.avatar }
              : require('../../assets/imgs/app_logo_blur.jpeg')
          }
          style={{ width, height: DEVICE_HEIGHT }}
        />
        {/* <View style={{ position: 'absolute', backgroundColor: 'rgba(0, 0, 0, 0.3921568627)', width, height, justifyContent: 'space-between' }}> */}
        <View
          style={{
            flex: 1,
            position: 'absolute',
            backgroundColor: 'rgba(0, 0, 0, 0.3921568627)',
            width: width + SCALE_RATIO_WIDTH_BASIS,
            height: this.state.hasNavBar ? height : height + getStatusBarHeight(),
            justifyContent: 'space-between'
          }}
        >
          <View
            style={{
              alignItems: 'center',
              paddingTop: 30 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
            }}
          >
            <MyImage
              source={
                this.props.userInfo && this.props.userInfo.avatar
                  ? { uri: this.props.userInfo.avatar }
                  : require('../../assets/imgs/app_logo.jpeg')
              }
              size={30 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
              borderDisabled
            />
            <Text
              style={{
                color: '#fff',
                fontSize: 5.625 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                fontFamily: 'helveticaneue',
                marginTop: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
              }}
            >
              {this.props.userInfo && this.props.userInfo.nickname ? this.props.userInfo.nickname : ''}
            </Text>
            <Text
              style={{
                color: '#fff',
                fontSize: 4.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                fontFamily: 'helveticaneue',
                marginTop: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
              }}
            >
              {this.props.actionMess ? this.props.actionMess : strings.is_calling_you}
            </Text>
          </View>
          {this.props.disabledButton ? (
            <View />
          ) : (
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'flex-end',
                marginBottom: 10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  paddingBottom: 16.4285714286 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                }}
              >
                <TouchableOpacity
                  style={{
                    width: 23 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                    height: 23 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                    borderRadius: 11.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                    backgroundColor: '#f03d25',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                  onPress={this.props.leftPress}
                >
                  <MaterialIcons
                    name={this.props.iconLeft ? this.props.iconLeft : 'close'}
                    size={12 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                    color="#fff"
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.props.rightPress}>
                  {this.props.disabledAnimation ? (
                    <View
                      style={{
                        width: 23 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                        height: 23 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                        borderRadius: 11.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                        backgroundColor: '#4cd137',
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}
                    >
                      <MaterialIcons
                        name={this.props.iconRight ? this.props.iconRight : 'videocam'}
                        // name='phone'
                        size={12 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                        color="#fff"
                      />
                    </View>
                  ) : (
                    <View
                      style={{
                        width: 23 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
                        height: 23 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}
                    >
                      <Animated.View
                        style={{
                          width: 23 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                          height: 23 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                          borderRadius: 11.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                          backgroundColor: '#4cd137',
                          justifyContent: 'center',
                          alignItems: 'center',
                          transform: [
                            {
                              scale: this.state.anim.interpolate({
                                inputRange: [0, 0.25, 0.5, 0.75, 1],
                                outputRange: [1, 1.1, 1, 1.1, 1]
                              })
                            }
                          ]
                        }}
                      >
                        <Animated.View
                          style={{
                            transform: [
                              {
                                rotate: this.state.anim.interpolate({
                                  inputRange: [0, 0.25, 0.5, 0.75, 1],
                                  outputRange: ['0deg', '-20deg', '0deg', '20deg', '0deg']
                                })
                              }
                            ]
                          }}
                        >
                          <MaterialIcons
                            name={this.props.iconRight ? this.props.iconRight : 'videocam'}
                            // name='phone'
                            size={12 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                            color="#fff"
                          />
                        </Animated.View>
                      </Animated.View>
                    </View>
                  )}
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
      </View>
    );
  }
}
