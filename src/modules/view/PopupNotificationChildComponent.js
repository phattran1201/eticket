import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
  DEVICE_HEIGHT,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../constants/Constants';
import style from '../../constants/style';
import MyButton from '../../style/MyButton';
import MyComponent from './MyComponent';
export default class PopupNotificationChildComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      widthButton: 110 * SCALE_RATIO_WIDTH_BASIS
    };
  }
  widthButton(layout) {
    this.setState({ widthButton: layout.width });
  }

  render() {
    const {
      title,
      content,
      subContent,
      styles,

      button1Text,
      onButton1Press,
      widthButton1,

      button2Text,
      onButton2Press,
      widthButton2
    } = this.props;
    return (
      <View style={[style.viewModal, { width: ' 90%' }]}>
        {/* <STouchableOpacity style={styles.closeButton} onPress={() => PopupNotification.hide()}>
            <MaterialIcons name="close" size={10 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224} color="#707070" />
          </STouchableOpacity> */}
        <Text style={style.textModal}>{title.toUpperCase()}</Text>
        <View
          style={{
            height: 1 * SCALE_RATIO_WIDTH_BASIS,
            backgroundColor: '#C7AE6D',
            marginTop: 7 * SCALE_RATIO_HEIGHT_BASIS,
            width: 160 * SCALE_RATIO_WIDTH_BASIS
          }}
        />
        <Text style={[style.textModal, { marginVertical: 20 * SCALE_RATIO_HEIGHT_BASIS }]}>
          {content}
        </Text>
        <Text style={style.textCaption}>{subContent}</Text>
        <View
          style={[
            stylesheet.buttonWrapper,
            styles,
            {
              marginTop: 10 * SCALE_RATIO_HEIGHT_BASIS,
              justifyContent: 'space-around',
              width: ' 100%'
            }
          ]}
        >
          {button1Text && onButton1Press ? (
            <MyButton
              width={widthButton1}
              onPress={onButton1Press}
              onLayout={event => {
                this.widthButton(event.nativeEvent.layout);
              }}
            >
              {button1Text}
            </MyButton>
          ) : (
            <View />
          )}
          {button2Text && onButton2Press ? (
            <MyButton width={this.state.widthButton} onPress={onButton2Press}>
              {button2Text}
            </MyButton>
          ) : (
            <View />
          )}
        </View>
      </View>
    );
  }
}

const stylesheet = StyleSheet.create({
  title: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue',
    fontSize: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: 'bold',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  },
  buttonWrapper: {
    flexDirection: 'row'
  },
  closeButton: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  }
});
