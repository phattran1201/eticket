import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import MyComponent from './MyComponent';

export default class PopupListItem extends MyComponent {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}> {this.props.name} </Text>
        {!this.props.choosed && this.props.rightIcon && (
          <Icon name="ios-arrow-forward" size={6 * SCALE_RATIO_WIDTH_BASIS} color="#bcbcbd" />
        )}
        {this.props.choosed && (
          <Icon name="ios-checkmark" size={12 * SCALE_RATIO_WIDTH_BASIS} color="#41b532" />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 7.7 * SCALE_RATIO_WIDTH_BASIS,
    paddingVertical: 5.6 * SCALE_RATIO_WIDTH_BASIS,
    borderBottomWidth: 0.2 * SCALE_RATIO_WIDTH_BASIS,
    borderStyle: 'solid',
    borderColor: '#bcbcbd'
  },
  text: {
    fontFamily: 'helveticaneue',
    fontSize: 5.3 * ((SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)) * 1.1,
    fontWeight: '400',
    fontStyle: 'normal',
    color: '#707070'
  }
});
