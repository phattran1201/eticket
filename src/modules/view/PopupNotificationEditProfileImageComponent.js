import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { FS, SCALE_RATIO_HEIGHT_BASIS, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import strings from '../../constants/Strings';
import style from '../../constants/style';
import MyButton from '../../style/MyButton';
import { launchCamera, launchImageLibrary } from '../../utils/imagePicker';
import MyComponent from './MyComponent';
import MyImage from './MyImage';
import {
  updateUserInfo,
  uploadImage,
  uploadAvatarImage
} from '../screen/editprofile/EditProfieActions';
import PopupNotification from '../view/PopupNotification';
import MySpinner from '../view/MySpinner';
import alert from '../../utils/alert';

class PopupNotificationEditProfileImageComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      image: null
    };
    this.launchImageLibrary = this.launchImageLibrary.bind(this);
    this.launchCamera = this.launchCamera.bind(this);
  }

  launchImageLibrary() {
    launchImageLibrary()
      .then(response => {
        this.setState({ image: response });
      })
      .catch(() => {});
  }

  launchCamera() {
    launchCamera()
      .then(response => {
        this.setState({ image: response });
      })
      .catch(() => {});
  }

  render() {
    return (
      <View style={[styles.bodyContainer, style.shadow]}>
        <MyImage
          source={{
            uri: this.state.image
              ? this.state.image.uri
              : this.props.userData.avatar
              ? this.props.userData.avatar
              : ''
          }}
          style={styles.image}
          styleContent={{ borderWidth: 0 }}
          resizeMode="cover"
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 18.5 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          <MyButton
            leftIcon="camera"
            leftIconType="Feather"
            textStyle={{ fontSize: FS(10) }}
            onPress={this.launchCamera}
          >
            {strings.take_photo_button_title}
          </MyButton>
          <View style={{ width: 20 * SCALE_RATIO_WIDTH_BASIS }} />
          <MyButton
            leftIcon="image"
            leftIconType="Feather"
            textStyle={{ fontSize: FS(10) }}
            onPress={this.launchImageLibrary}
          >
            {strings.choose_from_library_button_title}
          </MyButton>
        </View>
        <Text style={[style.textNormal, styles.descriptionText]}>
          {strings.edit_image_profile_description}
        </Text>
        <MyButton
          textStyle={{ fontSize: FS(12) }}
          style={{
            marginVertical: 16 * SCALE_RATIO_WIDTH_BASIS,
            marginHorizontal: 80 * SCALE_RATIO_WIDTH_BASIS
          }}
          onPress={() => {
            console.log('Hoang log user ID', this.props.userData.id);
            console.log('Hoang log user token', this.props.token);

            console.log('Hoang log res image', this.state.image);
            console.log('Hoang log token', this.props.token);
            uploadImage([this.state.image], this.props.token)
              .then(dataImage => {
                const temp = { ...this.props.userData };
                temp.avatar = dataImage[0];
                console.log('Hoang log temp', temp);
                MySpinner.show();
                this.props.updateUserInfo(temp, () => {
                  MySpinner.hide();
                  PopupNotification.hide();
                });
              })
              .catch(e => {
                MySpinner.hide();
                alert(strings.alert, strings.alert_edit_info_fail);
                console.log('Hoang log Error Upload Image', e);
              });
          }}
        >
          {strings.confirm.toLocaleUpperCase()}
        </MyButton>
      </View>
    );
  }
}
const mapStateToProps = state => ({
  userData: state.user.userData,
  token: state.user.token
});

const mapActionCreators = {
  updateUserInfo,
  uploadAvatarImage
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(PopupNotificationEditProfileImageComponent);
const styles = StyleSheet.create({
  bodyContainer: {
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
    overflow: 'hidden'
  },
  image: {
    width: 333 * SCALE_RATIO_WIDTH_BASIS,
    height: 333 * SCALE_RATIO_WIDTH_BASIS,
    borderTopLeftRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
    borderTopRightRadius: 15 * SCALE_RATIO_WIDTH_BASIS
  },
  descriptionText: {
    fontSize: FS(11),
    marginVertical: 13 * SCALE_RATIO_HEIGHT_BASIS,
    marginHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
    textAlign: 'center'
  }
});
