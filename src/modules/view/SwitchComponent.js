import React from 'react';
import { Platform, Switch as InternalSwitch } from 'react-native';
import { Switch as ExternalSwitch } from 'react-native-switch';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import MyComponent from './MyComponent';

export default class SwitchComponent extends MyComponent {
  render() {
    if (Platform.OS === 'ios') {
      return (
        <ExternalSwitch
          onValueChange={value => this.props.onValueChange(value)}
          value={this.props.value}
          {...SwitchProperties}
        />
      );
    }
    return (
      <InternalSwitch
        onValueChange={value => this.props.onValueChange(value)}
        value={this.props.value}
        tintColor="#e2ebfc"
        onTintColor="#e2ebfc"
        thumbTintColor={this.props.value ? '#6d77f7' : '#bcbcbd'}
      />
    );
  }
}

const SwitchProperties = {
  circleSize: 6.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
  barHeight: 3.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
  circleBorderWidth: 0,
  backgroundActive: '#e2ebfc',
  backgroundInactive: '#e2ebfc',
  circleActiveColor: '#6d77f7',
  circleInActiveColor: '#bcbcbd',
  switchWidthMultiplier: 1
};
