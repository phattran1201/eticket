import React from 'react';
import { Text, View } from 'react-native';
import Modal from 'react-native-modal';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { DEVICE_HEIGHT, DEVICE_WIDTH, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import strings from '../../constants/Strings';
import MyComponent from './MyComponent';
import MyPopupSpinner from './MyPopupSpinner';
import STouchableOpacity from './STouchableOpacity';

export default class PopupNotification extends MyComponent {
  static instance = null;

  static showText(notificationText = '') {
    PopupNotification.instance.setState({
      notificationText,
      notificationComponent: null,
      notificationComponentOnBottom: null,
      visible: true
    });
  }

  static showComponent(notificationComponent = null, cannotSwipeToClose = true) {
    PopupNotification.instance.setState({
      notificationText: '',
      notificationComponent,
      notificationComponentOnBottom: null,
      visible: true,
      canSwipeToClose: cannotSwipeToClose
    });
  }

  static showComponentOnBottom(notificationComponentOnBottom = null) {
    PopupNotification.instance.setState({
      notificationText: '',
      notificationComponent: null,
      notificationComponentOnBottom,
      visible: true
    });
  }

  static hide() {
    PopupNotification.instance.setState({
      notificationText: '',
      notificationComponent: null,
      notificationComponentOnBottom: null,
      visible: false,
      canSwipeToClose: true
    });
  }

  constructor(props) {
    super(props);
    PopupNotification.instance = this;
    this.state = {
      notificationText: '',
      notificationComponent: null,
      notificationComponentOnBottom: null,
      visible: false,
      canSwipeToClose: true
    };
  }

  onHide = () => {
    if (!PopupNotification.instance.state.canSwipeToClose) return;
    PopupNotification.hide();
  };

  render() {
    return (
      <Modal
        isVisible={PopupNotification.instance.state.visible}
        onBackdropPress={this.onHide}
        onBackButtonPress={this.onHide}
        onSwipe={this.onHide}
        swipeDirection={PopupNotification.instance.state.canSwipeToClose ? 'down' : null}
        style={{
          justifyContent: PopupNotification.instance.state.notificationComponentOnBottom ? 'flex-end' : 'center',
          alignItems: 'center',
          margin: 0,
          height: DEVICE_HEIGHT
        }}
        deviceWidth={DEVICE_WIDTH}
        deviceHeight={DEVICE_HEIGHT}
      >
        {PopupNotification.instance.state.notificationComponent}
        {PopupNotification.instance.state.notificationComponentOnBottom}
        {PopupNotification.instance.state.notificationText.length > 0 && (
          <View
            style={{
              borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              backgroundColor: '#fafafa',
              shadowColor: '#797e87',
              shadowOffset: {
                width: 2,
                height: 2
              },
              shadowRadius: 4,
              shadowOpacity: 1,
              elevation: 2,
              alignItems: 'center',
              paddingTop: 4.6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              paddingBottom: 7.8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              paddingHorizontal: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
            }}
          >
            <STouchableOpacity
              style={{
                position: 'absolute',
                zIndex: 2,
                right: 0,
                top: 0,
                padding: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
              }}
              onPress={() =>
                PopupNotification.instance.setState({
                  notificationText: '',
                  notificationComponent: null,
                  notificationComponentOnBottom: null,
                  visible: false,
                  canSwipeToClose: true
                })
              }
            >
              <MaterialIcons name="clear" size={8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)} color="#707070" />
            </STouchableOpacity>
            <View>
              <Text
                style={{
                  fontFamily: 'helveticaneue',
                  fontSize: 6.7 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
                  fontWeight: '800',
                  fontStyle: 'normal',
                  textAlign: 'center',
                  color: '#555667'
                }}
              >
                {strings.notification}
              </Text>

              <View
                style={{
                  width: 89.8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                  borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                  backgroundColor: '#fafafa',
                  shadowColor: '#797e87',
                  shadowOffset: {
                    width: 2,
                    height: 2
                  },
                  shadowRadius: 4,
                  shadowOpacity: 1,
                  elevation: 2,
                  marginTop: 7.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                }}
              >
                <Text
                  style={{
                    fontFamily: 'helveticaneue',
                    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
                    fontWeight: '600',
                    fontStyle: 'normal',
                    textAlign: 'left',
                    color: '#707070',
                    marginHorizontal: 13.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                    marginVertical: 9.2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                  }}
                >
                  {PopupNotification.instance.state.notificationText}
                </Text>
              </View>
            </View>
          </View>
        )}
        <MyPopupSpinner />
      </Modal>
    );
  }
}
