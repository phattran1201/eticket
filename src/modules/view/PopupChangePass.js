import { Form, Icon, Input, Item } from 'native-base';
import React from 'react';
import { ActivityIndicator, Animated, Dimensions, StyleSheet, Text, View } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import strings from '../../constants/Strings';
import { alert } from '../../utils/alert';
import MyComponent from './MyComponent';
import STouchableOpacity from './STouchableOpacity';

class PopupChangePass extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      pass: '',
      rePass: '',
      slideAnim: new Animated.Value(height),
      isLoading: false
    };
    this.onPressChangePassword = this.onPressChangePassword.bind(this);
  }

  onPressChangePassword() {
    const { pass, rePass } = this.state;
    if (pass === '') {
      alert(strings.please_input_all_information);
      return;
    }
    if (pass.length < 6) {
      alert(strings.password_must_atleast_6_charactor);
      return;
    }
    if (pass === rePass) {
      this.setState({ isLoading: true });
      this.props.onPress(pass, () => this.setState({ isLoading: false }));
    } else {
      alert(strings.password_verified_is_not_match);
      return;
    }
  }
  componentDidMount() {
    Animated.spring(this.state.slideAnim, {
      toValue: 0,
      bounciness: 1,
      duration: 300,
      useNativeDriver: true
    }).start();
  }

  render() {
    const { pass, rePass } = this.state;
    return (
      <Animated.View
        style={[styles.container, { transform: [{ translateY: this.state.slideAnim }] }]}
      >
        {/* <Spinner visible={isLoading} textContent={strings.loading} textStyle={{ color: '#fff' }} overlayColor="rgba(0, 0, 0, 0.5)" /> */}
        <STouchableOpacity
          style={styles.closeButtonContainer}
          onPress={() => this.props.onBackPress()}
        >
          <MaterialIcons
            name="clear"
            size={8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
            color="#707070"
          />
        </STouchableOpacity>
        <View>
          <Text style={styles.textName}>{strings.pop_up_change_pass_screen_name}</Text>
        </View>
        <View style={styles.bodyContainer}>
          <Form style={styles.form}>
            <Item rounded style={styles.itemTextBox}>
              <Icon style={{ color: 'rgb(102,123,157)' }} active name="lock" />
              <Input
                value={pass}
                onChangeText={text => this.setState({ pass: text })}
                autoCorrect={false}
                autoCapitalize="none"
                secureTextEntry
                placeholder={strings.new_password}
                placeholderTextColor="rgb(102,123,157)"
                style={styles.textBox}
                onSubmitEditing={() => this.txtNewPasswordConfirm._root.focus()}
              />
            </Item>
            <Item rounded style={styles.itemTextBox}>
              <Icon style={{ color: 'rgb(102,123,157)' }} active name="lock" />
              <Input
                ref={instance => (this.txtNewPasswordConfirm = instance)}
                value={rePass}
                onChangeText={text => this.setState({ rePass: text })}
                autoCorrect={false}
                autoCapitalize="none"
                secureTextEntry
                placeholder={strings.verified_new_password}
                placeholderTextColor="rgb(102,123,157)"
                style={styles.textBox}
                onSubmitEditing={this.onPressChangePassword}
              />
            </Item>
          </Form>
        </View>
        <STouchableOpacity
          ref={instance => (this.btnChangePassword = instance)}
          onPress={this.onPressChangePassword}
          disabled={this.state.isLoading}
        >
          <View style={styles.buttonAccept}>
            {this.state.isLoading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text style={styles.textButtonAccept}>{strings.change_password}</Text>
            )}
          </View>
        </STouchableOpacity>
      </Animated.View>
    );
  }
}

export default PopupChangePass;

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fafafa',
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    shadowColor: '#1b407e26',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 6.7,
    shadowOpacity: 1,
    elevation: 2,
    width: width - 20 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  closeButtonContainer: {
    position: 'absolute',
    zIndex: 2,
    right: 0 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    top: 0 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    padding: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  closeButton: {
    width: 4.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 4.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  textName: {
    fontFamily: 'helveticaneue',
    fontSize: 5.7 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '800',
    fontStyle: 'normal',
    margin: 4.6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    textAlign: 'center',
    color: '#555667'
  },
  bodyContainer: {
    margin: 6.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  buttonAccept: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#6d77f7',
    shadowColor: '#797e87',
    shadowOffset: {
      width: 1,
      height: 3
    },
    shadowRadius: 2,
    shadowOpacity: 1,
    elevation: 2,
    margin: 6.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginTop: 0,
    flexDirection: 'row'
  },
  textButtonAccept: {
    fontFamily: 'helveticaneue',
    fontSize: 5 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '500',
    fontStyle: 'normal',
    color: '#ffffff'
  },
  form: {
    alignItems: 'center'
  },
  itemTextBox: {
    width: 93 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 16 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingLeft: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingRight: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginBottom: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  textBox: {
    textAlign: 'left',
    color: 'rgb(102,123,157)',
    borderColor: 'rgb(240,243,246)',
    borderRadius: 16 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontSize: 5 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1
  }
});
