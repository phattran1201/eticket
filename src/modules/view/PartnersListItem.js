import React from 'react';
import { Dimensions, Image, StyleSheet, Text, View, ImageBackground } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  FS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  DEVICE_WIDTH
} from '../../constants/Constants';
import style from '../../constants/style';
import getGenderImage from '../../utils/getGenderImage';
import MyComponent from './MyComponent';
import MyImage from './MyImage';
import STouchableOpacity from './STouchableOpacity';
import getFlag from '../../utils/getFlag';

const { width } = Dimensions.get('window');
const imgA = require('../../assets/imgs/class/A.png');
const imgB = require('../../assets/imgs/class/B.png');
const imgC = require('../../assets/imgs/class/A.png');
const imgD = require('../../assets/imgs/class/D.png');
const imgE = require('../../assets/imgs/class/E.png');
export default class PartnersListItem extends MyComponent {
  render() {
    const { avatar, nickname, age, sex, locale, email, introduction } = this.props.item;
    const rank = this.props.item.class;
    return (
      <STouchableOpacity
        style={styles.container}
        onPress={() =>
          this.props.navigation.navigate(ROUTE_KEY.PARTNER_DETAIL_COMPONENT, {
            item: this.props.item
          })
        }
      >
        <ImageBackground
          source={avatar ? { uri: avatar } : require('../../assets/imgs/default_avatar.jpg')}
          style={{
            width: DEVICE_WIDTH / 2 - 1 * SCALE_RATIO_HEIGHT_BASIS,
            height: DEVICE_WIDTH / 2 - 1 * SCALE_RATIO_HEIGHT_BASIS
          }}
          resizeMode='cover'
        >
          <View style={styles.infoContainer}>
            {/* <Image source={getFlag(locale)} style={styles.flagInfoImage} /> */}
            <View style={{ flex: 3 }}>
              <Text numberOfLines={1} style={[style.textNormal, { fontSize: FS(14), color: '#fff' }]}>
                {introduction}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              {getGenderImage(sex, 1)}
              <Text style={[style.textNormal, { fontSize: FS(14), color: '#ffc125' }]}>{age}</Text>
            </View>


          </View>

          {/* <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={['#CD92D3', '#ffffff00']}
            // locations={[-1, -0.5, 0, 0.5, 1]}
            // colors={['#ffffff00', '#CD92D3', '#B492D3', '#CD92D3', '#ffffff00']}
            style={{
              backgroundColor: '#fff',
              position: 'absolute',
              top: SCALE_RATIO_WIDTH_BASIS,
              left: 5 * SCALE_RATIO_WIDTH_BASIS,
              height: 15 * SCALE_RATIO_HEIGHT_BASIS,
              right: 0,
              borderRadius: (20 * SCALE_RATIO_HEIGHT_BASIS) / 2,
              paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              alignContent: 'center'
              // justifyContent: 'center'
            }}
          >
            <Text
              numberOfLines={1}
              style={[
                style.textNormal,
                {
                  width: '70%',
                  marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
                  fontSize: FS(9),
                  color: '#fff',
                  justifyContent: 'flex-start'
                }
              ]}
            >
              Hot girl class {rank}
            </Text>
            <Image
              source={
                rank === 'A'
                  ? imgA
                  : rank === 'B'
                    ? imgB
                    : rank === 'C'
                      ? imgC
                      : rank === 'D'
                        ? imgD
                        : imgE
              }
              style={{
                overflow: 'visible',
                width: 22 * SCALE_RATIO_WIDTH_BASIS,
                height: 22 * SCALE_RATIO_WIDTH_BASIS,
                // alignSelf: 'center',
                justifyContent: 'flex-end'
              }}
              resizeMode="contain"
            />
          </LinearGradient> */}
          {/* <Image
          source={require('../../assets/imgs/danhhieu.png')}
          style={styles.danhhieu}
          resizeMode="contain"
        /> */}
        </ImageBackground>
      </STouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: DEVICE_WIDTH / 2,
    alignItems: 'center',
    borderColor: '#fff',
    borderWidth: 1 * SCALE_RATIO_HEIGHT_BASIS
  },
  infoContainer: {
    marginTop: DEVICE_WIDTH / 2 - 25 * SCALE_RATIO_WIDTH_BASIS,
    // position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
    // width: width / 3 - 30 * SCALE_RATIO_WIDTH_BASIS
    paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
  },
  gender: {
    width: 10 * SCALE_RATIO_WIDTH_BASIS,
    height: 14.4 * SCALE_RATIO_HEIGHT_BASIS,
    marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS
  },
  danhhieu: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: 15 * SCALE_RATIO_HEIGHT_BASIS,
    marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS
  },
  flagInfoImage: {
    width: 18 * SCALE_RATIO_WIDTH_BASIS,
    height: 13 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 4 * SCALE_RATIO_WIDTH_BASIS,
    marginRight: 2 * SCALE_RATIO_WIDTH_BASIS,
  },
});
