import React from 'react';
import { Alert, View, Text, StyleSheet, Image } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { SCALE_RATIO_WIDTH_BASIS, FS, SCALE_RATIO_HEIGHT_BASIS, MIN_HEART_TO_MAKE_A_PHONE_CALL, ROUTE_KEY } from '../../constants/Constants';
import MyComponent from './MyComponent';
import MyImage from './MyImage';
import STouchableOpacity from './STouchableOpacity';
import getFlag from '../../utils/getFlag';
import strings from '../../constants/Strings';
import PopupNotification from './PopupNotification';
import PopupNotificationCallComponent from './PopupNotificationCallComponent';
import style from '../../constants/style';
import getGenderImage from '../../utils/getGenderImage';
import MyButton from '../../style/MyButton';
import store from '../../config/redux/store';

export default class PartnerInRatingPageListItem extends MyComponent {
  render() {
    const { avatar, nickname, age, locale, introduction, sex, rating, email } = this.props.item;
    return (
      <View style={[styles.container, style.shadow]}>
        <MyImage
          source={{ uri: avatar }}
          style={styles.image}
          styleContent={{ borderWidth: 0, }}
        />
        <View style={styles.bodyContainer}>
          <View style={styles.infoContainer}>
            <Image
              source={getFlag(locale)}
              style={styles.flagInfoImage}
            />
            <Text style={[style.textNormal, styles.textInfo, { marginHorizontal: 8 * SCALE_RATIO_WIDTH_BASIS }]}>
              {nickname && nickname.length > 0 ? nickname : email}
            </Text>
            {getGenderImage(sex)}
            <Text style={[style.textNormal, styles.textInfo, { marginHorizontal: 8 * SCALE_RATIO_WIDTH_BASIS }]}>{age}</Text>
          </View>
          <Text style={[style.textNormal, styles.textInfo, { marginTop: 8 * SCALE_RATIO_HEIGHT_BASIS }]}>{introduction}</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'center', marginTop: 21 * SCALE_RATIO_HEIGHT_BASIS }}>
            {
              [0, 1, 2, 3, 4].map(e => (
                <STouchableOpacity
                  style={{ padding: 1 * SCALE_RATIO_WIDTH_BASIS }}
                  key={e.toString()}
                  onPress={() => this.props.onHideItem(e + 1)}
                >
                  <MaterialIcons
                    name={rating > e ? 'star' : 'star-border'}
                    size={35 * SCALE_RATIO_WIDTH_BASIS}
                    color="#C7AE6D"
                  />
                </STouchableOpacity>
              ))
            }
          </View>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: 20 * SCALE_RATIO_WIDTH_BASIS, marginTop: 10 * SCALE_RATIO_WIDTH_BASIS }}>
          <MyButton
            leftIcon={'clock'}
            leftIconType='Feather'
            textStyle={{ fontSize: FS(10) }}
          >
            {'1 day ago'}
          </MyButton>
          <MyButton
            leftIcon={'video'}
            leftIconType='Feather'
            textStyle={{ fontSize: FS(10) }}
            onPress={() => {
              if (store.getState().user.userData.heart < MIN_HEART_TO_MAKE_A_PHONE_CALL) {
                Alert.alert(
                  strings.alert,
                  strings.out_of_heart,
                  [
                    { text: strings.yes, onPress: () => this.props.navigation.navigate(ROUTE_KEY.STORE) },
                  ],
                  { cancelable: true }
                );
                this.props.navigation.goBack();
              } else {
                PopupNotification.showComponent(
                  <PopupNotificationCallComponent
                    item={this.props.item}
                    navigation={this.props.navigation}
                  />
                );
              }
            }
        }
          >
            {strings.video_chat}
          </MyButton>
        </View>
      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    marginTop: 10 * SCALE_RATIO_WIDTH_BASIS,
    marginHorizontal: 28 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
  },
  image: {
    width: 318 * SCALE_RATIO_WIDTH_BASIS,
    height: 302 * SCALE_RATIO_WIDTH_BASIS,
    borderTopLeftRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
    borderTopRightRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
  },
  bodyContainer: {
    paddingHorizontal: 28 * SCALE_RATIO_WIDTH_BASIS,
    paddingVertical: 19 * SCALE_RATIO_WIDTH_BASIS
  },
  infoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  flagInfoImage: {
    width: 24.4 * SCALE_RATIO_WIDTH_BASIS,
    height: 16.26 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 4 * SCALE_RATIO_WIDTH_BASIS,
  },
  textInfo: {
    fontSize: FS(14),
  },
});

