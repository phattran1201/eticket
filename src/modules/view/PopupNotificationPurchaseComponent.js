import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../constants/Constants';
import strings from '../../constants/Strings';
import style from '../../constants/style';
import MyButton from '../../style/MyButton';
import { alert } from '../../utils/alert';
import MyComponent from './MyComponent';

const won = require('../../assets/imgs/icons/won.png');

export default class PopupNotificationPurchaseComponent extends MyComponent {
  render() {
    const values = [5000, 10000, 20000, 50000, 100000, 200000];
    return (
      <View style={styles.bodyContentContainer}>
        <View style={styles.titleTextContainer}>
          <Text style={[style.textNormal, styles.titleText]}>{strings.purchase.toUpperCase()}</Text>
        </View>
        <View style={styles.buttonWrapper}>
          {values.map(e => (
            <View style={styles.buttonElementContainer} key={e.toString()}>
              <MaterialCommunityIcons
                name="heart"
                size={30 * SCALE_RATIO_WIDTH_BASIS}
                color="#C7AE6D"
              />
              <Text style={[style.text, styles.textNumber]}>{e / 1000} + 5</Text>
              <MyButton
                outline
                onPress={() => {
                  alert(strings.alert, strings.this_feature_is_in_development);
                }}
                style={{ marginTop: 3.7 * SCALE_RATIO_HEIGHT_BASIS }}
                leftIconType="Image"
                leftIcon={won}
                leftIconStyle={{
                  tintColor: '#C7AE6D',
                  width: 20 * SCALE_RATIO_WIDTH_BASIS,
                  height: 20 * SCALE_RATIO_WIDTH_BASIS
                }}
              >
                {e}
              </MyButton>
            </View>
          ))}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  bodyContentContainer: {
    borderRadius: 5 * SCALE_RATIO_HEIGHT_BASIS,
    backgroundColor: '#fafafa',
    shadowColor: '#797e87',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 35 * SCALE_RATIO_HEIGHT_BASIS,
    width: DEVICE_WIDTH
  },
  titleTextContainer: {
    textAlign: 'center',
    borderBottomWidth: 2 * SCALE_RATIO_WIDTH_BASIS,
    borderBottomColor: '#282828',
    paddingHorizontal: 13 * SCALE_RATIO_WIDTH_BASIS,
    paddingTop: 19 * SCALE_RATIO_WIDTH_BASIS,
    paddingBottom: 7.5 * SCALE_RATIO_HEIGHT_BASIS
  },
  titleText: {
    fontSize: FS(18)
  },
  buttonElementContainer: {
    width: DEVICE_WIDTH / 3,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 16 * SCALE_RATIO_WIDTH_BASIS
  },
  textNumber: {
    fontSize: FS(12)
  },
  subTitleText: {
    fontFamily: 'helveticaneue',
    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '600',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#707070',
    marginTop: 5.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  }
});
