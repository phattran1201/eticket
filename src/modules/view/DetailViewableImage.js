import React, { Component } from 'react';
import { ActivityIndicator, Dimensions, Modal, StyleSheet, View } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import MyImage from './MyImage';
import STouchableOpacity from './STouchableOpacity';

export default class DetailViewableImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false
    };
  }

  render() {
    const { source, children, defaultSource, sources, styleContent } = this.props;
    const images = [];
    if (sources) {
      sources.forEach(element => {
        if (typeof element === 'number') {
          images.push({
            props: { element }
          });
        } else {
          images.push({
            url: element.uri
          });
        }
      });
    } else if (source) {
      if (typeof source === 'number') {
        images.push({
          props: { source }
        });
      } else {
        images.push({
          url: source.uri
        });
      }
    }

    return (
      <View>
        <Modal
          transparent
          visible={this.state.modalVisible}
          onRequestClose={() => this.setState({ modalVisible: false })}
        >
          <View style={styles.modalContainer}>
            <ImageViewer
              imageUrls={images}
              enableImageZoom
              enableSwipeDown
              onSwipeDown={() => this.setState({ modalVisible: false })}
              loadingRender={() => (
                <ActivityIndicator size="small" color="white" style={{ marginTop: height / 2 }} />
              )}
              renderImage={props => (
                <MyImage
                  {...props}
                  styleContent={{ borderWidth: 0 }}
                  resizeMethod="auto"
                  resizeMode="contain"
                  defaultSource={defaultSource}
                />
              )}
              renderIndicator={images.length > 1 ? undefined : () => null}
            />
            <STouchableOpacity
              onPress={() => this.setState({ modalVisible: false })}
              style={styles.iconBackContainer}
            >
              <MaterialIcons
                name="keyboard-arrow-left"
                size={13 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                color="white"
              />
            </STouchableOpacity>
          </View>
        </Modal>
        <STouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
          {children}
          {!children && <MyImage {...this.props} styleContent={{ borderWidth: 0 }} />}
        </STouchableOpacity>
      </View>
    );
  }
}

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.9)'
  },
  iconBackContainer: {
    paddingVertical: 10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingHorizontal: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    position: 'absolute',
    zIndex: 99,
    elevation: 99
  },
  iconBack: {
    width: 10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    tintColor: 'white'
  }
});
