import React from 'react';
import {
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../constants/Constants';
import strings from '../../constants/Strings';
import style from '../../constants/style';
import MyButton from '../../style/MyButton';
import getGenderImage from '../../utils/getGenderImage';
import { sendMessageWithoutConversation } from '../screen/detailMessage/ListMessageAction';
import MyComponent from './MyComponent';
import MyImage from './MyImage';
import MySpinner from './MySpinner';
import PopupNotification from './PopupNotification';

class PopupNotificationMessageComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      inputHeight: 0
    };
  }
  _handleSizeChange = event => {
    this.setState({
      inputHeight:
        event.nativeEvent.contentSize.height > 150 * SCALE_RATIO_HEIGHT_BASIS
          ? 150 * SCALE_RATIO_HEIGHT_BASIS
          : event.nativeEvent.contentSize.height
    });
  };
  render() {
    const { onButtonPress, item } = this.props;
    const { avatar, locale, nickname, email, age, sex, rating, introduction } = this.props.item;
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'position' : null}
        style={{ flex: 1, position: 'absolute', bottom: 0 }}
        enabled
      >
        <View style={styles.bodyContainer}>
          <View
            style={{
              marginBottom: -60 * SCALE_RATIO_HEIGHT_BASIS,
              zIndex: 99,
              elevation: 2
            }}
          >
            <MyImage
              flag={locale}
              source={{ uri: avatar }}
              size={82 * SCALE_RATIO_WIDTH_BASIS}
              styleContent={{ backgroundColor: '#fff' }}
            />
          </View>
          <View style={styles.bodyContentContainer}>
            <View style={styles.infoContainer}>
              <Text style={[style.textNormal, { fontSize: FS(18) }]}>
                {nickname || email}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 7 * SCALE_RATIO_HEIGHT_BASIS,
                  marginBottom: 5 * SCALE_RATIO_HEIGHT_BASIS
                }}
              >
                {getGenderImage(sex, 0.7)}
                <Text style={[style.textNormal, { fontSize: FS(12) }]}>{age}</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginLeft: 18 * SCALE_RATIO_WIDTH_BASIS
                  }}
                >
                  {[0, 1, 2, 3, 4].map(e => (
                    <MaterialIcons
                      key={e.toString()}
                      name={rating > e ? 'star' : 'star-border'}
                      size={FS(12)}
                      color="#C7AE6D"
                      style={{ marginRight: 2 * SCALE_RATIO_WIDTH_BASIS }}
                    />
                  ))}
                </View>
              </View>
              <Text style={[style.textNormal, { fontSize: FS(10) }]}>{introduction}</Text>
            </View>
            <View
              style={{
                width: '100%',
                marginHorizontal: 27 * SCALE_RATIO_WIDTH_BASIS,
                borderWidth: SCALE_RATIO_WIDTH_BASIS,
                borderColor: '#C7AE6D',
                borderRadius: 20 * SCALE_RATIO_WIDTH_BASIS,
                paddingHorizontal: 8 * SCALE_RATIO_WIDTH_BASIS,
                height: Math.max(35, this.state.inputHeight),
                maxHeight: 150 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              <TextInput
                ref={ref => {
                  this.textInput = ref;
                }}
                placeholder="Nhập tin nhắn..."
                placeholderTextColor="#9297D350"
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                multiline
                value={this.state.text}
                onChangeText={text => this.setState({ text })}
                onContentSizeChange={event => this._handleSizeChange(event)}
                autoCorrect={false}
                style={{
                  fontFamily: 'helveticaneue',
                  fontSize: FS(15),
                  flex: 1,
                  color: '#282828',
                  height: Math.max(35, this.state.inputHeight),
                  maxHeight: 150 * SCALE_RATIO_HEIGHT_BASIS,
                  marginBottom: -2 * SCALE_RATIO_HEIGHT_BASIS
                }}
                onFocus={this.emojiFalse}
              />
            </View>

            <MyButton
              style={{ alignSelf: 'center', marginTop: 12 * SCALE_RATIO_HEIGHT_BASIS }}
              width={130 * SCALE_RATIO_WIDTH_BASIS}
              onPress={() => {
                MySpinner.show();
                this.props.sendMessageWithoutConversation(
                  this.props.item,
                  this.state.text,
                  MySpinner.hide,
                  PopupNotification.hide
                );
              }}
              rightIcon="send"
              rightIconType="Feather"
              textStyle={{ fontSize: FS(13), fontWeight: '600' }}
            >
              {strings.send.toUpperCase()}
            </MyButton>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => ({});

const mapActionCreators = {
  sendMessageWithoutConversation
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(PopupNotificationMessageComponent);

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  bodyContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  bodyContentContainer: {
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#fafafa',
    shadowColor: '#797e87',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 2,
    alignItems: 'center',
    padding: 7.8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    width
  },
  timeRemainingText: {
    fontSize: FS(30),
    textAlign: 'center',
    marginTop: 27 * SCALE_RATIO_HEIGHT_BASIS
  },
  subTitleText: {
    fontSize: FS(14),
    marginTop: 27 * SCALE_RATIO_HEIGHT_BASIS,
    textAlign: 'center'
  },
  infoContainer: {
    alignItems: 'center',
    marginTop: 38 * SCALE_RATIO_HEIGHT_BASIS,
    marginBottom: 20 * SCALE_RATIO_HEIGHT_BASIS
  }
});
