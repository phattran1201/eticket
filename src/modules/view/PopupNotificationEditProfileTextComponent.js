import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../constants/Constants';
import strings from '../../constants/Strings';
import style from '../../constants/style';
import MyButton from '../../style/MyButton';
import MyTextInput from '../../style/MyTextInput';
import { alert } from '../../utils/alert';
import { updateUserInfo } from '../screen/editprofile/EditProfieActions';
import PopupNotification from '../view/PopupNotification';
import MyComponent from './MyComponent';
import MySpinner from './MySpinner';

class PopupNotificationEditProfileTextComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  render() {
    const { title, icon, placeHolder, description } = this.props;
    return (
      <View style={styles.bodyContainer}>
        <View style={styles.bodyContentContainer}>
          <View style={styles.titleTextContainer}>
            <Text style={[style.textNormal, styles.titleText]}>{title.toUpperCase()}</Text>
          </View>
          <View style={{ height: 18 * SCALE_RATIO_HEIGHT_BASIS }} />
          <MyTextInput
            noShadow
            leftIconType="Feather"
            leftIcon={icon}
            placeholder={placeHolder}
            value={this.state.text}
            onChangeText={text => this.setState({ text })}
            keyboardType={description === strings.edit_age_description ? 'number-pad' : 'default'}
          />
          <Text style={[style.textNormal, styles.descriptionText]}>{description}</Text>
          <MyButton
            outline
            style={{
              marginBottom: 18 * SCALE_RATIO_HEIGHT_BASIS,
              marginHorizontal: 60 * SCALE_RATIO_WIDTH_BASIS
            }}
            onPress={() => {
              console.log('Hoang log user ID', this.props.userData.id);
              console.log('Hoang log user token', this.props.token);
              const temp = { ...this.props.userData };
              if (!this.state.text) {
                alert(strings.alert, strings.please_input_all_information);
              } else {
                if (description === strings.edit_nickname_description) {
                  temp.nickname = this.state.text;
                  console.log('Hoang log 1', temp);
                } else if (description === strings.edit_self_introduction_description) {
                  temp.introduction = this.state.text;
                  console.log('Hoang log 2');
                } else if (description === strings.edit_address_description) {
                  temp.address = this.state.text;
                } else if (description === strings.edit_age_description) {
                  temp.age = this.state.text;
                }

                MySpinner.show();
                this.props.updateUserInfo(temp, () => {
                  PopupNotification.hide();
                  MySpinner.hide();
                });
              }
            }}
          >
            {strings.confirm}
          </MyButton>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userData: state.user.userData,
  token: state.user
});

const mapActionCreators = {
  updateUserInfo
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(PopupNotificationEditProfileTextComponent);

const styles = StyleSheet.create({
  title: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue',
    fontSize: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: 'bold',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  },
  bodyContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  bodyContentContainer: {
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#fafafa',
    shadowColor: '#797e87',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 2,
    alignItems: 'center',
    marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
    paddingHorizontal: 18 * SCALE_RATIO_WIDTH_BASIS
  },
  titleText: {
    fontSize: FS(18)
  },
  descriptionText: {
    fontSize: FS(12),
    marginVertical: 13 * SCALE_RATIO_HEIGHT_BASIS
  },
  titleTextContainer: {
    textAlign: 'center',
    borderBottomWidth: 2 * SCALE_RATIO_WIDTH_BASIS,
    borderBottomColor: '#282828',
    paddingHorizontal: 13 * SCALE_RATIO_WIDTH_BASIS,
    paddingTop: 19 * SCALE_RATIO_WIDTH_BASIS,
    paddingBottom: 7.5 * SCALE_RATIO_HEIGHT_BASIS
  }
});
