import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import MyComponent from './MyComponent';
import {
  SCALE_RATIO_WIDTH_BASIS,
  DEVICE_WIDTH,
  FS,
  DEVICE_HEIGHT,
  SCALE_RATIO_HEIGHT_BASIS,
  ROUTE_KEY
} from '../../constants/Constants';
import { getBottomSpace } from 'react-native-iphone-x-helper';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import style, { FONT, APP_COLOR } from '../../constants/style';
import { getNumberWithCommas } from '../../utils/numberUtils';
import { DATA_MILK_TEA } from '../../constants/dataTest';
import MyButton from '../../style/MyButton';
import strings from '../../constants/Strings';
import { alert } from '../../utils/alert';
import STouchableOpacity from './STouchableOpacity';
import Modal from 'react-native-modal';

class CartDetailComponent extends MyComponent {
  constructor(props) {
    super(props);
    // const { timeRemaining } = props;
    // this.state = {
    //   remaining: timeRemaining || 30 //30s to call
    // };
    // global.cancelCallMySelf = true;
     this.state = {
      data: DATA_MILK_TEA
    };
  }

  render() {
    return (
      <View style={{ 
        position: 'absolute', 
        width: DEVICE_WIDTH, 
        height: DEVICE_HEIGHT,
        bottom: 70 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace()
        }}
      >
        <STouchableOpacity style={{ opacity: 0.3, backgroundColor: 'black', flex: 1 }} onPress={this.props.onClose} />
        <View
            style={[
              {
                borderTopLeftRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
                borderTopRightRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
                width: DEVICE_WIDTH,
                backgroundColor: '#fff',
                // position: 'absolute',
                paddingTop: 10 * SCALE_RATIO_WIDTH_BASIS,
                paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
                // bottom: 100 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace(),
                paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
          >
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.2,
                borderColor: '#70707020',
                alignItems: 'center',
                paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
                marginBottom: 5 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <View style={{ flex: 1 }}>
                <MaterialCommunityIcons
                  onPress={() => this.setState({ showMyCart: false })}
                  name="arrow-down-circle"
                  size={18 * SCALE_RATIO_WIDTH_BASIS}
                  color="#808080"
                  style={{ textAlign: 'left' }}
                />
              </View>
              <View style={{ flex: 3 }}>
                <Text style={[style.titleHeader, { textAlign: 'center' }]}>Giỏ hàng của tôi</Text>
              </View>
              <STouchableOpacity
                onPress={() => this.setState({ data: [] })}
              >
              <View style={{ flex: 1 }}>
                <Text style={[style.text, { textAlign: 'right' }]}>Xóa tất cả</Text>
              </View>
              </STouchableOpacity>
            </View>
            {this.state.data.map((item, i) => {
              if (item.total === 0) return true;
              return (
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 5 * SCALE_RATIO_WIDTH_BASIS
                  }}
                >
                  <View style={{ maxWidth: '70%' }}>
                    <Text
                      style={[
                        style.titleHeader,
                        {
                          fontSize: FS(12),
                          color: '#707070',
                          textAlign: 'left'
                        }
                      ]}
                    >
                      {item.name}
                    </Text>
                    <Text
                      style={[
                        style.text,
                        {
                          fontSize: FS(10),
                          textAlign: 'left'
                        }
                      ]}
                    >
                      {item.fill}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={[style.textCaption, { fontSize: FS(10) }]}>{getNumberWithCommas(item.price)}</Text>
                      <Text
                        style={[
                          style.textCaption,
                          {
                            fontSize: FS(10),
                            marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS
                          }
                        ]}
                      >
                        x
                      </Text>
                      <Text style={[style.textCaption, { fontSize: FS(10) }]}>{item.total}</Text>
                      <Text
                        style={[
                          style.textCaption,
                          {
                            fontSize: FS(10),
                            marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS
                          }
                        ]}
                      >
                        =
                      </Text>
                      <Text
                        style={[
                          style.textCaption,
                          {
                            fontSize: FS(10),
                            marginLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                            // textDecorationLine: 'line-through'
                          }
                        ]}
                      >
                        {getNumberWithCommas(item.price * item.total)}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}
                  >
                    {item.total > 0 ? (
                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={styles.productItemRightContainer}>
                          <TouchableOpacity onPress={() => (item.total -= 1)}>
                            <MaterialCommunityIcons
                              name="minus-circle"
                              size={24 * SCALE_RATIO_WIDTH_BASIS}
                              color="#7f9dd4"
                            />
                          </TouchableOpacity>
                        </View>
                        <Text
                          style={[
                            style.textHeader,
                            {
                              fontFamily: FONT.SemiBold,
                              alignSelf: 'center',
                              paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS
                            }
                          ]}
                        >
                          {item.total}
                        </Text>
                      </View>
                    ) : null}
                    <View style={styles.productItemRightContainer}>
                      <TouchableOpacity onPress={() => item.total + 1}>
                        <MaterialCommunityIcons
                          name="plus-circle"
                          size={24 * SCALE_RATIO_WIDTH_BASIS}
                          color="#7f9dd4"
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  bodyContainer: {
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bodyContentContainer: {
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#fafafa',
    shadowColor: '#797e87',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 2,
    alignItems: 'center',
    padding: 7.8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    width: DEVICE_WIDTH
  },
  timeRemainingText: {
    fontSize: FS(21),
    textAlign: 'center',
    marginVertical: 22 * SCALE_RATIO_HEIGHT_BASIS,
    fontWeight: '500'
  },
  subTitleText: {
    fontSize: FS(13),
    textAlign: 'center'
  },
  infoContainer: {
    alignItems: 'center',
    marginTop: 38 * SCALE_RATIO_HEIGHT_BASIS
  }
});

const mapStateToProps = state => ({
  userData: state.user.userData
});

const mapActionCreators = {};

export default connect(
  mapStateToProps,
  mapActionCreators
)(CartDetailComponent);
