import RNFetchBlob from 'react-native-fetch-blob';
import { Platform } from 'react-native';
import request from '../../utils/request';
import {
    BASE_URL, UPDTAE_LIST_EMOJI,
    UPDTAE_LIST_SAVED_IMAGE,
} from '../../constants/Constants';
import { getListSticker, saveListSticker } from '../../utils/asyncStorage';
import MySpinner from './MySpinner';

function downloadListSticker(data) {
    return new Promise((resolve, reject) => {
        const res = [];
        let index = 0;
        const length = data.length;

        data.forEach(element => {
            const local = res.find(e => e.link === element.image_url);
            if (local) {
                index++;
                if (index >= length) resolve(res);
            } else {
                RNFetchBlob
                    .config({
                        fileCache: true,
                        appendExt: 'png',
                        path: `${RNFetchBlob.fs.dirs.DocumentDir}/${new Date().getTime()}.png`
                    })
                    .fetch('GET', element.image_url)
                    .then((response) => {
                        index++;
                        if (index >= length) {
                            resolve([...res, {
                                link: element.image_url,
                                url: Platform.OS === 'android' ? 'file://' + response.path() : '' + response.path()
                            }]);
                        } else {
                            res.push({
                                link: element.image_url,
                                url: Platform.OS === 'android' ? 'file://' + response.path() : '' + response.path()
                            });
                        }
                    });
            }
        });
    });
}

export function downloadImage(url) {
    return (dispatch) => {
        RNFetchBlob
            .config({
                fileCache: true,
                appendExt: 'png',
                path: `${RNFetchBlob.fs.dirs.DocumentDir}/${new Date().getTime()}.png`
            })
            .fetch('GET', url)
            .then((res) => {
                dispatch({
                    type: UPDTAE_LIST_SAVED_IMAGE,
                    payload: {
                        link: url,
                        url: Platform.OS === 'android' ? `file://${res.path()}` : `${res.path()}`
                    }
                });
            });
    };
}

export function getListStickerFromLocal() {
    return (dispatch, store) => {
        const data = store();
        if (data.local.listStickerCategory.length > 0) return;
        getListSticker()
            .then(listSticker => {
                request.get(`${BASE_URL}emoji_category/?fields=["$all",{"emojis": ["$all"]}]`)
                    .finish((err, res) => {
                        if (!err && res.body.results.objects.rows.length > 0) {
                            const sticker = listSticker.find(i => i.id === res.body.results.objects.rows[0].id);
                            if (sticker) {
                                dispatch({
                                    type: UPDTAE_LIST_EMOJI,
                                    payload: res.body.results.objects.rows.map((e, index) => {
                                        if (index === 0) {
                                            return ({
                                                ...e,
                                                children: sticker.children
                                            });
                                        }
                                        const sticker2 = listSticker.find(i => i.id === e.id);
                                        if (sticker2) {
                                            return ({
                                                ...e,
                                                children: sticker2.children,
                                            });
                                        }
                                        return e;
                                    }),
                                });
                            } else {
                                downloadListSticker(res.body.results.objects.rows[0].emojis)
                                    .then(response => {
                                        saveListSticker({
                                            id: res.body.results.objects.rows[0].id,
                                            children: response
                                        });
                                        dispatch({
                                            type: UPDTAE_LIST_EMOJI,
                                            payload: res.body.results.objects.rows.map((e, index) => {
                                                if (index === 0) {
                                                    return ({
                                                        ...e,
                                                        children: response
                                                    });
                                                }
                                                const sticker2 = listSticker.find(i => i.id === e.id);
                                                if (sticker2) {
                                                    return ({
                                                        ...e,
                                                        children: sticker2.children,
                                                    });
                                                }
                                                return e;
                                            }),
                                        });
                                    });
                            }
                        }
                    });
            });
    };
}

export function downloadListStickerAndSave(data) {
    return (dispatch, store) => {
        MySpinner.show();
        downloadListSticker(data.emojis)
            .then(response => {
                saveListSticker({
                    id: data.id,
                    children: response
                });
                dispatch({
                    type: UPDTAE_LIST_EMOJI,
                    payload: store().local.listStickerCategory.map(e => {
                        if (e.id === data.id) {
                            return ({
                                ...e,
                                children: response
                            });
                        }
                        return e;
                    }),
                });
                MySpinner.hide();
            });
    };
}
