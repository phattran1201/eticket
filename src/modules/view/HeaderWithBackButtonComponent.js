/* eslint-disable max-line-length */
import PropTypes from 'prop-types';
import React from 'react';
import { StatusBar, Text, View } from 'react-native';
import Icons from 'react-native-vector-icons/Feather';
import { FS, IS_ANDROID, DEVICE_WIDTH } from '../../constants/Constants';
import style, { APP_COLOR, fixHeaderTranslucent, headerHeight, APP_COLOR_2 } from '../../constants/style';
import MyComponent from './MyComponent';
import MyTouchableOpacity from './MyTouchableOpacity';
import LinearGradient from 'react-native-linear-gradient';

export default class HeaderWithBackButtonComponent extends MyComponent {
  static propTypes = {
    translucent: PropTypes.bool
  };

  static defaultProps = {
    translucent: true
  };

  render() {
    const { styleContent, bodyTitle, onPress, noShadow, styleShadow, translucent, styleIcon, iconColor } = this.props;
    return (
      <View
        style={[
          style.header,
          {
            shadowColor: noShadow ? 'transparent' : APP_COLOR,
            borderBottomWidth: noShadow ? 0 : style.header.borderBottomWidth,
            elevation: noShadow ? 0 : style.header.elevation,
            backgroundColor: '#fff'
            // height: translucent ? (IS_ANDROID ? headerHeight + fixHeaderTranslucent : headerHeight) : headerHeight,
            // paddingTop: translucent ? fixHeaderTranslucent : IS_ANDROID ? 0 : fixHeaderTranslucent
          },
          styleContent
        ]}
      >
        <StatusBar backgroundColor="#fff" barStyle="dark-content" translucent={translucent} />
        <LinearGradient
          style={{
            width: DEVICE_WIDTH,
            height: headerHeight,
            position: 'absolute'
          }}
          start={{ x: 0.1, y: 0.75 }}
          end={{ x: 0.75, y: 0.25 }}
          colors={[APP_COLOR, APP_COLOR_2]}
        />
        <MyTouchableOpacity onPress={onPress} style={styleIcon}>
          <Icons name="arrow-left" size={FS(20)} color={iconColor || '#C7AE6D'} />
        </MyTouchableOpacity>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center'
          }}
        >
          <Text numberOfLines={1} style={[style.titleHeader, { fontSize: FS(18) }]}>
            {bodyTitle || ''}
          </Text>
        </View>
        <View style={{}}>
          <Icons name="arrow-right" size={FS(5)} color="transparent" />
        </View>
      </View>
    );
  }
}
