import React from 'react';
import { Dimensions, Image, StyleSheet, Text, View } from 'react-native';
import { FS, ROUTE_KEY, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import style from '../../constants/style';
import getFlag from '../../utils/getFlag';
import getGenderImage from '../../utils/getGenderImage';
import MyComponent from './MyComponent';
import MyImage from './MyImage';
import STouchableOpacity from './STouchableOpacity';

const { width } = Dimensions.get('window');

export default class QuickMatchListItem extends MyComponent {
  render() {
    const {
      avatar,
      nickname,
      age,
      sex,
      locale,
      email
    } = this.props.item;

    return (
      <STouchableOpacity
        style={styles.container}
        onPress={() =>
          this.props.navigation.navigate(ROUTE_KEY.PARTNER_DETAIL_COMPONENT, {
            item: this.props.item
          })
        }
      >
        <View>
          <MyImage
            source={{ uri: avatar }}
            style={styles.image}
            styleContent={{
              borderWidth: 0,
              borderRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
              overflow: 'hidden',
              padding: 0
            }}
          />
          <View
            style={[
              styles.image,
              {
                ...StyleSheet.absoluteFillObject,
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center'
              }
            ]}
          >
            <View
              style={{
                alignSelf: 'center',
                width: width / 1.8 - 10 * SCALE_RATIO_WIDTH_BASIS,
                height: width / 3 - 10 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: 8 * SCALE_RATIO_WIDTH_BASIS,
                overflow: 'hidden',
                borderColor: '#fff',
                borderWidth: 2 * SCALE_RATIO_WIDTH_BASIS,
                zIndex: 99
              }}
            />
          </View>
        </View>
        {this.props.noInfo ? (
          <View />
        ) : (
          <View style={styles.infoContainer}>
            <Text
              numberOfLines={1}
              style={[
                style.textNormal,
                styles.textInfo,
                {
                  marginRight: 5 * SCALE_RATIO_WIDTH_BASIS,
                  alignSelf: 'center'
                }
              ]}
            >
              {nickname || email}
            </Text>
            {getGenderImage(sex, 1.2)}
            <Text style={[style.textNormal, styles.textInfo]}>{age}</Text>
            <Image source={getFlag(locale)} style={styles.flagInfoImage} resizeMode="contain" />
          </View>
        )}
      </STouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
    paddingRight: 10 * SCALE_RATIO_WIDTH_BASIS
  },
  image: {
    width: width / 1.8,
    height: width / 3
  },
  textInfo: {
    fontSize: FS(13)
  },
  infoContainer: {
    marginTop: 10 * SCALE_RATIO_WIDTH_BASIS,
    flexDirection: 'row',
    alignItems: 'center'
  },
  flagInfoImage: {
    marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
    width: 20 * SCALE_RATIO_WIDTH_BASIS,
    height: 15 * SCALE_RATIO_WIDTH_BASIS
  }
});
