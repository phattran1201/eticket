/* eslint-disable max-line-length */
/* eslint-disable camelcase */
import React from 'react';
import { Dimensions, StyleSheet, Text, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Feather from 'react-native-vector-icons/dist/Feather';
import { connect } from 'react-redux';
import {
  FS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../constants/Constants';
import style from '../../constants/style';
import { formatDate } from '../../utils/dateUtils';
import getGenderImage from '../../utils/getGenderImage';
import MyComponent from './MyComponent';
import MyImage from './MyImage';
import PopupNotification from './PopupNotification';
import PopupNotificationMessageComponent from './PopupNotificationMessageComponent';
import PopupNotificationReportComponent from './PopupNotificationReportComponent';
import STouchableOpacity from './STouchableOpacity';

const { width, height } = Dimensions.get('window');
const defaultAvatar = require('../../assets/imgs/default_avatar.jpg');

class PostingListItem extends MyComponent {
  render() {
    const { content, title, created_at, list_image } = this.props.item;
    const { avatar, nickname, sex, age, id, email } = this.props.item.user;
    const isMyPost = id === this.props.userData.id;

    return (
      <View
        style={[styles.container, style.shadow, { borderLeftWidth: 0, borderRightWidth: 0 }]}
        onPress={() => {}}
      >
        <STouchableOpacity
          onPress={() =>
            this.props.navigation.navigate(ROUTE_KEY.PARTNER_DETAIL_COMPONENT, {
              item: this.props.item.user
            })
          }
        >
          <MyImage source={{ uri: avatar || defaultAvatar }} size={60 * SCALE_RATIO_WIDTH_BASIS} />
        </STouchableOpacity>
        <View style={{ paddingHorizontal: 8 * SCALE_RATIO_WIDTH_BASIS, width }}>
          <View style={{ width: (width * 2.8) / 5 }}>
            {title ? (
              <Text
                style={[style.textCaption, { fontSize: FS(11), fontWeight: '600' }]}
                numberOfLines={1}
              >
                {title}
              </Text>
            ) : null}
            {content ? (
              <Text style={[style.textCaption, {}]} numberOfLines={1}>
                {content}
              </Text>
            ) : null}

            {list_image && list_image.length > 0 ? (
              <FastImage
                style={{
                  height: 100 * SCALE_RATIO_HEIGHT_BASIS,
                  width: (width * 2.8) / 5,
                  borderRadius: 5*SCALE_RATIO_WIDTH_BASIS
                }}
                resizeMode={FastImage.resizeMode.cover}
                source={{ uri: list_image[0] }}
              />
            ) : (
              <View />
            )}
            <View
              style={{
                height: 0.5 * SCALE_RATIO_WIDTH_BASIS,
                backgroundColor: '#C7AE6D',
                marginVertical: 8 * SCALE_RATIO_HEIGHT_BASIS
              }}
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              width: (width * 3.6) / 5,
              marginRight: 8 * SCALE_RATIO_HEIGHT_BASIS,
              justifyContent: 'space-between'
            }}
          >
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={[style.textCaption, { marginRight: 5 * SCALE_RATIO_WIDTH_BASIS }]}>
                {nickname || email}
              </Text>
              {getGenderImage(sex)}
              <Text style={[style.textCaption, {}]}>{age}</Text>
            </View>
            <Text
              style={[
                style.textCaption,
                { textAlign: 'right', alignSelf: 'flex-end', justifyContent: 'flex-end' }
              ]}
            >
              {formatDate(created_at, 2)}
            </Text>
          </View>
        </View>

        <View
          style={{
            position: 'absolute',
            top: 15 * SCALE_RATIO_WIDTH_BASIS,
            right: 5 * SCALE_RATIO_WIDTH_BASIS,
            zIndex: 99,
            width: (width * 1) / 5,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          {isMyPost ? (
            <STouchableOpacity
              style={{ padding: 5 * SCALE_RATIO_WIDTH_BASIS }}
              onPress={() => {
                if (this.props.onPress) {
                  this.props.onPress();
                }
              }}
            >
              <Feather name="trash-2" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#EB5F5F" />
            </STouchableOpacity>
          ) : (
            <View style={{ flexDirection: 'row' }}>
              <STouchableOpacity
                style={{ padding: 5 * SCALE_RATIO_WIDTH_BASIS }}
                onPress={() =>
                  PopupNotification.showComponent(
                    <PopupNotificationMessageComponent item={this.props.item.user} />
                  )
                }
              >
                <Feather
                  name="message-square"
                  size={20 * SCALE_RATIO_WIDTH_BASIS}
                  color="#C7AE6D"
                />
              </STouchableOpacity>
              <STouchableOpacity
                style={{ padding: 5 * SCALE_RATIO_WIDTH_BASIS }}
                onPress={() =>
                  PopupNotification.showComponent(
                    <PopupNotificationReportComponent item={this.props.item.user} />
                  )
                }
              >
                <Feather name="alert-octagon" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
              </STouchableOpacity>
            </View>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    marginBottom: 15 * SCALE_RATIO_WIDTH_BASIS,
    flexDirection: 'row',
    padding: 10 * SCALE_RATIO_WIDTH_BASIS
  },
  userInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  flagInfoImage: {
    width: 15 * SCALE_RATIO_WIDTH_BASIS,
    height: 10 * SCALE_RATIO_WIDTH_BASIS
  },
  gender: {
    width: 10 * SCALE_RATIO_WIDTH_BASIS,
    height: 16 * SCALE_RATIO_WIDTH_BASIS,
    marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS
  }
});
const mapStateToProps = state => ({
  userData: state.user.userData
});

const mapActionCreators = {};
export default connect(
  mapStateToProps,
  mapActionCreators
)(PostingListItem);
