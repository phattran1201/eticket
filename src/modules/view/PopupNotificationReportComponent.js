import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { CheckBox } from 'react-native-elements';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../constants/Constants';
import strings from '../../constants/Strings';
import style from '../../constants/style';
import MyButton from '../../style/MyButton';
import { reportFunction } from '../screen/blockfriends/BlockFriendActions';
import MyComponent from './MyComponent';
import MySpinner from './MySpinner';
import PopupNotification from './PopupNotification';

class PopupNotificationReportComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      index: 0
    };
  }
  render() {
    const { item } = this.props;
    const { index } = this.state;
    return (
      <View style={style.viewModal}>
        {/* <STouchableOpacity style={styles.closeButton} onPress={() => PopupNotification.hide()}>
            <MaterialIcons name='close' size={10 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224} color='#000' />
          </STouchableOpacity> */}
        <Text style={style.textModal}>{strings.report.toUpperCase()}</Text>
        <View
          style={{
            height: 1 * SCALE_RATIO_WIDTH_BASIS,
            backgroundColor: '#C7AE6D',
            marginTop: 7 * SCALE_RATIO_HEIGHT_BASIS,
            width: 160 * SCALE_RATIO_WIDTH_BASIS
          }}
        />
        <View style={styles.buttonWrapper}>
          <View style={{ flex: 1 }}>
            <CheckBox
              title={strings.advertising}
              checked={index === 0}
              {...checkBoxConfig}
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checkedColor="#C7AE6D"
              uncheckedColor="#C7AE6D"
              size={18}
              onPress={() => this.setState({ index: 0 })}
            />
            <CheckBox
              title={strings.threat}
              checked={index === 2}
              {...checkBoxConfig}
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checkedColor="#C7AE6D"
              uncheckedColor="#C7AE6D"
              size={18}
              onPress={() => this.setState({ index: 2 })}
            />
            <CheckBox
              title={strings.sexual_act}
              checked={index === 4}
              {...checkBoxConfig}
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checkedColor="#C7AE6D"
              uncheckedColor="#C7AE6D"
              size={18}
              onPress={() => this.setState({ index: 4 })}
            />
          </View>
          <View style={{ flex: 1 }}>
            <CheckBox
              title={strings.abuse}
              checked={index === 1}
              {...checkBoxConfig}
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checkedColor="#C7AE6D"
              uncheckedColor="#C7AE6D"
              size={18}
              onPress={() => this.setState({ index: 1 })}
            />
            <CheckBox
              title={strings.fraud}
              checked={index === 3}
              {...checkBoxConfig}
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checkedColor="#C7AE6D"
              uncheckedColor="#C7AE6D"
              size={18}
              onPress={() => this.setState({ index: 3 })}
            />
            <CheckBox
              title={strings.etc}
              checked={index === 5}
              {...checkBoxConfig}
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checkedColor="#C7AE6D"
              uncheckedColor="#C7AE6D"
              size={18}
              onPress={() => this.setState({ index: 5 })}
            />
          </View>
        </View>
        <Text style={style.textCaption}>{strings.report_confirm}</Text>

        <MyButton
          style={{ marginTop: 5 * SCALE_RATIO_HEIGHT_BASIS, alignSelf: 'center' }}
          onPress={() => {
            PopupNotification.hide();
            setTimeout(() => {
              MySpinner.show();
              this.props.reportFunction(item.id, index, MySpinner.hide);
            }, 300);
          }}
          outline
        >
          {strings.report.toUpperCase()}
        </MyButton>
      </View>
    );
  }
}

const mapStateToProps = state => ({});

const mapActionCreators = {
  reportFunction
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(PopupNotificationReportComponent);

const styles = StyleSheet.create({
  title: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue',
    fontSize: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: 'bold',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  },
  buttonWrapper: {
    flexDirection: 'row',
    marginTop: 20 * SCALE_RATIO_HEIGHT_BASIS,
    marginBottom: 12 * SCALE_RATIO_HEIGHT_BASIS
  },
  closeButton: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  bodyContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  bodyContentContainer: {
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#fafafa',
    shadowColor: '#797e87',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 2,
    alignItems: 'center',
    padding: 7.8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginHorizontal: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  titleText: {
    fontFamily: 'helveticaneue',
    fontSize: 6.7 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '800',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#555667'
  },
  subTitleText: {
    fontFamily: 'helveticaneue',
    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '600',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#707070',
    marginTop: 5.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  notiText: {
    fontFamily: 'helveticaneue',
    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '600',
    fontStyle: 'italic',
    textAlign: 'center',
    color: '#707070',
    marginTop: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  buttonContainer: {
    marginTop: 8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 20 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#6d77f7',
    paddingVertical: 4.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    width: 93 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    alignSelf: 'center'
  },
  buttonText: {
    fontFamily: 'helveticaneue',
    fontSize: 5.7 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: 'normal',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  }
});

const checkBoxConfig = {
  uncheckedColor: '#6d77f7',
  checkedColor: '#6d77f7',
  containerStyle: {
    margin: 0,
    padding: 0,
    borderWidth: 0,
    borderColor: 'transparent',
    backgroundColor: 'transparent'
  }
};
