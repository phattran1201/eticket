import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StatusBar, View } from 'react-native';
import { Button } from 'react-native-material-buttons';
import styles from './styles';

export default class DropdownItem extends PureComponent {
  static defaultProps = {
    color: 'transparent',
    disabledColor: 'transparent',
    rippleContainerBorderRadius: 0,
    shadeBorderRadius: 0
  };

  static propTypes = {
    ...Button.propTypes,

    index: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);

    this.onPress = this.onPress.bind(this);
  }

  onPress() {
    const { onPress, index, blur } = this.props;
    blur();

    this.props.item &&
      this.props.item.onSelect &&
      this.props.item.onSelect(() => {
        if (typeof onPress === 'function') {
          onPress(index);
        }
      });
  }

  render() {
    const { children, style, statusBarColor, index, ...props } = this.props;
    return (
      <View>
        <StatusBar backgroundColor={statusBarColor || '#00000050'} barStyle="dark-content" />
        <Button {...props} style={[styles.container, style]} onPress={this.onPress}>
          {children}
        </Button>
      </View>
    );
  }
}
