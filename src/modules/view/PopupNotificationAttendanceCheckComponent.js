import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { DEVICE_HEIGHT, DEVICE_WIDTH, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import strings from '../../constants/Strings';
import MyComponent from './MyComponent';
import STouchableOpacity from './STouchableOpacity';

export default class PopupNotificationAttendanceCheckComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpened: false
    };
  }

  render() {
    const { onButtonPress } = this.props;
    return (
      <View style={styles.bodyContainer}>
        <Text style={styles.title}>{strings.attendance_check_description}</Text>
        <Image source={require('../../assets/imgs/icons/gift.png')} style={styles.image} />
        <STouchableOpacity style={styles.buttonContainer} onPress={onButtonPress}>
          <Text style={styles.buttonText}>{strings.confirm}</Text>
        </STouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue',
    fontSize: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: 'bold',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#6d77f7'
  },
  bodyContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: DEVICE_WIDTH,
    height: DEVICE_HEIGHT,
    backgroundColor: '#ffffff'
  },
  image: {
    width: DEVICE_WIDTH / 1.5,
    height: DEVICE_WIDTH / 1.5,
    marginVertical: 30 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },
  buttonContainer: {
    marginTop: 8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 20 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#6d77f7',
    paddingVertical: 4.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    width: 93 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    alignSelf: 'center'
  },
  buttonText: {
    fontFamily: 'helveticaneue',
    fontSize: 5.7 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: 'normal',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  }
});
