import React from 'react';
import { View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  DEVICE_HEIGHT,
  SCALE_RATIO_WIDTH_BASIS,
  USER_TYPE_NORMAL
} from '../../constants/Constants';
import DetailViewableImage from './DetailViewableImage';
import MyComponent from './MyComponent';
import MyImage from './MyImage';

const defaultAvatar = require('../../assets/imgs/default_avatar.jpg');

export default class AvatarImage extends MyComponent {
  render() {
    const { userType, isShowDetail, marginTopLeft, isBigImage } = this.props;

    return (
      <View>
        {userType !== USER_TYPE_NORMAL && (
          <View
            style={{
              width: 6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              height: 6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              backgroundColor: '#e7a744',
              position: 'absolute',
              zIndex: 99,
              elevation: 2,
              borderRadius: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              top: isBigImage ? 1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) : 0,
              left: isBigImage ? 1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) : 0,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <MaterialCommunityIcons
              name="crown"
              size={4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
              color="#fff"
            />
          </View>
        )}
        {isShowDetail ? (
          <View
            style={{
              marginTop: marginTopLeft
                ? marginTopLeft * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                : 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              marginLeft: marginTopLeft
                ? marginTopLeft * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                : 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
            }}
          >
            <DetailViewableImage {...this.props} defaultSource={defaultAvatar} />
          </View>
        ) : (
          <View
            style={{
              marginTop: marginTopLeft
                ? marginTopLeft * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                : 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              marginLeft: marginTopLeft
                ? marginTopLeft * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                : 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
            }}
          >
            <MyImage {...this.props} defaultSource={defaultAvatar} />
          </View>
        )}
      </View>
    );
  }
}
