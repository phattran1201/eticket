import React from 'react';
import { Dimensions, ScrollView, StyleSheet, Text, View } from 'react-native';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import MyComponent from './MyComponent';
import STouchableOpacity from './STouchableOpacity';

const { width } = Dimensions.get('window');

export default class MyTabView extends MyComponent {
  constructor(props) {
    super(props);

    this.state = {
      index: props.initalIndex ? props.initalIndex : 0
    };
    this.onChangeTabPrivate = this.onChangeTabPrivate.bind(this);
  }

  componentDidMount() {
    if (this.MyTabView) {
      this.MyTabView.scrollTo({ x: width * this.state.index, y: 0, animated: false });
    }
  }

  onChangeTabPrivate = index => () => {
    this.onChangeTab(index);
  };
  onChangeTab = index => {
    this.setState({ index });
    if (this.MyTabView) {
      this.MyTabView.scrollTo({ x: width * index, y: 0, animated: false });
    }
    if (this.props.onTabChanged) this.props.onTabChanged(index);
  };
  render() {
    const {
      children,
      tabbarData,
      onChangeTabPrivate,
      disableChildren,
      containerStyle
    } = this.props;
    const { index } = this.state;
    return (
      <View style={containerStyle || styles.container}>
        <View style={styles.tabbarContainer}>
          {tabbarData &&
            tabbarData.map(e => {
              if (onChangeTabPrivate) {
                return (
                  <STouchableOpacity
                    key={e.key}
                    style={
                      index === e.key
                        ? styles.tabbarSelectedButtonContainer
                        : styles.tabbarButtonContainer
                    }
                    onPress={() => this.props.onChangeTabPrivate(e.key)}
                  >
                    <Text style={index === e.key ? styles.labelSelectedStyle : styles.labelStyle}>
                      {e.title}
                    </Text>
                  </STouchableOpacity>
                );
              }
              return (
                <STouchableOpacity
                  key={e.key}
                  style={
                    index === e.key
                      ? styles.tabbarSelectedButtonContainer
                      : styles.tabbarButtonContainer
                  }
                  onPress={this.onChangeTabPrivate(e.key)}
                >
                  <Text style={index === e.key ? styles.labelSelectedStyle : styles.labelStyle}>
                    {e.title}
                  </Text>
                </STouchableOpacity>
              );
            })}
        </View>
        {disableChildren ? null : (
          <ScrollView
            style={{ flex: 1 }}
            horizontal
            keyboardShouldPersistTaps="always"
            showsHorizontalScrollIndicator={false}
            scrollEnabled={false}
            ref={ref => {
              this.MyTabView = ref;
            }}
          >
            {children}
          </ScrollView>
        )}
      </View>
    );
  }
}

export class MyStatelessTabView extends MyComponent {
  changeTab(index) {
    this.MyTabView.scrollTo({ x: width * index, y: 0, animated: false });
  }
  render() {
    const { children, tabbar, tabbarPosition } = this.props;
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        {tabbarPosition === 'top' && tabbar}
        <ScrollView
          style={{ flex: 1 }}
          horizontal
          keyboardShouldPersistTaps="always"
          showsHorizontalScrollIndicator={false}
          scrollEnabled={false}
          ref={ref => {
            this.MyTabView = ref;
          }}
        >
          {children}
        </ScrollView>
        {tabbarPosition !== 'top' && tabbar}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  tabbarContainer: {
    width,
    height: 16 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#e2e2e2',
    flexDirection: 'row'
  },
  tabbarButtonContainer: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#e2ebfc',
    borderBottomWidth: 0.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  tabbarSelectedButtonContainer: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#f7a823',
    borderBottomWidth: 0.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  labelStyle: {
    fontFamily: 'helveticaneue',
    fontSize: 3.8 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontStyle: 'normal',
    color: '#707070'
  },
  labelSelectedStyle: {
    fontFamily: 'helveticaneue',
    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: 'bold',
    fontStyle: 'normal',
    color: '#707070'
  }
});
