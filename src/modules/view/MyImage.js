import React from 'react';
import { ActivityIndicator, Image, Text, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import getFlag from '../../utils/getFlag';
import MyComponent from './MyComponent';
import style from '../../constants/style';
import AnimatedLottieView from 'lottie-react-native';

const defaultAvatar = require('../../assets/imgs/defaultLogo.jpg');

export default class MyImage extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isError: false
    };
  }

  replaceSource(source = '') {
    if (typeof source !== 'string') return '';
    return source ? source.replace('localhost', 'hitek.com.vn').replace('toi.innoway.info', 'hitek.com.vn') : '';
  }

  render() {
    let source = this.props.source;
    const sources = this.props.sources;
    if (!source || ((source.uri === '' || !source.uri) && isNaN(source))) {
      source = defaultAvatar;
    }
    let flag = this.props.flag;
    if (flag && (flag === '' || !flag)) {
      flag = '3b1213b0-0f0e-11e9-9424-8541c16c9230';
    }

    const visibility = this.props.visibility;
    const { marginTopLeft, isBigImage } = this.props;

    const { size, styleContent } = this.props;
    return (
      <View
        style={[
          style.shadow,
          {
            width: size + 8 * SCALE_RATIO_WIDTH_BASIS,
            height: size + 8 * SCALE_RATIO_WIDTH_BASIS,
            borderRadius: size + (8 * SCALE_RATIO_WIDTH_BASIS) / 2,
            borderWidth: 2 * SCALE_RATIO_WIDTH_BASIS,
            borderColor: this.props.borderDisabled ? 'transparent' : '#fff',
            justifyContent: 'center',
            alignContent: 'center',
            alignItems: 'center'
          },
          styleContent
        ]}
      >
        {sources && sources.length > 1 && (
          <View
            style={[
              // this.props.style,
              {
                backgroundColor: 'rgba(0, 0, 0, 0.4)',
                zIndex: 99,
                elevation: 2,
                position: 'absolute',
                justifyContent: 'center',
                alignItems: 'center',
                width: size,
                height: size,
                borderRadius: size / 2
              }
            ]}
          >
            <Text
              style={{
                fontFamily: 'helveticaneue',
                fontSize: 6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                fontStyle: 'normal',
                color: '#fff'
              }}
            >
              +{sources.length}
            </Text>
          </View>
        )}

        <View
          style={{
            top: -30,
            right: 0,
            zIndex: 999,
            position: 'absolute'
          }}
        >
          <FastImage
            source={require('../../assets/imgs/class/golden_crown.png')}
            resizeMode="contain"
            style={{ width: 30 * SCALE_RATIO_WIDTH_BASIS }}
          />
        </View>
        <FastImage
          style={{
            width: size,
            height: size,
            borderRadius: size / 2
          }}
          {...this.props}
          removeClippedSubviews
          source={
            source
              ? source.uri
                ? { uri: this.replaceSource(source.uri) }
                : source
              : sources
              ? sources[0].uri
                ? { uri: this.replaceSource(sources[0].uri) }
                : sources[0]
              : this.props.defaultSource
          }
          onLoad={() => this.setState({ isLoading: false, isError: false })}
          onError={() => this.setState({ isError: true, isLoading: false })}
          defaultSource={null}
        />
        {flag ? (
          <View
            style={[
              this.props.styleFlag,
              {
                width: size / 4.5,
                height: size / 4.5,
                borderRadius: size / 4.5 / 2,
                position: 'absolute',
                zIndex: 99,
                elevation: 2,
                bottom: 0,
                right: (size / 100) * 5,
                justifyContent: 'center',
                alignItems: 'center'
              }
            ]}
          >
            <Image
              source={getFlag(flag)}
              style={[
                this.props.styleFlag,
                {
                  width: size / 4.5,
                  height: size / 4.5,
                  borderRadius: size / 4.5 / 2,
                  borderWidth: SCALE_RATIO_WIDTH_BASIS,
                  borderColor: '#fff'
                }
              ]}
            />
          </View>
        ) : null}
        {visibility ? (
          <View
            style={[
              this.props.styleStatus,
              {
                opacity: 90,
                backgroundColor: visibility === 'ONLINE' ? '#A4FF04' : visibility === 'IDLE' ? '#EB5F5F' : '#A2A2A2',
                borderColor: '#fff',
                borderWidth: SCALE_RATIO_WIDTH_BASIS,
                width: size / 4.5,
                height: size / 4.5,
                borderRadius: size / 4.5 / 2,
                position: 'absolute',
                zIndex: 99,
                elevation: 2,
                bottom: 0,
                right: (size / 100) * 5,
                justifyContent: 'center',
                alignItems: 'center'
              }
            ]}
          />
        ) : null}
        {!this.props.hideIndicator && this.state.isLoading && !this.state.isError && (
          <View
            style={[
              // this.props.style,
              {
                overflow: 'hidden',
                position: 'absolute',
                backgroundColor: 'rgba(0,0,0,0.5)',
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 2 * SCALE_RATIO_WIDTH_BASIS,
                borderColor: '#fff',
                size,
                width: size,
                height: size,
                borderRadius: size / 2
              }
            ]}
          >
            <AnimatedLottieView
              source={require('../../assets/image_load.json')}
              autoPlay
              loop
              hardwareAccelerationAndroid
              style={{
                alignSelf: 'center',
                backgroundColor: '#F3F3F3',
                width: size,
                height: size,
                borderRadius: size / 2
              }}
              resizeMode="cover"
            />
          </View>
        )}
        {this.state.isError && !this.state.isLoading && (
          <Image
            source={this.props.defaultSource}
            style={[
              // this.props.style,
              {
                position: 'absolute',
                height: size,
                borderRadius: size / 2
              }
            ]}
          />
        )}
      </View>
    );
  }
}
