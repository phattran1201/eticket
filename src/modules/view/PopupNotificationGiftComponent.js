import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../constants/Constants';
import strings from '../../constants/Strings';
import style from '../../constants/style';
import MyComponent from './MyComponent';
import STouchableOpacity from './STouchableOpacity';

class PopupNotificationGiftComponent extends MyComponent {
  render() {
    const values = [50, 100, 300, 500, 1000];
    return (
      <View style={styles.bodyContentContainer}>
        <View style={styles.titleTextContainer}>
          <Text style={[style.textNormal, styles.titleText]}>{strings.gift.toUpperCase()}</Text>
        </View>
        <View style={styles.buttonWrapper}>
          {values.map(e => (
            <STouchableOpacity style={styles.buttonElementContainer} key={e.toString()}>
              <MaterialCommunityIcons
                name="heart"
                size={30 * SCALE_RATIO_WIDTH_BASIS}
                color="#C7AE6D"
              />
              <Text style={[style.text, styles.textNumber]}>{e}</Text>
            </STouchableOpacity>
          ))}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({});

const mapActionCreators = {};

export default connect(
  mapStateToProps,
  mapActionCreators
)(PopupNotificationGiftComponent);

const styles = StyleSheet.create({
  buttonWrapper: {
    flexDirection: 'row'
  },
  bodyContentContainer: {
    borderRadius: 5 * SCALE_RATIO_HEIGHT_BASIS,
    backgroundColor: '#fafafa',
    shadowColor: '#797e87',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 2,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 38 * SCALE_RATIO_WIDTH_BASIS,
    marginBottom: 35 * SCALE_RATIO_HEIGHT_BASIS,
    width: DEVICE_WIDTH
  },
  titleTextContainer: {
    textAlign: 'center',
    borderBottomWidth: 2 * SCALE_RATIO_WIDTH_BASIS,
    borderBottomColor: '#282828',
    paddingHorizontal: 13 * SCALE_RATIO_WIDTH_BASIS,
    paddingTop: 19 * SCALE_RATIO_WIDTH_BASIS,
    paddingBottom: 7.5 * SCALE_RATIO_HEIGHT_BASIS
  },
  titleText: {
    fontSize: FS(18)
  },
  buttonElementContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 14 * SCALE_RATIO_WIDTH_BASIS,
    paddingVertical: 22 * SCALE_RATIO_WIDTH_BASIS
  },
  textNumber: {
    fontSize: FS(23)
  },
  subTitleText: {
    fontFamily: 'helveticaneue',
    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '600',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#707070',
    marginTop: 5.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  }
});
