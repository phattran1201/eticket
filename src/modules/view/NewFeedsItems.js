import { Textarea } from 'native-base';
import React from 'react';
import {
  Alert,
  DeviceEventEmitter,
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import Modal from 'react-native-modal';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  ROUTE_KEY,
  SCALE_RATIO_WIDTH_BASIS,
  USER_TYPE_NORMAL
} from '../../constants/Constants';
import strings from '../../constants/Strings';
import { alert } from '../../utils/alert';
import { formatDate } from '../../utils/dateUtils';
import { getNumberString } from '../../utils/numberUtils';
import {
  addItemInListFavouritePost,
  deletePost,
  loadOtherUserInfo,
  removeItemInListFavouritePost
} from '../screen/newfeeds/NewFeedsActions';
import { postReport } from '../screen/otherProductDetail/OtherProductDetailActions';
import AvatarImage from '../view/AvatarImage';
import Dropdown from '../view/MyDropDown/dropdown';
import ImageSlideShow from './ImageSlideShow';
import MyComponent from './MyComponent';
import MySpinner from './MySpinner';
import STouchableOpacity from './STouchableOpacity';

const defaultProductImage = require('../../assets/imgs/default_product_image.jpg');
const ic_comment = require('../../assets/imgs/ic_comment.png');
const ic_like_2 = require('../../assets/imgs/ic_like_2.png');
const ic_liked = require('../../assets/imgs/ic_liked.png');
const ic_share_2 = require('../../assets/imgs/ic_share_2.png');
const ic_like = require('../../assets/imgs/emotion/ic_like.png');

class NewFeedsItems extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      txtReportDescription: '',
      isModalReportVisible: false,
      isModalVisible: false
    };
    this.showModalReportVisible = this.showModalReportVisible.bind(this);
    this.sendReport = this.sendReport.bind(this);

    const { data } = this.props;
    const user = data ? data.user : null;
    const nickname = user ? user.nickname : null;

    this.listOptions = [
      {
        name: 'Ẩn bài viết',
        function: () => this.hideModal()
      },
      {
        name: `Bỏ theo dõi ${nickname}`,
        function: () => this.hideModal()
      },
      {
        name: 'Lưu bài viết vào tin yêu thích',
        function: () => this.hideModal()
      },
      {
        name: 'Báo cáo bài đăng',
        function: () => this.hideModal()
      },
      {
        name: `Báo cáo ${nickname}`,
        function: () => this.hideModal()
      }
    ];
    this.handleLikePost = this.handleLikePost.bind(this);
    this.initData();
  }

  hideModal = () => {
    this.setState({ isModalVisible: false });
  };
  showModal = () => {
    this.setState({ isModalVisible: true });
  };

  handleLikePost() {
    if (this.props.token === '' || !this.props.userData) {
      alert(strings.alert, strings.please_login, () => {
        if (this.props.onMyProfilePressed) this.props.onMyProfilePressed();
      });
      return;
    }
    if (this.props.listFavouritePost.filter(e => e.post && e.post.id === this.data.id).length > 0) {
      if (this.props.isAlreadyInOtherProfile) {
        //special case: uses this to decrease like post in other profile component
        this.props.data.amount_of_like--;
      }
      if (this.props.isFromGroupDetailStatus) {
        setTimeout(() => {
          //special case: uses this to decrease like in group detail
          DeviceEventEmitter.emit('handleLike', { data: this.data, type: 'decreasement' });
        });
      }
      this.props.removeItemInListFavouritePost(this.data);
    } else {
      if (this.props.isAlreadyInOtherProfile) {
        //special case: uses this to increase like post in other profile component
        this.props.data.amount_of_like++;
      }
      if (this.props.isFromGroupDetailStatus) {
        //special case: uses this to increase like in group detail
        setTimeout(() => {
          DeviceEventEmitter.emit('handleLike', { data: this.data, type: 'increasement' });
        });
      }
      this.props.addItemInListFavouritePost(this.data);
    }
  }

  componentWillUnmount() {
    clearTimeout(this.buttonTimeout1);
    clearTimeout(this.buttonTimeout2);
    clearTimeout(this.buttonTimeout3);
  }
  showModalReportVisible = () => {
    this.setState({ isModalReportVisible: !this.state.isModalReportVisible });
  };
  sendReport() {
    const id = this.data && this.data.id ? this.data.id : [];
    if (this.props.token === '' || !this.props.userData) {
      alert(strings.alert, strings.please_login, () => {
        if (this.props.onMyProfilePressed) this.props.onMyProfilePressed();
      });
      return;
    }
    if (this.state.txtReportDescription === '') {
      alert(strings.alert, strings.insert_report);
    } else {
      const uploadReportData = {
        user_id: this.props.userData.id,
        description: this.state.txtReportDescription,
        reported_id_type: 'POST',
        reported_id: id
      };
      this.props.postReport(uploadReportData);
      this.setState({ isModalReportVisible: !this.state.isModalReportVisible });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.data !== nextProps.data) this.initData();
  }

  initData() {
    this.data = this.props.data;
    this.user = this.data && this.data !== undefined ? this.data.user : null;
    this.listImage = this.data && this.data.list_image ? this.data.list_image : [];
    this.listProducts = this.data && this.data.products ? this.data.products : [];
    this.userType = this.user && this.user.user_type ? this.user.user_type : USER_TYPE_NORMAL;

    this.content = this.data && this.data.content ? this.data.content : '';
    this.words = this.content.split('#');

    this.postLat = null;
    if (this.words.length > 2) {
      this.postLat = parseFloat(this.words[2]);
    }

    this.postLng = null;
    if (this.words.length > 3) {
      this.postLng = parseFloat(this.words[3]);
    }

    this.friend = this.content.split('@');
    this.listFriend = this.friend.slice(1, this.friend.length);

    if (this.content.includes('#')) {
      this.content =
        this.content.substring(0, this.content.indexOf('#')) +
        this.content.substring(this.content.lastIndexOf('#') + 1, this.content.length);
    }
    if (this.content.includes('@')) {
      this.content = this.content.substring(0, this.content.indexOf('@'));
    }

    //user MaterialIcons for privacy icon
    this.privacy = 'public';
    if (this.data && this.data.privacy === 'FRIEND') this.privacy = 'person';
    if (this.data && this.data.privacy === 'ONLY_ME') this.privacy = 'lock';
  }

  render() {
    // if (this.props.myPost) console.log('hinodi data new', this.data);
    return (
      <View
        style={[
          styles.container,
          {
            borderBottomWidth:
              (!this.props.sharingMode ? 2 : 0.2) * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
            borderWidth: this.props.sharingMode
              ? 0.2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
              : 0,
            paddingBottom: this.props.sharingMode
              ? 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
              : 0,
            width: this.props.myPost
              ? width - 25 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
              : undefined
          }
        ]}
      >
        <View style={styles.headerContainer}>
          <TouchableOpacity
            onPress={() => {
              if (this.user.id !== this.props.currentUserId) {
                if (this.props.navigation) {
                  if (!this.props.isAlreadyInOtherProfile) {
                    this.props.navigation.navigate(ROUTE_KEY.OTHER_PROFILE_COMPONENT, {
                      user: this.user
                    });
                    // this.props.navigation.navigate(ROUTE_KEY.POST_DETAIL, {
                    //   data
                    // });
                  }
                }
              } else if (this.props.onMyProfilePressed) {
                this.props.onMyProfilePressed();
              }
            }}
          >
            <AvatarImage
              userType={this.userType}
              source={this.user && this.user.avatar ? { uri: this.user.avatar } : { uri: '' }}
              style={styles.avatar}
              resizeMode="cover"
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              if (this.user.id !== this.props.currentUserId) {
                if (this.props.navigation) {
                  if (!this.props.isAlreadyInOtherProfile) {
                    this.props.navigation.navigate(ROUTE_KEY.OTHER_PROFILE_COMPONENT, {
                      user: this.user
                    });
                    // this.props.navigation.navigate(ROUTE_KEY.POST_DETAIL, {
                    //   data
                    // });
                  }
                }
              } else if (this.props.onMyProfilePressed) {
                this.props.onMyProfilePressed();
              }
            }}
          >
            <View style={styles.headerRightContainer}>
              <Text style={styles.username}>
                {this.user ? this.user.nickname || this.user.username : ''}
              </Text>
              <View
                style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}
              >
                <Text style={styles.textTime}>
                  {this.data && this.data.created_at ? formatDate(this.data.created_at) : ''}
                </Text>
                <MaterialIcons
                  name={this.privacy}
                  size={4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                  color="rgb(136, 142, 154)"
                />
              </View>
            </View>
          </TouchableOpacity>
          {/* <STouchableOpacity
          onPress={this.showModalReportVisible}
          style={{
            position: 'absolute',
            // top: 6 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
            right: 1 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
            zIndex: 99,
            padding: 2 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224
          }}
          >
          <MaterialIcons name="more-horiz" size={10 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224} color="#000" />
        </STouchableOpacity> */}
        </View>
        <Text style={styles.textDescription}>{this.content}</Text>
        {((this.listFriend && this.listFriend.length > 1) || this.words.length > 1) && (
          <View
            style={{
              flexDirection: 'row',
              marginLeft: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              alignItems: 'center'
            }}
          >
            <Text
              style={{
                fontFamily: 'helveticaneue',
                fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
                fontStyle: 'normal',
                textAlignVertical: 'top',
                maxWidth: this.props.myPost
                  ? 95 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125
                  : width - 6 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125
              }}
            >
              {((this.listFriend && this.listFriend.length > 1) || this.words.length > 1) && (
                <Text style={{ color: 'black' }}>{'— '}</Text>
              )}
              {this.listFriend && this.listFriend.length > 1 && (
                <Text style={{ color: 'black' }}>{strings.with}</Text>
              )}
              {this.listFriend
                ? this.listFriend.map((e, index) => {
                    try {
                      const taggedUser = JSON.parse(e);
                      return (
                        <Text key={taggedUser.id}>
                          <Text
                            onPress={() => {
                              if (taggedUser.id !== this.props.currentUserId) {
                                if (this.props.navigation) {
                                  MySpinner.show();
                                  this.props.loadOtherUserInfo(taggedUser.id, dataUser => {
                                    if (dataUser) {
                                      MySpinner.hide();
                                      this.props.navigation.navigate(
                                        ROUTE_KEY.OTHER_PROFILE_COMPONENT,
                                        {
                                          user: dataUser
                                        }
                                      );
                                    } else {
                                      MySpinner.hide();
                                      setTimeout(() => {
                                        alert(strings.alert, strings.network_require_fail);
                                      }, 100);
                                    }
                                  });
                                }
                              } else if (this.props.onMyProfilePressed) {
                                this.props.onMyProfilePressed();
                              }
                            }}
                            style={{ color: 'rgb(63, 96, 194)' }}
                          >
                            {' '}
                            {taggedUser.nickname}
                          </Text>
                          {index < this.listFriend.length - 1 && <Text>, </Text>}
                        </Text>
                      );
                    } catch (e) {
                      return null;
                    }
                  })
                : null}
              {this.listFriend && this.listFriend.length > 1 && <Text />}
              {this.words.length > 1 && (
                <Text style={{ color: 'black' }}>
                  {' '}
                  {strings.at}{' '}
                  <Text
                    style={{ color: 'rgb(63, 96, 194)' }}
                    onPress={() => {
                      if (!(this.postLng && this.postLat)) return;
                      this.props.navigation.navigate(ROUTE_KEY.ROAD_MAP, {
                        coordinates: {
                          latitude: this.postLat,
                          longitude: this.postLng
                        }
                      });
                    }}
                  >
                    {this.words[1]}
                  </Text>
                </Text>
              )}
            </Text>
          </View>
        )}
        {this.data && this.data.shared_post && (
          <View style={{ marginHorizontal: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}>
            <NewFeedsItems
              data={this.data.shared_post}
              id={this.data.shared_post.id}
              sharingMode
              navigation={this.props.navigation}
              currentUserId={this.props.currentUserId}
              onMyProfilePressed={this.props.onMyProfilePressed}
            />
          </View>
        )}
        <ImageSlideShow
          sources={this.listImage.map(e => ({ uri: e }))}
          defaultSource={defaultProductImage}
          width={
            this.props.myPost ? width - 25 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) : width
          }
        />
        <View style={{ paddingHorizontal: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {this.listProducts.map((e, index) => {
              if (!e.product || !e.product.list_image || e.product.list_image.length === 0) {
                return <View key={index.toString()} />;
              }
              return (
                <STouchableOpacity
                  key={e.product.id}
                  onPress={() => {
                    if (this.props.navigation) {
                      const product = e.product;
                      product.user = this.user;
                      if (
                        (this.props.userData && this.props.userData.id) ===
                        (product.user && product.user.id)
                      ) {
                        this.props.navigation.navigate(ROUTE_KEY.MY_PRODUCT_DETAIL, {
                          data: product
                        });
                      } else {
                        this.props.navigation.navigate(ROUTE_KEY.OTHER_PRODUCT_DETAIL, {
                          data: { product }
                        });
                      }
                    }
                  }}
                >
                  <View
                    style={{
                      borderRadius: 1.7 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 0.8,
                      shadowColor: '#000',
                      shadowOffset: {
                        width: 5,
                        height: 5
                      },
                      shadowRadius: 2,
                      elevation: 5,
                      marginTop: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                      width: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                      height: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                      marginRight: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                    }}
                  >
                    <Image
                      style={styles.scrollviewImage}
                      resizeMode="cover"
                      source={
                        e.product.list_image[0] && e.product.list_image[0] !== ''
                          ? { uri: e.product.list_image[0] }
                          : defaultProductImage
                      }
                      defaultSource={defaultProductImage}
                    />
                  </View>
                  <Text style={styles.scrollviewProductName} numberOfLines={2}>
                    {e.product.name}
                  </Text>
                  <Text style={styles.scrollviewProductPrice}>
                    {getNumberString(e.product.price)}
                  </Text>
                </STouchableOpacity>
              );
            })}
          </ScrollView>
        </View>
        {!this.props.sharingMode && (
          <View style={styles.infoContainer}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              {this.props.data.amount_of_like > 0 && (
                <Image
                  source={ic_like}
                  style={{
                    width: 6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                    height: 6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                  }}
                />
              )}
              {this.props.data.amount_of_like > 0 && (
                <Text style={styles.iconTextNormal}>{this.props.data.amount_of_like}</Text>
              )}
            </View>
            {this.props.data.amount_of_comment > 0 && (
              <STouchableOpacity
                onPress={() => {
                  if (this.props.token === '' || !this.props.userData) {
                    alert(strings.alert, strings.please_login, () => {
                      if (this.props.onMyProfilePressed) {
                        this.props.onMyProfilePressed();
                      }
                    });
                  } else {
                    this.props.popupCommentScreen(this.data.id, this.props.data.amount_of_like);
                  }
                }}
              >
                <Text style={styles.iconTextNormal}>
                  {this.props.data.amount_of_comment} {strings.comment}
                </Text>
              </STouchableOpacity>
            )}
          </View>
        )}
        {!this.props.sharingMode && (
          <View style={styles.buttonContainer}>
            <STouchableOpacity
              disabled={this.props.disableLike}
              onPress={this.handleLikePost}
              style={styles.iconContainer}
            >
              {/* <MaterialCommunityIcons
              name={
                this.props.listFavouritePost.filter(e => e.post && e.post.id === data.id).length > 0
                  ? 'heart'
                  : 'heart-outline'
              }
              size={6 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224}
              color={
                this.props.listFavouritePost.filter(e => e.post && e.post.id === data.id).length > 0
                  ? '#ffc851'
                  : '#707070'
              }
            /> */}
              <Image
                source={
                  this.props.listFavouritePost.filter(e => e.post && e.post.id === this.data.id)
                    .length > 0
                    ? ic_liked
                    : ic_like_2
                }
                style={styles.iconStyle}
              />
              <Text
                style={[
                  styles.iconText,
                  {
                    color:
                      this.props.listFavouritePost.filter(e => e.post && e.post.id === this.data.id)
                        .length > 0
                        ? 'rgb(57, 123, 226)'
                        : 'rgb(87, 93, 103)'
                  }
                ]}
              >
                {strings.like}
              </Text>
            </STouchableOpacity>
            <STouchableOpacity
              disabled={this.props.disableComment}
              onPress={() => {
                if (this.props.token === '' || !this.props.userData) {
                  alert(strings.alert, strings.please_login, () => {
                    if (this.props.onMyProfilePressed) {
                      this.props.onMyProfilePressed();
                    }
                  });
                } else if (this.props.popupCommentScreen) {
                  this.props.popupCommentScreen(this.data.id, this.props.data.amount_of_like);
                }
              }}
              style={styles.iconContainer}
            >
              {/* <MaterialCommunityIcons name='comment-outline' size={5 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224} color='#707070' /> */}
              <Image source={ic_comment} style={styles.iconStyle} />
              <Text style={styles.iconText}>{strings.comment}</Text>
            </STouchableOpacity>
            <STouchableOpacity
              disabled={this.props.disableShare}
              style={styles.iconContainer}
              onPress={() => {
                const { token, userData } = this.props;
                if (token === '' || !userData) {
                  alert(strings.alert, strings.please_login, () => {
                    if (this.props.onMyProfilePressed) {
                      this.props.onMyProfilePressed();
                    }
                  });
                  return;
                }
                if (
                  this.props.groupInfo &&
                  this.props.groupInfo.group_type &&
                  this.props.groupInfo.group_type === 'PRIVATE'
                ) {
                  alert(strings.alert, strings.private_group_cannot_share_post_alert);
                  return;
                }
                this.props.navigation.navigate(ROUTE_KEY.ADD_DIARY, {
                  post: this.data.shared_post || this.data
                });
              }}
            >
              <Image source={ic_share_2} style={styles.iconStyle} />
              <Text style={styles.iconText}>{strings.share}</Text>
            </STouchableOpacity>
          </View>
        )}
        {/* {this.props.userData && data.user_id && data.user_id === this.props.userData.id &&
        <STouchableOpacity
          onPress={() => {
            // console.log('bambi show modal here');
          }}
        >
        <MaterialIcons
            style={{
              position: 'absolute',
              right: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
              top: 2 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
              width: 10 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
              height: 10 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224
            }}
            name="more-horiz"
            size={10 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224}
            color="#555667"
          />
          </STouchableOpacity>
          } */}
        {!this.props.sharingMode && (
          <View
            style={{
              position: 'absolute',
              right: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              top: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
            }}
          >
            {this.props.userData && this.user && this.user.id === this.props.userData.id ? (
              <Dropdown
                itemTextStyle={{
                  justifyContent: 'center',
                  textAlign: 'center',
                  marginRight: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                }}
                dropdownOffset={{
                  top: 30 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                  left: -5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                }}
                data={[
                  {
                    value: strings.delete,
                    onSelect: onDone => {
                      this.buttonTimeout1 = setTimeout(() => {
                        Alert.alert(
                          strings.alert,
                          strings.delete_post_alert,
                          [
                            {
                              text: strings.back,
                              onPress: () => {},
                              style: 'cancel'
                            },
                            {
                              text: strings.delete,
                              onPress: () => {
                                MySpinner.show();
                                this.props.deletePost(this.data.id, isSuccess => {
                                  MySpinner.hide();
                                  if (!isSuccess) {
                                    this.buttonTimeout2 = setTimeout(() => {
                                      alert(strings.alert, strings.network_require_fail);
                                    }, 100);
                                  } else if (this.data.group_id) {
                                    DeviceEventEmitter.emit('deletePostInGroup', {
                                      data: this.data
                                    });
                                  }
                                });
                              }
                            }
                          ],
                          {
                            cancelable: true
                          }
                        );
                      }, 500);
                    }
                  },
                  {
                    value: strings.button_edit,
                    onSelect: onDone => {
                      this.buttonTimeout1 = setTimeout(() => {
                        this.props.navigation.navigate(ROUTE_KEY.ADD_DIARY, {
                          data: this.data,
                          post: this.data.shared_post ? this.data.shared_post : undefined
                        });
                      }, 500);
                    }
                  }
                ]}
              >
                <MaterialIcons
                  name="more-horiz"
                  size={10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                  color="#555667"
                />
              </Dropdown>
            ) : (
              <Dropdown
                itemTextStyle={{
                  justifyContent: 'center',
                  textAlign: 'center',
                  marginRight: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                }}
                dropdownOffset={{
                  top: 17 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                  left: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                }}
                data={[
                  {
                    value: strings.report,
                    onSelect: onDone => {
                      this.buttonTimeout3 = setTimeout(() => {
                        this.showModalReportVisible();
                      }, 500);
                    }
                  }
                ]}
              >
                <MaterialIcons
                  name="more-horiz"
                  size={10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                  color="#555667"
                />
              </Dropdown>
            )}
          </View>
        )}
        <Modal animationType="fade" isVisible={this.state.isModalReportVisible}>
          <View style={styles.container2}>
            <STouchableOpacity
              style={styles.closeButtonContainer}
              onPress={this.showModalReportVisible}
            >
              <MaterialIcons
                name="clear"
                size={10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                color="#8e9299"
              />
            </STouchableOpacity>
            <View style={{ paddingHorizontal: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}>
              <Text style={styles.textName}> {strings.report} </Text>
            </View>
            <Textarea
              style={styles.bodyContainer}
              autoFocus={false}
              rowSpan={8}
              bordered
              placeholder={strings.insert_report}
              onChangeText={text => this.setState({ txtReportDescription: text })}
            />
            <STouchableOpacity onPress={this.sendReport}>
              <View style={styles.buttonDetail}>
                <Text style={styles.textButtonDetail}> {strings.send_report} </Text>
              </View>
            </STouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  listFavouritePost: state.profilePost.listFavouritePost || [],
  token: state.user.token,
  userData: state.user.userData
});

const mapActionCreators = {
  addItemInListFavouritePost,
  removeItemInListFavouritePost,
  deletePost,
  postReport,
  loadOtherUserInfo
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(NewFeedsItems);

const { width } = Dimensions.get('window');
// const SCALE_RATIO_WIDTH_BASIS - ( DEVICE_WIDTH/ 125 ) = SCALE_RATIO_WIDTH_BASIS - ( DEVICE_WIDTH/ 125 ) * 1.3;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderColor: 'rgb(220, 222, 226)'
  },
  headerContainer: {
    flexDirection: 'row',
    // marginTop: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    marginLeft: 1.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingTop: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingLeft: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  headerRightContainer: {
    justifyContent: 'center',
    marginLeft: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginTop: 1.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  avatar: {
    width: 13.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 13.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 6.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    // borderStyle: 'solid',
    borderWidth: 0.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderColor: 'rgb(187, 190, 193)'
  },
  username: {
    fontFamily: 'helveticaneue',
    fontSize: 4.5 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    fontWeight: '600',
    fontStyle: 'normal',
    textAlign: 'left',
    color: 'rgb(31, 31, 31)'
  },
  textDescription: {
    fontFamily: 'helveticaneue',
    fontSize: 4.5 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    fontStyle: 'normal',
    textAlign: 'justify',
    color: 'rgb(31, 31, 31)',
    margin: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  textTime: {
    fontFamily: 'helveticaneue',
    fontSize: 3.8 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    fontStyle: 'normal',
    textAlign: 'left',
    color: 'rgb(136, 142, 154)',
    marginRight: 1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderTopWidth: 0.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderColor: 'rgb(247, 247, 248)',
    marginHorizontal: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  iconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    flex: 1
  },
  iconStyle: {
    width: 8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  iconText: {
    fontFamily: 'helveticaneue',
    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    fontWeight: 'bold',
    fontStyle: 'normal',
    color: 'rgb(87, 93, 103)',
    marginLeft: 1.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  modalItem: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  detailContentContainer: {
    paddingLeft: 4.9 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingRight: 4.9 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  scrollviewImage: {
    // marginTop: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    width: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
    // marginRight: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224
  },
  scrollviewProductName: {
    marginTop: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontFamily: 'helveticaneue',
    width: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontSize: 3.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#707070'
  },
  scrollviewProductPrice: {
    fontFamily: 'helveticaneue',
    width: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontSize: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#f7a823'
  },
  postImage: {
    borderRadius: 1.7 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_WIDTH / 125) * 1.5,
    //borderStyle: 'solid',
    borderWidth: 0.2 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    borderColor: '#bcbcbd'
  },
  scrollviewPrice: {
    fontFamily: 'helveticaneue',
    fontSize: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: '600',
    fontStyle: 'normal',
    color: '#6d77f7'
  },
  infoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginHorizontal: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  iconTextNormal: {
    fontFamily: 'helveticaneue',
    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    fontWeight: 'bold',
    fontStyle: 'normal',
    color: '#707070',
    marginLeft: 1.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  container2: {
    backgroundColor: '#fafafa',
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    // shadowColor: '#1b407e26',
    // shadowOffset: {
    //   width: 0,
    //   height: 3
    // },
    // shadowRadius: 6.7,
    // shadowOpacity: 1,
    elevation: 2
  },
  closeButtonContainer: {
    position: 'absolute',
    zIndex: 2,
    right: 3.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    top: 3.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    width: 10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    justifyContent: 'center',
    alignItems: 'center'
  },
  textName: {
    fontFamily: 'helveticaneue',
    fontWeight: '800',
    fontStyle: 'normal',
    fontSize: 6.7 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    marginVertical: 4.6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginHorizontal: 12 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    textAlign: 'center',
    color: '#555667'
  },
  bodyContainer: {
    borderRadius: 2,
    // shadowColor: '#797e87',
    backgroundColor: 'white',
    // shadowOffset: {
    //   width: 1,
    //   height: 3
    // },
    // shadowRadius: 2,
    // shadowOpacity: 1,
    // elevation: 2,
    margin: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginTop: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  imageContainer: {
    flex: 1.5,
    margin: 5.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginRight: 0,
    width: 24 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 24 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 12 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: 'gray'
  },
  image: {
    marginTop: 0.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginLeft: 0.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    width: 23 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 23 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 12 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  textRating: {
    color: '#ffc851',
    fontFamily: 'helveticaneue',
    fontWeight: '600',
    fontStyle: 'normal',
    fontSize: 3.7 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    marginRight: 1.9 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  textContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 1.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  text: {
    fontFamily: 'helveticaneue',
    fontWeight: 'normal',
    fontStyle: 'normal',
    opacity: 0.8,
    fontSize: 3.7 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    textAlign: 'left',
    color: '#707070',
    marginLeft: SCALE_RATIO_WIDTH_BASIS
  },
  textPrice: {
    opacity: 0.8,
    fontSize: 7.3 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    fontFamily: 'helveticaneue',
    fontWeight: '800',
    fontStyle: 'normal',
    color: '#707070',
    textAlign: 'left',
    marginLeft: -2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  buttonArea: {
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#e2ebfc',
    flexDirection: 'row',
    paddingVertical: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginHorizontal: 5.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginBottom: 6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginTop: SCALE_RATIO_WIDTH_BASIS
  },
  buttonText: {
    fontFamily: 'helveticaneue',
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontSize: 3.7 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    textAlign: 'left',
    color: '#707070',
    marginLeft: 1.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  buttonDetail: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 6.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#6d77f7',
    // shadowColor: '#797e87',
    // shadowOffset: {
    //   width: 1,
    //   height: 3
    // },
    // shadowRadius: 2,
    // shadowOpacity: 1,
    // elevation: 2,
    margin: 6.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginTop: 0
  },
  textButtonDetail: {
    fontFamily: 'helveticaneue',
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
    color: '#ffffff'
  }
});
