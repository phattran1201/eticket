import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import MyComponent from './MyComponent';
import { SCALE_RATIO_WIDTH_BASIS, DEVICE_WIDTH, FS, DEVICE_HEIGHT, SCALE_RATIO_HEIGHT_BASIS } from '../../constants/Constants';
import { getBottomSpace } from 'react-native-iphone-x-helper';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import style, { FONT } from '../../constants/style';
import { getNumberWithCommas } from '../../utils/numberUtils';
import { DATA_MILK_TEA } from '../../constants/dataTest';

class PopupCartComponent extends MyComponent {
    constructor(props) {
      super(props);
      const { timeRemaining } = props;
      this.state = {
        remaining: timeRemaining || 30 //30s to call
      };
      global.cancelCallMySelf = true;
    }
  
    render() {
    //   const { remaining } = this.state;
    //   const { avatar, locale, nickname, age, sex, rating, introduction, email } = this.props.item;
      return (
        
          <View
            style={[
              {
                borderTopLeftRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
                borderTopRightRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
                width: DEVICE_WIDTH,
                backgroundColor: '#fff',
                position: 'absolute',
                paddingTop: 10 * SCALE_RATIO_WIDTH_BASIS,
                paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
                bottom: 100 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace(),
                paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
          >
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.2,
                borderColor: '#70707020',
                alignItems: 'center',
                paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
                marginBottom: 5 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <View style={{ flex: 1 }}>
                <MaterialCommunityIcons
                  onPress={() => this.setState({ showMyCart: false })}
                  name="arrow-down-circle"
                  size={18 * SCALE_RATIO_WIDTH_BASIS}
                  color="#808080"
                  style={{ textAlign: 'left' }}
                />
              </View>
              <View style={{ flex: 3 }}>
                <Text style={[style.titleHeader, { textAlign: 'center' }]}>Giỏ hàng của tôi</Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={[style.text, { textAlign: 'right' }]}>Xóa tất cả</Text>
              </View>
            </View>
            {DATA_MILK_TEA.map((item, i) => {
              console.log('dauphaiphat: render -> i', i);
              console.log('dauphaiphat: render -> totalItem', item.total);
              if (item.total === 0) return true;
              return (
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 5 * SCALE_RATIO_WIDTH_BASIS
                  }}
                >
                  <View style={{ maxWidth: '70%' }}>
                    <Text style={[style.titleHeader, { fontSize: FS(12), color: '#707070', textAlign: 'left' }]}>
                      {item.name}
                    </Text>
                    <Text
                      style={[
                        style.text,
                        {
                          fontSize: FS(10),
                          textAlign: 'left'
                        }
                      ]}
                    >
                      {item.fill}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={[style.textCaption, { fontSize: FS(10) }]}>{getNumberWithCommas(item.price)}</Text>
                      <Text
                        style={[style.textCaption, { fontSize: FS(10), marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS }]}
                      >
                        x
                      </Text>
                      <Text style={[style.textCaption, { fontSize: FS(10) }]}>{item.total}</Text>
                      <Text
                        style={[style.textCaption, { fontSize: FS(10), marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS }]}
                      >
                        =
                      </Text>
                      <Text
                        style={[
                          style.textCaption,
                          {
                            fontSize: FS(10),
                            marginLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                            // textDecorationLine: 'line-through'
                          }
                        ]}
                      >
                        {getNumberWithCommas(item.price * item.total)}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    {item.total > 0 ? (
                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={styles.productItemRightContainer}>
                          <TouchableOpacity onPress={() => (item.total -= 1)}>
                            <MaterialCommunityIcons
                              name="minus-circle"
                              size={24 * SCALE_RATIO_WIDTH_BASIS}
                              color="#7f9dd4"
                            />
                          </TouchableOpacity>
                        </View>
                        <Text
                          style={[
                            style.textHeader,
                            {
                              fontFamily: FONT.SemiBold,
                              alignSelf: 'center',
                              paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS
                            }
                          ]}
                        >
                          {item.total}
                        </Text>
                      </View>
                    ) : null}
                    <View style={styles.productItemRightContainer}>
                      <TouchableOpacity onPress={() => item.total + 1}>
                        <MaterialCommunityIcons
                          name="plus-circle"
                          size={24 * SCALE_RATIO_WIDTH_BASIS}
                          color="#7f9dd4"
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              );
            })}
        </View>
      );
    }
  }
  

const styles = StyleSheet.create({
    bodyContainer: {
      position: 'absolute',
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center'
    },
    bodyContentContainer: {
      borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
      backgroundColor: '#fafafa',
      shadowColor: '#797e87',
      shadowOffset: {
        width: 2,
        height: 2
      },
      shadowRadius: 4,
      shadowOpacity: 1,
      elevation: 2,
      alignItems: 'center',
      padding: 7.8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
      width: DEVICE_WIDTH
    },
    timeRemainingText: {
      fontSize: FS(21),
      textAlign: 'center',
      marginVertical: 22 * SCALE_RATIO_HEIGHT_BASIS,
      fontWeight: '500'
    },
    subTitleText: {
      fontSize: FS(13),
      textAlign: 'center'
    },
    infoContainer: {
      alignItems: 'center',
      marginTop: 38 * SCALE_RATIO_HEIGHT_BASIS
    }
  });

  const mapStateToProps = state => ({
    userData: state.user.userData
  });
  
  const mapActionCreators = {};
  
  export default connect(
    mapStateToProps,
    mapActionCreators
  )(PopupCartComponent);
