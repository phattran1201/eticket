import React from 'react';
import { View, Text, DeviceEventEmitter, Image } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import { FS, SCALE_RATIO_WIDTH_BASIS, ROUTE_KEY } from '../../constants/Constants';
import MyComponent from './MyComponent';
import MyTouchableOpacity from './MyTouchableOpacity';
import style from '../../constants/style';

export default class IconMessageAndBadgeComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      number: 0
    };
    this.countReadMessages = this.countReadMessages.bind(this);
  }

  countReadMessages = e => {
    this.setState({ number: e.countReadMessages });
  };

  componentWillMount() {
    DeviceEventEmitter.addListener('countReadMessages', this.countReadMessages);
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener('countReadMessages', this.countReadMessages);
  }

  render() {
    return (
      <MyTouchableOpacity
        style={this.props.style}
        onPress={() => {
          // this.props.navigation.navigate(ROUTE_KEY.MESSAGE_COMPONENT);
        }}
      >
        {this.state.number > 0 ? (
          <View
            style={{
              position: 'absolute',
              zIndex: 99,
              elevation: 2,
              right: 0,
              width: 16 * SCALE_RATIO_WIDTH_BASIS,
              height: 16 * SCALE_RATIO_WIDTH_BASIS,
              borderRadius: 8 * SCALE_RATIO_WIDTH_BASIS,
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: -4 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <Text style={[style.textNormal, { color: '#fff', fontSize: FS(10) }]}>{this.state.number}</Text>
          </View>
        ) : (
          <View />
        )}
        <Image
          source={
            this.props.ringColor
              ? require('../../assets/imgs/icons/ringColor.png')
              : require('../../assets/imgs/icons/ring.png')
          }
          style={style.iconHeader}
          resizeMode="contain"
        />
        {/* <MaterialCommunityIcons
          name="bell"
          size={FS(20)}
          color={this.props.rightIconStyle ? this.props.rightIconStyle : '#C7AE6d'}
        /> */}
      </MyTouchableOpacity>
    );
  }
}
