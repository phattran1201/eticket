import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import strings from '../../constants/Strings';
import MyComponent from './MyComponent';

export default class MyFlatlist extends MyComponent {
  constructor(props) {
    super(props);

    this.state = {
      height: 0
    };
  }

  renderFooter = () => {
    if (!this.props.refreshing && (!this.props.data || this.props.data.length === 0)) {
      return (
        <Text
          style={[
            styles.title,
            {
              opacity: this.state.height > 0 ? 1 : 0,
              marginTop: this.state.height / 2 - 3.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
            }
          ]}
        >
          {strings.no_data}
        </Text>
      );
    }

    return null;
  };
  render() {
    const { ...other } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          onLayout={event => {
            const { height } = event.nativeEvent.layout;
            this.setState({ height });
          }}
          {...other}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue',
    fontSize: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontStyle: 'normal',
    textAlign: 'center',
    flex: 1,
    marginTop: 35 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  }
});
