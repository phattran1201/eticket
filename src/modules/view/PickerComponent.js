import React, { Component } from 'react';
import { Dimensions, Modal, StyleSheet, View } from 'react-native';
import strings from '../../constants/Strings';
import MyFlatlist from '../view/MyFlatlist';
import HeaderWithBackButtonComponent from './HeaderWithBackButtonComponent';
import MyComponent from './MyComponent';
import PopupListItem from './PopupListItem';
import STouchableOpacity from './STouchableOpacity';

export default class PickerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false
    };
  }

  renderItem = ({ item }) => (
    <STouchableOpacity
      id={item.id}
      onPress={() => {
        this.setState({ modalVisible: false });
        item.onPress();
      }}
    >
      <PopupListItem name={item.name} />
    </STouchableOpacity>
  );

  render() {
    const { children, data, disable } = this.props;
    return (
      <View>
        <Modal
          transparent
          visible={this.state.modalVisible}
          onRequestClose={() => this.setState({ modalVisible: false })}
        >
          <View style={styles.modalContainer}>
            <HeaderWithBackButtonComponent
              bodyTitle={this.props.title ? this.props.title : strings.sharing_mode}
              onPress={() => this.setState({ modalVisible: false })}
            />
            <MyFlatlist data={data} renderItem={this.renderItem} />
          </View>
        </Modal>
        <STouchableOpacity
          onPress={() => {
            if (disable) return;
            this.setState({ modalVisible: true });
          }}
        >
          {children}
        </STouchableOpacity>
      </View>
    );
  }
}

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: 'white',
    width,
    height
  }
});
