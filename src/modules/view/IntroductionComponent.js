import React from 'react';
import { View, StatusBar } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import { SCSCALE_RATIO_WIDTH_BASIS, SCALE_RATIO_WIDTH_BASIS, SCALE_RATIO_HEIGHT_BASIS, SOFT_MENU_BAR_HEIGHT } from '../../constants/Constants';
import MyComponent from './MyComponent';

export default class IntroductionComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isError: false,
      step: 0
    };
    this.slides = [
      {
        key: '1',
        title: 'MAKE FRIENDS',
        titleStyle: { color: '#21212199', fontSize: 20 * SCALE_RATIO_WIDTH_BASIS, fontFamily: 'helveticaneue', textAlign: 'center', fontWeight: 'bold', },
        text: 'Make friends with video chat',
        textStyle: { color: '#21212199', fontSize: 20 * SCALE_RATIO_WIDTH_BASIS, fontFamily: 'helveticaneue', textAlign: 'center', },
        image: require('../../assets/imgs/intro/intro_1.png'),
        imageStyle: { maxWidth: 327 * SCALE_RATIO_WIDTH_BASIS, height: 233 * SCALE_RATIO_WIDTH_BASIS },
        backgroundColor: '#fff',
      },
      {
        key: '2',
        title: 'QUICK CONNECT',
        titleStyle: { color: '#21212199', fontSize: 20 * SCALE_RATIO_WIDTH_BASIS, fontFamily: 'helveticaneue', textAlign: 'center', fontWeight: 'bold', },
        text: 'Quick matching without wait with optimized connection',
        textStyle: { color: '#21212199', fontSize: 20 * SCALE_RATIO_WIDTH_BASIS, fontFamily: 'helveticaneue', textAlign: 'center', },
        image: require('../../assets/imgs/intro/intro_3.png'),
        imageStyle: { maxWidth: 327 * SCALE_RATIO_WIDTH_BASIS, height: 265 * SCALE_RATIO_WIDTH_BASIS },
        backgroundColor: '#fff',
      },
      {
        key: '3',
        title: 'CHATTING',
        titleStyle: { color: '#21212199', fontSize: 20 * SCALE_RATIO_WIDTH_BASIS, fontFamily: 'helveticaneue', textAlign: 'center', fontWeight: 'bold', },
        text: 'Talk to new people',
        textStyle: { color: '#21212199', fontSize: 20 * SCALE_RATIO_WIDTH_BASIS, fontFamily: 'helveticaneue', textAlign: 'center', },
        image: require('../../assets/imgs/intro/intro_2.png'),
        imageStyle: { maxWidth: 327 * SCALE_RATIO_WIDTH_BASIS, height: 254 * SCALE_RATIO_WIDTH_BASIS },
        backgroundColor: '#fff',
      }
    ];
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {/* <StatusBar hidden /> */}
        <AppIntroSlider
          slides={this.slides}
          style={{ flex: 1, textAlign: 'center', alignSelf: 'center', backgroundColor: '#fff' }}
          buttonTextStyle={{ color: 'rgba(255, 0, 0,0.5)', marginBottom: 50 * SCALE_RATIO_HEIGHT_BASIS + SOFT_MENU_BAR_HEIGHT }}
          dotStyle={{ backgroundColor: 'rgba(0, 0, 0, .2)', marginBottom: 50 * SCALE_RATIO_HEIGHT_BASIS + SOFT_MENU_BAR_HEIGHT }}
          activeDotStyle={{ backgroundColor: 'rgba(255, 0, 0,0.5)', paddingHorizontal: 10, marginBottom: 50 * SCALE_RATIO_HEIGHT_BASIS + SOFT_MENU_BAR_HEIGHT }}
          onDone={this.props.onDone}
        />
      </View>
    );
  }
}
