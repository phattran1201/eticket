import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import MyComponent from './MyComponent';
import {
  SCALE_RATIO_WIDTH_BASIS,
  DEVICE_WIDTH,
  FS,
  DEVICE_HEIGHT,
  SCALE_RATIO_HEIGHT_BASIS,
  ROUTE_KEY
} from '../../constants/Constants';
import { getBottomSpace } from 'react-native-iphone-x-helper';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import style, { FONT, APP_COLOR } from '../../constants/style';
import { getNumberWithCommas } from '../../utils/numberUtils';
import { DATA_MILK_TEA } from '../../constants/dataTest';
import MyButton from '../../style/MyButton';
import strings from '../../constants/Strings';
import { alert } from '../../utils/alert';
import STouchableOpacity from './STouchableOpacity';
import Modal from 'react-native-modal';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import FastImage from 'react-native-fast-image';
import Dropdown from './MyDropDown/dropdown';

class CartDetailComponent extends MyComponent {
  constructor(props) {
    super(props);
    // const { timeRemaining } = props;
    // this.state = {
    //   remaining: timeRemaining || 30 //30s to call
    // };
    // global.cancelCallMySelf = true;
    this.state = {
      data: DATA_MILK_TEA
    };
  }

  renderItemListUser = ({ item, index }) => {
    return (
      <View
        style={{
          borderColor: '#EFEFEF',
          borderWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
          backgroundColor: '#fbfbfb'
        }}
      >
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'flex-end',
            height: 50 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          <FastImage
            source={{
              uri:
                'https://tea-3.lozi.vn/v1/images/resized/logo-toco-toco-1529484698-1-6041526-1529484698?w=960&type=o'
            }}
            resizeMode='cover'
            style={[
              {
                width: 40 * SCALE_RATIO_WIDTH_BASIS,
                height: 40 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: (40 * SCALE_RATIO_WIDTH_BASIS) / 2,
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center'
              }
              //   avatarStyle
            ]}
          />
        </View>
        <View>
          <Text
            style={[
              style.titleHeader,
              {
                fontSize: FS(10),
                color: '#707070',
                textAlign: 'center'
              }
            ]}
          >
            Hoàng Phương
          </Text>
        </View>
        <View>
          <Text
            style={[
              style.textCaption,
              {
                fontSize: FS(10),
                textAlign: 'center',
                color: index / 2 === 0 ? APP_COLOR : '#f0f0'
              }
            ]}
          >
            {index / 2 === 0 ? 'Hoàn tất' : 'Đang đặt'}
          </Text>
        </View>
      </View>
    );
  };

  renderOrderItem = ({ item }) => {
    return (
      <View>
        <View
          style={{
            height: 50 * SCALE_RATIO_WIDTH_BASIS,
            paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS,
            flexDirection: 'row',
            backgroundColor: '#fbfbfb',
            borderColor: '#EFEFEF',
            borderBottomWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
            borderTopWidth: 1 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          <View style={{ flexDirection: 'row', alignSelf: 'flex-start' }}>
            <View
              style={{
                justifyContent: 'flex-start',
                width: 40 * SCALE_RATIO_WIDTH_BASIS,
                height: 40 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: 40 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <FastImage
                source={{
                  uri:
                    'https://tea-3.lozi.vn/v1/images/resized/logo-toco-toco-1529484698-1-6041526-1529484698?w=960&type=o'
                }}
                resizeMode='cover'
                style={[
                  {
                    width: 40 * SCALE_RATIO_WIDTH_BASIS,
                    height: 40 * SCALE_RATIO_WIDTH_BASIS,
                    borderRadius: (40 * SCALE_RATIO_WIDTH_BASIS) / 2,
                    alignSelf: 'center',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }
                  //   avatarStyle
                ]}
              />
            </View>
            <View style={{ paddingLeft: 10 * SCALE_RATIO_WIDTH_BASIS }}>
              <Text
                style={[
                  style.titleHeader,
                  {
                    fontSize: FS(12),
                    color: '#707070',
                    textAlign: 'left'
                  }
                ]}
              >
                Aaron Smith
              </Text>
              <Text
                style={[
                  style.titleHeader,
                  {
                    fontSize: FS(10),
                    color: '#707070',
                    textAlign: 'left'
                  }
                ]}
              >
                Người tạo
              </Text>
            </View>
          </View>
          <View style={{ flex: 1 }} />
          <View>
            <View>
              <Text
                style={[
                  style.textCaption,
                  {
                    fontSize: FS(12),
                    textAlign: 'right',
                    marginLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                    // textDecorationLine: 'line-through'
                  }
                ]}
              >
                84,000đ
              </Text>
            </View>
            <View>
              <Text
                style={[
                  style.text,
                  {
                    fontSize: FS(10),
                    textAlign: 'right',
                    marginLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                    // textDecorationLine: 'line-through'
                  }
                ]}
              >
                2 cốc
              </Text>
            </View>
          </View>
        </View>
        {this.state.data.map((item, i) => {
          if (item.total === 0) return true;
          return (
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 5 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <View style={{ maxWidth: '70%' }}>
                <Text
                  style={[
                    style.titleHeader,
                    {
                      fontSize: FS(12),
                      color: '#707070',
                      textAlign: 'left'
                    }
                  ]}
                >
                  {item.name}
                </Text>
                <Text
                  style={[
                    style.text,
                    {
                      fontSize: FS(10),
                      textAlign: 'left'
                    }
                  ]}
                >
                  {item.fill}
                </Text>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[style.textCaption, { fontSize: FS(10) }]}>
                    {getNumberWithCommas(item.price)}
                  </Text>
                  <Text
                    style={[
                      style.textCaption,
                      {
                        fontSize: FS(10),
                        marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS
                      }
                    ]}
                  >
                    x
                  </Text>
                  <Text style={[style.textCaption, { fontSize: FS(10) }]}>
                    {item.total}
                  </Text>
                  <Text
                    style={[
                      style.textCaption,
                      {
                        fontSize: FS(10),
                        marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS
                      }
                    ]}
                  >
                    =
                  </Text>
                  <Text
                    style={[
                      style.textCaption,
                      {
                        fontSize: FS(10),
                        marginLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                        // textDecorationLine: 'line-through'
                      }
                    ]}
                  >
                    {getNumberWithCommas(item.price * item.total)}
                  </Text>
                </View>
              </View>
              {this.isGroupOwner ? (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  {item.total > 0 ? (
                    <View
                      style={{ flexDirection: 'row', alignItems: 'center' }}
                    >
                      <View style={styles.productItemRightContainer}>
                        <TouchableOpacity onPress={() => (item.total -= 1)}>
                          <MaterialCommunityIcons
                            name='minus-circle'
                            size={24 * SCALE_RATIO_WIDTH_BASIS}
                            color='#7f9dd4'
                          />
                        </TouchableOpacity>
                      </View>
                      <Text
                        style={[
                          style.textHeader,
                          {
                            fontFamily: FONT.SemiBold,
                            alignSelf: 'center',
                            paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS
                          }
                        ]}
                      >
                        {item.total}
                      </Text>
                    </View>
                  ) : null}
                  <View style={styles.productItemRightContainer}>
                    <TouchableOpacity onPress={() => item.total + 1}>
                      <MaterialCommunityIcons
                        name='plus-circle'
                        size={24 * SCALE_RATIO_WIDTH_BASIS}
                        color='#7f9dd4'
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              ) : null}
            </View>
          );
        })}
      </View>
    );
  };

  render() {
    const { isGroupOwner } = this.props;
    return (
      <View
        style={{
          position: 'absolute',
          width: DEVICE_WIDTH,
          height: DEVICE_HEIGHT,
          bottom: isGroupOwner
            ? 70 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace()
            : getBottomSpace()
        }}
      >
        <STouchableOpacity
          style={{ opacity: 0.3, backgroundColor: 'black', flex: 1 }}
          onPress={this.props.onClose}
        />
        <View
          style={[
            {
              borderTopLeftRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
              borderTopRightRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
              width: DEVICE_WIDTH,
              backgroundColor: '#fff',
              // position: 'absolute',
              paddingTop: 10 * SCALE_RATIO_WIDTH_BASIS,
              paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
              // bottom: 100 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace(),
              paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
        >
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.2,
              borderColor: '#70707020',
              alignItems: 'center',
              paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
              marginBottom: 5 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <View style={{ flex: 1 }}>
              <MaterialCommunityIcons
                onPress={() => this.setState({ showMyCart: false })}
                name='arrow-down-circle'
                size={18 * SCALE_RATIO_WIDTH_BASIS}
                color='#808080'
                style={{ textAlign: 'left' }}
              />
            </View>
            <View style={{ flex: 3 }}>
              <Text style={[style.titleHeader, { textAlign: 'center' }]}>
                Giỏ hàng của tôi
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text style={[style.text, { textAlign: 'right' }]}> </Text>
            </View>
          </View>
          <View
            style={{
              height: 40 * SCALE_RATIO_WIDTH_BASIS,
              padding: 5 * SCALE_RATIO_WIDTH_BASIS,
              justifyContent: 'center',
              flexDirection: 'row',
              borderColor: '#EFEFEF',
              // backgroundColor: '#fbfbfb',
              borderBottomWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
              borderTopWidth: 1 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <View
              style={{
                backgroundColor: '#799dd8',
                borderRadius: 4 * SCALE_RATIO_WIDTH_BASIS,
                paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Text
                style={[
                  style.titleHeader,
                  {
                    fontSize: FS(12),
                    color: '#fff',
                    textAlign: 'center'
                  }
                ]}
              >
                Nhóm: Aaron Smith
              </Text>
            </View>
            <View style={{ flex: 1 }} />
            {isGroupOwner ? (
              <Dropdown
                data={[
                  {
                    value: 'Xóa tất cả sản phẩm của tôi'
                  },
                  {
                    value: 'Xoá tất cả sản phẩm trong nhóm'
                  },
                  {
                    value: 'Xóa nhóm đặt món'
                  }
                ]}
              >
                <View
                  style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}
                >
                  <MaterialIcons
                    size={20 * SCALE_RATIO_WIDTH_BASIS}
                    name='more-vert'
                  />
                </View>
              </Dropdown>
            ) : null}
          </View>

          <ScrollView style={{ maxHeight: 200 * SCALE_RATIO_HEIGHT_BASIS }}>
            <View>
              <FlatList data={[{}, {}]} renderItem={this.renderOrderItem} />
            </View>

            {/* list user */}
            {isGroupOwner ? (
              <View>
                <View
                  style={{
                    height: 35 * SCALE_RATIO_WIDTH_BASIS,
                    paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS,
                    flexDirection: 'row',
                    backgroundColor: '#fbfbfb',
                    borderColor: '#EFEFEF',
                    borderBottomWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
                    borderTopWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  <View
                    style={{
                      flexDirection: 'row',
                      alignSelf: 'flex-start',
                      justifyContent: 'center'
                    }}
                  >
                    {DATA_MILK_TEA.map((e, index) => (
                      <FastImage
                        source={{
                          uri:
                            'https://tea-3.lozi.vn/v1/images/resized/logo-toco-toco-1529484698-1-6041526-1529484698?w=960&type=o'
                        }}
                        resizeMode='cover'
                        style={[
                          {
                            width: 25 * SCALE_RATIO_WIDTH_BASIS,
                            height: 25 * SCALE_RATIO_WIDTH_BASIS,
                            borderRadius: (25 * SCALE_RATIO_WIDTH_BASIS) / 2,
                            alignSelf: 'center',
                            justifyContent: 'center',
                            alignItems: 'center',
                            overflow: 'visible',
                            shadowColor: APP_COLOR,
                            shadowOffset: {
                              width: 0,
                              height: 2
                            },
                            shadowOpacity: 0.5,
                            shadowRadius: 2,
                            elevation: 2,
                            marginLeft:
                              index > 0 ? -8 * SCALE_RATIO_WIDTH_BASIS : 0,
                            zIndex: index
                          }
                          //   avatarStyle
                        ]}
                      />
                    ))}
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        paddingLeft: 5 * SCALE_RATIO_WIDTH_BASIS
                      }}
                    >
                      <Text
                        style={[
                          style.titleHeader,
                          {
                            fontSize: FS(12),
                            color: '#707070',
                            textAlign: 'left'
                          }
                        ]}
                      >
                        2 người đã hoàn tất
                      </Text>
                    </View>
                  </View>
                  <View style={{ flex: 1 }} />
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: '100%'
                    }}
                  >
                    <MaterialIcons
                      name='chevron-right'
                      size={8 * SCALE_RATIO_WIDTH_BASIS}
                      color='black'
                    />
                  </View>
                </View>
                <View
                  style={{
                    paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS,
                    flexDirection: 'row',
                    // backgroundColor: '#fbfbfb',
                    borderColor: '#EFEFEF'
                  }}
                >
                  <FlatList
                    data={DATA_MILK_TEA.slice(0, 3)}
                    keyExtractor={item => item.id}
                    renderItem={this.renderItemListUser}
                    showsVerticalScrollIndicator={false}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    ItemSeparatorComponent={() => (
                      <View style={{ width: 5 * SCALE_RATIO_WIDTH_BASIS }} />
                    )}
                  />
                </View>
              </View>
            ) : null}
          </ScrollView>
          {/* button invite more */}
          <View style={{ paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS }}>
            <STouchableOpacity>
              <View
                style={{
                  width: '60%',
                  padding: 5 * SCALE_RATIO_WIDTH_BASIS,
                  borderRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
                  borderWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
                  borderColor: APP_COLOR,
                  alignSelf: 'center',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text
                  style={[
                    style.textCaption,
                    {
                      fontSize: FS(12),
                      textAlign: 'center'
                    }
                  ]}
                >
                  + Mời tham gia vào nhóm
                </Text>
              </View>
            </STouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bodyContainer: {
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bodyContentContainer: {
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#fafafa',
    shadowColor: '#797e87',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 2,
    alignItems: 'center',
    padding: 7.8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    width: DEVICE_WIDTH
  },
  timeRemainingText: {
    fontSize: FS(21),
    textAlign: 'center',
    marginVertical: 22 * SCALE_RATIO_HEIGHT_BASIS,
    fontWeight: '500'
  },
  subTitleText: {
    fontSize: FS(13),
    textAlign: 'center'
  },
  infoContainer: {
    alignItems: 'center',
    marginTop: 38 * SCALE_RATIO_HEIGHT_BASIS
  }
});

const mapStateToProps = state => ({
  userData: state.user.userData
});

const mapActionCreators = {};

export default connect(
  mapStateToProps,
  mapActionCreators
)(CartDetailComponent);
