import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import {
  DEVICE_WIDTH,
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  DEVICE_HEIGHT
} from '../../constants/Constants';
import strings from '../../constants/Strings';
import style from '../../constants/style';
import getGenderImage from '../../utils/getGenderImage';
import global from '../../utils/globalUtils';
import MyComponent from './MyComponent';
import MyImage from './MyImage';

class PopupNotificationCallComponent extends MyComponent {
  constructor(props) {
    super(props);
    const { timeRemaining } = props;
    this.state = {
      remaining: timeRemaining || 30 //30s to call
    };
    global.cancelCallMySelf = true;
  }

  render() {
    const { remaining } = this.state;
    const { avatar, locale, nickname, age, sex, rating, introduction, email } = this.props.item;
    return (
      <View style={styles.bodyContainer}>
        <View
          style={{
            marginBottom: -60 * SCALE_RATIO_HEIGHT_BASIS,
            zIndex: 99,
            elevation: 2
          }}
        >
          <MyImage
            flag={locale}
            source={{ uri: avatar }}
            size={82 * SCALE_RATIO_WIDTH_BASIS}
            styleContent={{ backgroundColor: '#fff' }}
          />
        </View>
        <View style={styles.bodyContentContainer}>
          <View style={styles.infoContainer}>
            <Text style={[style.textNormal, { fontSize: FS(18) }]}>
              {nickname && nickname.length > 0 ? nickname : email}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 7 * SCALE_RATIO_HEIGHT_BASIS,
                marginBottom: 5 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              {getGenderImage(sex, 0.7)}
              <Text style={[style.textNormal, { fontSize: FS(12) }]}>{age}</Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginLeft: 18 * SCALE_RATIO_WIDTH_BASIS
                }}
              >
                {[0, 1, 2, 3, 4].map(e => (
                  <MaterialIcons
                    key={e.toString()}
                    name={rating > e ? 'star' : 'star-border'}
                    size={FS(12)}
                    color="#C7AE6D"
                    style={{ marginRight: 2 * SCALE_RATIO_WIDTH_BASIS }}
                  />
                ))}
              </View>
            </View>
            <Text style={[style.textNormal, { fontSize: FS(10) }]}>{introduction}</Text>
          </View>
          <Text style={[style.textNormal, styles.timeRemainingText]}>
            {strings.connecting} {remaining}s ...
          </Text>
          <Text style={[style.textNormal, styles.subTitleText]}>{strings.video_chat_noti}</Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userData: state.user.userData
});

const mapActionCreators = {};

export default connect(
  mapStateToProps,
  mapActionCreators
)(PopupNotificationCallComponent);

const styles = StyleSheet.create({
  bodyContainer: {
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bodyContentContainer: {
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#fafafa',
    shadowColor: '#797e87',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 2,
    alignItems: 'center',
    padding: 7.8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    width: DEVICE_WIDTH
  },
  timeRemainingText: {
    fontSize: FS(21),
    textAlign: 'center',
    marginVertical: 22 * SCALE_RATIO_HEIGHT_BASIS,
    fontWeight: '500'
  },
  subTitleText: {
    fontSize: FS(13),
    textAlign: 'center'
  },
  infoContainer: {
    alignItems: 'center',
    marginTop: 38 * SCALE_RATIO_HEIGHT_BASIS
  }
});
