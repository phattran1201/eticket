import React from 'react';
import { Dimensions, Image, ScrollView, Text, View } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../constants/Constants';
import strings from '../../constants/Strings';
import { downloadImage, downloadListStickerAndSave, getListStickerFromLocal } from './EmojiActions';
import MyComponent from './MyComponent';
import STouchableOpacity from './STouchableOpacity';

class EmojiComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      index: 0
    };

    this.onEmojiPressed = this.onEmojiPressed.bind(this);
  }

  onEmojiPressed = stickerUri => () => {
    if (this.props.onEmojiPressed) {
      this.props.onEmojiPressed(stickerUri);
    }
  };

  componentDidMount() {
    this.props.getListStickerFromLocal();
  }

  toggle(val) {
    if (val !== undefined) this.setState({ visible: val });
    else this.setState({ visible: !this.state.visible });
  }

  render() {
    if (!this.state.visible) return null;
    return (
      <View
        style={{ width, backgroundColor: 'white', borderTopColor: '#e2ebfc', borderTopWidth: 0.7 }}
      >
        <View style={{ width, height: 60 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}>
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            scrollEnabled={false}
            ref={ref => {
              this.ScrollView = ref;
            }}
          >
            {this.props.listStickerCategory.map(e => {
              if (e.children && e.children.length > 0) {
                return (
                  <View key={e.id} style={{ width }}>
                    <ScrollView>
                      <View
                        style={{
                          padding: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                          flexWrap: 'wrap',
                          flexDirection: 'row'
                        }}
                      >
                        {e.emojis.map(i => {
                          const link = e.children.find(item => item.link === e.image_url);
                          return (
                            <STouchableOpacity
                              onPress={this.onEmojiPressed(link ? link.url : i.image_url)}
                              key={i.id}
                              style={{
                                padding: 2.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                              }}
                            >
                              <Image
                                source={{ uri: link ? link.url : i.image_url }}
                                style={{
                                  width:
                                    width / 5 - (6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)),
                                  height:
                                    width / 5 - (6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224))
                                }}
                              />
                            </STouchableOpacity>
                          );
                        })}
                      </View>
                    </ScrollView>
                  </View>
                );
              }
              return (
                <View key={e.id} style={{ width, justifyContent: 'center', alignItems: 'center' }}>
                  <STouchableOpacity
                    style={{
                      backgroundColor: '#6d77f7',
                      paddingVertical: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                      paddingHorizontal: 16 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                      borderRadius: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                    }}
                    onPress={() => this.props.downloadListStickerAndSave(e)}
                  >
                    <Text
                      style={{
                        fontFamily: 'helveticaneue',
                        fontSize: 5 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
                        fontStyle: 'normal',
                        fontWeight: 'bold',
                        color: '#fff'
                      }}
                    >
                      {strings.download}
                    </Text>
                  </STouchableOpacity>
                </View>
              );
            })}
          </ScrollView>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            borderTopColor: '#e2ebfc',
            borderTopWidth: 0.7
          }}
        >
          <STouchableOpacity
            style={{ padding: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}
            onPress={() => this.props.textInputForcus()}
          >
            <Text
              style={{
                fontFamily: 'helveticaneue',
                fontSize: 5.3 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
                fontWeight: 'bold',
                fontStyle: 'normal',
                color: '#707070'
              }}
            >
              Aa
            </Text>
          </STouchableOpacity>
          <View style={{ flex: 1 }}>
            <ScrollView horizontal>
              {this.props.listStickerCategory.map((e, index) => {
                let ImageURL;
                const hadImage = this.props.listSavedImage.find(
                  i => i.link === e.emojis[0].image_url
                );
                if (hadImage) {
                  ImageURL = hadImage.url;
                } else {
                  ImageURL = e.emojis[0].image_url;
                  this.props.downloadImage(e.emojis[0].image_url);
                }
                return (
                  <STouchableOpacity
                    key={e.id}
                    style={{
                      padding: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                      borderLeftWidth: this.state.index === index ? 0.7 : 0,
                      borderRightWidth: this.state.index === index ? 0.7 : 0,
                      borderColor: '#e2ebfc',
                      backgroundColor: this.state.index === index ? '#e2ebfc' : '#fff',
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}
                    onPress={() => {
                      this.ScrollView.scrollTo({ x: width * index, y: 0, animated: false });
                      this.setState({ index });
                    }}
                  >
                    <Image
                      source={{ uri: ImageURL }}
                      style={{
                        width: 8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                        height: 8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                      }}
                    />
                  </STouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
          <STouchableOpacity
            style={{ padding: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}
            onPress={() => this.setState({ visible: false })}
          >
            <MaterialIcons
              name="close"
              size={10 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
              color="#707070"
            />
          </STouchableOpacity>
        </View>
      </View>
    );
  }
}

const { width, height } = Dimensions.get('window');

const mapStateToProps = state => ({
  listStickerCategory: state.local.listStickerCategory,
  listSavedImage: state.local.listSavedImage
});

const mapActionCreators = {
  getListStickerFromLocal,
  downloadListStickerAndSave,
  downloadImage
};

export default connect(
  mapStateToProps,
  mapActionCreators,
  null,
  { withRef: true }
)(EmojiComponent);
