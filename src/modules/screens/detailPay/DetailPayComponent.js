import React from 'react';
import { FlatList, Image, ScrollView, Text, TouchableOpacity, View, WebView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel from 'react-native-snap-carousel';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import { connect } from 'react-redux';
import {
  DEVICE_WIDTH,
  FS,
  ROUTE_KEY,
  SCALE_RATIO_WIDTH_BASIS,
  SLICE_NUM,
  IS_ANDROID,
  DEVICE_HEIGHT
} from '../../../constants/Constants';
import { DATA_TEST } from '../../../constants/dataTest';
import style, {
  APP_COLOR,
  APP_COLOR_2,
  APP_COLOR_TEXT,
  APP_COLOR_TEXT_GRAY,
  APP_COLOR_TEXT_GRAY_2,
  FONT,
  APP_COLOR_BLUE,
  APP_COLOR_BLUE_2
} from '../../../constants/style';
import { getDetailArticle } from '../../screen/news/NewsAction';
import HeaderWithAvatar from '../../view/HeaderWithAvatar';
import MyComponent from '../../view/MyComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import { loadListNewsArticleData, loadListPopularEvents, loadListPromotionArticleData } from '../home/HomeActions';
import HeaderWithBackButtonComponent from '../../view/HeaderWithBackButtonComponent';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import AutoHeightWebView from 'react-native-autoheight-webview';
import { height } from '../../../style/MyTextInput';
import Modal from 'react-native-modal';

class DetailEventPayComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = { activeSlide: 0, loadDone: false, dialogVisible: false };
    this.store = DATA_TEST;
    this.data = DATA_TEST;
    this.isFirstTimeLoadNews = true;
    this.isFirstTimeLoadPromotion = true;
  }

  render() {
    return (
      <View style={{ backgroundColor: '#fff', flex: 1 }}>
        <HeaderWithBackButtonComponent translucent={false} iconColor="#fff" bodyTitle="Ticket Detail" />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS }}>
            <Text
              style={[
                style.textCaption,
                {
                  fontSize: FS(18),
                  color: APP_COLOR_TEXT,
                  // textDecorationLine: 'underline',
                  // letterSpacing: 3,
                  lineHeight: 30
                }
              ]}
            >
              HIGH PERFORMANCE WEB APP: FROM IDEA TO PRODUCTION
            </Text>
            <Text style={[style.text, { fontSize: FS(12), color: APP_COLOR_TEXT, marginTop: 10 }]}>
              by Cybozu Viet Nam
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
              marginTop: 25,
              marginBottom: 15
            }}
          >
            <FontAwesome
              name="calendar"
              style={{ marginTop: 5 }}
              size={14 * SCALE_RATIO_WIDTH_BASIS}
              color={APP_COLOR_TEXT_GRAY_2}
            />
            <View style={{ marginLeft: 10 }}>
              <Text style={[style.text, { fontSize: FS(16), color: APP_COLOR_TEXT }]}>Monday, February 4</Text>
              <Text style={[style.text, { fontSize: FS(10), fontcolor: APP_COLOR_TEXT_GRAY_2 }]}>17:30 - 21:00</Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
              marginBottom: 15
            }}
          >
            <FontAwesome
              name="calendar"
              style={{ marginTop: 5 }}
              size={14 * SCALE_RATIO_WIDTH_BASIS}
              color={APP_COLOR_TEXT_GRAY_2}
            />
            <View style={{ marginLeft: 10 }}>
              <Text style={[style.text, { fontSize: FS(16), color: APP_COLOR_TEXT }]}>The Tree Academy’s Office</Text>
              <Text style={[style.text, { fontSize: FS(10), fontcolor: APP_COLOR_TEXT_GRAY_2 }]}>
                29 Huynh Van Banh Street Ward 17, Ho Chi Minh City
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
              marginBottom: 15
            }}
          >
            <FontAwesome
              name="calendar"
              style={{ marginTop: 5 }}
              size={14 * SCALE_RATIO_WIDTH_BASIS}
              color={APP_COLOR_TEXT_GRAY_2}
            />
            <View style={{ marginLeft: 10 }}>
              <Text style={[style.text, { fontSize: FS(16), color: APP_COLOR_TEXT }]}>Select the type of ticket</Text>
              <Text style={[style.text, { fontSize: FS(10), fontcolor: APP_COLOR_TEXT_GRAY_2 }]}>
                Pay free tickets within 3 days before the event
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <TouchableOpacity
              onPress={() => this.setState({ dialogVisible: true })}
              style={{
                width: 100 * SCALE_RATIO_WIDTH_BASIS,
                marginLeft: 25,
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 1,
                borderColor: APP_COLOR,
                backgroundColor: APP_COLOR,
                paddingHorizontal: 15,
                paddingVertical: 5,
                borderRadius: 4
              }}
            >
              <Text style={[style.text, { fontSize: FS(10), color: '#fff' }]}>Common Ticket</Text>
              <Text style={[style.text, { fontFamily: FONT.Bold, fontSize: FS(12), color: '#fff' }]}>$14</Text>
              <Text style={[style.text, { fontSize: FS(10), color: '#fff' }]}>QTY: 100</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.setState({ dialogVisible: true })}
              style={{
                width: 100 * SCALE_RATIO_WIDTH_BASIS,
                marginLeft: 15,
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 1,
                borderColor: APP_COLOR_BLUE_2,
                paddingHorizontal: 15,
                paddingVertical: 5,
                borderRadius: 4
              }}
            >
              <Text style={[style.text, { fontSize: FS(10), color: APP_COLOR_BLUE_2 }]}>VIP Ticket</Text>
              <Text style={[style.text, { fontFamily: FONT.Bold, fontSize: FS(12), color: APP_COLOR_BLUE_2 }]}>
                $20
              </Text>
              <Text style={[style.text, { fontSize: FS(10), color: APP_COLOR_BLUE_2 }]}>QTY: 200</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: 10 * SCALE_RATIO_WIDTH_BASIS,
              marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <View
              style={{
                marginLeft: 25,
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 1,
                borderColor: '#70707020',
                backgroundColor: '#70707020',
                paddingHorizontal: 15,
                paddingVertical: 10,
                borderRadius: 4
              }}
            >
              <Text style={[style.text, { fontSize: FS(12) }]}>
                Tickets include event attendance and drinking water. No presentation slides.
              </Text>
            </View>
          </View>
        </ScrollView>
        <Modal
          onBackdropPress={() => this.setState({ dialogVisible: false })}
          onSwipe={() => this.setState({ dialogVisible: false })}
          swipeDirection={'down'}
          swipeThreshold={20}
          visible={this.state.dialogVisible}
        >
          <View
            style={{
              borderWidth: 1,
              borderColor: APP_COLOR_TEXT_GRAY_2,
              borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
              paddingHorizontal: 30 * SCALE_RATIO_WIDTH_BASIS,
              paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
              // width: DEVICE_WIDTH * 0.7,
              // height: DEVICE_WIDTH * 0.7,
              backgroundColor: 'white'
            }}
          >
            <Text style={[style.textCaption, { fontSize: FS(24), textAlign: 'center' }]}>Opps! </Text>
            <Text
              style={[
                style.text,
                { fontSize: FS(12), textAlign: 'center', marginVertical: 15 * SCALE_RATIO_WIDTH_BASIS }
              ]}
            >
              Account in your wallet is not enough to pay. Please top up your account to continue using.
            </Text>
            <View
              style={{
                justifyContent: 'center',
                flexDirection: 'row',
                alignItems: 'center'
              }}
            >
              <STouchableOpacity
                style={{
                  marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS,
                  marginRight: 10 * SCALE_RATIO_WIDTH_BASIS,

                  backgroundColor: '#fff',
                  borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                  paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
                  paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS
                }}
                onPress={() => {
                  // console.log('poi this.props.listNewsArticles:', this.props.listNewsArticles);
                  // this.props.navigation.navigate(ROUTE_KEY.NEWS, {
                  //   title: articleTitle,
                  //   data: this.props.listNewsArticles
                  // });
                }}
              >
                <Text
                  onPress={() => {
                    this.setState({ dialogVisible: false });
                    // console.log('poi this.props.listNewsArticles:', this.props.listNewsArticles);
                    // this.props.navigation.navigate(ROUTE_KEY.NEWS, {
                    //   title: articleTitle,
                    //   data: this.props.listNewsArticles
                    // });
                  }}
                  style={[
                    style.text,
                    {
                      textAlign: 'center',
                      fontSize: FS(14),
                      color: APP_COLOR_TEXT
                    }
                  ]}
                >
                  Cancel
                </Text>
              </STouchableOpacity>

              <STouchableOpacity
                style={{
                  marginLeft: 10 * SCALE_RATIO_WIDTH_BASIS,

                  backgroundColor: APP_COLOR,
                  borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS,
                  paddingHorizontal: 30 * SCALE_RATIO_WIDTH_BASIS,
                  paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS
                }}
                onPress={() => {
                  // console.log('poi this.props.listNewsArticles:', this.props.listNewsArticles);
                  // this.props.navigation.navigate(ROUTE_KEY.NEWS, {
                  //   title: articleTitle,
                  //   data: this.props.listNewsArticles
                  // });
                }}
              >
                <Text
                  style={[
                    style.textCaption,
                    {
                      textAlign: 'center',
                      fontFamily: FONT.Bold,
                      fontSize: FS(14),
                      color: '#fff'
                    }
                  ]}
                >
                  Recharge{' '}
                </Text>
              </STouchableOpacity>
            </View>
          </View>
        </Modal>
        <STouchableOpacity
          style={{
            width: '90%',
            position: 'absolute',
            bottom: 15 * SCALE_RATIO_WIDTH_BASIS,
            alignSelf: 'center',
            backgroundColor: APP_COLOR,
            marginVertical: 20 * SCALE_RATIO_WIDTH_BASIS,
            marginHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
            borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            paddingVertical: 15 * SCALE_RATIO_WIDTH_BASIS,
            alignItems: 'center'
          }}
          onPress={() => {
            // console.log('poi this.props.listNewsArticles:', this.props.listNewsArticles);
            // this.props.navigation.navigate(ROUTE_KEY.NEWS, {
            //   title: articleTitle,
            //   data: this.props.listNewsArticles
            // });
          }}
        >
          <Text
            style={[
              style.textCaption,
              {
                fontFamily: FONT.Bold,
                fontSize: FS(14),
                color: '#fff'
              }
            ]}
          >
            PAY NOW
          </Text>
        </STouchableOpacity>
      </View>
    );
  }
}

const mapActionCreators = {
  loadListPopularEvents,
  loadListNewsArticleData,
  loadListPromotionArticleData,
  getDetailArticle
};

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData,
  regionData: state.setting.regionData,
  listNewsArticles: state.setting.listNewsArticles,
  newsArticlesPaginate: state.setting.newsArticlesPaginate,
  listPromotionArticles: state.setting.listPromotionArticles,
  promotionArticlesPaginate: state.setting.promotionArticlesPaginate,
  currentLocation: state.user.currentLocation,
  detailArticle: state.setting.detailArticle,
  listEventPopular: state.eventPopular.listEventPopular
});

export default connect(
  mapStateToProps,
  mapActionCreators
)(DetailEventPayComponent);
