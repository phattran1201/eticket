import React from 'react';
import { Image, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import Carousel from 'react-native-snap-carousel';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { DEVICE_WIDTH, FS, IS_ANDROID, SCALE_RATIO_WIDTH_BASIS, SLICE_NUM } from '../../../constants/Constants';
import { DATA_TEST } from '../../../constants/dataTest';
import style, {
  APP_COLOR,
  APP_COLOR_TEXT,
  APP_COLOR_TEXT_GRAY,
  APP_COLOR_TEXT_GRAY_2,
  FONT
} from '../../../constants/style';
import { getDetailArticle } from '../../screen/news/NewsAction';
import HeaderWithBackButtonComponent from '../../view/HeaderWithBackButtonComponent';
import MyComponent from '../../view/MyComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import { loadListNewsArticleData, loadListPopularEvents, loadListPromotionArticleData } from '../home/HomeActions';

class DetailEventComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = { activeSlide: 0, loadDone: false };
    this.store = DATA_TEST;
    this.data = DATA_TEST;
    this.isFirstTimeLoadNews = true;
    this.isFirstTimeLoadPromotion = true;
  }

  // componentDidMount() {
  //   if (this.props.regionData && this.props.regionData.home_screen && this.props.regionData.home_screen.section_article) {
  //     this.props.loadListNewsArticleData(this.props.regionData.home_screen.section_article.blog_handle, () => {

  //     });
  //   }
  //   if (this.props.regionData && this.props.regionData.home_screen && this.props.regionData.home_screen.section_article_promotion) {
  //     this.props.loadListPromotionArticleData(this.props.regionData.home_screen.section_article_promotion.blog_handle, () => {

  //     });
  //   }
  // }
  componentDidMount() {
    this.props.loadListPopularEvents(1);
  }

  renderBannerItem = ({ item, index }) => (
    <TouchableOpacity onPress={() => {}}>
      <Image
        style={{
          width: '100%',
          height: 158 * SCALE_RATIO_WIDTH_BASIS,
          borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS
        }}
        source={{
          uri: item.slide_img ? item.slide_img : 'https://www.fancyhands.com/images/default-avatar-250x250.png'
        }}
      />
    </TouchableOpacity>
  );

  renderPromotionItem = ({ item, index }) => (
    <TouchableOpacity>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          width: 40 * SCALE_RATIO_WIDTH_BASIS,
          height: 40 * SCALE_RATIO_WIDTH_BASIS,
          borderRadius: (40 * SCALE_RATIO_WIDTH_BASIS) / 2,
          position: 'absolute',
          top: 5,
          right: 10,
          backgroundColor: 'white',
          zIndex: 999
        }}
      >
        <MaterialCommunityIcons
          name="heart-outline"
          size={25 * SCALE_RATIO_WIDTH_BASIS}
          color={APP_COLOR}
          style={{ marginBottom: -5 * SCALE_RATIO_WIDTH_BASIS }}
        />
      </View>
      <View
        style={[
          {
            borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            flex: 1,
            backgroundColor: 'white',
            marginLeft: 15,
            overflow: 'hidden'
          }
        ]}
      >
        <View>
          <Image
            style={{
              width: '100%',
              height: 150 * SCALE_RATIO_WIDTH_BASIS
            }}
            source={{
              uri: item.image ? item.image : 'https://www.fancyhands.com/images/default-avatar-250x250.png'
            }}
          />
        </View>

        <Text
          style={[
            style.textCaption,
            {
              color: APP_COLOR_TEXT,
              fontSize: FS(14),
              padding: 5 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
          numberOfLines={2}
        >
          {item.title}
        </Text>

        <Text
          style={[
            style.textCaption,
            {
              color: APP_COLOR_TEXT_GRAY,
              fontSize: FS(10),
              padding: 5 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
          numberOfLines={2}
        >
          Sun, February 22 - 17:00
        </Text>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ borderWidth: 0.5, borderColor: '#70707050', padding: 1, borderRadius: 3 }}>
            <Text
              style={[
                style.textCaption,
                {
                  alignSelf: 'flex-start',
                  color: APP_COLOR_TEXT,
                  fontSize: FS(10),
                  padding: 5 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
              numberOfLines={2}
            >
              Bussiness
            </Text>
          </View>
          <View style={{ marginLeft: 5, borderWidth: 0.5, borderColor: APP_COLOR, padding: 1, borderRadius: 3 }}>
            <Text
              style={[
                style.textCaption,
                {
                  alignSelf: 'flex-start',
                  color: APP_COLOR,
                  fontSize: FS(10),
                  padding: 5 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
              numberOfLines={2}
            >
              From $14
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );

  render() {
    const jsCode = `
            document.querySelector('#content').style.background = '#fff';
        document.querySelector('section.banner').style.display='none';
        document.querySelector('div.btn-contact-organizer').style.display='none';
        document.querySelector('section.ticket').style.display='none';
        document.querySelector('#navbarheader').style.display='none';
        document.querySelector('footer.footer').style.display='none';
        document.querySelector('div.app-ads').style.display='none';
        document.querySelector('div.app-ads').style.position='unset';
    `;
    const url =
      'https://ticketbox.vn/event/satoshi-gogo-live-in-vietnam-2019-hanoi-74840?utm_medium=TKB&utm_source=TKBHomePage&utm_campaign=homepage_hot_14';
    if (!this.props.regionData) {
      return <View />;
    }
    if (this.isFirstTimeLoadNews) {
      if (
        this.props.regionData &&
        this.props.regionData.home_screen &&
        this.props.regionData.home_screen.section_article
      ) {
        this.props.loadListNewsArticleData(this.props.regionData.home_screen.section_article.blog_handle, () => {
          this.isFirstTimeLoadNews = false;
        });
      }
    }
    if (this.isFirstTimeLoadPromotion) {
      if (
        this.props.regionData &&
        this.props.regionData.home_screen &&
        this.props.regionData.home_screen.section_article_promotion
      ) {
        this.props.loadListPromotionArticleData(
          this.props.regionData.home_screen.section_article_promotion.blog_handle,
          () => {
            this.isFirstTimeLoadPromotion = false;
          }
        );
      }
    }
    const { home_screen } = this.props.regionData;
    const section_article = home_screen && home_screen.section_article ? home_screen.section_article : '';
    const section_article_promotion =
      home_screen && home_screen.section_article_promotion ? home_screen.section_article_promotion : '';
    const section_slider = home_screen && home_screen.section_slider ? home_screen.section_slider : [];
    const articlePromotionTitle = section_article_promotion.module_title;
    const articleTitle = section_article.module_title;
    const listSlider = section_slider.list_slider ? section_slider.list_slider.filter(e => e.slide_use) : [];
    const nickname =
      this.props.userData && this.props.userData.nickname !== null ? this.props.userData.nickname : 'Đăng Nhập';
    const point =
      (this.props.userData && this.props.userData.cash) || (this.props.userData && this.props.userData.heart)
        ? this.props.userData.cash || this.props.userData.heart
        : 0;
    const avatar = this.props.userData && this.props.userData.avatar ? this.props.userData.avatar : null;
    return (
      <View style={{ backgroundColor: '#fff' }}>
        {/* <LinearGradient
          style={{
            top: -(DEVICE_WIDTH * 1.25),
            left: -(DEVICE_WIDTH * 0.55),
            width: DEVICE_WIDTH * 2,
            height: DEVICE_WIDTH * 2,
            borderRadius: DEVICE_WIDTH,
            position: 'absolute'
          }}
          start={{ x: 0.1, y: 0.75 }}
          end={{ x: 0.75, y: 0.25 }}
          colors={[APP_COLOR, APP_COLOR_2]}
        /> */}
        <HeaderWithBackButtonComponent
          noShadow
          styleContent={{ position: 'absolute', zIndex: 999 }}
          iconColor="#fff"
          styleIcon={{}}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Image
            style={{
              width: '100%',
              height: 150 * SCALE_RATIO_WIDTH_BASIS,
              marginBottom: 20
            }}
            source={{
              uri: 'https://tkbvn-tokyo.s3.amazonaws.com/Upload/eventcover/2019/02/26/DFB19E.jpg'
            }}
          />
          <View style={{ paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS }}>
            <Text
              style={[
                style.textCaption,
                {
                  fontSize: FS(18),
                  color: APP_COLOR_TEXT,
                  // textDecorationLine: 'underline',
                  // letterSpacing: 3,
                  lineHeight: 30
                }
              ]}
            >
              HIGH PERFORMANCE WEB APP: FROM IDEA TO PRODUCTION
            </Text>
            <Text style={[style.text, { fontSize: FS(12), color: APP_COLOR_TEXT, marginTop: 10 }]}>
              by Cybozu Viet Nam
            </Text>
          </View>
          <View
            style={{
              marginVertical: 20 * SCALE_RATIO_WIDTH_BASIS,
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
              paddingVertical: 15,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: APP_COLOR_TEXT_GRAY_2
            }}
          >
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <FontAwesome name="heart-o" size={23 * SCALE_RATIO_WIDTH_BASIS} color={APP_COLOR_TEXT_GRAY_2} />
              <Text style={[style.textCaption, { color: APP_COLOR_TEXT_GRAY }]}>Like Event</Text>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <FontAwesome name="map-o" size={23 * SCALE_RATIO_WIDTH_BASIS} color={APP_COLOR_TEXT_GRAY_2} />
              <Text style={[style.textCaption, { color: APP_COLOR_TEXT_GRAY }]}>Share Event</Text>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <FontAwesome name="share-square-o" size={23 * SCALE_RATIO_WIDTH_BASIS} color={APP_COLOR_TEXT_GRAY_2} />
              <Text style={[style.textCaption, { color: APP_COLOR_TEXT_GRAY }]}>Directions</Text>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <FontAwesome name="ellipsis-h" size={23 * SCALE_RATIO_WIDTH_BASIS} color={APP_COLOR_TEXT_GRAY_2} />
              <Text style={[style.textCaption, { color: APP_COLOR_TEXT_GRAY }]}>More</Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <FontAwesome
              name="calendar"
              style={{ marginTop: 5 }}
              size={14 * SCALE_RATIO_WIDTH_BASIS}
              color={APP_COLOR_TEXT_GRAY_2}
            />
            <View style={{ marginLeft: 10 }}>
              <Text style={[style.text, { fontSize: FS(16), color: APP_COLOR_TEXT }]}>Monday, February 4</Text>
              <Text style={[style.text, { fontSize: FS(12), fontcolor: APP_COLOR_TEXT_GRAY }]}>17:30 - 21:00</Text>
            </View>
          </View>
          <STouchableOpacity
            style={{
              backgroundColor: APP_COLOR,
              marginVertical: 20 * SCALE_RATIO_WIDTH_BASIS,
              marginHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
              borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
              paddingVertical: 15 * SCALE_RATIO_WIDTH_BASIS,
              alignItems: 'center'
            }}
            onPress={() => {
              // console.log('poi this.props.listNewsArticles:', this.props.listNewsArticles);
              // this.props.navigation.navigate(ROUTE_KEY.NEWS, {
              //   title: articleTitle,
              //   data: this.props.listNewsArticles
              // });
            }}
          >
            <Text
              style={[
                style.textCaption,
                {
                  fontFamily: FONT.Bold,
                  fontSize: FS(14),
                  color: '#fff'
                }
              ]}
            >
              Get Ticket Now
            </Text>
          </STouchableOpacity>
          <Text style={[style.text, { fontSize: FS(12), fontcolor: APP_COLOR_TEXT_GRAY, textAlign: 'center' }]}>
            Pay free tickets within 3 days before the event
          </Text>
          <View
            style={{ flex: 1, marginTop: 10 * SCALE_RATIO_WIDTH_BASIS, marginBottom: this.state.loadDone ? -30 : 0 }}
          >
            <AutoHeightWebView
              customStyle={`
      * {
        box-shadow: none !important;
       font-family: 'Barlow', sans-serif;
      }
      h3 {
        font-size: 1em !important;
      }
      body,a, button, h1, h2, h3, h4, h5, h6, li, option, p, span, td, th {
        font-size: 0.9em !important;
         box-shadow: none !important;
      }
 .section {
  box-shadow: none !important;
}
.relative {
  margin-top: 0px !important;
}
 .organizer,organizer {
         -webkit-box-shadow: none !important;
         -moz-box-shadow: none !important;
         box-shadow: none !important;
    }
.mainContainer .title {
    padding-bottom: 5px !important;
    border-bottom:  none !important;
}
`}
              onLoadEnd={() => this.setState({ loadDone: true })}
              style={{ width: DEVICE_WIDTH }}
              javaScriptEnabled
              customScript={jsCode}
              scalesPageToFit={!!IS_ANDROID}
              source={{ uri: url }}
            />
          </View>
          <View style={{}}>
            <View
              style={{
                paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                alignContent: 'center',
                marginBottom: 20 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <Text
                style={[
                  style.textCaption,
                  {
                    fontSize: FS(16),
                    color: APP_COLOR_TEXT,
                    fontFamily: FONT.SemiBold
                  }
                ]}
              >
                Suggested Events
              </Text>
            </View>

            <Carousel
              ref={c => {
                this._carousel = c;
              }}
              hasParallaxImages
              inactiveSlideScale={0.94}
              inactiveSlideOpacity={0.7}
              data={this.props.listPromotionArticles ? this.props.listPromotionArticles.slice(0, SLICE_NUM) : []}
              renderItem={this.renderPromotionItem}
              sliderWidth={DEVICE_WIDTH}
              itemWidth={(DEVICE_WIDTH * 70) / 100}
              enableMomentum
              activeSlideAlignment={'start'}
              activeAnimationType={'spring'}
              activeAnimationOptions={{
                friction: 4,
                tension: 40
              }}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapActionCreators = {
  loadListPopularEvents,
  loadListNewsArticleData,
  loadListPromotionArticleData,
  getDetailArticle
};

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData,
  regionData: state.setting.regionData,
  listNewsArticles: state.setting.listNewsArticles,
  newsArticlesPaginate: state.setting.newsArticlesPaginate,
  listPromotionArticles: state.setting.listPromotionArticles,
  promotionArticlesPaginate: state.setting.promotionArticlesPaginate,
  currentLocation: state.user.currentLocation,
  detailArticle: state.setting.detailArticle,
  listEventPopular: state.eventPopular.listEventPopular
});

export default connect(
  mapStateToProps,
  mapActionCreators
)(DetailEventComponent);
