import React from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View, FlatList } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import { connect } from 'react-redux';
import {
  DEVICE_WIDTH,
  FS,
  IS_IOS,
  ROUTE_KEY,
  SCALE_RATIO_WIDTH_BASIS,
  ARTICLE_TYPES,
  BASE_URL_TOCOTOCO,
  SLICE_NUM,
  THEMEID,
  SCALE_RATIO_HEIGHT_BASIS
} from '../../../constants/Constants';
import { DATA_TEST } from '../../../constants/dataTest';
import strings from '../../../constants/Strings';
import style, { APP_COLOR_TEXT, APP_COLOR, APP_COLOR_2, APP_COLOR_TEXT_GRAY, FONT } from '../../../constants/style';
import { alert } from '../../../utils/alert';
import HeaderWithAvatar from '../../view/HeaderWithAvatar';
import MyComponent from '../../view/MyComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import { loadListNewsArticleData, loadListPromotionArticleData } from './HomeActions';
import { getDetailArticle } from '../../screen/news/NewsAction';
import LinearGradient from 'react-native-linear-gradient';
import { loadListPopularEvents } from './HomeActions';
import { getDeviceInfo } from '../../../utils/asyncStorage';

class HomeComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = { activeSlide: 0 };
    this.store = DATA_TEST;
    this.data = DATA_TEST;
    this.isFirstTimeLoadNews = true;
    this.isFirstTimeLoadPromotion = true;
  }

  // componentDidMount() {
  //   if (this.props.regionData && this.props.regionData.home_screen && this.props.regionData.home_screen.section_article) {
  //     this.props.loadListNewsArticleData(this.props.regionData.home_screen.section_article.blog_handle, () => {

  //     });
  //   }
  //   if (this.props.regionData && this.props.regionData.home_screen && this.props.regionData.home_screen.section_article_promotion) {
  //     this.props.loadListPromotionArticleData(this.props.regionData.home_screen.section_article_promotion.blog_handle, () => {

  //     });
  //   }
  // }
  componentDidMount() {
    this.props.loadListPopularEvents(1);
  }

  renderBannerItem = ({ item, index }) => (
    <TouchableOpacity onPress={() => {}}>
      <Image
        style={{
          width: '100%',
          height: 158 * SCALE_RATIO_WIDTH_BASIS,
          borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS
        }}
        source={{
          uri: item.slide_img ? item.slide_img : 'https://www.fancyhands.com/images/default-avatar-250x250.png'
        }}
      />
    </TouchableOpacity>
  );

  renderPromotionItem = ({ item, index }) => (
    <TouchableOpacity>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          width: 40 * SCALE_RATIO_WIDTH_BASIS,
          height: 40 * SCALE_RATIO_WIDTH_BASIS,
          borderRadius: (40 * SCALE_RATIO_WIDTH_BASIS) / 2,
          position: 'absolute',
          top: 5,
          right: 10,
          backgroundColor: 'white',
          zIndex: 999
        }}
      >
        <MaterialCommunityIcons
          name="heart-outline"
          size={25 * SCALE_RATIO_WIDTH_BASIS}
          color={APP_COLOR}
          style={{ marginBottom: -5 * SCALE_RATIO_WIDTH_BASIS }}
        />
      </View>
      <View style={[styles.carouselItemContainer, { marginLeft: 15, overflow: 'hidden' }]}>
        <View>
          <Image
            style={{
              width: '100%',
              height: 150 * SCALE_RATIO_WIDTH_BASIS
            }}
            source={{
              uri: item.image ? item.image : 'https://www.fancyhands.com/images/default-avatar-250x250.png'
            }}
          />
        </View>

        <Text
          style={[
            style.textCaption,
            {
              color: APP_COLOR_TEXT,
              fontSize: FS(14),
              padding: 5 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
          numberOfLines={2}
        >
          {item.title}
        </Text>

        <Text
          style={[
            style.textCaption,
            {
              color: APP_COLOR_TEXT_GRAY,
              fontSize: FS(10),
              padding: 5 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
          numberOfLines={2}
        >
          Sun, February 22 - 17:00
        </Text>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ borderWidth: 0.5, borderColor: '#70707050', padding: 1, borderRadius: 3 }}>
            <Text
              style={[
                style.textCaption,
                {
                  alignSelf: 'flex-start',
                  color: APP_COLOR_TEXT,
                  fontSize: FS(10),
                  padding: 5 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
              numberOfLines={2}
            >
              Bussiness
            </Text>
          </View>
          <View style={{ marginLeft: 5, borderWidth: 0.5, borderColor: APP_COLOR, padding: 1, borderRadius: 3 }}>
            <Text
              style={[
                style.textCaption,
                {
                  alignSelf: 'flex-start',
                  color: APP_COLOR,
                  fontSize: FS(10),
                  padding: 5 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
              numberOfLines={2}
            >
              From $14
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );

  renderNewsItem = ({ item, index }) => (
    <TouchableOpacity>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          position: 'absolute',
          bottom: 10,
          right: 20,
          zIndex: 999
        }}
      >
        <MaterialCommunityIcons
          name="heart-outline"
          size={20 * SCALE_RATIO_WIDTH_BASIS}
          color={APP_COLOR}
          style={{ marginBottom: -5 * SCALE_RATIO_WIDTH_BASIS }}
        />
      </View>
      <View
        style={[
          {
            paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
            marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
            flex: 1,
            backgroundColor: 'white',
            overflow: 'hidden',
            flexDirection: 'row',
            alignItems: 'center'
          }
        ]}
      >
        <View style={{ flex: 3 }}>
          <Image
            style={{
              borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
              width: '100%',
              height: 86 * SCALE_RATIO_WIDTH_BASIS
            }}
            source={{
              uri: item.image ? item.image : 'https://www.fancyhands.com/images/default-avatar-250x250.png'
            }}
          />
        </View>
        <View style={{ flex: 6, paddingLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>
          <Text
            style={[
              style.textCaption,
              {
                color: APP_COLOR_TEXT,
                fontSize: FS(14),
                padding: 5 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
            numberOfLines={2}
          >
            {item.title}
          </Text>

          <Text
            style={[
              style.textCaption,
              {
                color: APP_COLOR_TEXT_GRAY,
                fontSize: FS(10),
                padding: 5 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
            numberOfLines={2}
          >
            Sun, February 22 - 17:00
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ borderWidth: 0.5, borderColor: '#70707050', padding: 1, borderRadius: 3 }}>
              <Text
                style={[
                  style.textCaption,
                  {
                    alignSelf: 'flex-start',
                    color: APP_COLOR_TEXT,
                    fontSize: FS(10),
                    padding: 5 * SCALE_RATIO_WIDTH_BASIS
                  }
                ]}
                numberOfLines={2}
              >
                Bussiness
              </Text>
            </View>
            <View style={{ marginLeft: 5, borderWidth: 0.5, borderColor: APP_COLOR, padding: 1, borderRadius: 3 }}>
              <Text
                style={[
                  style.textCaption,
                  {
                    alignSelf: 'flex-start',
                    color: APP_COLOR,
                    fontSize: FS(10),
                    padding: 5 * SCALE_RATIO_WIDTH_BASIS
                  }
                ]}
                numberOfLines={2}
              >
                From $14
              </Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
  render() {
    if (!this.props.regionData) {
      return <View />;
    }
    if (this.isFirstTimeLoadNews) {
      if (
        this.props.regionData &&
        this.props.regionData.home_screen &&
        this.props.regionData.home_screen.section_article
      ) {
        this.props.loadListNewsArticleData(this.props.regionData.home_screen.section_article.blog_handle, () => {
          this.isFirstTimeLoadNews = false;
        });
      }
    }
    if (this.isFirstTimeLoadPromotion) {
      if (
        this.props.regionData &&
        this.props.regionData.home_screen &&
        this.props.regionData.home_screen.section_article_promotion
      ) {
        this.props.loadListPromotionArticleData(
          this.props.regionData.home_screen.section_article_promotion.blog_handle,
          () => {
            this.isFirstTimeLoadPromotion = false;
          }
        );
      }
    }
    const { home_screen } = this.props.regionData;
    const section_article = home_screen && home_screen.section_article ? home_screen.section_article : '';
    const section_article_promotion =
      home_screen && home_screen.section_article_promotion ? home_screen.section_article_promotion : '';
    const section_slider = home_screen && home_screen.section_slider ? home_screen.section_slider : [];
    const articlePromotionTitle = section_article_promotion.module_title;
    const articleTitle = section_article.module_title;
    const listSlider = section_slider.list_slider ? section_slider.list_slider.filter(e => e.slide_use) : [];
    const nickname =
      this.props.userData && this.props.userData.nickname !== null ? this.props.userData.nickname : 'Đăng Nhập';
    const point =
      (this.props.userData && this.props.userData.cash) || (this.props.userData && this.props.userData.heart)
        ? this.props.userData.cash || this.props.userData.heart
        : 0;
    const avatar = this.props.userData && this.props.userData.avatar ? this.props.userData.avatar : null;
    return (
      <View style={{ backgroundColor: '#fff', flex: 1 }}>
        <LinearGradient
          style={{
            top: -(DEVICE_WIDTH * 1.25),
            left: -(DEVICE_WIDTH * 0.55),
            width: DEVICE_WIDTH * 2,
            height: DEVICE_WIDTH * 2,
            borderRadius: DEVICE_WIDTH,
            position: 'absolute'
          }}
          start={{ x: 0.1, y: 0.75 }}
          end={{ x: 0.75, y: 0.25 }}
          colors={[APP_COLOR, APP_COLOR_2]}
        />
        <HeaderWithAvatar
          noShadow
          name={nickname}
          styleName={{ color: '#fff' }}
          rightIconStyle={{ color: '#fff' }}
          point={point}
          avatar={avatar}
          onAvatarPress={() => this.props.navigation.navigate(ROUTE_KEY.PERSONALINFO)}
          onPointPress={() => this.props.navigation.navigate(ROUTE_KEY.REWARDS)}
          rightIcon="bell"
          rightIconType="SimpleLineIcons"
          onRightPress={() => {}}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS }}>
            <Text style={[style.text, { fontSize: FS(17), color: '#fff' }]}>Near by</Text>
            <Text
              style={[
                style.textCaption,
                {
                  fontSize: FS(32),
                  color: '#fff',
                  textDecorationLine: 'underline',
                  letterSpacing: 3,
                  lineHeight: 50
                }
              ]}
            >
              Hồ Chí Minh
            </Text>
          </View>
          {/* <Carousel
            ref={c => {
              this._carousel = c;
            }}
            containerCustomStyle={{ alignSelf: 'center' }}
            hasParallaxImages
            inactiveSlideScale={0.94}
            inactiveSlideOpacity={0.7}
            data={listSlider}
            renderItem={this.renderBannerItem}
            sliderWidth={DEVICE_WIDTH}
            itemWidth={(DEVICE_WIDTH * 95) / 100}
            enableSnap
            loop
            autoplay
            autoplayDelay={6000}
            autoplayInterval={3000}
            activeAnimationType={'spring'}
            activeAnimationOptions={{
              friction: 4,
              tension: 40
            }}
            onSnapToItem={index => this.setState({ activeSlide: index })}
          />
          <Pagination
            dotsLength={listSlider.length}
            activeDotIndex={this.state.activeSlide}
            containerStyle={{ paddingVertical: 0.5 * SCALE_RATIO_WIDTH_BASIS }}
            dotColor="#70707090"
            dotStyle={{
              marginTop: 12,
              width: 6 * SCALE_RATIO_WIDTH_BASIS,
              height: 6 * SCALE_RATIO_WIDTH_BASIS,
              borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS
            }}
            inactiveDotColor="#242424"
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
            carouselRef={this._sliderRef}
            tappableDots={!!this._sliderRef}
          /> */}
          {/* Cửa hàng gần bạn */}
          <View
            style={{
              marginTop: 15 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <View
              style={{
                paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                alignContent: 'center',
                marginBottom: 20 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <Text
                style={[
                  style.textCaption,
                  {
                    fontSize: FS(16),
                    color: 'white',
                    fontFamily: FONT.SemiBold
                  }
                ]}
              >
                Popular events
              </Text>
              {/* <STouchableOpacity
                style={{ flexDirection: 'row' }}
                // onPress={() => alert(strings.alert, strings.this_feature_is_in_development)}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_KEY.PROMOTION_COMPONENT, {
                    title: articlePromotionTitle,
                    data: this.props.listPromotionArticles
                  });
                }}
              >
                <Text
                  style={[
                    style.textCaption,
                    {
                      color: '#9c9faa'
                    }
                  ]}
                >
                  Xem tất cả
                </Text>
                <MaterialCommunityIcons
                  name="chevron-right"
                  size={16 * SCALE_RATIO_WIDTH_BASIS}
                  style={{ marginRight: 3 * SCALE_RATIO_WIDTH_BASIS }}
                  color="#9c9faa"
                />
              </STouchableOpacity> */}
            </View>

            <Carousel
              ref={c => {
                this._carousel = c;
              }}
              hasParallaxImages
              inactiveSlideScale={0.94}
              inactiveSlideOpacity={0.7}
              data={this.props.listPromotionArticles ? this.props.listPromotionArticles.slice(0, SLICE_NUM) : []}
              renderItem={this.renderPromotionItem}
              sliderWidth={DEVICE_WIDTH}
              itemWidth={(DEVICE_WIDTH * 70) / 100}
              enableMomentum
              activeSlideAlignment={'start'}
              activeAnimationType={'spring'}
              activeAnimationOptions={{
                friction: 4,
                tension: 40
              }}
            />
          </View>
          <View
            style={{
              marginTop: 30 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <View
              style={{
                paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                alignContent: 'center',
                marginBottom: 20 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <Text
                style={[
                  style.textCaption,
                  {
                    fontSize: FS(16),
                    color: APP_COLOR_TEXT,
                    fontFamily: FONT.SemiBold
                  }
                ]}
              >
                Upcoming in week
              </Text>
            </View>
            <FlatList
              style={{ flex: 1 }}
              data={this.props.listNewsArticles ? this.props.listNewsArticles.slice(0, SLICE_NUM) : []}
              renderItem={this.renderNewsItem}
              onRefresh={this.onRefresh}
              refreshing={this.state.refreshing}
              onEndReached={this.handleLoadMore}
              onEndReachedThreshold={0.01}
              ListFooterComponent={this.renderFooter}
            />
            <STouchableOpacity
              style={{
                marginVertical: 20 * SCALE_RATIO_WIDTH_BASIS,
                marginHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                borderWidth: 1,
                borderColor: APP_COLOR_TEXT_GRAY,
                padding: 5 * SCALE_RATIO_WIDTH_BASIS,
                alignItems: 'center'
              }}
              onPress={() => {
                console.log('poi this.props.listNewsArticles:', this.props.listNewsArticles);
                this.props.navigation.navigate(ROUTE_KEY.NEWS, {
                  title: articleTitle,
                  data: this.props.listNewsArticles
                });
              }}
            >
              <Text
                style={[
                  style.textCaption,
                  {
                    fontSize: FS(14),
                    color: '#000'
                  }
                ]}
              >
                See more event (2000+)
              </Text>
            </STouchableOpacity>
          </View>
          <View
            style={{
              marginBottom: 30 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <View
              style={{
                paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                alignContent: 'center',
                marginBottom: 20 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <Text
                style={[
                  style.textCaption,
                  {
                    fontSize: FS(16),
                    color: APP_COLOR_TEXT,
                    fontFamily: FONT.SemiBold
                  }
                ]}
              >
                Free entry ticket
              </Text>
            </View>
            <FlatList
              style={{ flex: 1 }}
              data={this.props.listNewsArticles ? this.props.listNewsArticles.slice(0, SLICE_NUM) : []}
              renderItem={this.renderNewsItem}
              onRefresh={this.onRefresh}
              refreshing={this.state.refreshing}
              onEndReached={this.handleLoadMore}
              onEndReachedThreshold={0.01}
              ListFooterComponent={this.renderFooter}
            />
            <STouchableOpacity
              style={{
                marginVertical: 20 * SCALE_RATIO_WIDTH_BASIS,
                marginHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                borderWidth: 1,
                borderColor: APP_COLOR_TEXT_GRAY,
                padding: 5 * SCALE_RATIO_WIDTH_BASIS,
                alignItems: 'center'
              }}
              onPress={() => {
                console.log('poi this.props.listNewsArticles:', this.props.listNewsArticles);
                this.props.navigation.navigate(ROUTE_KEY.NEWS, {
                  title: articleTitle,
                  data: this.props.listNewsArticles
                });
              }}
            >
              <Text
                style={[
                  style.textCaption,
                  {
                    fontSize: FS(14),
                    color: '#000'
                  }
                ]}
              >
                See more event (2000+)
              </Text>
            </STouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  markerImageStyle: {
    width: 40 * SCALE_RATIO_WIDTH_BASIS,
    height: 40 * SCALE_RATIO_WIDTH_BASIS
  },
  searchInputContainer: {
    paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
    position: 'absolute',
    top: 20 * SCALE_RATIO_WIDTH_BASIS,
    width: DEVICE_WIDTH,
    height: 50 * SCALE_RATIO_WIDTH_BASIS
  },
  searchInput: {
    borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS,
    flex: 1,
    height: 50 * SCALE_RATIO_WIDTH_BASIS,
    flexDirection: 'row',
    backgroundColor: '#FFFFFF'
  },
  searchNameContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchNameTextStyle: {
    fontSize: 14 * SCALE_RATIO_WIDTH_BASIS,
    color: '#767676',
    fontWeight: '500'
  },
  searchIconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 40 * SCALE_RATIO_WIDTH_BASIS,
    height: '100%'
  },
  carouselItemContainer: {
    overflow: 'visible',
    borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
    marginBottom: IS_IOS ? 5 : 6,
    flex: 1,
    backgroundColor: 'white'
  },
  carouselItemMoreInfo: {
    textAlign: 'center',
    fontSize: 10 * SCALE_RATIO_WIDTH_BASIS
  }
});
const mapActionCreators = {
  loadListPopularEvents,
  loadListNewsArticleData,
  loadListPromotionArticleData,
  getDetailArticle
};

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData,
  regionData: state.setting.regionData,
  listNewsArticles: state.setting.listNewsArticles,
  newsArticlesPaginate: state.setting.newsArticlesPaginate,
  listPromotionArticles: state.setting.listPromotionArticles,
  promotionArticlesPaginate: state.setting.promotionArticlesPaginate,
  currentLocation: state.user.currentLocation,
  detailArticle: state.setting.detailArticle,
  listEventPopular: state.eventPopular.listEventPopular
});

export default connect(
  mapStateToProps,
  mapActionCreators
)(HomeComponent);
