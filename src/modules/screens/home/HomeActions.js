import { Platform } from 'react-native';
import * as RNIap from 'react-native-iap';
import {
  BASE_URL,
  USER_API,
  BASE_URL_TOCOTOCO,
  BLOGS,
  THEMEID,
  ARTICLE_TYPES,
  LOAD_LIST_NEWS_ARTICLES_DATA,
  LOAD_LIST_PROMOTION_ARTICLES_DATA,
  BASE_URL_FBS,
  STORE_API,
  STORES,
  UPDATE_LIST_STORES,
  UPDATE_LIST_POPUPAR_EVENTS
} from '../../../constants/Constants';
import request from '../../../utils/request';
import { alert } from '../../../utils/alert';
import strings from '../../../constants/Strings';
import { loadUserProfile } from '../../screen/splash/SplashActions';

export function loadListPopularEvents(page = 1, onOutOfData = () => {}) {
  return (dispatch, store) => {
    // const { id } = store().user.userData;
    const filter = `limit=10&page=${page}`;
    // const filter = `filter={"rater_user_id": "${id}", "type": "PENDING"}&limit=10&page=${page}`;

    request.get(`https://eticket-vhu.herokuapp.com/api/v1/eticket/get-event?${filter}`).finish((err, res) => {
      // console.log('dauphaiphat: loadListPopularEvents -> res', res.body.data);
      if (!err && res && res.body && res.body.data) {
        if (res.body.data.length === 0) onOutOfData();
        dispatch({
          type: UPDATE_LIST_POPUPAR_EVENTS,
          payload: res.body.data
        });
      }
    });
  };
}

////////////////////////////////////////////
export function loadListStoreNearByUser(coordinate, onDoneFunc = isSuccess => {}) {
  return (dispatch, store) => {
    // {{baseUrl}}/stores?longitude=134&latitude=123
    const url = `${BASE_URL_FBS}/${STORES}?longitude=${coordinate.longitude}&latitude=${coordinate.latitude}`;
    console.log('poi loadListStoreNearByUser url :', url);
    request
      .get(url)
      .set('X-Tocotoco-Region', store().user.currentLocation.region)
      .finish((err, res) => {
        console.log('poi loadListStoreNearByUser response :', res);
        if (!err && res && res.body && res.body.stores) {
          //NEWS:
          dispatch({
            type: UPDATE_LIST_STORES,
            payload: res.body.stores
          });
          onDoneFunc(true);
        } else {
          onDoneFunc(false);
        }
      });
  };
}

export function loadListNewsArticleData(blog_handle, onDoneFunc = isSuccess => {}) {
  return (dispatch, store) => {
    const url = `${BASE_URL_TOCOTOCO}/${BLOGS}/${blog_handle}?view=detail.json&themeid=${THEMEID}`;
    console.log('poi loadListArticleData url :', url);
    request
      .get(url)
      .set('X-Tocotoco-Region', store().user.currentLocation.region)
      .finish((err, res) => {
        const response = JSON.parse(res && res.text ? res.text : undefined);
        console.log('poi loadListArticleData response :', response);
        if (!err && response) {
          //NEWS:
          dispatch({
            type: LOAD_LIST_NEWS_ARTICLES_DATA,
            payload: {
              listNewsArticles: response.articles,
              newsArticlesPaginate: response.paginate
            }
          });
          onDoneFunc(true);
        } else {
          onDoneFunc(false);
        }
      });
  };
}

export function loadListPromotionArticleData(blog_handle, onDoneFunc = isSuccess => {}) {
  return (dispatch, store) => {
    const url = `${BASE_URL_TOCOTOCO}/${BLOGS}/${blog_handle}?view=detail.json&themeid=${THEMEID}`;
    console.log('poi loadListArticleData url :', url);
    request
      .get(url)
      .set('X-Tocotoco-Region', store().user.currentLocation.region)
      .finish((err, res) => {
        console.log('poi loadListArticleData res :', res);
        const response = JSON.parse(res && res.text ? res.text : undefined);
        console.log('poi loadListArticleData response :', response);
        if (!err && response) {
          dispatch({
            type: LOAD_LIST_PROMOTION_ARTICLES_DATA,
            payload: {
              listPromotionArticles: response.articles,
              promotionArticlesPaginate: response.paginate
            }
          });
          onDoneFunc(true);
        } else {
          onDoneFunc(false);
        }
      });
  };
}

export function loadMoreListNewsArticleData(blog_handle, page, onDoneFunc = (isSuccess, data, paginate) => {}) {
  return (dispatch, store) => {
    const url = `${BASE_URL_TOCOTOCO}/${BLOGS}/${blog_handle}?view=detail.json&themeid=${THEMEID}&page=${page}`;
    console.log('poi loadListArticleData url :', url);
    request
      .get(url)
      .set('X-Tocotoco-Region', store().user.currentLocation.region)
      .finish((err, res) => {
        const response = JSON.parse(res && res.text ? res.text : undefined);
        console.log('poi loadListArticleData response :', response);
        if (!err && response) {
          //NEWS:
          dispatch({
            type: LOAD_LIST_NEWS_ARTICLES_DATA,
            payload: {
              listNewsArticles: response.articles,
              newsArticlesPaginate: response.paginate
            }
          });
          onDoneFunc(true, response.articles, response.paginate);
        } else {
          onDoneFunc(false, [], {});
        }
      });
  };
}

export function loadMoreListPromotionArticleData(blog_handle, page, onDoneFunc = (isSuccess, data, paginate) => {}) {
  return (dispatch, store) => {
    const url = `${BASE_URL_TOCOTOCO}/${BLOGS}/${blog_handle}?view=detail.json&themeid=${THEMEID}&page=${page}`;
    console.log('poi loadListArticleData url :', url);
    request
      .get(url)
      .set('X-Tocotoco-Region', store().user.currentLocation.region)
      .finish((err, res) => {
        const response = JSON.parse(res && res.text ? res.text : undefined);
        console.log('poi loadListArticleData response :', response);
        if (!err && response) {
          dispatch({
            type: LOAD_LIST_PROMOTION_ARTICLES_DATA,
            payload: {
              listPromotionArticles: response.articles,
              promotionArticlesPaginate: response.paginate
            }
          });
          onDoneFunc(true, response.articles, response.paginate);
        } else {
          onDoneFunc(false, [], {});
        }
      });
  };
}

// export function loadMoreListArticleData(blog_handle, onDoneFunc = isSuccess => {}) {
//   return (dispatch, store) => {
//     const url = `${BASE_URL_TOCOTOCO}/${BLOGS}/${blog_handle}?view=detail.json&themeid=${THEMEID}`;
//     console.log('poi loadListArticleData url :', url);
//     request.get(url).finish((err, res) => {
//       const response = JSON.parse(res && res.text ? res.text : undefined);
//       console.log('poi loadListArticleData response :', response);
//       if (!err && response) {
//         if (blog_handle === ARTICLE_TYPES.NEWS) {
//           //NEWS:
//           dispatch({
//             type: LOAD_LIST_NEWS_ARTICLES_DATA,
//             payload: {
//               listNewsArticles: response.articles,
//               newsArticlesPaginate: response.paginate
//             }
//           });
//         } else {
//           //DISCOUNT:
//           dispatch({
//             type: LOAD_LIST_NEWS_ARTICLES_DATA,
//             payload: {
//               listPromotionArticles: response.articles,
//               promotionArticlesPaginate: response.paginate
//             }
//           });
//         }
//         onDoneFunc(true);
//       } else {
//         onDoneFunc(false);
//       }
//     });
//   };
// }

export function buyHeart(productId, token, ip, onSuccess = () => {}) {
  return new Promise((resolve, reject) => {
    console.log('hinodi buyHeart bat dau', productId);
    RNIap.initConnection()
      .then(res => {
        console.log('hinodi initConnection ok', res);
        RNIap.buyProduct(productId)
          .then(purchase => {
            console.log('hinodix buyProduct ok', purchase);
            request
              .post(`${BASE_URL}${USER_API}/buy_heart`)
              .set('Content-Type', 'application/json')
              .set('Authorization', `Bearer ${token}`)
              .send({
                receipt: {
                  data: Platform.OS === 'android' ? purchase.transactionReceipt : purchase.transactionReceipt, // Never test IOS, IOS data must be different
                  signature: Platform.OS === 'android' ? purchase.signatureAndroid : purchase.signatureAndroid // Never test IOS, IOS data must be different
                },
                ip
              })
              .finish((err, res2) => {
                console.log('hinodi buy_heart', err, res2);
                if (!err && res2.body.code === 200) {
                  alert(strings.alert, strings.buy_success, () => {
                    onSuccess();
                  });
                } else {
                  alert(strings.alert, strings.buy_fail);
                }
              });
          })
          .catch(err => {
            console.log('hinodix buyProduct fail', err.code);
            if (err.code !== 'E_USER_CANCELLED') alert(strings.alert, strings.buy_fail);
          });
      })
      .catch(err => {
        console.log('hinodi initConnection fail', err);
        reject(err);
      });
  });
}

export function buyHeartSuccess(onDoneFunc = () => {}) {
  return (dispatch, store) => {
    loadUserProfile(store, dispatch, onDoneFunc);
  };
}

export function getListProduct() {
  return new Promise((resolve, reject) => {
    const itemSkus = Platform.select({
      ios: [],
      android: [
        'com.chattingapp.day2.55',
        'com.chattingapp.day2.100',
        'com.chattingapp.day2.200',
        'com.chattingapp.day2.500',
        'com.chattingapp.day2.1000',
        'com.chattingapp.day2.2000'
      ]
    });
    RNIap.initConnection()
      .then(res => {
        console.log('hinodi initConnection ok', res);
        RNIap.getProducts(itemSkus)
          .then(purchase => {
            console.log('hinodix getProducts ok', purchase);
            resolve(purchase);
          })
          .catch(err => {
            console.log('hinodix getProducts fail', err);
            reject(err);
          });
      })
      .catch(err => {
        console.log('hinodi initConnection fail', err);
        reject(err);
      });
  });
}
