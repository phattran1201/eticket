import React from 'react';
import CodePush from 'react-native-code-push';
import { MenuProvider } from 'react-native-popup-menu';
import { Provider } from 'react-redux';
import AppNavigator from '../config/AppNavigator';
import store from '../config/redux/store';
import { UPDATE_CURRENT_TOKEN, UPDATE_CURRENT_USER_DATA } from '../constants/Constants';
import { getUserIdentity, isFirstTimeUseApp } from '../utils/asyncStorage';
import MyComponent from './view/MyComponent';
import MySpinner from './view/MySpinner';
import PopupNotification from './view/PopupNotification';

class App extends MyComponent {
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    CodePush.disallowRestart();
    isFirstTimeUseApp().then(isFirstTime => {
      if (isFirstTime) {
        setTimeout(() => {
          this.setState({ startRealApp: false });
        }, 100);
      }
    });
  }

  async updateUserIndentityFromAsyncStorageToRedux() {
    const userIdentity = await getUserIdentity();
    store.dispatch({
      type: UPDATE_CURRENT_TOKEN,
      payload: userIdentity.token
    });
    store.dispatch({
      type: UPDATE_CURRENT_USER_DATA,
      payload: userIdentity.userData
    });
  }

  render() {
    return (
      <Provider store={store}>
        <MenuProvider style={{ flex: 1 }}>
          <AppNavigator />
          <PopupNotification />
          <MySpinner />
        </MenuProvider>
      </Provider>
    );
  }
}

const codepushoptions = { checkFrequency: CodePush.CheckFrequency.MANUAL };
const AppC = CodePush(codepushoptions)(App);
export default AppC;
