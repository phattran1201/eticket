import React from 'react';
import {
  Alert,
  Dimensions,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  FS,
  MIN_HEART_TO_MAKE_A_PHONE_CALL,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import MyButton from '../../../style/MyButton';
import getFlag from '../../../utils/getFlag';
import getGenderImage from '../../../utils/getGenderImage';
import MyComponent from '../../view/MyComponent';
import Dropdown from '../../view/MyDropDown/dropdown';
import MySpinner from '../../view/MySpinner';
import PopupNotification from '../../view/PopupNotification';
import PopupNotificationCallComponent from '../../view/PopupNotificationCallComponent';
import PopupNotificationChildComponent from '../../view/PopupNotificationChildComponent';
import PopupNotificationMessageComponent from '../../view/PopupNotificationMessageComponent';
import PopupNotificationReportComponent from '../../view/PopupNotificationReportComponent';
import { blockFunction } from '../blockfriends/BlockFriendActions';

const { width } = Dimensions.get('window');

const hashtag = require('../../../assets/imgs/hashtag.png');

class PartnersDetailComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      widthButton: 140 * SCALE_RATIO_WIDTH_BASIS
    };
  }

  find_dimesions(layout) {
    this.setState({ widthButton: layout.width });
  }

  replaceSource(source = '') {
    if (typeof source !== 'string') return '';
    return source
      ? source.replace('localhost', 'hitek.com.vn').replace('toi.innoway.info', 'hitek.com.vn')
      : '';
  }
  render() {
    const { params } = this.props.navigation.state;
    const item = params && params.item ? params.item : null;

    if (!item) return null;

    const {
      avatar,
      locale,
      nickname,
      email,
      age,
      introduction,
      address,
      interests,
      rating,
      sex
    } = item;

    return (
      <View style={styles.container}>
        <Feather
          onPress={() => {
            this.props.navigation.goBack();
          }}
          name="arrow-left"
          size={FS(20)}
          color="#fff"
          style={
            (style.shadow,
            {
              position: 'absolute',
              top: 30 * SCALE_RATIO_HEIGHT_BASIS + getStatusBarHeight(true),
              left: 14 * SCALE_RATIO_WIDTH_BASIS,
              zIndex: 99,
              backgroundColor: '#00000030',
              borderRadius: 20 * SCALE_RATIO_WIDTH_BASIS,
              padding: 2 * SCALE_RATIO_WIDTH_BASIS
            })
          }
        />
        <View
          style={{
            position: 'absolute',
            top: 30 * SCALE_RATIO_HEIGHT_BASIS + getStatusBarHeight(true),
            right: 12 * SCALE_RATIO_WIDTH_BASIS,
            zIndex: 99
          }}
        >
          <Dropdown
            // statusBarColor="transparent"
            dropdownPosition={0}
            pickerStyle={{
              width: 80 * SCALE_RATIO_WIDTH_BASIS,
              borderColor: Platform.OS === 'ios' ? '#AE92D330' : '#70707010',
              borderWidth: 1,
              flexDirection: 'row',
              borderRadius: (31 * SCALE_RATIO_HEIGHT_BASIS) / 2,
              borderTopRightRadius: 0
            }}
            textColor="#C7AE6D"
            fontSize={15 * SCALE_RATIO_WIDTH_BASIS}
            itemTextStyle={{
              fontSize: 4.5 * SCALE_RATIO_WIDTH_BASIS,
              fontFamily: 'helveticaneue',
              color: '#C7AE6D'
            }}
            itemPadding={5}
            dropdownOffset={{
              top: 30 * SCALE_RATIO_HEIGHT_BASIS + getStatusBarHeight(true),
              left: -8 * SCALE_RATIO_WIDTH_BASIS
            }}
            data={[
              {
                value: strings.block,
                onSelect: onDone => {
                  setTimeout(() => {
                    PopupNotification.showComponent(
                      <PopupNotificationChildComponent
                        title={strings.block}
                        content={strings.block_member_comfirm}
                        subContent={strings.block_member_noti}
                        button1Text={strings.cancel}
                        onButton1Press={() => PopupNotification.hide()}
                        button2Text={strings.block}
                        onButton2Press={() => {
                          PopupNotification.hide();
                          setTimeout(() => {
                            MySpinner.show();
                            this.props.blockFunction(item, MySpinner.hide, () => {
                              setTimeout(() => this.props.navigation.goBack(), 300);
                            });
                          }, 300);
                        }}
                      />
                    );
                  }, 500);
                }
              },
              {
                value: strings.report,
                onSelect: onDone => {
                  setTimeout(() => {
                    PopupNotification.showComponent(
                      <PopupNotificationReportComponent item={item} />
                    );
                  }, 500);
                }
              }
            ]}
          >
            <Feather
              name="more-horizontal"
              size={FS(20)}
              color="#fff"
              style={{
                backgroundColor: '#00000030',
                borderRadius: 20 * SCALE_RATIO_WIDTH_BASIS,
                padding: 2 * SCALE_RATIO_WIDTH_BASIS
              }}
            />
          </Dropdown>
        </View>
        <ScrollView style={{ flex: 1 }}>
          <View
            style={
              (style.shadow,
              {
                borderBottomWidth: 2,
                borderColor: '#C7AE6D',
                marginBottom: 8 * SCALE_RATIO_HEIGHT_BASIS
              })
            }
          >
            <FastImage
              style={styles.avatarImage}
              removeClippedSubviews
              source={
                avatar
                  ? { uri: this.replaceSource(avatar) }
                  : require('../../../assets/imgs/default_avatar.jpg')
              }
              defaultSource={null}
            />
          </View>
          {/* </View> */}
          <View style={styles.infoContainer}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={[style.textNormal, { fontSize: FS(20) }]}>{nickname || email}</Text>
              <Image source={getFlag(locale)} style={styles.flagInfoImage} />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 5 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              {getGenderImage(sex, 1.3)}
              <Text style={[style.textNormal, { fontSize: FS(15) }]}>{age}</Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginLeft: 13 * SCALE_RATIO_WIDTH_BASIS
                }}
              >
                {[0, 1, 2, 3, 4].map(e => (
                  <MaterialIcons
                    key={e.toString()}
                    name={rating > e ? 'star' : 'star-border'}
                    size={16 * SCALE_RATIO_WIDTH_BASIS}
                    color="#C7AE6D"
                    style={{ marginRight: 1 * SCALE_RATIO_WIDTH_BASIS }}
                  />
                ))}
              </View>
            </View>
            {introduction ? (
              <View style={styles.moreDetailContainer}>
                <Feather name="info" size={23 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
                <Text
                  style={[
                    style.text,
                    {
                      color: '#282828',
                      marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS
                    }
                  ]}
                  numberOfLines={1}
                >
                  {introduction}
                </Text>
              </View>
            ) : null}
            {address ? (
              <View style={styles.moreDetailContainer}>
                <Feather name="map-pin" size={23 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
                <Text
                  style={[
                    style.text,
                    {
                      color: '#282828',
                      marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS
                    }
                  ]}
                  numberOfLines={1}
                >
                  {address}
                </Text>
              </View>
            ) : null}
            {interests ? (
              <View style={styles.moreDetailContainer}>
                <Image
                  source={hashtag}
                  style={{
                    width: 22 * SCALE_RATIO_WIDTH_BASIS,
                    height: 22 * SCALE_RATIO_WIDTH_BASIS
                  }}
                />
                <Text
                  style={[
                    style.text,
                    {
                      color: '#282828',
                      marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS
                    }
                  ]}
                >
                  {!interests ? 'interests' : interests.toString()}
                </Text>
              </View>
            ) : null}
          </View>
        </ScrollView>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            margin: 12 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          <MyButton
            onLayout={event => {
              this.find_dimesions(event.nativeEvent.layout);
            }}
            leftIcon={'message-square'}
            leftIconType="Feather"
            onPress={() =>
              PopupNotification.showComponent(
                <PopupNotificationMessageComponent
                  onButtonPress={() => PopupNotification.hide()}
                  item={item}
                />
              )
            }
          >
            {strings.messages}
          </MyButton>
          <MyButton
            width={this.state.widthButton}
            leftIcon={'video'}
            leftIconType="Feather"
            onPress={() => {
              if (this.props.userData.heart < MIN_HEART_TO_MAKE_A_PHONE_CALL) {
                Alert.alert(
                  strings.alert,
                  strings.out_of_heart,
                  [
                    {
                      text: strings.yes,
                      onPress: () => this.props.navigation.navigate(ROUTE_KEY.STORE)
                    }
                  ],
                  { cancelable: true }
                );
                this.props.navigation.goBack();
              } else {
                PopupNotification.showComponent(
                  <PopupNotificationCallComponent item={item} navigation={this.props.navigation} />
                );
              }
            }}
          >
            {strings.call}
          </MyButton>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userData: state.user.userData
});

const mapActionCreators = {
  blockFunction
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(PartnersDetailComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
    // marginTop: getStatusBarHeight()
  },
  avatarImage: {
    width,
    height: width
  },
  infoContainer: {
    paddingHorizontal: 31 * SCALE_RATIO_WIDTH_BASIS
    // paddingVertical: 18 * SCALE_RATIO_WIDTH_BASIS
  },
  flagInfoImage: {
    width: 37.38 * SCALE_RATIO_WIDTH_BASIS,
    height: 24.92 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 4 * SCALE_RATIO_WIDTH_BASIS,
    marginLeft: 25.92 * SCALE_RATIO_WIDTH_BASIS
  },
  textInfo: {
    fontSize: FS(22),
    fontWeight: 'normal',
    color: '#282828'
  },
  moreDetailContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10 * SCALE_RATIO_HEIGHT_BASIS
  },
  moreDetailText: {
    fontSize: FS(18),
    fontWeight: 'normal',
    color: '#282828',
    marginLeft: 11 * SCALE_RATIO_WIDTH_BASIS
  },
  buttonContainer: {
    flex: 1,
    borderWidth: 1 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    borderColor: '#000',
    padding: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    flexDirection: 'row',
    margin: 10 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
