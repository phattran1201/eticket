import { BASE_URL, UPDATE_LIST_SEARCH_PARTNERS, USER_API, ADD_LIST_SEARCH_PARTNERS } from '../../../constants/Constants';
import request from '../../../utils/request';

export function searchListPartners(key, page, onDone = () => { }) {
    return (dispatch, store) => {
        const limit = 20;
        request.get(
            `${BASE_URL}${USER_API}?fields=["$all"]&filter={"$or":[{"nickname":{"$iLike":"%${key}%"}},{"email":{"$iLike":"%${key}%"}}]}&page=${page}&limit=${limit}`
        )
            .finish((err, res) => {
                if (res &&
                    res.body &&
                    res.body.results &&
                    res.body.results.objects &&
                    res.body.results.objects.rows) {
                    dispatch({
                        type: UPDATE_LIST_SEARCH_PARTNERS,
                        payload: res.body.results.objects.rows
                    });
                    onDone();
                    console.log("Hoang log list Search Partner asd", res.body.results.objects.rows);
                } else {

                }
            });
    };
}
export function searchMoreListPartners(key, page, onDone = () => { }) {
    return (dispatch, store) => {
        const limit = 20;
        request.get(
            `${BASE_URL}${USER_API}?fields=["$all"]&filter={"$or":[{"nickname":{"$iLike":"%${key}%"}},{"email":{"$iLike":"%${key}%"}}]}&page=${page}&limit=${limit}`
        )
            .finish((err, res) => {
                if (res &&
                    res.body &&
                    res.body.results &&
                    res.body.results.objects &&
                    res.body.results.objects.rows) {
                    if (res.body.results.objects.rows.length === 0) {
                        onDone(false);
                    } else {
                        onDone(true);
                    }
                    dispatch({
                        type: ADD_LIST_SEARCH_PARTNERS,
                        payload: res.body.results.objects.rows
                    });
                    console.log("Hoang log list Search More Partner aaaaa", res.body.results.objects.rows);
                } else {
                    onDone(true);
                }
            });
    };
}


