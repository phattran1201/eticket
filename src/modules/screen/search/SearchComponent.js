import React from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Dimensions,
  Platform,
  FlatList,
  Text,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icons from 'react-native-vector-icons/Feather';
import { connect } from 'react-redux';
import {
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  ROUTE_KEY,
  DEVICE_WIDTH
} from '../../../constants/Constants';
import style from '../../../constants/style';
import strings from '../../../constants/Strings';
import MyComponent from '../../view/MyComponent';
import MyTouchableOpacity from '../../view/MyTouchableOpacity';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import MyImage from '../../view/MyImage';
import getGenderImage from '../../../utils/getGenderImage';
import { searchListPartners, searchMoreListPartners } from './SearchActions';
import MySpinner from '../../view/MySpinner';
const _ = require('lodash');

const width = Dimensions.get('window').width;
const headerHeight = 48 * SCALE_RATIO_HEIGHT_BASIS + getStatusBarHeight(true);
class SearchComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      userResults: [],
      listSearch: [],
      refreshing: false,
      isLoadMore: false
    };
    this.page = 1;
    this.pageSearch = 1;
    this.stillHaveData = true;
    this.onSearchBarChangeText = this.onSearchBarChangeText.bind(this);
    this.runDebounceSearchListPartners = _.debounce(
      this.searchListPartnersByText,
      500
    );
  }

  renderFooter = () => {
    if (!this.state.isLoadMore) {
      return null;
    }

    return (
      <View style={{ paddingVertical: 10 * SCALE_RATIO_HEIGHT_BASIS }}>
        <ActivityIndicator animating size='small' />
      </View>
    );
  };
  onSearchBarChangeText(text) {
    this.setState({ searchText: text });
    this.runDebounceSearchListPartners(text);
  }
  searchListPartnersByText(searchText, onDoneFunc = () => {}) {
    this.props.searchListPartners(searchText, 1, () => {
      this.page = 1;
      this.forceUpdate();
      onDoneFunc();
    });
  }
  renderListPartners = ({ item, index }) => (
    <TouchableOpacity
      style={{
        height: 60 * SCALE_RATIO_HEIGHT_BASIS,
        borderBottomWidth: 1 * SCALE_RATIO_HEIGHT_BASIS,
        borderBottomColor: '#AE92D350',
        borderRadius: 2 * SCALE_RATIO_HEIGHT_BASIS,
        // marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
        paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
        marginVertical: 2 * SCALE_RATIO_HEIGHT_BASIS,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff'
      }}
      onPress={() =>
        this.props.navigation.navigate(ROUTE_KEY.PARTNER_DETAIL_COMPONENT, {
          item: item
        })
      }
    >
      <MyImage
        size={50 * SCALE_RATIO_WIDTH_BASIS}
        source={{ uri: item.avatar }}
      />
      <View style={{ flexDirection: 'row', width: (DEVICE_WIDTH * 3) / 4 }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 2 }}>
            <Text
              numberOfLines={1}
              style={[
                style.textNormal,
                {
                  marginLeft: 8 * SCALE_RATIO_WIDTH_BASIS,
                  maxWidth: 150 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
            >
              {item.nickname ? item.nickname : item.email}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 8 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              {getGenderImage(item.sex)}
              <Text
                style={[
                  style.textNormal,
                  { marginLeft: 8 * SCALE_RATIO_WIDTH_BASIS }
                ]}
              >
                {item.age}
              </Text>
            </View>
          </View>
          <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          >
            {this.state.searchText ? (
              <View />
            ) : (
              <Text
                style={[
                  style.textNormal,
                  { color: '#9d9b9d', fontSize: 12 * SCALE_RATIO_WIDTH_BASIS }
                ]}
              >
                {
                  item.receiver_history.filter(
                    e => e.caller_id === this.props.userData.id
                  )[0].duration
                }
                {strings.second}
              </Text>
            )}
          </View>
        </View>
      </View>
      {/* <Text style={[style.textNormal, styles.textInfo, { marginHorizontal: 8 * SCALE_RATIO_WIDTH_BASIS }]}>{item.email}</Text> */}
    </TouchableOpacity>
  );
  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            height: headerHeight,
            width,
            paddingTop: Platform.OS === 'ios' ? getStatusBarHeight(true) : 0,
            alignItems: 'center',
            flexDirection: 'row',
            backgroundColor: '#fff',
            justifyContent: 'space-between'
          }}
        >
          <MyTouchableOpacity
            onPress={() => {
              this.props.navigation.goBack();
            }}
            style={{ paddingHorizontal: 16 * SCALE_RATIO_WIDTH_BASIS }}
          >
            <Icons name='arrow-left' size={FS(20)} color='#C7AE6D' />
          </MyTouchableOpacity>
          <TextInput
            style={{ width: DEVICE_WIDTH - 100 * SCALE_RATIO_WIDTH_BASIS }}
            autoFocus
            ref='searchBar'
            clearButtonMode='always'
            placeholder={strings.search_title}
            placeholderTextColor='#00000050'
            underlineColorAndroid='transparent'
            selectionColor='#C7AE6D'
            autoCapitalize='none'
            autoCorrect={false}
            onChangeText={this.onSearchBarChangeText}
            onSubmitEditing={() => {
              MySpinner.show();
              this.searchListPartnersByText(this.state.searchText, () => {
                MySpinner.hide();
              });
            }}
          />
          <MyTouchableOpacity
            onPress={() => {
              this.searchListPartnersByText(this.state.searchText);
            }}
            style={{ paddingHorizontal: 16 * SCALE_RATIO_WIDTH_BASIS }}
          >
            <Icons name='search' size={FS(20)} color='#C7AE6D' />
          </MyTouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          <FlatList
              data={this.state.listSearch}
              renderItem={this.renderListPartners}
              keyExtractor={item => item.id}
            //   onRefresh={() => {
            //     this.page = 1;
            //     this.stillHaveData = true;
            //     this.setState({ refreshing: true }, () => {
            //       this.searchListPartnersByText(this.state.searchText, () => {
            //         this.setState({
            //           refreshing: false
            //         });
            //       });
            //     });
            //   }}
            //   refreshing={this.state.refreshing}
            //   onEndReached={() => {
            //     if (!this.stillHaveData || this.state.isLoadMore) return;
            //     this.setState({ isLoadMore: true });
            //     this.page = this.page + 1;
            //     this.props.searchMoreListPartners(
            //       this.state.searchText,
            //       this.page,
            //       stillHaveData => {
            //         this.setState({
            //           isLoadMore: false
            //           // listSearch: this.props.listSearchPartners
            //         });
            //         this.stillHaveData = stillHaveData;
            //       }
            //     );
            //   }}
            //   onEndReachedThreshold={0.01}
            //   ListFooterComponent={this.renderFooter}
            />     
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  currentSettings: state.user.currentSettings,
  userData: state.user.userData,
  token: state.user.token,
  listPartners: state.partners.listAllPartners,
  listBlocked: state.blocking.listBlocked,
  listSearchPartners: state.partners.listSearchPartners,
  listCallHistory: state.callHistory.listCallHistory
});

const mapActionCreators = {
  searchListPartners,
  searchMoreListPartners
};
export default connect(
  mapStateToProps,
  mapActionCreators
)(SearchComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  textLabelContainer: {
    padding: 10 * SCALE_RATIO_HEIGHT_BASIS
  },
  textLabel: {
    fontSize: 18 * SCALE_RATIO_HEIGHT_BASIS,
    fontFamily: 'Helvetica Neue',
    color: '#282828',
    borderBottomWidth: 0.5 * SCALE_RATIO_HEIGHT_BASIS,
    borderColor: '#C7AE6D'
  },
  itemContainer: {
    flexDirection: 'row',
    padding: 10 * SCALE_RATIO_HEIGHT_BASIS,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  itemLeftContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  }
});
