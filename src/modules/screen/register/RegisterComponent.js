import React from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Text,
  View,
  StatusBar,
  Platform
} from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import {
  SCALE_RATIO_WIDTH_BASIS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  DEVICE_WIDTH,
  FS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style, { APP_COLOR, APP_COLOR_TEXT, FONT } from '../../../constants/style';
import MyTextInputFloat from '../../../style/MyTextInputFloat';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import MyButton from '../../../style/MyButton';

import { loadConversation } from '../message/ListMessageAction';
import { updateUserInfo } from '../editprofile/EditProfieActions';
import { alert } from '../../../utils/alert';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const loginBackground = require('../../../assets/imgs/login_background.png');

class RegisterComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      email: '',
      password: '',
      confirm: '',
      isLoading: false
    };
  }

  onPressRegister(email, pass, confirm) {
    const at = email.indexOf('@');
    const dot = email.lastIndexOf('.');
    const space = email.indexOf(' ');
    if (at !== -1 && at !== 0 && dot !== -1 && dot > at + 1 && dot < email.length - 1 && space === -1) {
      if (pass !== confirm) {
        setTimeout(() => {
          alert('Xác thực', 'Nhập lại mật khẩu không đúng');
          this.setState({ confirm: '' });
        }, 100);
      } else {
        setTimeout(() => {
          alert('Đăng ký thành công', `Bạn đã đăng ký với email\n${email}!\nChào mừng bạn đến với Tocotoco`);
          this.setState({
            userName: '',
            email: '',
            password: '',
            confirm: ''
          }, () => {
            this.props.navigation.navigate(ROUTE_KEY.MAIN);
          });
        }, 100);
      }
    } else {
      alert('Đăng ký thất bại', `Email ${email} !\nNhập không đúng vui lòng nhập lại`);
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground source={loginBackground} style={{ flex: 1 }}>
          <Image
            style={{
              position: 'absolute',
              top: 0,
              right: 0,
              width: DEVICE_WIDTH / 1.5,
              height: DEVICE_WIDTH / 1.5
            }}
            resizeMode="contain"
            source={require('../../../assets/imgs/login_backgroundStar.png')}
          />
          <BaseHeader
            noShadow
            translucent
            styleContent={{
              backgroundColor: 'transparent'
            }}
            // styleLeftContent
            // leftIconStyle={{ color: APP_COLOR_TEXT }}
            leftIcon="arrow-left"
            leftIconType="Feather"
            onLeftPress={() => this.props.navigation.goBack()}
          />
          <KeyboardAwareScrollView behavior="padding">
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Text
                style={[
                  style.textHeader,
                  {
                    fontFamily: FONT.SemiBold,
                    fontSize: FS(35),
                    marginBottom: 20 * SCALE_RATIO_WIDTH_BASIS
                  }
                ]}
              >
                ĐĂNG KÝ
              </Text>
              <View
                style={{
                  width: (85 / 100) * DEVICE_WIDTH
                }}
              >
                <MyTextInputFloat
                  label={'Họ và tên'}
                  styleContent={{ marginBottom: 30 * SCALE_RATIO_WIDTH_BASIS }}
                  labelStyle={{ paddingHorizontal: 0, color: APP_COLOR_TEXT }}
                  inputStyle={{ paddingHorizontal: 0 }}
                  onChangeText={userName => this.setState({ userName })}
                  value={this.state.userName}
                  returnKeyType="next"
                  onSubmitEditing={() => this.txtEmail.focus()}
                />
                <MyTextInputFloat
                  label={'Email'}
                  styleContent={{ marginBottom: 30 * SCALE_RATIO_WIDTH_BASIS }}
                  labelStyle={{ paddingHorizontal: 0, color: APP_COLOR_TEXT }}
                  inputStyle={{ paddingHorizontal: 0 }}
                  onChangeText={email => this.setState({ email })}
                  value={this.state.email}
                  keyboardType="email-address"
                  returnKeyType="next"
                  ref={instance => (this.txtEmail = instance)}
                  onSubmitEditing={() => this.txtPassword.focus()}
                />

                <MyTextInputFloat
                  label={'Mật khẩu'}
                  styleContent={{ marginBottom: 30 * SCALE_RATIO_WIDTH_BASIS }}
                  labelStyle={{ paddingHorizontal: 0, color: APP_COLOR_TEXT }}
                  inputStyle={{ paddingHorizontal: 0 }}
                  onChangeText={password => this.setState({ password })}
                  value={this.state.password}
                  secureTextEntry
                  returnKeyType="next"
                  ref={instance => (this.txtPassword = instance)}
                  onSubmitEditing={() => this.txtConfirm.focus()}
                  disabled={this.state.isLoading || this.state.email === '' || this.state.password === ''}
                />
                <MyTextInputFloat
                  label={'Nhập lại mật khẩu'}
                  styleContent={{ marginBottom: 0 }}
                  labelStyle={{ paddingHorizontal: 0, color: APP_COLOR_TEXT }}
                  inputStyle={{ paddingHorizontal: 0 }}
                  onChangeText={confirm => this.setState({ confirm })}
                  value={this.state.confirm}
                  secureTextEntry
                  returnKeyType="go"
                  ref={instance => (this.txtConfirm = instance)}
                  onSubmitEditing={() => {
                    this.onPressRegister(this.state.email, this.state.password, this.state.confirm);
                  }}
                  disabled={this.state.isLoading || this.state.email === '' || this.state.password === ''}
                />
                <View
                  style={{
                    width: (85 / 100) * DEVICE_WIDTH
                  }}
                />
              </View>
              <MyButton
                outline={
                  this.state.userName === '' ||
                  this.state.email === '' ||
                  this.state.password === '' ||
                  this.state.confirm === ''
                }
                width={(80 / 100) * DEVICE_WIDTH}
                style={[
                  style.button,
                  style.shadow,
                  { marginVertical: 30 * SCALE_RATIO_WIDTH_BASIS, alignSelf: 'center' }
                ]}
                onPress={() => this.onPressRegister(this.state.email, this.state.password, this.state.confirm)}
                isDisabled={this.state.isLoading || this.state.email === '' || this.state.password === ''}
                isLoading={this.state.isLoading}
              >
                Đăng ký
              </MyButton>
              <Text
                style={[
                  style.text,
                  {
                    fontSize: FS(15),
                    width: (85 / 100) * DEVICE_WIDTH,
                    // marginVertical: 20 * SCALE_RATIO_WIDTH_BASIS,
                    textAlign: 'center',
                    alignSelf: 'center',
                    color: APP_COLOR_TEXT
                  }
                ]}
              >
                Bạn đã có tài khoản?{' '}
                <Text style={[style.textCaption, { fontSize: FS(15), fontFamily: FONT.Bold, color: APP_COLOR_TEXT }]}>
                  Đăng nhập
                </Text>
              </Text>
            </View>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const mapActionCreators = {};

const mapStateToProps = state => ({
  userData: state.user.userData
});
export default connect(
  mapStateToProps,
  mapActionCreators
)(RegisterComponent);
