import moment from 'moment';
import 'moment/locale/vi';
import React from 'react';
import {
  ActivityIndicator,
  Alert,
  BackHandler,
  DeviceEventEmitter,
  Dimensions,
  findNodeHandle,
  FlatList,
  KeyboardAvoidingView,
  PermissionsAndroid,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  Vibration,
  View
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import TextInputReset from 'react-native-text-input-reset';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  FS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import { loadMore20Messages, sendMessage, uploadAndGetImageURL } from '../../../utils/chatManager';
import { formatDate } from '../../../utils/dateUtils';
import getGenderImage from '../../../utils/getGenderImage';
import globalUtils from '../../../utils/globalUtils';
import { cameraRoll, launchCamera, launchImageLibrary } from '../../../utils/imagePicker';
import { onRequestPermission } from '../../../utils/permissionUtils';
import BaseHeader from '../../view/BaseHeader';
import DetailViewableImage from '../../view/DetailViewableImage';
import EmojiComponent from '../../view/EmojiComponent';
import MyComponent from '../../view/MyComponent';
import MyImage from '../../view/MyImage';
import MySpinner from '../../view/MySpinner';
import PopupNotification from '../../view/PopupNotification';
import PopupNotificationChildComponent from '../../view/PopupNotificationChildComponent';
import PopupNotificationGiftComponent from '../../view/PopupNotificationGiftComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import { blockFunction } from '../blockfriends/BlockFriendActions';
import {
  addNewInterestConversation,
  addNewMessageToConversation,
  deleteConversation,
  disInterestConversation,
  setMessageRead
} from './ListMessageAction';
const defaultProductImage = require('../../../assets/imgs/default_product_image.jpg');
const defaultAvatar = require('../../../assets/imgs/default_avatar.jpg');
const { width, height } = Dimensions.get('window');

class DetailMessageComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      listMessages: [],
      content: '',
      isLoading: false,
      isShowMoreButton: false,
      inputHeight: 0,
      showIndicator: true
    };

    this.messageIsImg = false;
    this.messageIsEmoji = false;
    this.outOfData = false;
    this.imgSource = '';
    this.loading = false;

    this.uid = '';
    this.lastLoadedMessageObject = null;
    this.currentGroupChatId = '';
    this.currentGroupChatData = {};
    this.isFirstLoad = true;
    this.avatar = '';
    this.show_seen_when_chat = true;
    this.goBack = this.goBack.bind(this);
    this.videoCall = this.videoCall.bind(this);
    this.launchImageLibrary = this.launchImageLibrary.bind(this);
    this.launchCamera = this.launchCamera.bind(this);
    this.emojiFalse = this.emojiFalse.bind(this);
    this.emoji = this.emoji.bind(this);
    this.stickerUri = this.stickerUri.bind(this);
    this.requestPermissionsList = [
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
    ];
    this.haveDeniedPermission = false;
    this.haveNeverAskAgain = false;
    this.onMessageUpdateFunc = this.onMessageUpdateFunc.bind(this);
  }
  _handleSizeChange = event => {
    this.setState({
      inputHeight:
        event.nativeEvent.contentSize.height > 150 * SCALE_RATIO_HEIGHT_BASIS
          ? 150 * SCALE_RATIO_HEIGHT_BASIS
          : event.nativeEvent.contentSize.height
    });
  };
  componentWillUnmount() {
    globalUtils.isChatScreenForeground = false;
    globalUtils.chattingConversationId = '';
    DeviceEventEmitter.removeListener('onMessagesUpdate', this.onMessageUpdateFunc);
    clearTimeout(this.timeout);
    clearInterval(this.updateTimeInterval);
    clearTimeout(this.willMountTimeout);
    clearTimeout(this.didMountTimeout);
  }

  onMessageUpdateFunc = data => {
    if (data.conversation_id === this.currentGroupChatId) {
      this.onMessagesUpdate(data);
    }
  };

  addEventListener() {
    BackHandler.addEventListener('hardwareBackPress', () => this.handleBackPress());
  }

  removeEventListener() {
    BackHandler.removeEventListener('hardwareBackPress', () => this.handleBackPress());
  }

  handleBackPress() {
    const { onGoBack } = this.props.navigation.state.params;
    if (typeof onGoBack === 'function') {
      onGoBack();
    }
    this.props.navigation.goBack();
    return true;
  }

  componentDidMount() {
    this.props.navigation.addListener('didFocus', () => this.addEventListener());
    this.props.navigation.addListener('willBlur', () => this.removeEventListener());

    if (strings.getLanguage() === 'vi') {
      moment.locale('vi');
    } else {
      moment.locale('en');
    }

    DeviceEventEmitter.addListener('onMessagesUpdate', this.onMessageUpdateFunc);

    //To update the relative time, there will be an offset that's is maxium 59 seconds, accept it
    this.updateTimeInterval = setInterval(() => {
      this.forceUpdate();
    }, 60000);

    console.log('hinodi this.props.navigation.state', this.props.navigation.state);

    const { params } = this.props.navigation.state;
    const conversation = params && params.conversation ? params.conversation : null;

    console.log('hinodi params', params);
    console.log('hinodi conversation', conversation);

    this.currentGroupChatId = conversation.id;
    this.currentGroupChatData = conversation;

    this.checkUserSetting(conversation);

    this.handleLoadMore(() => this.setState({ showIndicator: false }));

    globalUtils.isChatScreenForeground = true;
    globalUtils.chattingConversationId = this.currentGroupChatId;
  }

  checkUserSetting(conversation) {
    if (!conversation) return;
    if (!conversation.users_in_conversation) return;

    if (conversation.users_in_conversation.length === 2) {
      this.show_seen_when_chat =
        conversation.users_in_conversation[1].user.id === this.props.userData.id
          ? conversation.users_in_conversation[0].user.user_setting.show_seen_when_chat
          : conversation.users_in_conversation[1].user.user_setting.show_seen_when_chat;
    }
  }

  onSubmitHandler() {
    this.props.userData.token = this.props.token;
    this.onPressSendChatMessage(this.props.userData);
  }

  onMessagesUpdate(lastMessage) {
    if (lastMessage) {
      this.lastLoadedMessageObject = lastMessage;
      const currentListMessages = [];
      let foundDuplicateMessage = false;
      this.state.listMessages.forEach(item => {
        if (item.created_at === lastMessage.created_at) {
          if (lastMessage.type === 'IMAGE') {
            lastMessage.content = item.content;
          }
          currentListMessages.push(lastMessage);
          foundDuplicateMessage = true;
        } else {
          currentListMessages.push(item);
        }
      });

      if (!foundDuplicateMessage) {
        if (
          lastMessage.sender_id !== this.props.userData.id &&
          lastMessage.read_user_ids.find(e => e !== this.props.userData.id)
        ) {
          this.props.setMessageRead(lastMessage, () => {
            DeviceEventEmitter.emit('addNewMessageToConversation', {});
          });
        }

        currentListMessages.unshift(lastMessage);
        Vibration.vibrate(50);
      }
      this.setState({ listMessages: currentListMessages });
      this.props.addNewMessageToConversation(this.currentGroupChatId, lastMessage);
    }
  }

  onMessagesUpdateFailed(err) {
    console.log('onMessagesUpdateFailed', err);
  }

  addATempMessageToListMessages(type, messageContent, currentTime) {
    const currentTimeString = currentTime.format('YYYY-MM-DDTHH:mm:ss.SSS') + 'Z'; //eslint-disable-line
    const tempMessage = {
      id: currentTimeString,
      type,
      content: messageContent,
      created_at: currentTimeString,
      sender_id: this.props.userData.id,
      status: false //This field is to differentiate if this message has been sent to server successfully or not
    };
    this.setState({ listMessages: [tempMessage, ...this.state.listMessages] });
  }

  onPressSendChatMessage = user => {
    const currentTime = moment();
    if (!this.messageIsImg) {
      if (this.state.content !== '') {
        this.addATempMessageToListMessages('TEXT', this.state.content, currentTime);

        sendMessage(this.currentGroupChatId, user, 'TEXT', this.state.content, currentTime).then(
          message => {
            this.props.addNewMessageToConversation(this.currentGroupChatId, message);
            DeviceEventEmitter.emit('addNewMessageToConversation', {});
          }
        );
        this.setState({ content: '' });
        this.textInput.setNativeProps({ text: '' });
        this.textInput.clear();
        if (Platform.OS === 'android') {
          TextInputReset.resetKeyboardInput(findNodeHandle(this.textInput));
        }
      }
    } else if (!this.messageIsEmoji) {
      MySpinner.show();
      uploadAndGetImageURL(this.props.token, [{ uri: this.imgSource }])
        .then(dataImages => {
          dataImages.forEach(data => {
            this.addATempMessageToListMessages('IMAGE', data, currentTime);

            sendMessage(this.currentGroupChatId, user, 'IMAGE', data, currentTime).then(message => {
              this.props.addNewMessageToConversation(this.currentGroupChatId, message);
            });
          });
          DeviceEventEmitter.emit('addNewMessageToConversation', {});

          this.messageIsImg = false;
          this.imgSource = '';
          MySpinner.hide();
        })
        .catch(() => {});
    } else {
      this.addATempMessageToListMessages('STICKER', this.imgSource, currentTime);
      sendMessage(this.currentGroupChatId, user, 'STICKER', this.imgSource, currentTime).then(
        message => {
          this.props.addNewMessageToConversation(this.currentGroupChatId, message);
          DeviceEventEmitter.emit('addNewMessageToConversation', {});
        }
      );

      this.messageIsImg = false;
      this.messageIsEmoji = false;
      this.imgSource = '';
    }
  };

  handleLoadMore = (onDone = () => {}) => {
    if (this.outOfData || this.loading || (!this.lastLoadedMessageObject && !this.isFirstLoad)) {
      return;
    }
    this.isFirstLoad = false;
    this.loading = true;
    loadMore20Messages(
      this.props.token,
      this.currentGroupChatId,
      this.lastLoadedMessageObject,
      lastLoadedMessageObject => {
        this.lastLoadedMessageObject = lastLoadedMessageObject;
        this.loading = false;
      }
    )
      .then(data => {
        if (data.length < 10) {
          this.outOfData = true;
        }
        if (this.state.listMessages.length === 0 && data.length > 0) {
          if (
            data[0].sender_id !== this.props.userData.id &&
            data[0].read_user_ids.find(e => e !== this.props.userData.id)
          ) {
            this.props.setMessageRead(data[0], () => {
              DeviceEventEmitter.emit('addNewMessageToConversation', {});
            });
          }
        }
        this.setState({ listMessages: [...this.state.listMessages, ...data] });
        onDone();
      })
      .catch(() => {});
  };

  // renderTitle() {
  //   const { params } = this.props.navigation.state;
  //   const conversation =
  //     params && params.conversation ? params.conversation : null;

  //   if (!conversation) return <View />;
  //   if (!conversation.users_in_conversation) return <View />;

  //   let conversationTitle = conversation.title;
  //   let visibility = 'OFFLINE';
  //   if (conversation.users_in_conversation.length === 2) {
  //     this.user =
  //       conversation.users_in_conversation[1].user.id === this.props.userData.id
  //         ? conversation.users_in_conversation[0].user
  //         : conversation.users_in_conversation[1].user;
  //     this.avatar = this.user.avatar;
  //     conversationTitle = this.user.nickname;
  //   }

  //   this.props.listConnectedUserIds.forEach(uid => {
  //     if (this.user.id === uid) {
  //       visibility = this.user.visibility;
  //     }
  //   });

  //   return (
  //     <STouchableOpacity
  //       onPress={() => {
  //         this.props.navigation.navigate(ROUTE_KEY.OTHER_PROFILE_COMPONENT, {
  //           user: this.user
  //         });
  //       }}
  //       style={styles.container}
  //     >
  //       <View style={styles.avatarContainer}>
  //         <MyImage
  //           source={{ uri: this.avatar }}
  //           style={styles.avatar}
  //           defaultSource={defaultAvatar}
  //         />
  //       </View>
  //       <View style={styles.textContainer}>
  //         <Text numberOfLines={1} style={styles.textName}>
  //           {conversationTitle}
  //         </Text>
  //         <Text numberOfLines={1} style={styles.textStatus}>
  //           {!visibility || visibility === 'OFFLINE'
  //             ? strings.status_offline
  //             : visibility === 'ONLINE'
  //             ? strings.status_online
  //             : strings.status_idle}
  //         </Text>
  //       </View>
  //     </STouchableOpacity>
  //   );
  // }

  // content: "test thu message 4 ne"
  // content_en: "test thu message 4 ne"
  // conversation_id: "b8653bb0-b029-11e8-af51-fb985042989c"
  // created_at: "2018-09-05T11:57:46.503Z"
  // deleted_at: null
  // id: "3b0792e0-b0c8-11e8-b513-a3bfc8e24a89"
  // image: null
  // read_user_ids: []
  // sender_id: "3bf20860-67d6-11e8-ae48-33a9ba0791f7"
  // status: true
  // type: "TEXT"
  // updated_at: "2018-09-05T11:57:46.503Z"
  // url: null
  renderItem = ({ item, index }) => {
    let needShowTimeText = false;
    let clusteredTime;
    if (!this.showedTimeMessage) {
      needShowTimeText = true;
      this.showedTimeMessage = item;
    } else if (this.showedTimeMessage.sender_id !== item.sender_id) {
      needShowTimeText = true;
      this.showedTimeMessage = item;
    } else if (item.created_at - this.showedTimeMessage.created_at <= 15 * 60 * 1000) {
      //15 minutes
      needShowTimeText = true;
      this.showedTimeMessage = item;
    }

    if (this.lastItem) {
      clusteredTime = this.lastItem.created_at;
    }
    this.lastItem = item;

    const owner = item.sender_id === this.props.userData.id;
    const readUsers = [];
    if (index === 0 && item.read_user_ids && owner) {
      item.read_user_ids.forEach(userId => {
        if (userId !== this.props.userData.id) {
          readUsers.push(userId);
        }
      });
    }
    return (
      <View
        style={[
          styles.messageContainer,
          { marginBottom: index === 0 ? 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) : 0 }
        ]}
      >
        {owner ? (
          <View style={{ flexDirection: 'row', alignSelf: 'flex-end' }}>
            <Text style={owner ? styles.ownMessageTime : styles.messageTime}>
              {formatDate(item.created_at)}
            </Text>
            <LinearGradient
              start={{ x: 0.0, y: 0.25 }}
              end={{ x: 0.5, y: 1.0 }}
              colors={item.type !== 'TEXT' ? ['#00000000', '#00000000'] : ['#AA87D9', '#C7AE6D']}
              style={
                item.type === 'TEXT'
                  ? styles.ownTextMessageContainer
                  : styles.ownImageMessageContainer
              }
            >
              {item.type === 'TEXT' ? (
                <Text style={owner ? styles.ownTextMessage : styles.textMessage}>
                  {item.content}
                </Text>
              ) : item.type === 'STICKER' ? (
                <MyImage
                  style={{
                    width: 20 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                    height: 20 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                  }}
                  hideIndicator
                  source={{ uri: item.content }}
                  resizeMode="cover"
                />
              ) : (
                <DetailViewableImage
                  style={{
                    height: width / 2,
                    width: width / 2,
                    borderRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
                    borderBottomRightRadius: 0
                  }}
                  source={{ uri: item.content }}
                  defaultSource={defaultProductImage}
                  resizeMode="cover"
                />
              )}
            </LinearGradient>
          </View>
        ) : (
          <View style={{ flexDirection: 'row', alignSelf: 'flex-start' }}>
            <View
              style={
                item.type === 'TEXT' ? styles.textMessageContainer : styles.imageMessageContainer
              }
            >
              {item.type === 'TEXT' ? (
                <Text style={owner ? styles.ownTextMessage : styles.textMessage}>
                  {item.content}
                </Text>
              ) : item.type === 'STICKER' ? (
                <MyImage
                  style={{
                    width: 20 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                    height: 20 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                  }}
                  hideIndicator
                  source={{ uri: item.content }}
                  resizeMode="cover"
                />
              ) : (
                <DetailViewableImage
                  style={{
                    height: width / 2,
                    width: width / 2,
                    borderRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
                    borderBottomLeftRadius: 0
                  }}
                  source={{ uri: item.content }}
                  defaultSource={defaultProductImage}
                  resizeMode="cover"
                />
              )}
            </View>
            <Text style={owner ? styles.ownMessageTime : styles.messageTime}>
              {formatDate(item.created_at)}
            </Text>
          </View>
        )}
        <View
          style={{
            marginTop: 2.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: owner ? 'flex-end' : 'flex-start'
          }}
        >
          {owner &&
            index === 0 &&
            ((readUsers.length > 0 && this.show_seen_when_chat && (
              <DetailViewableImage
                source={{ uri: this.avatar }}
                style={styles.small_avatar}
                defaultSource={defaultAvatar}
              />
            )) || (
              <FontAwesome
                name={item.status ? 'check-circle' : 'circle'}
                size={4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                color="#C7AE6D"
              />
            ))}
        </View>
      </View>
    );
  };

  openCameraRoll() {
    cameraRoll()
      .then(photos =>
        this.setState({
          photos,
          isLoading: false
        })
      )
      .catch(err => {
        this.setState({ isLoading: false });
        // //console.log(err);
      });
  }

  renderFooter = () => {
    if (!this.outOfData && this.state.listMessages.length > 0) {
      return (
        <View style={{ paddingVertical: 20 }}>
          <ActivityIndicator animating size="small" />
        </View>
      );
    }
    return null;
  };

  videoCall() {
    this.props.navigation.navigate(ROUTE_KEY.WEB_RTC, {
      user: this.user
    });
  }

  render() {
    const { params } = this.props.navigation.state;
    const conversation = params && params.conversation ? params.conversation : null;
    const idInterested = params && params.idInterested ? params.idInterested : null;
    const id = params && params.id ? params.id : null;

    if (!conversation) return null;

    const { conversationName, conversationAge, conversationGender, otherUser } = conversation;

    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        {this.state.showIndicator ? (
          <View
            style={{
              position: 'absolute',
              left: DEVICE_WIDTH / 2,
              top: DEVICE_HEIGHT / 2,
              zIndex: 999,
              elevation: 999
            }}
          >
            <ActivityIndicator animating size="small" />
          </View>
        ) : (
          <View />
        )}
        <BaseHeader
          // styleContent={{ shadowColor: 'transparent', elevation: 0 }}
          children={
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
                alignSelf: 'flex-start'
              }}
            >
              <Text
                style={[
                  style.textCaption,
                  {
                    color: '#282828',
                    fontSize: FS(15),
                    fontWeight: '600',
                    marginRight: 5 * SCALE_RATIO_WIDTH_BASIS
                  }
                ]}
              >
                {conversationName}
              </Text>
              {getGenderImage(conversationGender)}
              <Text
                style={[
                  style.textCaption,
                  {
                    color: '#282828',
                    fontSize: FS(15),
                    fontWeight: '600'
                  }
                ]}
              >
                {conversationAge}
              </Text>
            </View>
          }
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
          rightIconMenu="more-vert"
          rightIconTypeMenu="MaterialIcons"
          dataMenu={[
            {
              value: idInterested ? strings.dismiss_interest : strings.interest,
              onSelect: onDone => {
                if (!idInterested) {
                  MySpinner.show();
                  this.props.addNewInterestConversation(id, this.props.userData.id, () => {
                    MySpinner.hide();
                    this.forceUpdate();
                    this.props.navigation.goBack();
                  });
                } else {
                  MySpinner.show();
                  this.props.disInterestConversation(idInterested, () => {
                    MySpinner.hide();
                    this.forceUpdate();
                    this.props.navigation.goBack();
                  });
                }
              }
            },
            {
              value: strings.block,
              onSelect: onDone => {
                setTimeout(() => {
                  PopupNotification.showComponent(
                    <PopupNotificationChildComponent
                      title={strings.block}
                      content={strings.block_member_comfirm}
                      subContent={strings.block_member_noti}
                      button1Text={strings.cancel}
                      onButton1Press={() => PopupNotification.hide()}
                      button2Text={strings.block}
                      onButton2Press={() => {
                        PopupNotification.hide();
                        setTimeout(() => {
                          MySpinner.show();
                          this.props.blockFunction(otherUser, MySpinner.hide, () => {
                            this.props.navigation.goBack();
                          });
                        }, 300);
                      }}
                    />
                  );
                }, 500);
              }
            },
            {
              value: strings.delete,
              onSelect: onDone => {
                setTimeout(
                  () =>
                    PopupNotification.showComponent(
                      <PopupNotificationChildComponent
                        title={strings.delete_conversation_title}
                        content={strings.delete_conversation_content}
                        button1Text={strings.cancel}
                        onButton1Press={PopupNotification.hide}
                        button2Text={strings.confirm}
                        onButton2Press={() => {
                          PopupNotification.hide();
                          setTimeout(() => {
                            MySpinner.show();
                            this.props.deleteConversation(conversation.id, ok => {
                              MySpinner.hide();
                              setTimeout(() => {
                                if (!ok) {
                                  Alert.alert(
                                    strings.alert,
                                    strings.delete_conversation_fail,
                                    [{ text: strings.yes, onPress: () => {} }],
                                    { cancelable: false }
                                  );
                                } else {
                                  this.props.navigation.goBack();
                                }
                              }, 300);
                            });
                          }, 300);
                        }}
                      />
                    ),
                  300
                );
              }
            }
          ]}
        />
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          enabled
        >
          <FlatList
            data={this.state.listMessages.length ? this.state.listMessages : []}
            keyExtractor={item => item.id.toString()}
            renderItem={this.renderItem}
            style={{ zIndex: 1, flex: 1, backgroundColor: '#fff' }}
            inverted
            ListFooterComponent={this.renderFooter}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.01}
          />
          <View style={styles.inputContainer}>
            <STouchableOpacity
              onPress={() => {
                this.setState({ isShowMoreButton: !this.state.isShowMoreButton });
              }}
            >
              <View style={{ padding: 8 * SCALE_RATIO_WIDTH_BASIS }}>
                {this.state.isShowMoreButton ? (
                  <MaterialCommunityIcons
                    name="chevron-left-circle"
                    size={25 * SCALE_RATIO_WIDTH_BASIS}
                    color="#C7AE6D"
                  />
                ) : (
                  <MaterialIcons
                    name="add-circle-outline"
                    size={23 * SCALE_RATIO_WIDTH_BASIS}
                    color="#C7AE6D"
                  />
                )}
              </View>
            </STouchableOpacity>
            {this.state.isShowMoreButton ? (
              <View style={{ flexDirection: 'row' }}>
                {/* <STouchableOpacity onPress={this.emoji}>
                  <View style={{ padding: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 }}>
                    <SimpleLineIcons name="emotsmile" size={6.7 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224} color="#707070" />
                  </View>
                </STouchableOpacity> */}
                <STouchableOpacity
                  onPress={this.launchCamera}
                  style={{ padding: 8 * SCALE_RATIO_WIDTH_BASIS }}
                >
                  <Feather name="camera" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#707070" />
                </STouchableOpacity>
                <STouchableOpacity
                  onPress={this.launchImageLibrary}
                  style={{
                    padding: 8 * SCALE_RATIO_WIDTH_BASIS
                  }}
                >
                  <Feather name="image" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#707070" />
                </STouchableOpacity>
                <STouchableOpacity
                  onPress={() => {
                    PopupNotification.showComponent(
                      <PopupNotificationGiftComponent user={otherUser} />
                    );
                  }}
                  style={{
                    padding: 8 * SCALE_RATIO_WIDTH_BASIS
                  }}
                >
                  <Feather name="gift" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#707070" />
                </STouchableOpacity>
              </View>
            ) : (
              <View />
            )}
            <View
              style={{
                flex: 1,
                borderWidth: Platform.OS === 'ios' ? SCALE_RATIO_WIDTH_BASIS : 0,
                borderColor: Platform.OS === 'ios' ? '#C7AE6D' : '#fff',
                backgroundColor: '#AE92D310',
                borderRadius: 20 * SCALE_RATIO_WIDTH_BASIS,
                paddingHorizontal: 8 * SCALE_RATIO_WIDTH_BASIS,
                height: Math.max(35, this.state.inputHeight),
                maxHeight: 150 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              <TextInput
                ref={ref => {
                  this.textInput = ref;
                }}
                placeholder={strings.input_message}
                placeholderTextColor="#bcc5d3"
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                value={this.state.content}
                multiline={!this.state.isShowMoreButton}
                // onSubmitEditing={() => this.onSubmitHandler()}
                onChangeText={content => this.setState({ content })}
                onContentSizeChange={event => this._handleSizeChange(event)}
                autoCorrect={false}
                style={{
                  fontFamily: 'helveticaneue',
                  fontSize: FS(15),
                  flex: 1,
                  color: '#BCC5D3',
                  height: Math.max(35, this.state.inputHeight),
                  maxHeight: 150 * SCALE_RATIO_HEIGHT_BASIS,
                  marginBottom: -2 * SCALE_RATIO_HEIGHT_BASIS
                }}
                onFocus={this.emojiFalse}
              />
            </View>

            <STouchableOpacity
              style={{ padding: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}
              onPress={() =>
                setTimeout(() => {
                  this.onSubmitHandler();
                })
              }
            >
              <View
                style={{
                  width: 24 * SCALE_RATIO_WIDTH_BASIS,
                  height: 24 * SCALE_RATIO_WIDTH_BASIS,
                  borderRadius: (24 * SCALE_RATIO_WIDTH_BASIS) / 2,
                  backgroundColor: '#C7AE6D',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <FontAwesome name="send" size={13 * SCALE_RATIO_WIDTH_BASIS} color="#ffffff" />
              </View>
            </STouchableOpacity>
          </View>
          <EmojiComponent
            ref={ref => {
              this.emojiComponent = ref;
            }}
            textInputForcus={() => this.textInput.focus()}
            onEmojiPressed={this.stickerUri}
          />
        </KeyboardAvoidingView>
      </View>
    );
  }

  stickerUri(stickerUri) {
    this.messageIsImg = true;
    this.messageIsEmoji = true;
    this.imgSource = stickerUri;
    this.setState(
      {
        isLoading: false
      },
      () => {
        this.props.userData.token = this.props.token;
        this.onPressSendChatMessage(this.props.userData);
      }
    );
  }

  launchImageLibrary() {
    if (this.state.isLoading) {
      return;
    }
    onRequestPermission(
      [
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
      ],
      this,
      () => {
        setTimeout(() => {
          this.setState({ isLoading: true }, () => {
            launchImageLibrary()
              .then(response => {
                this.messageIsImg = true;
                this.imgSource = response.uri;

                this.setState(
                  {
                    isLoading: false
                  },
                  () => {
                    this.props.userData.token = this.props.token;
                    this.onPressSendChatMessage(this.props.userData);
                  }
                );
              })
              .catch(() => {
                this.setState({ isLoading: false });
              });
          });
        });
      },
      true,
      false
    );
  }

  launchCamera() {
    if (this.state.isLoading) {
      return;
    }
    onRequestPermission(
      [
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
      ],
      this,
      () => {
        this.setState({ isLoading: true });
        launchCamera()
          .then(response => {
            this.messageIsImg = true;
            this.imgSource = response.uri;
            this.setState(
              {
                isLoading: false
              },
              () => {
                this.props.userData.token = this.props.token;
                this.onPressSendChatMessage(this.props.userData);
              }
            );
          })
          .catch(() => this.setState({ isLoading: false }));
      },
      true,
      false
    );
  }

  emojiFalse() {
    this.emojiComponent.getWrappedInstance().toggle(false);
  }

  emoji() {
    this.textInput.blur();
    this.emojiComponent.getWrappedInstance().toggle();
  }

  goBack() {
    this.props.navigation.goBack();
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  avatarContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  avatar: {
    width: 13.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 13.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: (13.3 / 2) * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  small_avatar: {
    width: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  textContainer: {
    flex: 4,
    paddingRight: 1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  textName: {
    fontFamily: 'helveticaneue',
    fontSize: 5.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: '600',
    fontStyle: 'normal',
    color: '#ffffff'
  },
  textStatus: {
    fontFamily: 'helveticaneue',
    fontSize: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: 'normal',
    fontStyle: 'normal',
    color: '#ffffff'
  },
  messageContainer: {
    margin: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginBottom: 0
  },
  ownTextMessageContainer: {
    paddingHorizontal: 12 * SCALE_RATIO_HEIGHT_BASIS,
    paddingVertical: 9 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
    borderBottomRightRadius: 0,
    maxWidth: 230 * SCALE_RATIO_WIDTH_BASIS,
    alignSelf: 'flex-end'
  },
  ownImageMessageContainer: {
    maxWidth: 90 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    alignSelf: 'flex-end'
  },
  ownTextMessage: {
    fontFamily: 'helveticaneue',
    fontSize: FS(13),
    color: '#ffffff'
  },
  textMessage: {
    fontFamily: 'helveticaneue',
    fontSize: FS(13),
    color: '#4c5264'
  },
  imageMessageContainer: {
    // borderStyle: 'solid',
    // borderWidth: 0.6,
    // borderColor: '#e2e8ed',
    // padding: 6 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    // borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    maxWidth: 90 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    alignSelf: 'flex-start'
  },
  textMessageContainer: {
    backgroundColor: '#c9ddf0',
    paddingHorizontal: 12 * SCALE_RATIO_HEIGHT_BASIS,
    paddingVertical: 9 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
    maxWidth: 230 * SCALE_RATIO_WIDTH_BASIS,
    alignSelf: 'flex-start',
    borderBottomLeftRadius: 0
  },
  messageTime: {
    fontFamily: 'helveticaneue',
    fontSize: FS(8),
    textAlign: 'left',
    marginLeft: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    color: '#bcc5d3',
    alignSelf: 'flex-end'
  },
  ownMessageTime: {
    alignSelf: 'flex-end',
    fontFamily: 'helveticaneue',
    fontSize: FS(8),
    textAlign: 'right',
    marginRight: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    color: '#282828'
  },
  inputContainer: {
    backgroundColor: 'white',
    padding: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    flexDirection: 'row',
    alignItems: 'center'
  }
});

function mapStateToProps(state) {
  return {
    userData: state.user.userData || {},
    token: state.user.token,
    listConnectedUserIds: state.conversation.list_connected_user_id
  };
}

const mapActionCreators = {
  addNewMessageToConversation,
  setMessageRead,
  deleteConversation,
  blockFunction,
  addNewInterestConversation,
  disInterestConversation
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(DetailMessageComponent);
