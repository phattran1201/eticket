import { Alert } from 'react-native';
import request from '../../../utils/request';
import {
  CLEAR_CONNECTION_STATE,
  UPDATE_CONVERSATION_CONNECTION_STATE,
  ADD_A_CONVERSATION,
  MESSAGE_API,
  REMOVE_A_CONVERSATION,
  UPDATE_LAST_MESSAGE_OF_A_CONVERSATION,
  UPDATE_LIST_CONVERSATION,
  BASE_URL,
  CONVERSATION_API,
  REFRESH_LIST_CONVERSATION,
  USER_API,
  UPDATE_LIST_SUGGESTED_FRIEND_TO_CHAT,
  ADD_A_INTERESTED_CONVERSATION,
  DISINTERESTED_CONVERSATION
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import { reloadUnreadNotificationNumber } from '../../../utils/chatManager';
import MySpinner from '../../view/MySpinner';

const LIMIT = 10;
export function loadMoreConversation(
  page,
  onDoneFunc = () => { }
) {
  return (dispatch, store) => {
    request
      .post(
        `${BASE_URL}${CONVERSATION_API}/get_conversation?limit=${LIMIT}&page=${page}&fields=["$all"]`
      )
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send()
      .finish((err, res) => {
        if (!err) {
          if (
            res.body.results.object &&
            res.body.results.object.rows.length < LIMIT
          ) {
            dispatch({
              type: UPDATE_LIST_CONVERSATION,
              payload: res.body.results.object
                ? res.body.results.object.rows
                : []
            });
            onDoneFunc(true, true);
          } else {
            dispatch({
              type: UPDATE_LIST_CONVERSATION,
              payload: res.body.results.object
                ? res.body.results.object.rows
                : []
            });
            onDoneFunc(true, false);
          }
        } else {
          onDoneFunc(false, null);
        }
      });
  };
}

export function refreshConversation(self, onSuccess) {
  return (dispatch, store) => {
    request
      .post(
        `${BASE_URL}${CONVERSATION_API}/get_conversation?limit=${LIMIT}&page=1&fields=["$all"]`
      )
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send()
      .finish((err, res) => {
        if (!err) {
          //Save posts post to reducer
          dispatch({
            type: REFRESH_LIST_CONVERSATION,
            payload: res.body.results.object ? res.body.results.object.rows : []
          });
          onSuccess();
          self.setState({
            refreshing: false
          });
        } else {
          self.setState({
            refreshing: false
          });
        }
      });
  };
}

export function deleteConversation(conversationId, onDoneFunc = () => { }) {
  return (dispatch, store) => {
    const state = store();
    request.delete(`${BASE_URL}${CONVERSATION_API}/${conversationId}/delete`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${state.user.token}`)
      .send()
      .finish((err, res) => {
        onDoneFunc(!err);
        if (!err && res && res.body.code === 200) {
          const { list_conversation, list_connected_user_id } = state.conversation;
          reloadUnreadNotificationNumber(
            list_conversation.filter(e => e.id !== conversationId),
            state.user.userData.id,
            list_connected_user_id,
          );
          dispatch({
            type: REMOVE_A_CONVERSATION,
            payload: { conversationId }
          });
        }
      });
  };
}

export function loadConversation(onDoneFunc = () => { }) {
  return (dispatch, store) => {
    request
      .post(
        `${BASE_URL}${CONVERSATION_API}/get_conversation?limit=${LIMIT}&page=1&fields=["$all"]`
      )
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send()
      .finish((err, res) => {
        if (!err) {
          dispatch({
            type: REFRESH_LIST_CONVERSATION,
            payload: res.body.results.object ? res.body.results.object.rows : []
          });
          setTimeout(() => {
            onDoneFunc();
          }, 100);
        }
      });
  };
}

export function addNewMessageToConversation(conversationId, message) {
  return dispatch => {
    dispatch({
      type: UPDATE_LAST_MESSAGE_OF_A_CONVERSATION,
      payload: { conversationId, message }
    });
  };
}

export function addNewConversation(conversation) {
  return dispatch => {
    dispatch({
      type: ADD_A_CONVERSATION,
      payload: { conversation }
    });
  };
}

export function addNewInterestConversation(conversationId, user_id, onDoneFunc) {
  console.log('Hoang vo day khong');
  return (dispatch, store) => {
    request
      .post(`${BASE_URL}conversation_interest`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send({ "conversation_id": conversationId, "user_id": user_id })
      .finish((err, res) => {
        if (!err) {
          console.log('Hoang log res interested Conversation', res.body.results.object);
          dispatch({
            type: ADD_A_INTERESTED_CONVERSATION,
            payload: res.body.results.object ? res.body.results.object : {}
          })
          onDoneFunc();
        }
        else {
          MySpinner.hide();
          console.log('Hoang log err interested Conversation', err);
        }

      })
  }
}
export function disInterestConversation(id, onDoneFunc) {
  console.log('Hoang vo day khong', `${BASE_URL}conversation_interest/${id}`);
  return (dispatch, store) => {
    request
      .delete(`${BASE_URL}conversation_interest/${id}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .finish((err, res) => {
        if (!err) {
          console.log('Hoang log res DISinterested Conversation', res.body.results.object);
          dispatch({
            type: DISINTERESTED_CONVERSATION,
            payload: res.body.results.object ? res.body.results.object : {}
          })
          onDoneFunc();
        }
        else {
          MySpinner.hide();
          console.log('Hoang log err DISinterested Conversation', err);
        }

      })
  }
}

function setMessageReadReduxOnly(dispatch, store, message) {
  const myUserId = store().user.userData.id;
  message.read_user_ids.filter(element => element !== myUserId);
  message.read_user_ids.push(myUserId);
  dispatch({
    type: UPDATE_LAST_MESSAGE_OF_A_CONVERSATION,
    payload: { conversationId: message.conversation_id, message }
  });
}

export function setMessageRead(message, onDoneFunc) {
  return (dispatch, store) => {
    setMessageReadReduxOnly(dispatch, store, message);
    request
      .post(`${BASE_URL}${MESSAGE_API}/${message.id}/readmessage`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send()
      .finish((err) => {
        if (!err) {
          setMessageReadReduxOnly(dispatch, store, message);
          onDoneFunc();
        }
      });
  };
}

export function getListFriendToChat(key, onDoneFunc) {
  return (dispatch, store) => {
    request
      .post(
        `${BASE_URL}${USER_API}/search_friend?fields=["$all"]&page=1&limit=${LIMIT}`
      )
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send({
        keyword: key
      })
      .finish((err, res) => {
        if (!err && res.body.results.objects.rows) {
          dispatch({
            type: UPDATE_LIST_SUGGESTED_FRIEND_TO_CHAT,
            payload: res.body.results.objects.rows
          });
          setTimeout(() => {
            onDoneFunc(res.body.results.objects.rows);
          }, 100);
        }
      });
  };
}

export function setConnectionState(userIds, isConnected) {
  return dispatch => {
    dispatch({
      type: UPDATE_CONVERSATION_CONNECTION_STATE,
      payload: { userIds, isConnected }
    });
  };
}

export function clearConnectionState() {
  return dispatch => {
    dispatch({
      type: CLEAR_CONNECTION_STATE
    });
  };
}

export function sendMessageWithoutConversation(user, message, onFinish = () => { }, onSuccess = () => { }) {
  return (dispatch, store) => {
    request.post(`${BASE_URL}${CONVERSATION_API}/${user.id}/send_message`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send({
        message: {
          content: message,
          content_en: '',
          type: 'TEXT'
        }
      })
      .finish((err, res) => {
        onFinish();
        if (err) {
          Alert.alert(
            strings.alert,
            strings.send_message_fail,
            [
              { text: strings.yes, onPress: () => { } },
            ],
            { cancelable: true }
          );
        } else {
          onSuccess();
        }
      });
  };
}

export function loadConversationById(id, token) {
  return new Promise((resolve, reject) => {
    request
      .get(`${BASE_URL}${CONVERSATION_API}?fields=["$all"]&filter={"id":"${id}"}`)
      .set('Authorization', `Bearer ${token}`)
      .finish((err, res) => {
        if (!err &&
          res &&
          res.body &&
          res.body.results &&
          res.body.results.objects &&
          res.body.results.objects.rows &&
          res.body.results.objects.rows.length > 0
        ) {
          resolve(res.body.results.objects.rows);
        } else {
          reject();
        }
      });
  });
}
