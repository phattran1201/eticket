import React from 'react';
import { Image, Platform, StyleSheet, Text, View } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import Toast from 'react-native-easy-toast';
import LinearGradient from 'react-native-linear-gradient';
import Icons from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  FS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import getGenderImage from '../../../utils/getGenderImage';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import MyImage from '../../view/MyImage';
import MySpinner from '../../view/MySpinner';
import PopupNotification from '../../view/PopupNotification';
import PopupNotificationAttendanceCheckComponent from '../../view/PopupNotificationAttendanceCheckComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import { attendanceCheck, getListAttendanceCheck } from '../editprofile/EditProfieActions';

class MoreComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      appVersion: ''
    };
    this.onAttendanceCheck = this.onAttendanceCheck.bind(this);
  }

  onAttendanceCheck() {
    MySpinner.show();
    this.props.getListAttendanceCheck(
      () => {
        MySpinner.hide();
        PopupNotification.showComponent(
          <PopupNotificationAttendanceCheckComponent
            onButtonPress={() => {
              MySpinner.show();
              console.log('Hoang log user id when attendance Check', this.props.userData.id);
              this.props.attendanceCheck(this.props.userData.id, () => {
                MySpinner.hide();
                PopupNotification.hide();
              });
              this.refs.toast.show(strings.attendance_check_complete, 1000);
            }}
          />
        );
      },
      () => {
        MySpinner.hide();
        this.refs.toast.show(strings.attendance_already_checked, 1000);
      }
    );
  }

  componentDidMount() {
    const version = DeviceInfo.getVersion();
    this.setState({ appVersion: version });
    console.log('Hoang log userData in Redux More', this.props.userData);
  }

  render() {
    const { userData } = this.props;
    if (!userData) return null;

    const { avatar, nickname, age, introduction, cash, heart, sex } = userData;

    return (
      <View style={styles.container}>
        <Toast ref="toast" />
        <BaseHeader
          children={<Text style={style.titleHeader}>{strings.more.toUpperCase()}</Text>}
          rightIcon="envelope"
          navigation={this.props.navigation}
          // noShadow
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          <MyImage source={{ uri: avatar }} size={58 * SCALE_RATIO_WIDTH_BASIS} />
          <View
            style={{
              paddingLeft: 5 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
              paddingVertical: 20 * SCALE_RATIO_WIDTH_BASIS,
              width: '51%'
            }}
          >
            <View style={styles.userInfoContainer}>
              <Text
                numberOfLines={1}
                style={[
                  style.text,
                  {
                    color: '#282828',
                    fontWeight: '500'
                  }
                ]}
              >
                {nickname || this.props.userData.email}
              </Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              {getGenderImage(sex)}
              <Text style={[style.text, { color: '#282828' }]}>{age}</Text>
            </View>
            <Text
              numberOfLines={1}
              style={[
                style.textCaption,
                {
                  maxWidth: 160 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
            >
              {introduction}
            </Text>
            <View style={{ flexDirection: 'row' }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: 15 * SCALE_RATIO_WIDTH_BASIS
                }}
              >
                <Text style={style.textCaption}>{heart}</Text>
                <MaterialCommunityIcons
                  name="heart"
                  size={15 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540}
                  color="#C7AE6D"
                />
              </View>
              <View
                style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}
              >
                <Text style={style.textCaption}>{cash}</Text>
                <MaterialCommunityIcons
                  name="coin"
                  size={15 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540}
                  color="#C7AE6D"
                />
              </View>
            </View>
          </View>
          <STouchableOpacity
            style={{
              width: 42 * SCALE_RATIO_HEIGHT_BASIS,
              height: 42 * SCALE_RATIO_HEIGHT_BASIS,
              borderRadius: 21 * SCALE_RATIO_HEIGHT_BASIS,
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: '#C7AE6D',
              borderWidth: SCALE_RATIO_WIDTH_BASIS
            }}
            onPress={() => this.props.navigation.navigate(ROUTE_KEY.EXCHANGE_TAB)}
          >
            <Image
              resizeMode={'contain'}
              source={require('../../../assets/imgs/icons/money_bag.png')}
              style={{
                height: 18 * SCALE_RATIO_HEIGHT_BASIS,
                width: 18 * SCALE_RATIO_HEIGHT_BASIS
              }}
            />
            <Text
              style={{
                color: '#282828',
                fontSize: 7 * SCALE_RATIO_HEIGHT_BASIS,
                fontFamily: 'Helvetica Neue'
              }}
            >
              {strings.exchange}
            </Text>
          </STouchableOpacity>
          <STouchableOpacity
            style={{
              marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
              width: 42 * SCALE_RATIO_HEIGHT_BASIS,
              height: 42 * SCALE_RATIO_HEIGHT_BASIS,
              borderRadius: 21 * SCALE_RATIO_HEIGHT_BASIS,
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: '#C7AE6D',
              borderWidth: SCALE_RATIO_WIDTH_BASIS,
              backgroundColor: '#C7AE6D'
            }}
            onPress={() => this.props.navigation.navigate(ROUTE_KEY.EDIT_PROFILE)}
          >
            <Text
              style={{
                color: '#fff',
                fontSize: 7 * SCALE_RATIO_HEIGHT_BASIS,
                fontFamily: 'Helvetica Neue'
              }}
            >
              {strings.edit_profile}
            </Text>
          </STouchableOpacity>
        </View>
        {Platform.OS === 'ios' ? (
          <View
            style={{
              height: 3,
              width: DEVICE_WIDTH,
              backgroundColor: '#fff',
              shadowColor: '#C7AE6D',
              shadowOffset: {
                width: 0,
                height: 3
              },
              shadowOpacity: 0.5,
              shadowRadius: 2
            }}
          />
        ) : (
          <LinearGradient
            colors={['#AE92D350', 'transparent']}
            style={{
              zIndex: 9,
              left: 0,
              right: 0,
              height: 4,
              overflow: 'visible'
            }}
          />
        )}

        <View
          style={{
            flex: 1,
            marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
            marginTop: 10 * SCALE_RATIO_HEIGHT_BASIS
          }}
        >
          <STouchableOpacity
            style={styles.moreItemContainer}
            onPress={() => this.props.navigation.navigate(ROUTE_KEY.SETTINGS_COMPONENT)}
          >
            <Icons name="settings" size={25 * SCALE_RATIO_HEIGHT_BASIS} color="#C7AE6D" />
            <Text
              style={[
                styles.text,
                { marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540 }
              ]}
            >
              {strings.settings}
            </Text>
          </STouchableOpacity>
          <STouchableOpacity style={styles.moreItemContainer} onPress={this.onAttendanceCheck}>
            <Icons name="check-circle" size={25 * SCALE_RATIO_HEIGHT_BASIS} color="#C7AE6D" />
            <Text
              style={[
                styles.text,
                { marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540 }
              ]}
            >
              {strings.attendance_check}
            </Text>
          </STouchableOpacity>
          <STouchableOpacity
            style={styles.moreItemContainer}
            onPress={() => this.props.navigation.navigate(ROUTE_KEY.BLOCKFRIENDS)}
          >
            <Icons name="slash" size={25 * SCALE_RATIO_HEIGHT_BASIS} color="#C7AE6D" />
            <Text
              style={[
                styles.text,
                { marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540 }
              ]}
            >
              {strings.blocked_friends}
            </Text>
          </STouchableOpacity>
          <STouchableOpacity
            style={styles.moreItemContainer}
            onPress={() => this.props.navigation.navigate(ROUTE_KEY.TERMSCONDITIONS)}
          >
            <Icons name="file-text" size={25 * SCALE_RATIO_HEIGHT_BASIS} color="#C7AE6D" />
            <Text
              style={[
                styles.text,
                { marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540 }
              ]}
            >
              {strings.terms_conditions}
            </Text>
          </STouchableOpacity>
          <STouchableOpacity
            style={styles.moreItemContainer}
            onPress={() => this.props.navigation.navigate(ROUTE_KEY.STORE)}
          >
            <Icons name="shopping-cart" size={25 * SCALE_RATIO_HEIGHT_BASIS} color="#C7AE6D" />
            <Text
              style={[
                styles.text,
                { marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540 }
              ]}
            >
              {strings.store}
            </Text>
          </STouchableOpacity>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 25 * SCALE_RATIO_HEIGHT_BASIS
            }}
          >
            <Text style={[styles.text, { fontSize: 15 * SCALE_RATIO_WIDTH_BASIS }]}>
              {strings.version} {this.state.appVersion}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userData: state.user.userData,
  token: state.user.token
});

const mapActionCreators = {
  attendanceCheck,
  getListAttendanceCheck
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(MoreComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  titleStyle: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue-Bold',
    fontSize: 18 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  },
  image: {
    width: 58 * SCALE_RATIO_HEIGHT_BASIS,
    height: 58 * SCALE_RATIO_HEIGHT_BASIS,
    borderRadius: 29 * SCALE_RATIO_HEIGHT_BASIS,
    borderWidth: 2 * SCALE_RATIO_HEIGHT_BASIS,
    borderColor: '#F95896'
  },
  userInfoContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  moreItemContainer: {
    flexDirection: 'row',
    borderBottomWidth: SCALE_RATIO_WIDTH_BASIS,
    borderColor: '#AE92D320',
    padding: 15 * SCALE_RATIO_HEIGHT_BASIS,
    alignItems: 'center'
  },
  text: {
    color: '#282828',
    fontFamily: 'Helvetica Neue',
    fontSize: FS(15)
  }
});
