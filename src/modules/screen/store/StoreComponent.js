import AnimatedLottieView from 'lottie-react-native';
import React from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { DEVICE_WIDTH, FS, IS_IOS, ROUTE_KEY, SCALE_RATIO_WIDTH_BASIS } from '../../../constants/Constants';
import { DATA_TEST } from '../../../constants/dataTest';
import style, { APP_COLOR_TEXT } from '../../../constants/style';
import HeaderWithAvatar from '../../view/HeaderWithAvatar';
import MyComponent from '../../view/MyComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import { getDetailArticle } from '../news/NewsAction';
import { loadListNewsArticleData, loadListPromotionArticleData } from './StoreActions';

class StoreComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = { activeSlide: 0 };
    this.store = DATA_TEST;
    this.data = DATA_TEST;
    this.isFirstTimeLoadNews = true;
    this.isFirstTimeLoadPromotion = true;
  }

  // componentDidMount() {
  //   if (this.props.regionData && this.props.regionData.home_screen && this.props.regionData.home_screen.section_article) {
  //     this.props.loadListNewsArticleData(this.props.regionData.home_screen.section_article.blog_handle, () => {

  //     });
  //   }
  //   if (this.props.regionData && this.props.regionData.home_screen && this.props.regionData.home_screen.section_article_promotion) {
  //     this.props.loadListPromotionArticleData(this.props.regionData.home_screen.section_article_promotion.blog_handle, () => {

  //     });
  //   }
  // }

  renderBannerItem = ({ item, index }) => (
    <TouchableOpacity onPress={() => {}}>
      <Image
        style={{
          width: '100%',
          height: 158 * SCALE_RATIO_WIDTH_BASIS,
          borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS
        }}
        source={item.slide_img ? { uri: item.slide_img } : require('../../../assets/imgs/tocotoco_star_img.png')}
      />
    </TouchableOpacity>
  );

  renderPromotionItem = ({ item, index }) => (
    <TouchableOpacity
      onPress={() => {
        if (this.props.detailArticle.title === item.title) {
          //if user click the same item with previous clicked item, then we will get content of html and send to webview
          this.props.navigation.navigate(ROUTE_KEY.WEB_VIEW, {
            name: item.title,
            html: this.props.detailArticle.content,
            url: item.url
          });
        } else {
          //user click different item with previous clicked item then we will get data and stored in redux and show it on webview
          //to reduces api call number AMAP.
          this.props.getDetailArticle(item.url, (isSuccess, response) => {
            this.props.navigation.navigate(ROUTE_KEY.WEB_VIEW, {
              name: item.title,
              html: response.content,
              url: item.url
            });
          });
        }
      }}
    >
      <View style={[item && item.image ? style.shadow : {}, styles.carouselItemContainer, { marginLeft: 15 }]}>
        <View style={{ overflow: 'hidden' }}>
          {item && item.image ? (
            <Image
              style={{
                width: '100%',
                height: 100 * SCALE_RATIO_WIDTH_BASIS
              }}
              source={item.image ? { uri: item.image } : require('../../../assets/imgs/tocotoco_star_img.png')}
            />
          ) : (
            <View
              style={{
                borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                backgroundColor: '#F3F3F3',
                width: '100%',
                justifyContent: 'center'
              }}
            >
              <AnimatedLottieView
                source={require('../../../assets/image_load.json')}
                autoPlay
                loop
                hardwareAccelerationAndroid
                style={{
                  alignSelf: 'center',
                  backgroundColor: '#F3F3F3',
                  width: '100%',
                  height: 120 * SCALE_RATIO_WIDTH_BASIS
                }}
                resizeMode="cover"
              />
            </View>
          )}
        </View>

        <Text
          style={[
            style.text,
            {
              padding: 5 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
          numberOfLines={2}
        >
          {item.title}
        </Text>
      </View>
    </TouchableOpacity>
  );

  renderNewsItem = ({ item, index }) => (
    <TouchableOpacity
      onPress={() => {
        if (this.props.detailArticle.title === item.title) {
          //if user click the same item with previous clicked item, then we will get content of html and send to webview
          this.props.navigation.navigate(ROUTE_KEY.WEB_VIEW, {
            name: item.title,
            html: this.props.detailArticle.content
          });
        } else {
          //user click different item with previous clicked item then we will get data and stored in redux and show it on webview
          //to reduces api call number AMAP.
          this.props.getDetailArticle(item.url, (isSuccess, response) => {
            this.props.navigation.navigate(ROUTE_KEY.WEB_VIEW, {
              name: item.title,
              html: response.content
            });
          });
        }
      }}
    >
      <View style={[style.shadow, styles.carouselItemContainer, { marginLeft: 15 }]}>
        <View style={{ overflow: 'hidden' }}>
          {item && item.image ? (
            <Image
              style={{
                width: '100%',
                height: 150 * SCALE_RATIO_WIDTH_BASIS
              }}
              source={item.image ? { uri: item.image } : require('../../../assets/imgs/tocotoco_star_img.png')}
            />
          ) : (
            <View
              style={{
                borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                backgroundColor: '#F3F3F3',
                width: '100%',
                justifyContent: 'center'
              }}
            >
              <AnimatedLottieView
                source={require('../../../assets/image_load.json')}
                autoPlay
                loop
                hardwareAccelerationAndroid
                style={{
                  alignSelf: 'center',
                  backgroundColor: '#F3F3F3',
                  width: '100%',
                  height: 170 * SCALE_RATIO_WIDTH_BASIS
                }}
                resizeMode="cover"
              />
            </View>
          )}
        </View>

        <Text
          style={[
            style.text,
            {
              marginTop: IS_IOS ? -5 * SCALE_RATIO_WIDTH_BASIS : 0,
              padding: 5 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
          numberOfLines={2}
        >
          {item.title}
        </Text>
      </View>
    </TouchableOpacity>
  );
  render() {
    if (!this.props.regionData) {
      return <View />;
    }
    if (this.isFirstTimeLoadNews) {
      if (
        this.props.regionData &&
        this.props.regionData.home_screen &&
        this.props.regionData.home_screen.section_article
      ) {
        this.isFirstTimeLoadNews = false;
        this.props.loadListNewsArticleData(this.props.regionData.home_screen.section_article.blog_handle, () => {});
      }
    }
    if (this.isFirstTimeLoadPromotion) {
      if (
        this.props.regionData &&
        this.props.regionData.home_screen &&
        this.props.regionData.home_screen.section_article_promotion
      ) {
        this.isFirstTimeLoadPromotion = false;
        this.props.loadListPromotionArticleData(
          this.props.regionData.home_screen.section_article_promotion.blog_handle,
          () => {}
        );
      }
    }
    const { home_screen } = this.props.regionData;
    const section_article = home_screen && home_screen.section_article ? home_screen.section_article : '';
    const section_article_promotion =
      home_screen && home_screen.section_article_promotion ? home_screen.section_article_promotion : '';
    const section_slider = home_screen && home_screen.section_slider ? home_screen.section_slider : [];
    const articlePromotionTitle = section_article_promotion.module_title;
    const articleTitle = section_article.module_title;
    const listSlider = section_slider.list_slider ? section_slider.list_slider.filter(e => e.slide_use) : [];
    const nickname =
      this.props.userData && this.props.userData.nickname !== null ? this.props.userData.nickname : 'Đăng Nhập';
    const point =
      (this.props.userData && this.props.userData.cash) || (this.props.userData && this.props.userData.heart)
        ? this.props.userData.cash || this.props.userData.heart
        : 0;
    const avatar = this.props.userData && this.props.userData.avatar ? this.props.userData.avatar : null;
    return (
      <View style={{ backgroundColor: '#fff', flex: 1 }}>
        <HeaderWithAvatar
          noShadow
          name={nickname}
          point={point}
          avatar={avatar}
          onAvatarPress={() => this.props.navigation.navigate(ROUTE_KEY.PERSONALINFO)}
          onPointPress={() => this.props.navigation.navigate(ROUTE_KEY.REWARDS)}
          rightIcon="bell"
          rightIconType="MaterialCommunityIcons"
          onRightPress={() => {}}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Carousel
            ref={c => {
              this._carousel = c;
            }}
            containerCustomStyle={{ alignSelf: 'center' }}
            hasParallaxImages
            inactiveSlideScale={0.94}
            inactiveSlideOpacity={0.7}
            data={listSlider}
            renderItem={this.renderBannerItem}
            sliderWidth={DEVICE_WIDTH}
            itemWidth={(DEVICE_WIDTH * 95) / 100}
            enableSnap
            loop
            autoplay
            autoplayDelay={6000}
            autoplayInterval={3000}
            activeAnimationType={'spring'}
            activeAnimationOptions={{
              friction: 4,
              tension: 40
            }}
            onSnapToItem={index => this.setState({ activeSlide: index })}
          />
          <Pagination
            dotsLength={listSlider.length}
            activeDotIndex={this.state.activeSlide}
            containerStyle={{ paddingVertical: 0.5 * SCALE_RATIO_WIDTH_BASIS }}
            dotColor="#70707090"
            dotStyle={{
              marginTop: 12,
              width: 6 * SCALE_RATIO_WIDTH_BASIS,
              height: 6 * SCALE_RATIO_WIDTH_BASIS,
              borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS
            }}
            inactiveDotColor="#242424"
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
            carouselRef={this._sliderRef}
            tappableDots={!!this._sliderRef}
          />
          {/* Cửa hàng gần bạn */}
          <View
            style={{
              marginTop: 15 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <View
              style={{
                paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                alignContent: 'center',
                marginBottom: 20 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <Text
                style={[
                  style.text,
                  {
                    fontSize: FS(18),
                    color: APP_COLOR_TEXT,
                    fontFamily: 'helveticaneue-Bold'
                  }
                ]}
              >
                {articlePromotionTitle}
              </Text>
              <STouchableOpacity
                style={{ flexDirection: 'row' }}
                // onPress={() => alert(strings.alert, strings.this_feature_is_in_development)}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_KEY.PROMOTION_COMPONENT, {
                    title: articlePromotionTitle
                    // data: this.props.listPromotionArticles
                  });
                }}
              >
                <Text
                  style={[
                    style.textCaption,
                    {
                      color: '#9c9faa'
                    }
                  ]}
                >
                  Xem tất cả
                </Text>
                <MaterialCommunityIcons
                  name="chevron-right"
                  size={16 * SCALE_RATIO_WIDTH_BASIS}
                  style={{ marginRight: 3 * SCALE_RATIO_WIDTH_BASIS }}
                  color="#9c9faa"
                />
              </STouchableOpacity>
            </View>
            <Carousel
              ref={c => {
                this._carousel = c;
              }}
              hasParallaxImages
              inactiveSlideScale={0.94}
              inactiveSlideOpacity={0.7}
              data={
                // this.props.listPromotionArticles ? this.props.listPromotionArticles.slice(0, SLICE_NUM) : [{}, {}, {}]
                this.props.listPromotionArticles ? this.props.listPromotionArticles : [{}, {}, {}]
              }
              renderItem={this.renderPromotionItem}
              sliderWidth={DEVICE_WIDTH}
              itemWidth={(DEVICE_WIDTH * 60) / 100}
              enableMomentum
              activeSlideAlignment={'start'}
              activeAnimationType={'spring'}
              activeAnimationOptions={{
                friction: 4,
                tension: 40
              }}
            />
          </View>
          <View
            style={{
              marginTop: 15 * SCALE_RATIO_WIDTH_BASIS,
              marginBottom: 30 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <View
              style={{
                paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                alignContent: 'center',
                marginBottom: 20 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <Text
                style={[
                  style.text,
                  {
                    fontSize: FS(18),
                    color: APP_COLOR_TEXT,
                    fontFamily: 'helveticaneue-Bold'
                  }
                ]}
              >
                {articleTitle}
              </Text>
              <STouchableOpacity
                style={{ flexDirection: 'row' }}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_KEY.NEWS, {
                    title: articleTitle
                    // data: this.props.listNewsArticles
                  });
                }}
              >
                <Text
                  style={[
                    style.textCaption,
                    {
                      color: '#9c9faa'
                    }
                  ]}
                >
                  Xem tất cả
                </Text>
                <MaterialCommunityIcons
                  name="chevron-right"
                  size={16 * SCALE_RATIO_WIDTH_BASIS}
                  style={{ marginRight: 3 * SCALE_RATIO_WIDTH_BASIS }}
                  color="#9c9faa"
                />
              </STouchableOpacity>
            </View>
            <Carousel
              hasParallaxImages
              inactiveSlideScale={0.94}
              inactiveSlideOpacity={0.7}
              // data={this.props.listNewsArticles ? this.props.listNewsArticles.slice(0, SLICE_NUM) : [{}, {}, {}]}
              data={this.props.listNewsArticles ? this.props.listNewsArticles : [{}, {}, {}]}
              renderItem={this.renderNewsItem}
              sliderWidth={DEVICE_WIDTH}
              itemWidth={(DEVICE_WIDTH * 80) / 100}
              enableMomentum
              activeSlideAlignment={'start'}
              activeAnimationType={'spring'}
              activeAnimationOptions={{
                friction: 4,
                tension: 40
              }}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  markerImageStyle: {
    width: 40 * SCALE_RATIO_WIDTH_BASIS,
    height: 40 * SCALE_RATIO_WIDTH_BASIS
  },
  searchInputContainer: {
    paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
    position: 'absolute',
    top: 20 * SCALE_RATIO_WIDTH_BASIS,
    width: DEVICE_WIDTH,
    height: 50 * SCALE_RATIO_WIDTH_BASIS
  },
  searchInput: {
    borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS,
    flex: 1,
    height: 50 * SCALE_RATIO_WIDTH_BASIS,
    flexDirection: 'row',
    backgroundColor: '#FFFFFF'
  },
  searchNameContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchNameTextStyle: {
    fontSize: 14 * SCALE_RATIO_WIDTH_BASIS,
    color: '#767676',
    fontWeight: '500'
  },
  searchIconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 40 * SCALE_RATIO_WIDTH_BASIS,
    height: '100%'
  },
  carouselItemContainer: {
    overflow: 'visible',
    borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
    marginBottom: IS_IOS ? 5 : 6,
    flex: 1,
    backgroundColor: 'white'
  },
  carouselItemMoreInfo: {
    textAlign: 'center',
    fontSize: 10 * SCALE_RATIO_WIDTH_BASIS
  }
});
const mapActionCreators = {
  loadListNewsArticleData,
  loadListPromotionArticleData,
  getDetailArticle
};

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData,
  regionData: state.setting.regionData,
  listNewsArticles: state.setting.listNewsArticles,
  newsArticlesPaginate: state.setting.newsArticlesPaginate,
  listPromotionArticles: state.setting.listPromotionArticles,
  promotionArticlesPaginate: state.setting.promotionArticlesPaginate,
  currentLocation: state.user.currentLocation,
  detailArticle: state.setting.detailArticle
});

export default connect(
  mapStateToProps,
  mapActionCreators
)(StoreComponent);
