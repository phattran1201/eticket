import React from 'react';
import { asyncComponent } from 'react-async-component';
import LazyPlaceholder from '../../view/LazyLoadComponent';

export default asyncComponent({
  resolve: () =>
    new Promise(resolve => {
      requestAnimationFrame(() => {
        // setTimeout(() => { //uncomment this to test async
        resolve(require('./StoreComponent'));
        // }, 5000);
      });
    }),
  LoadingComponent: ({ navigation }) => <LazyPlaceholder />
});
