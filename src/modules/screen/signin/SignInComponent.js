import React from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Text,
  View,
  StatusBar,
  Platform
} from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import {
  SCALE_RATIO_WIDTH_BASIS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  DEVICE_WIDTH,
  FS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style, { APP_COLOR, APP_COLOR_TEXT, FONT } from '../../../constants/style';
import MyTextInputFloat from '../../../style/MyTextInputFloat';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import MyButton from '../../../style/MyButton';

import { loadConversation } from '../message/ListMessageAction';
import { updateUserInfo } from '../editprofile/EditProfieActions';
import { loginFail, loginPromise, loginSuccess } from './SignInActions';
import { alert } from '../../../utils/alert';
import MySpinner from '../../view/MySpinner';
import DeviceInfo from 'react-native-device-info';
import STouchableOpacity from '../../view/STouchableOpacity';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
const loginBackground = require('../../../assets/imgs/login_background.png');

class PreLoginComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isLoading: false
    };
  }

  loginFunction() {
    MySpinner.show();
    if (this.state.username === '' || this.state.password === '') {
      alert(strings.alert, strings.please_input_all_information);
      MySpinner.hide();
      return;
    }
    if (this.state.password.length < 6) {
      alert(strings.alert, strings.password_must_atleast_6_charactor);
      MySpinner.hide();
      return;
    }

    if (this.state.isLoading) return;
    this.setState({ isLoading: true });
    loginPromise(this.state.username, this.state.password, () => MySpinner.hide())
      .then(res => {
        this.props.loginSuccess(res, this, null, () => {
          const temp = { ...this.props.userData };
          temp.imei = DeviceInfo.getUniqueID();
          this.props.updateUserInfo(temp, () => {
            console.log('Hoang log uniqueId', temp.imei);
            const { params } = this.props.navigation.state;
            const previousScreenName = params && params.fromScreen ? params.fromScreen : '';
            console.log('poi previousScreenName:', previousScreenName);
            if (previousScreenName === '') {
              this.props.navigation.navigate(ROUTE_KEY.MAIN);
            } else {
              this.props.navigation.replace(previousScreenName);
            }
          });
          MySpinner.hide();
        });
      })
      .catch(err => loginFail(err, this, () => MySpinner.hide()));
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={loginBackground}
          style={{
            flex: 1
          }}
        >
          <Image
            style={{
              position: 'absolute',
              top: 0,
              right: 0,
              width: DEVICE_WIDTH / 1.5,
              height: DEVICE_WIDTH / 1.5
            }}
            resizeMode="contain"
            source={require('../../../assets/imgs/login_backgroundStar.png')}
          />
          <BaseHeader
            noShadow
            translucent
            leftIcon="arrow-left"
            leftIconType="Feather"
            styleContent={{
              backgroundColor: 'transparent'
            }}
            onLeftPress={() => this.props.navigation.goBack()}
          />
          <KeyboardAwareScrollView behavior="padding">
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Text
                style={[
                  style.textHeader,
                  {
                    fontFamily: FONT.SemiBold,
                    fontSize: FS(35),
                    marginBottom: 30 * SCALE_RATIO_WIDTH_BASIS
                  }
                ]}
              >
                ĐĂNG NHẬP
              </Text>
              <View
                style={{
                  width: (85 / 100) * DEVICE_WIDTH
                }}
              >
                <MyTextInputFloat
                  styleContent={{ marginBottom: 30 * SCALE_RATIO_WIDTH_BASIS }}
                  labelStyle={{ paddingHorizontal: 0, color: APP_COLOR_TEXT }}
                  inputStyle={{ paddingHorizontal: 0 }}
                  label={'Email'}
                  iconClass={FontAwesomeIcon}
                  iconName={'heart'}
                  iconColor={'#C7AE6D'}
                  onChangeText={username => this.setState({ username })}
                  value={this.state.username}
                  keyboardType="email-address"
                  returnKeyType="next"
                  onSubmitEditing={() => this.txtPassword.focus()}
                />

                <MyTextInputFloat
                  styleContent={{ marginBottom: 0 }}
                  labelStyle={{ paddingHorizontal: 0, color: APP_COLOR_TEXT }}
                  inputStyle={{ paddingHorizontal: 0 }}
                  label={'Mật khẩu'}
                  iconClass={FontAwesomeIcon}
                  iconName={'heart'}
                  iconColor={'#C7AE6D'}
                  onChangeText={password => this.setState({ password })}
                  value={this.state.password}
                  secureTextEntry
                  returnKeyType="go"
                  ref={instance => (this.txtPassword = instance)}
                  onSubmitEditing={() => this.loginFunction()}
                  disabled={this.state.isLoading || this.state.username === '' || this.state.password === ''}
                />
                <View
                  style={{
                    width: (85 / 100) * DEVICE_WIDTH
                  }}
                />
              </View>
              <Text
                style={[
                  style.text,
                  {
                    fontSize: FS(15),
                    width: (85 / 100) * DEVICE_WIDTH,
                    marginTop: 30 * SCALE_RATIO_WIDTH_BASIS,
                    textAlign: 'right',
                    color: APP_COLOR_TEXT
                  }
                ]}
              >
                Quên mật khẩu?
              </Text>
              <MyButton
                outline={this.state.username === '' || this.state.password === ''}
                width={(80 / 100) * DEVICE_WIDTH}
                style={[
                  style.button,
                  style.shadow,
                  {
                    marginVertical: 30 * SCALE_RATIO_WIDTH_BASIS,
                    alignSelf: 'center'
                  }
                ]}
                onPress={() => this.loginFunction()}
                isDisabled={this.state.isLoading || this.state.username === '' || this.state.password === ''}
                isLoading={this.state.isLoading}
              >
                Đăng nhập
              </MyButton>
              <Text
                style={[
                  style.text,
                  {
                    fontSize: FS(15),
                    width: (85 / 100) * DEVICE_WIDTH,
                    // marginVertical: 20 * SCALE_RATIO_WIDTH_BASIS,
                    textAlign: 'center',
                    alignSelf: 'center',
                    color: APP_COLOR_TEXT
                  }
                ]}
              >
                Bạn chưa có tài khoản?{' '}
                <Text
                  style={[
                    style.textCaption,
                    {
                      fontSize: FS(15),
                      fontFamily: FONT.Bold,
                      color: APP_COLOR_TEXT
                    }
                  ]}
                  onPress={() => this.props.navigation.navigate(ROUTE_KEY.REGISTER)}
                >
                  Đăng ký
                </Text>
              </Text>
            </View>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const mapActionCreators = {
  loginSuccess,
  loadConversation,
  updateUserInfo
};

const mapStateToProps = state => ({
  userData: state.user.userData
});
export default connect(
  mapStateToProps,
  mapActionCreators
)(PreLoginComponent);
