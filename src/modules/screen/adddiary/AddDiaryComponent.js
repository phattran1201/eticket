import React from 'react';
import {
  ActivityIndicator,
  DeviceEventEmitter,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View
} from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import ImageResizer from 'react-native-image-resizer';
import Modal from 'react-native-modal';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  IMAGE_RESIZE_MAXHIEGHT,
  IMAGE_RESIZE_MAXWIDTH,
  IMAGE_RESIZE_QUALITY,
  SCALE_RATIO_WIDTH_BASIS,
  ROUTE_KEY
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import { alert } from '../../../utils/alert';
import { getNumberString } from '../../../utils/numberUtils';
import { uploadImage } from '../../../utils/uploadImage';
import ImageSlideShow from '../../view/ImageSlideShow';
import MyComponent from '../../view/MyComponent';
import MyImage from '../../view/MyImage';
import NewFeedsItems from '../../view/NewFeedsItems';
import PickerComponent from '../../view/PickerComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import { editPost, uploadPost, uploadPostSuccess } from './AddDiaryActions';

const defaultAvatar = require('../../../assets/imgs/default_avatar.jpg');
const defaultProductImage = require('../../../assets/imgs/default_product_image.jpg');
const ic_tag_friend = require('../../../assets/imgs/ic_tag_friend.png');
const ic_tag_image = require('../../../assets/imgs/ic_tag_image.png');
const ic_tag_location = require('../../../assets/imgs/ic_tag_location.png');
const ic_tag_product = require('../../../assets/imgs/ic_tag_product.png');

class AddDiaryComponent extends MyComponent {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.editPost = params && params.data ? params.data : null;
    this.groupId = params && params.groupId ? params.groupId : null;
    this.makeNewPostFromGroup =
      params && params.makeNewPostFromGroup ? params.makeNewPostFromGroup : null;
    let privacy = {
      icon: 'public',
      name: 'Công khai',
      key: 'PUBLIC'
    };

    if (this.editPost && this.editPost.privacy === 'FRIEND') {
      privacy = {
        icon: 'person',
        name: 'Bạn bè',
        key: 'FRIEND'
      };
    }

    if (this.editPost && this.editPost.privacy === 'ONLY_ME') {
      privacy = {
        icon: 'lock',
        name: 'Chỉ mình tôi',
        key: 'ONLY_ME'
      };
    }

    let content = this.editPost && this.editPost.content ? this.editPost.content : '';
    const words = content.split('#');

    const friend = content.split('@');
    // const listFriend = friend.slice(1, friend.length).map(e => JSON.parse(e));
    const listFriend = [];
    friend.slice(1, friend.length).forEach(e => {
      try {
        const obj = JSON.parse(e);
        listFriend.push(obj);
      } catch (err) {}
    });

    let postLat = 0;
    if (words.length > 2) {
      postLat = parseFloat(words[2]);
    }

    let postLng = 0;
    if (words.length > 3) {
      postLng = parseFloat(words[3]);
    }

    if (content.includes('#')) {
      content =
        content.substring(0, content.indexOf('#')) +
        content.substring(content.lastIndexOf('#') + 1, content.length);
    }
    if (content.includes('@')) content = content.substring(0, content.indexOf('@'));

    if (content.length === 0) {
      content = this.editPost && this.editPost.content ? this.editPost.content : '';
    }

    this.state = {
      selectedPrivacy: privacy,
      postContent: content || '',
      isLoading: false,
      products:
        this.editPost && this.editPost.products && this.editPost.products.length > 0
          ? this.editPost.products.map(e => ({ ...e.product, uploaded: true }))
          : [],
      photos:
        this.editPost && this.editPost.list_image && this.editPost.list_image.length > 0
          ? this.editPost.list_image.map(e => ({ uri: e }))
          : [],
      dialogVisible: false,
      tagFriends: listFriend,
      tagAddress: words.length > 1 ? words[1] : '',
      tagAddressLocation: {
        lat: postLat,
        lng: postLng
      }
    };
    this.sharingMode = params && params.post;
    if (this.sharingMode) {
      this.post = params.post;
    }

    this.listPrivacySelection = [
      {
        id: 0,
        name: 'Công khai',
        icon: 'public',
        onPress: () =>
          this.setState({
            selectedPrivacy: {
              name: 'Công khai',
              key: 'PUBLIC',
              icon: 'public'
            }
          })
      },
      {
        id: 0,
        name: 'Bạn bè',
        icon: 'person',
        onPress: () =>
          this.setState({
            selectedPrivacy: {
              name: 'Bạn bè',
              key: 'FRIEND',
              icon: 'person'
            }
          })
      },
      {
        id: 1,
        name: 'Chỉ mình tôi',
        icon: 'lock',
        onPress: () =>
          this.setState({
            selectedPrivacy: {
              name: 'Chỉ mình tôi',
              key: 'ONLY_ME',
              icon: 'lock'
            }
          })
      }
    ];
  }

  onPlaceSearch = (data, details) => {
    this.setState({
      dialogVisible: false,
      tagAddress: data.description,
      tagAddressLocation: details.geometry.location
    });
  };

  setProduct = products => {
    this.setState({ products });
  };

  preUploadPost(photos) {
    uploadImage(photos, this.props.token)
      .then(listImageUploaded => {
        // this.sharingMode
        // const uploadPostData = {
        //   content: this.state.postContent,
        //   user_id: this.props.userData.id,
        //   token: this.props.token,
        //   listImage: listImageUploaded,
        //   privacy: this.state.selectedPrivacy.key,
        //   id: this.editPost && this.editPost.id ? this.editPost.id : null,
        //   group_id: this.groupId ? this.groupId : null,
        // };
        let uploadPostData = {};
        if (this.groupId) {
          uploadPostData = {
            content: this.state.postContent,
            user_id: this.props.userData.id,
            token: this.props.token,
            listImage: listImageUploaded,
            privacy: this.state.selectedPrivacy.key,
            id: this.editPost && this.editPost.id ? this.editPost.id : null,
            group_id: this.groupId
          };
        } else {
          uploadPostData = {
            content: this.state.postContent,
            user_id: this.props.userData.id,
            token: this.props.token,
            listImage: listImageUploaded,
            privacy: this.state.selectedPrivacy.key,
            id: this.editPost && this.editPost.id ? this.editPost.id : null
          };
        }
        if (this.sharingMode) {
          uploadPostData.post_id = this.post.id;
        }
        this.actualUploadPost(uploadPostData);
      })
      .catch(err => {
        // this.setState({ isLoading: false });
        // console.log('uploadImage err ', err);
      });
  }

  actualUploadPost(uploadPostData) {
    let haveError = false;
    if (this.state.tagAddress.length > 0) {
      uploadPostData.content = `${uploadPostData.content}#${this.state.tagAddress}#${
        this.state.tagAddressLocation.lat
      }#${this.state.tagAddressLocation.lng}#`;
    }

    if (this.state.tagFriends && this.state.tagFriends.length > 0) {
      uploadPostData.content = `${uploadPostData.content}`;
      for (let i = 0; i < this.state.tagFriends.length; i++) {
        uploadPostData.content = `${uploadPostData.content}@{"nickname":"${
          this.state.tagFriends[i].nickname
        }", "id":"${this.state.tagFriends[i].id}"}`;
      }
    }

    if (this.editPost) {
      if (this.editPost.group_id) {
        uploadPostData.group_id = this.editPost.group_id;
      }
      editPost(uploadPostData, this.state.products, this.editPost)
        .then(post => {
          post.user = this.props.userData;
          this.props.uploadPostSuccess(
            post,
            isSuccess => {
              if (!isSuccess) {
                haveError = true;
              }
              this.setState({
                postContent: '',
                isLoading: false
              });
              if (this.editPost.group_id && isSuccess) {
                DeviceEventEmitter.emit('editPostInGroup', {
                  post,
                  oldEditPostId: this.editPost.id
                });
              }
              this.props.navigation.goBack();
            },
            this.editPost
          );
        })
        .catch(err => {
          this.setState({ isLoading: false });
          if (haveError) {
            alert(strings.alert, strings.post_contains_vulnerable_word, () => {});
          } else {
            alert(strings.alert, strings.network_require_fail, () => {});
          }
        });
    } else {
      uploadPost(uploadPostData, this.state.products)
        .then(post => {
          post.user = this.props.userData;
          this.props.uploadPostSuccess(post, isSuccess => {
            if (!isSuccess) {
              haveError = true;
            }
            this.setState({
              postContent: '',
              isLoading: false
            });
            if (this.groupId && isSuccess) {
              DeviceEventEmitter.emit('createPostInGroup', { post });
            }
            this.props.navigation.goBack();
          });
        })
        .catch(err => {
          this.setState({ isLoading: false });
          if (haveError) {
            alert(strings.alert, strings.post_contains_vulnerable_word, () => {});
          } else {
            alert(strings.alert, strings.network_require_fail, () => {});
          }
        });
    }
  }

  createAPost(listImage = []) {
    const photos = [];
    let index = -1;
    if (listImage.length > 0) {
      listImage.forEach((e, i, array) => {
        if (e.uri.slice(0, 4) === 'http') {
          photos.push({
            uri: e.uri
          });
          index++;
          if (index >= array.length - 1) this.preUploadPost(photos);
        } else {
          ImageResizer.createResizedImage(
            e.uri,
            IMAGE_RESIZE_MAXWIDTH,
            IMAGE_RESIZE_MAXHIEGHT,
            'JPEG',
            IMAGE_RESIZE_QUALITY
          )
            .then(({ uri }) => {
              photos.push({
                ...e,
                uri
              });
              index++;
              if (index >= array.length - 1) this.preUploadPost(photos);
            })
            .catch(err => {
              // console.log(err);
            });
        }
      });
    } else if (this.sharingMode || this.state.postContent !== '') {
      let uploadPostData = {};
      if (this.groupId) {
        uploadPostData = {
          content: this.state.postContent,
          user_id: this.props.userData.id,
          token: this.props.token,
          listImage: [],
          privacy: this.state.selectedPrivacy.key,
          group_id: this.groupId
        };
      } else {
        uploadPostData = {
          content: this.state.postContent,
          user_id: this.props.userData.id,
          token: this.props.token,
          listImage: [],
          privacy: this.state.selectedPrivacy.key
        };
      }
      if (this.sharingMode) {
        uploadPostData.post = this.post;
      }
      if (this.editPost && this.editPost.id) {
        uploadPostData.id = this.editPost.id;
      }
      this.actualUploadPost(uploadPostData);
    } else {
      this.setState({ isLoading: false });
      alert(strings.alert, strings.whats_on_your_mind, () => {});
    }
  }
  showDialog = () => {
    this.setState({ dialogVisible: !this.state.dialogVisible });
  };

  handleCancel = () => {
    this.setState({ dialogVisible: false });
  };

  handleAgree = () => {
    // this.props.navigation.navigate(ROUTE_KEY.PAYMENT, { data: { price: parseInt(this.state.txtAmountMoney, 10) } });
    this.setState({ dialogVisible: false });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <StatusBar backgroundColor="#ffff" barStyle="dark-content" />
          <View style={{ flexDirection: 'row' }}>
            <STouchableOpacity
              onPress={() => {
                if (this.state.isLoading) return;
                this.props.navigation.goBack();
              }}
              style={{ padding: 6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}
            >
              <Ionicons
                name="md-arrow-round-back"
                size={8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                color="#ffffff"
              />
            </STouchableOpacity>
            <Text style={styles.headerText}>
              {this.editPost ? strings.edit_post : strings.created_post}
            </Text>
          </View>
          <STouchableOpacity
            onPress={() => {
              if (
                !this.sharingMode &&
                this.state.dialogVisible === false &&
                this.state.isLoading === false &&
                this.state.photos.length === 0 &&
                this.state.postContent.length === 0 &&
                this.state.products.length === 0
              ) {
                return;
              }
              if (this.state.isLoading) return;
              this.setState({ isLoading: true });
              this.createAPost(this.state.photos);
            }}
            style={{ padding: 6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}
          >
            {this.state.isLoading ? (
              <ActivityIndicator size="small" color="#fff" />
            ) : (
              <Text
                style={[
                  styles.headerText,
                  {
                    opacity:
                      !this.sharingMode &&
                      (this.state &&
                        this.state.dialogVisible === false &&
                        this.state.isLoading === false &&
                        this.state.photos.length === 0 &&
                        this.state.postContent.length === 0 &&
                        this.state.products.length === 0)
                        ? 0.5
                        : 1
                  }
                ]}
              >
                {this.sharingMode ? strings.share : strings.post}
              </Text>
            )}
          </STouchableOpacity>
        </View>
        <Modal
          visible={this.state.dialogVisible}
          style={{
            position: 'relative',
            backgroundColor: 'rgb(255, 255, 255)',
            margin: 0,
            padding: 0
          }}
        >
          <View
            style={{
              backgroundColor: '#6d77f7',
              height: Platform.OS === 'ios' ? getStatusBarHeight() : 0
            }}
          />
          <GooglePlacesAutocomplete
            onPress={(data, details) => this.onPlaceSearch(data, details)}
            isRowScrollable={false}
            enablePoweredByContainer={false}
            placeholder={strings.search_place}
            // placeholderTextColor="#fff"
            autoFocus
            autoCorrect
            // listViewDisplayed="auto"
            fetchDetails
            renderRow={row => (
              // console.log('bambi row', row);
              <View
                style={{
                  flexDirection: 'row'
                }}
              >
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <View>
                    <MaterialIcons
                      name="place"
                      size={4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                      color="#707070"
                      style={{ alignSelf: 'center' }}
                    />
                    {/* <Text
                      style={{
                        fontFamily: 'helveticaneue',
                        fontSize: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 * 1.1
                      }}
                    >
                      900m
                    </Text> */}
                  </View>
                </View>
                <View style={{ marginLeft: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <View>
                      <Text
                        style={{
                          fontFamily: 'helveticaneue',
                          fontSize: 3 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
                          fontWeight: 'bold'
                        }}
                      >
                        {row.structured_formatting.main_text}
                      </Text>
                      <Text
                        style={{
                          fontFamily: 'helveticaneue',
                          fontSize: 3 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1
                        }}
                      >
                        {row.structured_formatting.secondary_text}{' '}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            )}
            query={{
              key: 'AIzaSyAUYfbKtctkIibOgFnNN2x9Xg9i0sVxlhQ',
              language: 'vi'
            }}
            styles={{
              textInputContainer: {
                backgroundColor: '#6d77f7',
                borderWidth: 0,
                borderTopColor: 'rgba(0,0,0,0)',
                borderBottomColor: 'rgba(0,0,0,0)',
                borderTopWidth: 0,
                borderBottomWidth: 0
              },
              textInput: {
                fontSize: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                borderWidth: 0,
                borderRadius: 25
              },
              listView: {
                backgroundColor: 'white',
                width: '100%',
                borderTopColor: 'rgba(0,0,0,0)',
                borderBottomColor: 'rgba(0,0,0,0)',
                borderTopWidth: 0,
                borderBottomWidth: 0
              },
              separator: {
                backgroundColor: 'transparent'
              }
            }}
            nearbyPlacesAPI={'GooglePlacesSearch'}
            GooglePlacesSearchQuery={{
              // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
              rankby: 'distance',
              typles: 'country'
            }}
            renderRightButton={() => (
              <MaterialIcons
                onPress={this.handleCancel}
                name="clear"
                size={6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                style={{
                  color: '#fff',
                  textAlign: 'center',
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  paddingRight: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                }}
              />
            )}
          />
        </Modal>
        <ScrollView>
          <KeyboardAvoidingView
            behavior="padding"
            enabled
            style={{ flex: 1 }}
            keyboardVerticalOffset={1}
          >
            <View
              style={{
                flexDirection: 'row',
                padding: 6.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                backgroundColor: 'white'
              }}
            >
              <View style={{ height: 18 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}>
                <MyImage
                  style={{
                    height: 18 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                    width: 18 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                    borderRadius: 9 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                  }}
                  resizeMode="cover"
                  source={{ uri: this.props.userData.avatar }}
                  defaultSource={defaultAvatar}
                />
              </View>
              <View
                style={{
                  marginLeft: 6.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                  justifyContent: 'center'
                }}
              >
                <Text style={[styles.headerText, { color: '#707070' }]}>
                  {this.props.userData.nickname}
                </Text>
                <PickerComponent
                  children={
                    <View
                      onPress={() => alert('This feature is in development')}
                      style={{
                        opacity: this.makeNewPostFromGroup ? 0 : 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: SCALE_RATIO_WIDTH_BASIS,
                        borderStyle: 'solid',
                        borderWidth: 0.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                        borderColor: '#707070',
                        padding: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                        width: 38 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                        marginTop: SCALE_RATIO_WIDTH_BASIS
                      }}
                    >
                      <MaterialIcons
                        name={this.state.selectedPrivacy.icon}
                        size={5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                        color="#707070"
                      />
                      <Text style={styles.placeText}> {this.state.selectedPrivacy.name} </Text>
                      <MaterialIcons
                        name="play-arrow"
                        size={5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                        color="#707070"
                        style={{ transform: [{ rotate: '90deg' }] }}
                      />
                    </View>
                  }
                  data={this.listPrivacySelection}
                  disable={this.state.isLoading || this.makeNewPostFromGroup}
                />
              </View>
            </View>
            <View
              style={{
                flex: 1,
                paddingHorizontal: 6.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
              }}
            >
              <TextInput
                placeholder={strings.whats_on_your_mind}
                placeholderTextColor="rgb(156, 159, 170)"
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                autoCorrect={false}
                style={{
                  fontFamily: 'helveticaneue',
                  fontSize: 5 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
                  fontStyle: 'normal',
                  color: '#707070',
                  textAlignVertical: 'top'
                }}
                multiline
                value={this.state.postContent}
                onChangeText={value => this.setState({ postContent: value })}
              />
              <STouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate(ROUTE_KEY.FRIEND_FOR_DIARY, {
                    setFriends: tagFriends => this.setState({ tagFriends }),
                    tagFriends: this.state.tagFriends
                  })
                }
              >
                {this.state.tagFriends &&
                  this.state.tagFriends.length > 0 &&
                  this.state.tagFriends.map(e => {
                    if (e.nickname) {
                      return (
                        <Text
                          style={{
                            fontFamily: 'helveticaneue',
                            fontSize: 5 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
                            fontStyle: 'normal',
                            color: '#707070',
                            textAlignVertical: 'top'
                          }}
                        >
                          {e.nickname ? e.nickname : ''}
                        </Text>
                      );
                    }
                    return null;
                  })}
              </STouchableOpacity>
              {this.state.tagAddress.length > 0 && (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                  }}
                >
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View
                      style={{
                        width: 12 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                        height: 12 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                        borderRadius: 6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                        backgroundColor: '#dae1ed',
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}
                    >
                      <Entypo
                        name="location"
                        size={8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                        color="white"
                      />
                    </View>
                    <Text
                      style={{
                        fontFamily: 'helveticaneue',
                        fontSize: 5 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
                        fontStyle: 'normal',
                        color: 'black',
                        textAlignVertical: 'top',
                        marginLeft: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                        maxWidth: 85 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                      }}
                    >
                      {this.state.tagAddress}
                    </Text>
                  </View>
                  <STouchableOpacity
                    style={{ padding: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}
                    onPress={() => this.setState({ tagAddress: '' })}
                  >
                    <MaterialIcons
                      name="clear"
                      size={8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                      color="black"
                    />
                  </STouchableOpacity>
                </View>
              )}
              {this.sharingMode && <NewFeedsItems data={this.post} id={this.post.id} sharingMode />}
            </View>
            <View style={{ paddingVertical: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}>
              <ImageSlideShow
                sources={this.state.photos.map(e => ({ uri: e.uri }))}
                defaultSource={defaultProductImage}
              />
            </View>
            <View style={{ paddingHorizontal: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224) }}>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {this.state.products.map(e => {
                  if (!e || !e.list_image || e.list_image.length === 0) {
                    return <View />;
                  }
                  return (
                    <STouchableOpacity
                      key={e.id}
                      onPress={() => {
                        this.props.navigation.navigate(ROUTE_KEY.OTHER_PRODUCT_DETAIL, {
                          data: { product: e }
                        });
                      }}
                    >
                      <STouchableOpacity
                        onPress={() =>
                          this.setState({
                            products: this.state.products.filter(i => i.id !== e.id)
                          })
                        }
                        style={{
                          right: 0 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                          top: 0 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                          backgroundColor: '#6d77f7',
                          borderRadius: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                          position: 'absolute',
                          zIndex: 99,
                          elevation: 2
                        }}
                      >
                        <MaterialIcons
                          name="clear"
                          size={8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)}
                          color="#fff"
                        />
                      </STouchableOpacity>
                      <Image
                        style={styles.scrollviewImage}
                        resizeMode="cover"
                        source={
                          e.list_image[0] && e.list_image[0] !== ''
                            ? { uri: e.list_image[0] }
                            : defaultProductImage
                        }
                        defaultSource={defaultProductImage}
                      />
                      <Text style={styles.scrollviewProductName} numberOfLines={1}>
                        {e.name}
                      </Text>
                      <Text style={styles.scrollviewProductPrice}>{getNumberString(e.price)}</Text>
                    </STouchableOpacity>
                  );
                })}
              </ScrollView>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
        {!this.sharingMode && (
          <View style={styles.footerContainer}>
            <Text
              style={[
                styles.headerText,
                {
                  color: '#707070',
                  paddingLeft: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
                  fontSize: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
                }
              ]}
            >
              {strings.add_to_your_post}
            </Text>
            <View style={{ flexDirection: 'row' }}>
              <STouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_KEY.CHOOSE_IMAGE_POST, {
                    limitNumberImage: false,
                    listImage: this.state.photos,
                    onDone: (navigation, listImage) => {
                      navigation.goBack();
                      this.setState({ photos: listImage });
                    }
                  });
                }}
              >
                <Image source={ic_tag_image} resizeMode="contain" style={styles.iconStyle} />
              </STouchableOpacity>
              <STouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate(ROUTE_KEY.FRIEND_FOR_DIARY, {
                    setFriends: tagFriends => this.setState({ tagFriends }),
                    tagFriends: this.state.tagFriends
                  })
                }
              >
                <Image source={ic_tag_friend} resizeMode="contain" style={styles.iconStyle} />
              </STouchableOpacity>
              <STouchableOpacity onPress={this.showDialog}>
                <Image source={ic_tag_location} resizeMode="contain" style={styles.iconStyle} />
              </STouchableOpacity>
              <STouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate(ROUTE_KEY.PRODUCT_FOR_DIARY, {
                    setProduct: products => this.setProduct(products),
                    products: this.state.products
                  })
                }
              >
                <Image source={ic_tag_product} resizeMode="contain" style={styles.iconStyle} />
              </STouchableOpacity>
            </View>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  headerContainer: {
    backgroundColor: '#6d77f7',
    height:
      60 * SCALE_RATIO_WIDTH_BASIS -
      DEVICE_HEIGHT / 540 -
      (Platform.OS === 'ios' ? 0 : getStatusBarHeight()),
    paddingTop: Platform.OS === 'ios' ? getStatusBarHeight() : 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  headerText: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue',
    fontSize: 6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: '600',
    textAlign: 'center',
    color: '#ffffff'
  },
  inputContainer: {
    // margin: 6.4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    // borderRadius: 3.3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    padding: 6.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: 'white'
    // shadowColor: '#797e87',
    // shadowOffset: {
    // width: 2,
    // height: 3
    // },
    // shadowRadius: 3,
    // shadowOpacity: 0.5,
    // elevation: 2
  },
  topInputContainer: {
    marginTop: 3.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingHorizontal: 4.8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingBottom: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderStyle: 'solid',
    borderBottomWidth: 0.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderColor: '#e2ebfc'
  },
  emojiContainer: {
    flexDirection: 'row',
    flex: 0.7,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  placeContainer: {
    flex: 0.8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: SCALE_RATIO_WIDTH_BASIS,
    borderStyle: 'solid',
    borderWidth: 0.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderColor: '#707070',
    padding: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginHorizontal: 1.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  privacyContainer: {
    flex: 0.9,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: SCALE_RATIO_WIDTH_BASIS,
    borderStyle: 'solid',
    borderWidth: 0.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderColor: '#707070',
    padding: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginHorizontal: 1.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  placeText: {
    fontFamily: 'helveticaneue',
    fontSize: 3.7 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '600',
    fontStyle: 'normal',
    textAlign: 'left',
    color: '#707070'
  },
  bottomInputContainer: {
    paddingTop: 4.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingBottom: 3.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    paddingHorizontal: 4.8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  submitText: {
    fontFamily: 'helveticaneue',
    fontSize: 3.7 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '600',
    fontStyle: 'normal',
    textAlign: 'left',
    color: '#6d77f7'
  },
  imageContainer: {
    width: 34.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 34.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 0.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderColor: '#bcbcbd',
    margin: 1.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    width: 34.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 34.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#ffffff',
    borderWidth: 0.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderColor: '#bcbcbd',
    margin: 1.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    justifyContent: 'center',
    alignItems: 'center'
  },
  scrollviewImage: {
    marginTop: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    width: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderStyle: 'solid',
    borderWidth: 0.2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderColor: '#bcbcbd',
    marginRight: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  scrollviewProductName: {
    width: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    // marginLeft: 1.35 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    // marginRight: 1.35 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    // height: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    fontFamily: 'helveticaneue',
    fontSize: 3.5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontStyle: 'normal',
    // lineHeight: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    // letterSpacing: 0,
    textAlign: 'center',
    color: '#707070'
  },
  scrollviewProductPrice: {
    // height: 3.7 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    fontFamily: 'helveticaneue',
    width: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    // marginLeft: 1.35 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    // marginRight: 1.35 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    fontSize: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontStyle: 'normal',
    // lineHeight: 3.7 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
    // letterSpacing: 0,
    textAlign: 'center',
    color: '#f7a823'
  },
  // postImage: {
  //   borderRadius: 1.7 * SCALE_RATIO_WIDTH_BASIS - ( DEVICE_WIDTH/ 125 ) * 1.5,
  //   //borderStyle: 'solid',
  //   borderWidth: 0.2 * SCALE_RATIO_WIDTH_BASIS - ( DEVICE_WIDTH/ 125 ),
  //   borderColor: '#bcbcbd'
  // },
  scrollviewPrice: {
    fontFamily: 'helveticaneue',
    fontSize: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: '600',
    fontStyle: 'normal',
    color: '#6d77f7'
  },
  blockContainer: {
    backgroundColor: 'white',
    borderRadius: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  iconStyle: {
    width: 12 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 12 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderColor: 'rgb(236, 237, 239)',
    borderTopWidth: 0.1 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  sliderImage: {
    width: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    height: 34.4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderWidth: 0.2 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderColor: '#bcbcbd',
    marginRight: 3.6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  }
});

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData
});

const mapActionCreators = {
  uploadPostSuccess
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(AddDiaryComponent);
