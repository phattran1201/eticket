import request from '../../../utils/request';
import {
  BASE_URL,
  POST_API,
  ADD_ITEM_IN_START_POSITION_LIST_POSTS,
  ADD_ITEM_IN_START_POSITION_LIST_USER_POSTS,
  LIST_POST_FOR_USER,
  UPDATE_LIST_POST_AFTER_USER_CREATE_POST
} from '../../../constants/Constants';

export const uploadPost = (data, products) =>
  new Promise((resolve, reject) => {
    // console.log('updatepost data', data);
    request
      .post(`${BASE_URL}${POST_API}`)
      .set('Authorization', `Bearer ${data.token}`)
      .set('Content-Type', 'application/json')
      .send({
        content: data.content,
        user_id: data.user_id,
        list_image: data.listImage.length === 0 ? null : data.listImage,
        privacy: data.privacy,
        shared_post_id: data.post ? data.post.id : null,
        group_id: data.group_id ? data.group_id : null
      })
      .finish((err, res) => {
        console.log('updatePost res:', res);
        if (err) {
          reject(err);
        } else {
          res.body.results.object.shared_post = data.post;
          if (products.length === 0) {
            resolve(res.body.results.object);
          } else {
            if (products.length === 0) {
              resolve(res.body.results.object);
            } else {
              let index = -1;
              products.forEach((element, i, array) => {
                request
                  .post(`${BASE_URL}product_post`)
                  .set('Authorization', `Bearer ${data.token}`)
                  .set('Content-Type', 'application/json')
                  .send({
                    post_id: res.body.results.object.id,
                    product_id: element.id
                  })
                  .finish((err2, res2) => {
                    if (err2) reject(err2);
                  });
                index++;
                if (index >= array.length - 1) {
                  resolve({
                    ...res.body.results.object,
                    products: products.map(e => ({
                      product: { ...e }
                    }))
                  });
                }
              });
            }
          }
        }
      });
  });

export const editPost = (data, products, oldData) =>
  new Promise((resolve, reject) => {
    //products
    request
      .put(`${BASE_URL}${POST_API}/${data.id}`)
      .set('Authorization', `Bearer ${data.token}`)
      .set('Content-Type', 'application/json')
      .send({
        content: data.content,
        user_id: data.user_id,
        list_image: !data.listImage.length ? null : data.listImage,
        privacy: data.privacy,
        shared_post_id: data.post ? data.post.id : null,
        group_id: data.group_id ? data.group_id : null
      })
      .finish((err, res) => {
        if (err) {
          reject(err);
        } else {
          if (products.length === 0) {
            resolve({
              ...res.body.results.object,
              //add this attribute because response from server dơn't have it
              shared_post: oldData.shared_post
            });
            oldData && oldData.products && oldData.products.length > 0 && oldData.products.forEach(i => {
              request
                .delete(`${BASE_URL}product_post/${i.id}`)
                .set('Authorization', `Bearer ${data.token}`)
                .finish((err2, res2) => {
                  // console.log(err2, res2);
                });
            });
          } else {
            let index = -1;
            products.forEach((element, i, array) => {
              const uploaded = oldData.products.find(
                ii => ii.product_id === element.id
              );
              if (uploaded) {
                index++;
                if (index >= array.length - 1) {
                  resolve({
                    ...res.body.results.object,
                    products: products.map(e => ({
                      product: { ...e }
                    }))
                  });
                }
              } else {
                request
                  .post(`${BASE_URL}product_post`)
                  .set('Authorization', `Bearer ${data.token}`)
                  .set('Content-Type', 'application/json')
                  .send({
                    post_id: res.body.results.object.id,
                    product_id: element.id
                  })
                  .finish((err2, res2) => {
                    if (err2) {
                      reject(err2);
                    } else {
                      index++;
                      if (index >= array.length - 1) {
                        resolve({
                          ...res.body.results.object,
                          products: products.map(e => ({
                            product: { ...e }
                          }))
                        });
                      }
                    }
                  });
              }
            });
            [...oldData.products].forEach(i => {
              const uploaded = products.find(ii => ii.id === i.product_id);
              if (!uploaded) {
                request
                  .delete(`${BASE_URL}product_post/${i.id}`)
                  .set('Authorization', `Bearer ${data.token}`)
                  .finish((err2, res2) => {
                    // console.log(err2, res2);
                  });
              }
            });
          }
        }
      });
  });

export function uploadPostSuccess(
  payload,
  onDoneFunction,
  oldPost = { id: '' }
) {
  if (payload.message) { //this will run if the post contains vulnerable words
    onDoneFunction(false);
  } else {
    return (dispatch, store) => {
      dispatch({
        type: LIST_POST_FOR_USER,
        payload: [
          payload,
          ...store().profilePost.listPost.filter(e => e.id !== oldPost.id)
        ]
      });
      dispatch({
        type: UPDATE_LIST_POST_AFTER_USER_CREATE_POST,
        payload: [
          payload,
          ...store().post.listPosts.filter(e => e.id !== oldPost.id)
        ]
      });
      onDoneFunction(true);
    };
  }
}
