import React from 'react';
import { Image, StyleSheet, Text, View, FlatList } from 'react-native';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import {
  DEVICE_WIDTH,
  FS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import { DATA_MILK_TEA, DATA_STORE, DATA_TEST } from '../../../constants/dataTest';
import style, { APP_COLOR, APP_COLOR_TEXT } from '../../../constants/style';
import MyButton from '../../../style/MyButton';
import CustomTabBar from '../../../style/Tabbar/CustomTabBar';
import { getNumberWithCommas } from '../../../utils/numberUtils';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';

class RewardsHistoryComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      showAllItem: false
    };
    this.store = DATA_STORE;
    this.data = DATA_TEST;
    this.orderCode = '#123456';
    this.order = ['Thanh toán đơn hàng', 'Chia sẻ Facebook'];
    this.order2 = ['Tặng 1 Topping bất kì', 'Tặng 1 đồ uống bất kì size M', 'Thêm lượt quay may mắn'];
  }

  componentDidMount() {}
  renderItem = ({ item }) => {
    const order = this.order[Math.floor(Math.random() * this.order.length)];
    return (
      <View
        style={{
          // borderRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
          backgroundColor: '#fff'
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
            padding: 15 * SCALE_RATIO_WIDTH_BASIS,
            borderBottomColor: '#70707030'
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              flex: 1
            }}
          >
            <View style={{ justifyContent: 'flex-start' }}>
              <Text style={[style.titleHeader, { fontSize: FS(16), color: APP_COLOR_TEXT, textAlign: 'left' }]}>
                {order} {order === 'Chia sẻ Facebook' ? '' : `#${Math.floor(Math.random() * 9999) + 1000}`}
              </Text>
              <Text
                style={[
                  style.text,
                  {
                    fontSize: FS(12),
                    textAlign: 'left'
                  }
                ]}
              >
                {item.date}
              </Text>
            </View>
            <View style={{ justifyContent: 'center' }}>
              <Text style={[style.titleHeader, { fontSize: FS(16), color: APP_COLOR, textAlign: 'right' }]}>
                {order === 'Chia sẻ Facebook' ? '+1' : `+${Math.floor(Math.random() * 99) + 10}`}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };
  renderItem2 = ({ item }) => {
    const order = this.order2[Math.floor(Math.random() * this.order.length)];
    return (
      <View
        style={{
          // borderRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
          backgroundColor: '#fff'
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
            padding: 15 * SCALE_RATIO_WIDTH_BASIS,
            borderBottomColor: '#70707030'
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              flex: 1
            }}
          >
            <View style={{ justifyContent: 'flex-start' }}>
              <Text style={[style.titleHeader, { fontSize: FS(16), color: APP_COLOR_TEXT, textAlign: 'left' }]}>
                {order}
              </Text>
              <Text
                style={[
                  style.text,
                  {
                    fontSize: FS(12),
                    textAlign: 'left'
                  }
                ]}
              >
                {item.date}
              </Text>
            </View>
            <View style={{ justifyContent: 'center' }}>
              <Text style={[style.titleHeader, { fontSize: FS(16), color: APP_COLOR, textAlign: 'right' }]}>
                -{Math.floor(Math.random() * 10) + 1}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View
        style={{
          backgroundColor: '#F7F7F7',
          flex: 1
        }}
      >
        <BaseHeader
          // translucent
          children={<Text style={[style.titleHeader, { fontSize: FS(18) }]}>Lịch sử điểm thưởng</Text>}
          leftIcon="arrow-left"
          leftIconType="MaterialCommunityIcons"
          onLeftPress={() => this.props.navigation.goBack()}
        />

        <ScrollableTabView
          initialPage={0}
          style={{ paddingTop: 10 * SCALE_RATIO_HEIGHT_BASIS }}
          renderTabBar={() => (
            <CustomTabBar
              tabStyle={{
                width: 200 * SCALE_RATIO_WIDTH_BASIS,
                height: 36 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: 18 * SCALE_RATIO_WIDTH_BASIS,
                marginRight: -20 * SCALE_RATIO_WIDTH_BASIS,
                marginLeft: -20 * SCALE_RATIO_WIDTH_BASIS
              }}
            />
          )}
        >
          <View
            tabLabel="Số điểm đã nhận"
            style={[
              style.shadow,
              {
                marginHorizontal: 0,
                marginTop: 10 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
          >
            <FlatList numColumns={1} data={DATA_TEST} renderItem={this.renderItem} />
          </View>
          <View
            tabLabel="Số điểm đã dùng"
            style={[
              style.shadow,
              {
                marginHorizontal: 0,
                marginTop: 10 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
          >
            <FlatList numColumns={1} data={DATA_TEST} renderItem={this.renderItem2} />
          </View>
        </ScrollableTabView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 30
  },
  icon: {
    width: 300,
    height: 300,
    alignSelf: 'center'
  },
  markerImageStyle: {
    width: 40 * SCALE_RATIO_WIDTH_BASIS,
    height: 40 * SCALE_RATIO_WIDTH_BASIS
  },
  searchInputContainer: {
    paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
    position: 'absolute',
    top: 20 * SCALE_RATIO_WIDTH_BASIS,
    width: DEVICE_WIDTH,
    height: 50 * SCALE_RATIO_WIDTH_BASIS
  },
  searchInput: {
    borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS,
    flex: 1,
    height: 50 * SCALE_RATIO_WIDTH_BASIS,
    flexDirection: 'row',
    backgroundColor: '#FFFFFF'
  },
  searchNameContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchNameTextStyle: {
    fontSize: 14 * SCALE_RATIO_WIDTH_BASIS,
    color: '#767676',
    fontWeight: '500'
  },
  searchIconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 40 * SCALE_RATIO_WIDTH_BASIS,
    height: '100%'
  },
  carouselItemContainer: {
    overflow: 'visible',
    borderRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
    width: 150 * SCALE_RATIO_WIDTH_BASIS,
    height: 180 * SCALE_RATIO_WIDTH_BASIS,
    backgroundColor: 'white'
  },
  carouselItemMoreInfo: {
    textAlign: 'center',
    fontSize: 10 * SCALE_RATIO_WIDTH_BASIS
  }
});
const mapActionCreators = {};

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData
});

// export default connect(
//   mapStateToProps,
//   mapActionCreators
// )(RewardsHistoryComponent);
export default RewardsHistoryComponent;
