import React from 'react';
import { connect } from 'react-redux';
import { Image, ScrollView, Text, View } from 'react-native';
import { ListItem } from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  FS,
  ORDER_COMPONENT,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import { DATA_STORE, DATA_TEST } from '../../../constants/dataTest';
import strings from '../../../constants/Strings';
import style, { APP_COLOR, FONT, APP_COLOR_TEXT } from '../../../constants/style';
import MyButton from '../../../style/MyButton';
import { alert } from '../../../utils/alert';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import MyImage from '../../view/MyImage';
import { Tabs, Tab, TabHeading, Icon, ScrollableTab } from 'native-base';
import AnimatedBar from 'react-native-animated-bar';

class RewardsComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = { activeTab: 0, progress: 100000 / 200000 };
    this.store = DATA_STORE;
    this.data = DATA_TEST;
    this.onChangeTab = this.onChangeTab.bind(this);
    this.listMenu = [
      {
        title: 'Lịch sử điểm thưởng',
        icon: 'menu',
        iconImg: require('../../../assets/imgs/menu/menu_1.png'),
        route: ROUTE_KEY.REWARDS_HISTORY
      },
      {
        title: 'Cách để tích lũy điểm thưởng',
        icon: 'start',
        iconImg: require('../../../assets/imgs/menu/menu_2.png'),
        route: ROUTE_KEY
      },
      {
        title: 'Đổi điểm thưởng',
        icon: 'start',
        iconImg: require('../../../assets/imgs/menu/discount.png'),
        route: ROUTE_KEY
      }
    ];
  }
  onChangeTab(e) {
    this.setState({ activeTab: e.i });
    this.forceUpdate();
  }

  render() {
    console.log('dauphaiphat: RewardsComponent -> render -> this.state.activeTab', this.state.activeTab);
    return (
      <ScrollView style={{ backgroundColor: '#fff', flex: 1 }}>
        <BaseHeader
          noShadow
          translucent
          styleContent={{
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 99999
          }}
          styleLeftContent
          // leftIcon="arrow-left"
          // leftIconType="Feather"
          // leftIconStyle={{ color: APP_COLOR_TEXT }}
          // onLeftPress={() => this.props.navigation.goBack()}
        />

        <FastImage
          style={{
            zIndex: -1,
            width: DEVICE_WIDTH,
            height: (20 / 100) * DEVICE_HEIGHT,
            position: 'absolute'
          }}
          removeClippedSubviews
          source={{
            uri: 'https://i.pinimg.com/originals/81/f1/8d/81f18dbd8bef71772d6db3b379101310.jpg'
          }}
          defaultSource={null}
        />

        <View
          style={[
            style.shadow,
            {
              backgroundColor: '#fff',
              borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
              marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
              marginTop: (16 / 100) * DEVICE_HEIGHT
            }
          ]}
        >
          <View
            style={{
              paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
              paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <Text
              style={[
                style.text,
                {
                  fontSize: FS(12),
                  marginBottom: 4 * SCALE_RATIO_HEIGHT_BASIS,
                  marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
            >
              Điểm tích lũy hiện tại
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <Image
                source={require('../../../assets/imgs/icons/coin.png')}
                style={{
                  width: 20 * SCALE_RATIO_WIDTH_BASIS,
                  height: 20 * SCALE_RATIO_WIDTH_BASIS,
                  alignSelf: 'center',
                  marginRight: 3 * SCALE_RATIO_WIDTH_BASIS
                }}
                resizeMode="contain"
              />
              <Text style={[style.titleHeader, { fontSize: FS(22), color: APP_COLOR }]}>123,456</Text>
            </View>
            <View
              style={{
                paddingVertical: 20 * SCALE_RATIO_WIDTH_BASIS,
                marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <AnimatedBar
                progress={this.state.progress}
                borderWidth={0}
                barColor={APP_COLOR}
                fillColor={`${APP_COLOR}30`}
                height={8 * SCALE_RATIO_WIDTH_BASIS}
                fillStyle={{ borderRadius: (8 * SCALE_RATIO_WIDTH_BASIS) / 2 }}
                barStyle={{ borderRadius: (8 * SCALE_RATIO_WIDTH_BASIS) / 2 }}
                duration={500}
              />
              <View style={{ flexDirection: 'row', flex: 1 }}>
                <Text style={[style.text, { fontSize: FS(8), flex: 3 }]}>0</Text>
                <Text style={[style.text, { fontSize: FS(8), flex: 3.75, alignSelf: 'center' }]}>50,000</Text>
                <Text style={[style.text, { fontSize: FS(8), flex: 3.5, alignSelf: 'center' }]}>100,000</Text>
                <Text style={[style.text, { fontSize: FS(8), textAlign: 'right', flex: 4.5 }]}>200,000</Text>
                {/* <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text style={[style.text, { fontSize: FS(8) }]}>0</Text>
                  <Text style={[style.text, { fontSize: FS(8), textAlign: 'center' }]}>50,000</Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <Text style={[style.text, { fontSize: FS(8), textAlign: 'center' }]}>100,000</Text>
                </View>
                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                  <Text style={[style.text, { fontSize: FS(8), textAlign: 'right' }]}>200,000</Text>
                </View> */}
              </View>
            </View>

            {this.listMenu.map((item, i) => (
              <ListItem
                containerStyle={{
                  padding: 5 * SCALE_RATIO_WIDTH_BASIS,
                  borderBottomColor: `${APP_COLOR}80`,
                  borderBottomWidth: 0 * SCALE_RATIO_HEIGHT_BASIS
                }}
                key={i}
                onPress={() =>
                  this.props.navigation.navigate(`${item.route}`, {
                    name: 'Chính sách và hổ trợ',
                    uri: 'https://termsfeed.com/privacy-policy/cdb387d60ff1377a43250ae682690806'
                  })
                }
                title={item.title}
                leftAvatar={{
                  source: item.iconImg,
                  avatarStyle: {
                    width: 18 * SCALE_RATIO_WIDTH_BASIS,
                    height: 18 * SCALE_RATIO_WIDTH_BASIS,
                    resizeMode: 'contain'
                  },
                  containerStyle: { backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' },
                  overlayContainerStyle: { backgroundColor: 'transparent' }
                }}
                // leftIcon={{ name: item.icon }}
                titleStyle={[style.text, { fontFamily: FONT.Medium, fontSize: FS(16), marginLeft: -15 }]}
                chevron
              />
            ))}
          </View>
        </View>
        <View
          style={[
            style.shadow,
            {
              backgroundColor: '#fff',
              borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
              marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
              marginTop: 5 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
        >
          <Tabs
            onChangeTab={e => this.onChangeTab(e)}
            tabContainerStyle={{
              height: 80 * SCALE_RATIO_WIDTH_BASIS
            }}
            tabBarUnderlineStyle={{ height: 0 }}
          >
            <Tab
              style={{ padding: 10 * SCALE_RATIO_WIDTH_BASIS }}
              heading={
                <TabHeading
                  style={
                    this.state.activeTab === 0
                      ? [style.shadow, { margin: 0, flexDirection: 'column', backgroundColor: '#fff' }]
                      : { flexDirection: 'column', backgroundColor: '#f1f1f1' }
                  }
                >
                  <Icon name="check-circle" type="MaterialIcons" style={{ color: '#d8b979', fontSize: FS(22) }} />
                  <Text style={style.text}>Thành viên</Text>
                </TabHeading>
              }
            >
              <Text style={[style.text, { fontWeight: 'bold' }]}>Thành viên ToCoToCo Rewards</Text>
              <Text style={style.text}>
                Chào mừng bạn đến với ToCoToCo Rewards! Hãy cùng thưởng thức ToCoToCo và tích điểm nâng hạng và kèm theo
                đó là những ưu đãi đặc biệt cho các thành viên thân thiết của ToCoToCo.
              </Text>
            </Tab>
            <Tab
              style={{ padding: 10 * SCALE_RATIO_WIDTH_BASIS }}
              heading={
                <TabHeading
                  style={
                    this.state.activeTab === 1
                      ? [style.shadow, { margin: 0, flexDirection: 'column', backgroundColor: '#fff' }]
                      : { flexDirection: 'column', backgroundColor: '#f1f1f1' }
                  }
                >
                  <Icon name="check-circle" type="MaterialIcons" style={{ color: '#79b0d9', fontSize: FS(22) }} />
                  <Text style={style.text}>Bạc</Text>
                </TabHeading>
              }
            >
              <Text style={[style.text, { fontWeight: 'bold' }]}>Thành viên ToCoToCo Rewards</Text>
              <Text style={style.text}>
                Chào mừng bạn đến với ToCoToCo Rewards! Hãy cùng thưởng thức ToCoToCo và tích điểm nâng hạng và kèm theo
                đó là những ưu đãi đặc biệt cho các thành viên thân thiết của ToCoToCo.
              </Text>
            </Tab>
            <Tab
              style={{ padding: 10 * SCALE_RATIO_WIDTH_BASIS }}
              heading={
                <TabHeading
                  style={
                    this.state.activeTab === 2
                      ? [style.shadow, { margin: 0, flexDirection: 'column', backgroundColor: '#fff' }]
                      : { flexDirection: 'column', backgroundColor: '#f1f1f1' }
                  }
                >
                  <Icon name="check-circle" type="MaterialIcons" style={{ color: '#ffd202', fontSize: FS(22) }} />
                  <Text style={style.text}>Vàng</Text>
                </TabHeading>
              }
            >
              <Text style={[style.text, { fontWeight: 'bold' }]}>Thành viên ToCoToCo Rewards</Text>
              <Text style={style.text}>
                Chào mừng bạn đến với ToCoToCo Rewards! Hãy cùng thưởng thức ToCoToCo và tích điểm nâng hạng và kèm theo
                đó là những ưu đãi đặc biệt cho các thành viên thân thiết của ToCoToCo.
              </Text>
            </Tab>
            <Tab
              style={{ padding: 10 * SCALE_RATIO_WIDTH_BASIS }}
              heading={
                <TabHeading
                  style={
                    this.state.activeTab === 3
                      ? [style.shadow, { margin: 0, flexDirection: 'column', backgroundColor: '#fff' }]
                      : { flexDirection: 'column', backgroundColor: '#f1f1f1' }
                  }
                >
                  <Icon name="lock" type="MaterialIcons" style={{ color: '#000000', fontSize: FS(22) }} />
                  <Text style={style.text}>Kim Cương</Text>
                </TabHeading>
              }
            >
              <Text style={[style.text, { fontWeight: 'bold' }]}>Thành viên ToCoToCo Rewards</Text>
              <Text style={style.text}>
                Chào mừng bạn đến với ToCoToCo Rewards! Hãy cùng thưởng thức ToCoToCo và tích điểm nâng hạng và kèm theo
                đó là những ưu đãi đặc biệt cho các thành viên thân thiết của ToCoToCo.
              </Text>
            </Tab>
          </Tabs>
        </View>
      </ScrollView>
    );
  }
}
const mapActionCreators = {};

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData
});

export default connect(
  mapStateToProps,
  mapActionCreators
)(RewardsComponent);
