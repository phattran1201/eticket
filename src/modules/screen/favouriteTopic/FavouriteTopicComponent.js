import React from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import { alert } from '../../../utils/alert';
import HeaderWithBackButtonComponent from '../../view/HeaderWithBackButtonComponent';
import MyComponent from '../../view/MyComponent';
import MySpinner from '../../view/MySpinner';
import { updateUserInfo } from '../editprofile/EditProfieActions';
const width = Dimensions.get('window').width;

class FavouriteTopicComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      totalInterest: 0,
      dataInterest: [
        {
          selected: false,
          key: 'Travel',
          url: require('../../../assets/imgs/favou/travel.png')
        },
        {
          selected: false,
          key: 'Games',
          url: require('../../../assets/imgs/favou/game.png')
        },
        {
          selected: false,
          key: 'Blind date',
          url: require('../../../assets/imgs/favou/blind_date.png')
        },
        {
          selected: false,
          key: 'Chat',
          url: require('../../../assets/imgs/favou/chat.png')
        },
        {
          selected: false,
          key: 'Pet',
          url: require('../../../assets/imgs/favou/pet.png')
        },
        {
          selected: false,
          key: 'Exercise',
          url: require('../../../assets/imgs/favou/excercise.png')
        },
        {
          selected: false,
          key: 'Music',
          url: require('../../../assets/imgs/favou/music.png')
        },
        {
          selected: false,
          key: 'Movie',
          url: require('../../../assets/imgs/favou/movie.png')
        },
        {
          selected: false,
          key: 'Videochat',
          url: require('../../../assets/imgs/favou/video_chat.png')
        },
        {
          selected: false,
          key: 'Food',
          url: require('../../../assets/imgs/favou/food.png')
        },
        {
          selected: false,
          key: 'Comics',
          url: require('../../../assets/imgs/favou/comics.png')
        },
        {
          selected: false,
          key: 'Fashion',
          url: require('../../../assets/imgs/favou/fashion.png')
        }
      ]
    };
  }

  renderItem = ({ item, index }) => (
    <TouchableOpacity
      onPress={() => {
        if (this.state.totalInterest === 3) {
          if (item.selected) {
            item.selected = !item.selected;
            const temp = [...this.state.dataInterest];
            temp[index].selected = item.selected;
            this.setState({ dataInterest: temp });
            this.setState({ totalInterest: this.state.totalInterest - 1 });
          } else {
            alert(strings.alert, strings.maximun_select);
          }
        } else {
          item.selected = !item.selected;
          const temp = [...this.state.dataInterest];
          temp[index].selected = item.selected;
          this.setState({ dataInterest: temp });
          if (item.selected) {
            this.setState({ totalInterest: this.state.totalInterest + 1 });
          } else {
            this.setState({ totalInterest: this.state.totalInterest - 1 });
          }
        }
      }}
      style={{ justifyContent: 'center', alignItems: 'center', width: DEVICE_WIDTH / 3 }}
    >
      <View
        style={[
          styles.eachInterest,
          {
            backgroundColor: item.selected ? '#C7AE6D' : '#fff'
          }
        ]}
      >
        <Image
          style={{
            height: 28 * SCALE_RATIO_HEIGHT_BASIS,
            width: 28 * SCALE_RATIO_HEIGHT_BASIS
          }}
          resizeMode={'cover'}
          source={item.url}
        />
        <Text
          style={{
            fontFamily: 'Helvetica Neue',
            color: item.selected ? '#fff' : '#C7AE6D',
            fontSize: 10 * SCALE_RATIO_HEIGHT_BASIS
          }}
        >
          {item.key}
        </Text>
      </View>
    </TouchableOpacity>
  );

  render() {
    const { params } = this.props.navigation.state;

    return (
      <View style={styles.container}>
        <HeaderWithBackButtonComponent
          bodyTitle={strings.favourite}
          onPress={() => this.props.navigation.goBack()}
        />
        <View style={{ flex: 1 }}>
          <Text
            style={{
              color: '#282828',
              fontFamily: 'Helvetica Neue',
              fontSize: 12 * SCALE_RATIO_WIDTH_BASIS,
              alignSelf: 'center',
              marginVertical: 20 * SCALE_RATIO_HEIGHT_BASIS
            }}
          >
            {strings.favourite_description}
          </Text>
          <FlatList
            data={this.state.dataInterest}
            keyExtractor={item => item.key}
            renderItem={this.renderItem}
            style={{ zIndex: 1, flex: 1 }}
            numColumns={3}
          />
          <TouchableOpacity
            style={{
              height: 48 * SCALE_RATIO_HEIGHT_BASIS,
              width,
              bottom: 0,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#C7AE6D'
            }}
            onPress={() => {
              const temp = { ...this.props.userData };
              console.log('Hoang log 1', temp);
              temp.interests = '';
              this.state.dataInterest.forEach(e => {
                if (e.selected) {
                  temp.interests = `${temp.interests} #${e.key}`;
                }
              });
              MySpinner.show();
              this.props.updateUserInfo(temp, () => {
                MySpinner.hide();
                this.props.navigation.goBack();
              });
            }}
          >
            <Text
              style={{
                color: '#fff',
                fontFamily: 'Helvetica Neue',
                fontSize: 14 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              {strings.confirm}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userData: state.user.userData
});

const mapActionCreators = {
  updateUserInfo
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(FavouriteTopicComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  infoContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    position: 'absolute',
    bottom: 0
  },
  flagInfoImage: {
    width: 50 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    height: 40 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },
  textInfo: {
    color: '#fff',
    fontSize: 17 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },
  moreDetailContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },
  moreDetailText: {
    fontSize: 14 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },
  buttonContainer: {
    flex: 1,
    borderWidth: 1 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    borderColor: '#000',
    padding: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    flexDirection: 'row',
    margin: 10 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    alignItems: 'center',
    justifyContent: 'center'
  },
  eachInterest: {
    height: 75 * SCALE_RATIO_HEIGHT_BASIS,
    width: 75 * SCALE_RATIO_HEIGHT_BASIS,
    borderRadius: 37.5 * SCALE_RATIO_HEIGHT_BASIS,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20 * SCALE_RATIO_WIDTH_BASIS,
    marginHorizontal: 18 * SCALE_RATIO_WIDTH_BASIS,
    borderWidth: 1 * SCALE_RATIO_HEIGHT_BASIS,
    borderColor: '#C7AE6D'
  }
});
