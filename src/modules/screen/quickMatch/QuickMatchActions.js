import request from '../../../utils/request';
import {
    BASE_URL, USER_API,
    UPDATE_LIST_RECOMMENDED_PARTNERS,
    TYPE_NEAR_BY,
    TYPE_RECENTLY_VISITED,
    TYPE_TOP_10_RANK,
    TYPE_RECENTLY_VISITED_FOREIGNER
} from '../../../constants/Constants';

export function loadPerfectMatchList(type, onDone = () => { }) {
    return (dispatch, store) => {
        const { user } = store();
        let url;
        switch (type) {
            case TYPE_NEAR_BY:
                url = `${BASE_URL}${USER_API}/get_10_near_by_users`;
            break;
            case TYPE_RECENTLY_VISITED:
                url = `${BASE_URL}${USER_API}?fields=["$all"]&order=[["sign_in_time", "DESC"]]&filter={"sex": {"$ne": "${user.userData.sex}"}}&page=1&limit=10`;
            break;
            case TYPE_TOP_10_RANK:
                url = `${BASE_URL}${USER_API}?fields=["$all"]&order=[["rating", "DESC"]]&filter={"sex": {"$ne": "${user.userData.sex}"}}&page=1&limit=10`;
            break;
            case TYPE_RECENTLY_VISITED_FOREIGNER:
                url = `${BASE_URL}${USER_API}?fields=["$all"]&order=[["sign_in_time", "DESC"]]&filter={"sex": {"$ne": "${user.userData.sex}"}, "locale": {"$ne": "${user.userData.locale}"}}&page=1&limit=10`;
            break;
            default:
                onDone();
                return;
          }
            request.get(url)
            .set('Content-Type', 'application/json')
            .set('Authorization', `Bearer ${store().user.token}`)
            .finish((err, res) => {
                if (res &&
                    res.body &&
                    res.body.results &&
                    res.body.results.objects &&
                    res.body.results.objects.rows) {
                    onDone(res.body.results.objects.rows);
                } else {
                    onDone([]);
                }
            });
    };
}

export function loadRecommendedPartnerList(onDone = () => { }) {
    return (dispatch, store) => {
        const { user } = store();
        let countryFilter = `, "locale": "${user.userData.locale}"`;
        if (!user.userData.locale) {
            countryFilter = '';
        }
        request.get(`${BASE_URL}${USER_API}?fields=["$all"]&order=[["sign_in_time", "DESC"]]&filter={"sex": {"$ne": "${user.userData.sex}"}${countryFilter}}&page=1&limit=10`)
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${store().user.token}`)
        .finish((err, res) => {
                onDone();
                if (res &&
                    res.body &&
                    res.body.results &&
                    res.body.results.objects &&
                    res.body.results.objects.rows) {
                    dispatch({
                        type: UPDATE_LIST_RECOMMENDED_PARTNERS,
                        payload: res.body.results.objects.rows
                    });
                }
            });
    };
}
