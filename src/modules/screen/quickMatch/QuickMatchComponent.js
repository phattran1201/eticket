import React from 'react';
import { Alert, ScrollView, StyleSheet, Text, View } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import {
  DEVICE_WIDTH,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  TYPE_NEAR_BY,
  TYPE_RECENTLY_VISITED,
  TYPE_RECENTLY_VISITED_FOREIGNER,
  TYPE_TOP_10_RANK,
  MIN_HEART_TO_MAKE_A_PHONE_CALL,
  ROUTE_KEY
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import MyButton from '../../../style/MyButton';
import { alert } from '../../../utils/alert';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import MySpinner from '../../view/MySpinner';
import PopupNotification from '../../view/PopupNotification';
import PopupNotificationCallComponent from '../../view/PopupNotificationCallComponent';
import QuickMatchListItem from '../../view/QuickMatchListItem';
import STouchableOpacity from '../../view/STouchableOpacity';
import { loadPerfectMatchList, loadRecommendedPartnerList } from './QuickMatchActions';

class QuickMatchComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      gender: strings.male,
      type: strings.recently_visited,
      activeSlide: 1
    };
    this.renderItem = this.renderItem.bind(this);
    this.callForMultiUser = this.callForMultiUser.bind(this);
    this.showCallingPopup = this.showCallingPopup.bind(this);
  }

  componentDidMount() {
    this.props.loadRecommendedPartnerList(() => {
      MySpinner.hide();
    });
  }

  renderItem = ({ item }) => <QuickMatchListItem item={item} navigation={this.props.navigation} />;

  callForMultiUser = type => () => {
    MySpinner.show();
    this.props.loadPerfectMatchList(type, listItem => {
      MySpinner.hide();
      setTimeout(() => {
        if (listItem.length > 0) {
          let subTitle;
          switch (type) {
            case TYPE_NEAR_BY:
              subTitle = strings.nearby;
              break;
            case TYPE_RECENTLY_VISITED:
              subTitle = strings.recently_visited;
              break;
            case TYPE_TOP_10_RANK:
              subTitle = strings.top_10_rating;
              break;
            case TYPE_RECENTLY_VISITED_FOREIGNER:
              subTitle = strings.recently_visited_foreigner;
              break;
            default:
              subTitle = '';
              break;
          }
          this.showCallingPopup(listItem, 0, subTitle);
        } else {
          alert(strings.alert, strings.perfect_match_no_data);
        }
      }, 100);
    });
  };

  showCallingPopup = (listItem, index, subTitle) => {
    if (index === listItem.length) {
      return;
    }

    if (this.props.userData.heart < MIN_HEART_TO_MAKE_A_PHONE_CALL) {
      Alert.alert(
        strings.alert,
        strings.out_of_heart,
        [{ text: strings.yes, onPress: () => this.props.navigation.navigate(ROUTE_KEY.STORE) }],
        { cancelable: true }
      );
      this.props.navigation.goBack();
    } else {
      PopupNotification.showComponent(
        <PopupNotificationCallComponent
          item={listItem[index]}
          subTitle={`(${subTitle} mode)`}
          timeRemaining={15} //15s to call
          onCallDeny={this.showCallingPopup(listItem, index + 1, subTitle)}
          navigation={this.props.navigation}
        />
      );
    }
  };

  render() {
    this.data = this.props.listRecommendedPartners.filter(e => {
      const isBlocked = this.props.listBlocked.find(i => i.receiver.id === e.id);
      return !isBlocked;
    });
    return (
      <ScrollView style={styles.container}>
        <BaseHeader
          children={<Text style={style.titleHeader}>{strings.quick_match.toUpperCase()}</Text>}
          rightIcon="envelope"
          navigation={this.props.navigation}
        />
        <View style={{ flex: 1 }}>
          <Text style={[style.titleHeader, styles.textLabel]}>
            {strings.quick_match_today_recommended}
          </Text>
          <View style={{ padding: 10 * SCALE_RATIO_WIDTH_BASIS }}>
            <Carousel
              ref={c => (this._sliderRef = c)}
              data={this.data}
              renderItem={this.renderItem}
              hasParallaxImages
              sliderWidth={DEVICE_WIDTH}
              itemWidth={Math.round((60 * DEVICE_WIDTH) / 100)}
              firstItem={1}
              inactiveSlideScale={0.94}
              inactiveSlideOpacity={0.7}
              inactiveSlideShift={20 * SCALE_RATIO_WIDTH_BASIS}
              containerCustomStyle={{
                overflow: 'visible'
              }}
              contentContainerCustomStyle={{ paddingVertical: SCALE_RATIO_WIDTH_BASIS }}
              loop
              loopClonesPerSide={2}
              autoplay
              autoplayDelay={6000}
              autoplayInterval={3000}
              onSnapToItem={index => this.setState({ activeSlide: index })}
            />
            <Pagination
              dotsLength={this.data.length}
              activeDotIndex={this.state.activeSlide}
              containerStyle={{ paddingVertical: 0.5 * SCALE_RATIO_WIDTH_BASIS }}
              dotColor="#70707090"
              dotStyle={{
                width: 6 * SCALE_RATIO_WIDTH_BASIS,
                height: 6 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS
              }}
              inactiveDotColor="#242424"
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
              carouselRef={this._sliderRef}
              tappableDots={!!this._sliderRef}
            />
          </View>
          <MyButton
            styleContent={{
              alignItems: 'center',
              justifyContent: 'center'
            }}
            leftIcon="heart"
            leftIconStyle={{ color: 'red' }}
            leftIconType="MaterialCommunityIcons"
            rightIcon="heart"
            rightIconStyle={{ color: 'red' }}
            rightIconType="MaterialCommunityIcons"
            style={{ marginHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS }}
            onPress={() => {
              MySpinner.show();
              setTimeout(() => {
                this.props.loadRecommendedPartnerList(() => {
                  MySpinner.hide();
                });
              }, 100);
            }}
          >
            {strings.quick_match_refresh_member.toUpperCase()}
          </MyButton>
          <Text style={[style.titleHeader, styles.textLabel]}>
            {strings.quick_match_find_match}
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <STouchableOpacity
              style={styles.buttonFilterContainer}
              onPress={this.callForMultiUser(TYPE_NEAR_BY)}
            >
              <MaterialIcons name="near-me" size={30 * SCALE_RATIO_WIDTH_BASIS} color="#707070" />
              <Text style={[style.text, styles.textDescription]}>{strings.nearby}</Text>
              <Text style={[style.textCaption, styles.textDescription]}>
                {strings.nearby_detail}
              </Text>
            </STouchableOpacity>
            <STouchableOpacity
              style={styles.buttonFilterContainer}
              onPress={this.callForMultiUser(TYPE_RECENTLY_VISITED)}
            >
              <MaterialIcons
                name="recent-actors"
                size={30 * SCALE_RATIO_WIDTH_BASIS}
                color="#707070"
              />
              <Text style={[style.text, styles.textDescription]}>{strings.recently_visited}</Text>
              <Text style={[style.textCaption, styles.textDescription]}>
                {strings.recently_visited_detail}
              </Text>
            </STouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <STouchableOpacity
              style={styles.buttonFilterContainer}
              onPress={this.callForMultiUser(TYPE_TOP_10_RANK)}
            >
              <MaterialIcons
                name="vertical-align-top"
                size={30 * SCALE_RATIO_WIDTH_BASIS}
                color="#707070"
              />
              <Text style={[style.text, styles.textDescription]}>{strings.top_10_rating}</Text>
              <Text style={[style.textCaption, styles.textDescription]}>
                {strings.top_10_rating_detail}
              </Text>
            </STouchableOpacity>
            <STouchableOpacity
              style={styles.buttonFilterContainer}
              onPress={this.callForMultiUser(TYPE_RECENTLY_VISITED_FOREIGNER)}
            >
              <MaterialIcons
                name="recent-actors"
                size={30 * SCALE_RATIO_WIDTH_BASIS}
                color="#707070"
              />
              <Text style={[style.text, styles.textDescription]}>
                {strings.recently_visited_foreigner_members}
              </Text>
              <Text style={[style.textCaption, styles.textDescription]}>
                {strings.recently_visited_foreigner_members_detail}
              </Text>
            </STouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  listRecommendedPartners: state.partners.listRecommendedPartners,
  listBlocked: state.blocking.listBlocked,
  userData: state.user.userData
});

const mapActionCreators = {
  loadRecommendedPartnerList,
  loadPerfectMatchList
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(QuickMatchComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  textLabel: {
    marginVertical: 10 * SCALE_RATIO_HEIGHT_BASIS
  },
  textDescription: {
    textAlign: 'center'
  },
  buttonFilterContainer: {
    alignItems: 'center',
    borderColor: '#C7AE6D',
    borderWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
    marginHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
    marginTop: 10 * SCALE_RATIO_WIDTH_BASIS,
    padding: 10 * SCALE_RATIO_WIDTH_BASIS,
    flex: 1
  }
});
