import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation';
import {
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import ExchangeComponent from '../exchange/ExchangeComponent';
import PaypalComponent from '../paypal/PaypalComponent';
import MyComponent from '../../view/MyComponent';
import BaseHeader from '../../view/BaseHeader';
import style from '../../../constants/style';
import strings from '../../../constants/Strings';
import { loadListEvent } from './ExchangeActions';

const TopTab = createMaterialTopTabNavigator(
  {
    Exchange: {
      screen: ExchangeComponent,
      navigationOption: {
        header: null,
        title: 'Exchange'
      }
    },
    Paypal: {
      screen: PaypalComponent,
      navigationOption: {
        header: null,
        title: 'Paypal'
      }
    }
  },
  {
    tabBarOptions: {
      labelStyle: {
        color: '#fff',
        fontSize: FS(12),
        textAlign: 'center',
        fontFamily: 'helveticaneue',
        fontWeight: 'bold',
        backgroundColor: 'transparent'
      },
      tabStyle: {
        backgroundColor: '#C7AE6D',
        margin: 5 * SCALE_RATIO_WIDTH_BASIS,
        borderRadius: 33 * SCALE_RATIO_HEIGHT_BASIS,
        paddingVertical: 0 * SCALE_RATIO_HEIGHT_BASIS,
        paddingHorizontal: 25 * SCALE_RATIO_WIDTH_BASIS
      },
      style: {
        backgroundColor: '#fff',
        borderWidth: 0
      },
      activeBackgroundColor: '#C7AE6D',
      activeTintColor: '#fff',
      inactiveTintColor: '#C7AE6D',
      indicatorStyle: {
        backgroundColor: 'transparent'
      }
    }
  }
);

class ExchangeTab extends MyComponent {
  componentDidMount() {
    this.props.loadListEvent();
  }

  render() {
    return (
      <View style={styles.container}>
        <BaseHeader
          noShadow
          styleContent={{ shadowColor: 'transparent', elevation: 0 }}
          children={<Text style={style.titleHeader}>{strings.exchange.toUpperCase()}</Text>}
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
        />
        <TopTab screenProps={this.props.navigation} style={{ flex: 1 }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const mapStateToProps = state => ({});

const mapActionCreators = {
  loadListEvent
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(ExchangeTab);
