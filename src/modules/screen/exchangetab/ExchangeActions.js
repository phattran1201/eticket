import { BASE_URL, LIST_EVENT } from '../../../constants/Constants';
import request from '../../../utils/request';

// export const loadCashHistory = id =>
//   new Promise((resolve, reject) => {
//     request.get(`${BASE_URL}event?fields=["$all"]`).finish((err, res) => {
//       if (err) {
//         reject(err);
//       } else {
//         resolve(res.body.results.objects.rows);
//       }
//     });
//   });
export function loadListEvent(onDone = () => {}) {
  return (dispatch, store) => {
    request.get(`${BASE_URL}event?fields=["$all"]`).finish((err, res) => {
      onDone();
      if (
        res &&
        res.body &&
        res.body.results &&
        res.body.results.objects &&
        res.body.results.objects.rows
      ) {
        dispatch({
          type: LIST_EVENT,
          payload: res.body.results.objects.rows
        });
      } else {
      }
    });
  };
}
