import React from 'react';
import {
  ActivityIndicator,
  Animated,
  Dimensions,
  FlatList,
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { connect } from 'react-redux';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../../../constants/Constants';
import strings from '../../../../constants/Strings';
import { alert } from '../../../../utils/alert';
import CommentModalComponent from '../../../view/CommentModalComponent';
import MyComponent from '../../../view/MyComponent';
import MySpinner from '../../../view/MySpinner';
import NewFeedsItems from '../../../view/NewFeedsItems';
import {
  commentPost,
  deleteComment,
  editComment,
  getListComment,
  getListLikeComment,
  likeComment,
  loadFavouritePostForUser,
  loadListFollowings,
  loadListFriend,
  loadMorePost,
  loadPosts,
  refreshPosts,
  unlikeComment
} from '../../newfeeds/NewFeedsActions';
import {
  loadMoreFavouriteList,
  outOfDataFavouriteList,
  reloadFavouriteList
} from '../FavouriteAction';

const { height, width } = Dimensions.get('window');
const logo = require('../../../../assets/imgs/logo.png');
const iclike = require('../../../../assets/imgs/emotion/ic_like.png');
const ichaha = require('../../../../assets/imgs/emotion/ic_haha.png');
const iclove = require('../../../../assets/imgs/emotion/ic_love.png');
const defaultAvatar = require('../../../../assets/imgs/default_avatar.jpg');

const TYPE = 'POST';
const LIMIT = 5;
class PostComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      outOfData: false,
      refreshing: false,
      flatListNewFeedScrollEnabled: true,
      topAnim: new Animated.Value(height),
      isFilterCommentVisible: false,
      listComment: [],
      currentPostId: '',
      txtComment: '',
      loadingComment: false,
      txtEditComment: '',
      popupCommentLikeAmount: 0,
      listFavPost: []
    };
    this.popupCommentScreen = this.popupCommentScreen.bind(this);
    this.handleAddComment = this.handleAddComment.bind(this);
    // this.onReloadPosts = this.onReloadPosts.bind(this);
  }

  popupCommentScreen(postId, popupCommentLikeAmount) {
    this.setState({
      flatListNewFeedScrollEnabled: false,
      isFilterCommentVisible: true,
      listComment: [],
      currentPostId: postId,
      loadingComment: true,
      popupCommentLikeAmount,
      hideAddCommentTextInput: false
    });
    this.props.getListComment(postId, res =>
      this.setState({
        listComment: res.sort(
          (a, b) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
        ),
        loadingComment: false
      })
    );
    this.popupCommentScreenTimeout = setTimeout(() => {
      Animated.spring(this.state.topAnim, {
        toValue: Platform.OS === 'ios' ? getStatusBarHeight() : 0,
        bounciness: 1,
        duration: 300
      }).start();
    }, 10);
  }

  renderItem = ({ item }) => {
    if (item.post) {
      return (
        <NewFeedsItems
          data={item.post}
          navigation={this.props.navigation}
          currentUserId={this.props.currentUserId}
          // popupCommentScreen={this.popupCommentScreen}
          popupCommentScreen={
            this.commentModalComponent && this.commentModalComponent.popupCommentScreen
              ? this.commentModalComponent.popupCommentScreen
              : null
          }
        />
      );
    }
  };

  renderListCommentsFooterListComment = () => {
    if (this.state.loadingComment) {
      return (
        <View
          style={{
            paddingTop: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
            paddingBottom: 15 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
          }}
        >
          <ActivityIndicator animating size="small" />
        </View>
      );
    }

    return null;
  };

  handleAddComment() {
    if (!this.props.userData) {
      alert(strings.alert, strings.please_login);
      return;
    }
    if (this.state.txtComment.length < 1) {
      alert(strings.alert, strings.comment_must_atleast_1_charactor);
      return;
    }
    MySpinner.show();
    this.props.commentPost(this.state.currentPostId, this.state.txtComment, newComment => {
      if (newComment) {
        this.setState({
          listComment: [newComment, ...this.state.listComment],
          txtComment: ''
        });
        this.cmtFlatList.scrollToIndex({ index: 0 });
      }
      setTimeout(() => {
        MySpinner.hide();
      }, 100);
    });
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    const temp = [];
    this.props.reloadFavouriteList(
      TYPE,
      () => {
        //doFunc
        this.props.listFavouritePost
          .filter(e => e && e.post)
          .forEach((value, index) => {
            if (index < this.props.listFavouritePostPages * LIMIT) {
              if (value) {
                temp.push(value);
              }
            }
          });
      },
      () => {
        //doneFunc
        this.setState({ listFavPost: temp, refreshing: false });
      }
    );
  };

  renderFooter = () => {
    if (!this.props.listFavouritePostOutOfData || this.state.listFavPost.length === 0) {
      return (
        <Text
          style={{
            alignSelf: 'center',
            fontFamily: 'helveticaneue',
            fontSize: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
            fontStyle: 'normal',
            textAlign: 'center',
            marginTop: height / 2 - 25 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
          }}
        >
          {strings.no_data}
        </Text>
      );
    }
    return (
      !this.props.listFavouritePostOutOfData &&
      this.props.listFavPost.length !== 0 &&
      this.state.loading && (
        <View
          style={{
            paddingTop: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
            paddingBottom: 15 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
          }}
        >
          <ActivityIndicator animating size="small" />
        </View>
      )
    );
  };

  componentDidMount() {
    const temp = [];
    if (this.props.listFavouritePostPages === 1) {
      this.props.reloadFavouriteList(
        TYPE,
        () => {
          //doFunc
          this.props.listFavouritePost
            .filter(e => e && e.post)
            .forEach((value, index) => {
              if (index < this.props.listFavouritePostPages * LIMIT) {
                if (value) {
                  temp.push(value);
                }
              }
            });
        },
        () => {
          //doneFunc
          this.setState({ listFavPost: temp });
        }
      );
    } else {
      this.props.listFavouritePost
        .filter(e => e && e.post)
        .forEach((value, index) => {
          if (index < this.props.listFavouritePostPages * LIMIT) {
            if (value) {
              temp.push(value);
            }
          }
        });
      this.setState({ listFavPost: temp });
    }
  }

  handleLoadMore = () => {
    if (this.props.listFavouritePostOutOfData || this.state.loading) return;
    this.setState({ loading: true });
    const temp = [];
    this.props.loadMoreFavouriteList(
      TYPE,
      () => {
        //doFunc
        this.props.listFavouritePost
          .filter(e => e && e.post)
          .forEach((value, index) => {
            if (index < this.props.listFavouritePostPages * LIMIT) {
              if (value) {
                temp.push(value);
              }
            }
          });
      },
      () => {
        //doneFunc
        if (temp.length < this.props.listFavouritePostPages * LIMIT) {
          this.props.outOfDataFavouriteList(TYPE);
        }
        setTimeout(() => {
          this.setState({
            listFavPost: temp,
            loading: false
          });
        });
      }
    );
  };

  render() {
    const listFavPostData = this.props.listFavouritePost.filter(
      e => this.state.listFavPost.includes(e) === true
    );
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          data={listFavPostData}
          keyExtractor={item => item.id}
          renderItem={this.renderItem}
          style={{
            zIndex: 1,
            flex: 1,
            backgroundColor: 'white',
            paddingTop: 6 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
          }}
          initialNumToRender={LIMIT}
          // onEndReached={this.handleLoadMore}
          // onEndReachedThreshold={0.01}
          // refreshing={this.state.refreshing}
          // onRefresh={this.onRefresh}
          ListFooterComponent={this.renderFooter}
        />
        <CommentModalComponent
          ref={ref => {
            if (!ref) {
              return;
            }
            this.commentModalComponent = ref;
            this.commentModalComponent = this.commentModalComponent.getWrappedInstance();
          }}
        />
        {/*

 */}
        {/* <Modal
          transparent
          animationType="fade"
          hardwareAccelerated
          visible={this.state.isFilterCommentVisible}
          onRequestClose={() => { }}
        >
          <Animated.View
            style={{
              zIndex: 100,
              elevation: 100,
              position: 'absolute',
              top: this.state.topAnim,
              width,
              height,
              flex: 1
            }}
          >
            <KeyboardAvoidingView
              style={{ flex: 1, backgroundColor: 'white' }}
              behavior="padding"
              enabled
            >
              <View style={{ flex: 1 }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    borderBottomColor: '#e2ebfc',
                    borderBottomWidth: 0.7
                  }}
                >
                  <View
                    style={{
                      flexDirection: 'row',
                      padding: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                      justifyContent: 'center'
                    }}
                  >
                    {this.state.popupCommentLikeAmount > 0 && (
                      <Image
                        source={iclike}
                        style={{
                          width: 6 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                          height: 6 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                          justifyContent: 'center',
                          alignItems: 'center'
                        }}
                      />
                    )}
                    {this.state.popupCommentLikeAmount > 0 && (
                      <Text style={{ marginLeft: 2 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 }}>
                        {this.state.popupCommentLikeAmount}
                      </Text>
                    )}
                  </View>
                  <STouchableOpacity
                    onPress={() => {
                      this.setState({
                        flatListNewFeedScrollEnabled: true,
                        isFilterCommentVisible: false
                      });
                      Animated.spring(this.state.topAnim, {
                        toValue: height,
                        bounciness: 1,
                        duration: 300
                      }).start();
                    }}
                    style={{ padding: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 }}
                  >
                    <MaterialIcons
                      name="clear"
                      size={8 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224}
                      color="#707070"
                    />
                  </STouchableOpacity>
                </View>
                <FlatList
                  ref={ref => (this.cmtFlatList = ref)}
                  data={this.state.listComment}
                  style={{ flex: 1 }}
                  keyExtractor={item => item.id}
                  ListFooterComponent={this.renderListCommentsFooterListComment}
                  renderItem={({ item }) => {
                    const isLiked = this.props.listLikeCmt.find(
                      e => e.entity_id === item.id
                    );
                    if (item.isEdditing) {
                      return (
                        <View
                          style={{
                            marginTop: 5 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                            marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                            flexDirection: 'row'
                          }}
                        >
                          <STouchableOpacity
                            style={{ paddingTop: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224, paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 }}
                            onPress={() => {
                              this.setState({
                                flatListNewFeedScrollEnabled: true,
                                isFilterCommentVisible: false
                              });
                              Animated.spring(this.state.topAnim, {
                                toValue: height,
                                bounciness: 1,
                                duration: 300
                              }).start();
                              this.props.navigation.navigate(ROUTE_KEY.OTHER_PROFILE_COMPONENT, {
                                user: item.user
                              });
                            }}
                          >
                            <AvatarImage
                              userType={item.user && item.user.user_type ? item.user.user_type : USER_TYPE_NORMAL}
                              source={
                                item.user && item.user.avatar
                                  ? { uri: item.user.avatar }
                                  : defaultAvatar
                              }
                              style={{
                                width: 14 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                                height: 14 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                                borderRadius: 7 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                              }}
                            />
                          </STouchableOpacity>
                          <View style={{ marginLeft: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 }}>
                            <View style={{ flexDirection: 'row' }}>
                              <View
                                style={{
                                  backgroundColor: '#EFF1F3',
                                  borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                                  padding: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                                  maxWidth: width - 50 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224
                                }}
                              >
                                <Text
                                  style={{
                                    fontFamily: 'helveticaneue',
                                    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 * 1.1,
                                    fontWeight: 'bold'
                                  }}
                                >
                                  {item.user && item.user.nickname
                                    ? item.user.nickname
                                    : ''}
                                </Text>
                                <TextInput
                                  ref={ref => {
                                    this.textEditComment = ref;
                                  }}
                                  placeholder={strings.edit_comment}
                                  placeholderTextColor="gray"
                                  underlineColorAndroid="transparent"
                                  autoCapitalize="none"
                                  multiline
                                  // autoFocus
                                  style={{
                                    margin: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                                    marginBottom: 0,
                                    padding: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                                    color: 'black',
                                    fontFamily: 'helveticaneue',
                                    fontSize: 5 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                                    borderColor: '#e3e9f2',
                                    borderWidth: 0.9,
                                    backgroundColor: '#f9f9f9',
                                    borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    flex: 1,
                                    maxHeight: 50 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224
                                  }}
                                  value={this.state.txtEditComment}
                                  onChangeText={txtEditComment =>
                                    this.setState({ txtEditComment })
                                  }
                                />
                              </View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                              <STouchableOpacity
                                style={{ padding: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 }}
                                onPress={() => {
                                  this.setState({
                                    hideAddCommentTextInput: false,
                                    txtEditComment: '',
                                    listComment: this.state.listComment.map(
                                      e => {
                                        if (e.id !== item.id) return e;
                                        return {
                                          ...e,
                                          isEdditing: false
                                        };
                                      }
                                    )
                                  });
                                }}
                              >
                                <Text
                                  style={{
                                    fontFamily: 'helveticaneue',
                                    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 * 1.1,
                                    fontWeight: 'bold',
                                    color: '#ea5241'
                                  }}
                                >
                                  Huỷ
                                </Text>
                              </STouchableOpacity>
                              <STouchableOpacity
                                style={{ padding: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 }}
                                onPress={() => {
                                  if (this.state.txtEditComment.length < 1) {
                                    alert(
                                      strings.alert,
                                      strings.comment_must_atleast_1_charactor
                                    );
                                    return;
                                  }
                                  MySpinner.show();
                                  this.props.editComment(
                                    this.state.currentPostId,
                                    item.id,
                                    this.state.txtEditComment,
                                    () => {
                                      this.setState({
                                        hideAddCommentTextInput: false,
                                        listComment: this.state.listComment.map(
                                          e => {
                                            if (e.id !== item.id) return e;
                                            return {
                                              ...e,
                                              isEdditing: false,
                                              content: this.state.txtEditComment
                                            };
                                          }
                                        )
                                      });
                                      MySpinner.hide();
                                    }
                                  );
                                }}
                              >
                                <Text
                                  style={{
                                    fontFamily: 'helveticaneue',
                                    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 * 1.1,
                                    fontWeight: 'bold',
                                    color: '#416eea'
                                  }}
                                >
                                  {strings.button_edit}
                                </Text>
                              </STouchableOpacity>
                            </View>
                          </View>
                        </View>
                      );
                    }
                    return (
                      <View
                        style={{
                          marginTop: 5 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                          marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                          flexDirection: 'row'
                        }}
                      >
                        <STouchableOpacity
                          style={{ paddingTop: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224, paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 }}
                          onPress={() => {
                            this.setState({
                              flatListNewFeedScrollEnabled: true,
                              isFilterCommentVisible: false
                            });
                            Animated.spring(this.state.topAnim, {
                              toValue: height,
                              bounciness: 1,
                              duration: 300
                            }).start();
                            this.props.navigation.navigate(ROUTE_KEY.OTHER_PROFILE_COMPONENT, {
                              user: item.user
                            });
                          }}
                        >
                          <AvatarImage
                            userType={item.user && item.user.user_type ? item.user.user_type : USER_TYPE_NORMAL}
                            source={
                              item.user && item.user.avatar
                                ? { uri: item.user.avatar }
                                : defaultAvatar
                            }
                            style={{
                              width: 14 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                              height: 14 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                              borderRadius: 7 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224
                            }}
                          />
                        </STouchableOpacity>
                        <View style={{ marginLeft: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 }}>
                          <View style={{ flexDirection: 'row' }}>
                            <View
                              style={{
                                backgroundColor: '#EFF1F3',
                                borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                                padding: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224,
                                maxWidth: width - 50 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224
                              }}
                            >
                              <Text
                                style={{
                                  fontFamily: 'helveticaneue',
                                  fontSize: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 * 1.1,
                                  fontWeight: 'bold'
                                }}
                              >
                                {item.user && item.user.nickname
                                  ? item.user.nickname
                                  : ''}
                              </Text>
                              <Text
                                style={{
                                  fontFamily: 'helveticaneue',
                                  fontSize: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 * 1.1
                                }}
                              >
                                {item.content ? item.content : ''}{' '}
                              </Text>
                            </View>
                            {this.props.userData &&
                              this.props.userData.id === item.user.id && (
                                <View
                                  style={{
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                  }}
                                >
                                  <Dropdown
                                    data={[
                                      {
                                        value: strings.button_edit,
                                        onSelect: onDone => {
                                          this.setState({
                                            txtEditComment: item.content,
                                            listComment: this.state.listComment.map(
                                              e => {
                                                if (e.id !== item.id) return e;
                                                return {
                                                  ...e,
                                                  isEdditing: true
                                                };
                                              }
                                            ),
                                            hideAddCommentTextInput: true
                                          });
                                          this.textEditComment.focus();
                                        }
                                      },
                                      {
                                        value: 'Xoá',
                                        onSelect: onDone => {
                                          setTimeout(() => {
                                            Alert.alert(
                                              strings.alert,
                                              strings.delete_comment_alert,
                                              [
                                                {
                                                  text: strings.back,
                                                  onPress: () => { },
                                                  style: 'cancel'
                                                },
                                                {
                                                  text: strings.delete,
                                                  onPress: () => {
                                                    MySpinner.show();
                                                    this.props.deleteComment(
                                                      this.state.currentPostId,
                                                      item.id,
                                                      isSuccess => {
                                                        if (isSuccess) {
                                                          this.setState({
                                                            listComment: this.state.listComment.filter(
                                                              e =>
                                                                e.id !== item.id
                                                            ),

                                                          });
                                                          onDone();
                                                          MySpinner.hide();
                                                        }
                                                        MySpinner.hide();
                                                      }
                                                    );
                                                  }
                                                }
                                              ],
                                              {
                                                cancelable: true
                                              }
                                            );
                                          }, 300);
                                        }
                                      }
                                    ]}
                                  >
                                    <MaterialIcons
                                      name="more-horiz"
                                      size={10 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224}
                                      color="#555667"
                                    />
                                  </Dropdown>
                                </View>
                              )}
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center'
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: 'helveticaneue',
                                fontSize: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 * 1.1
                              }}
                            >
                              {item.created_at
                                ? formatDate(item.created_at)
                                : ''}
                            </Text>
                            <STouchableOpacity
                              style={{ padding: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 }}
                              onPress={() => {
                                if (!this.props.userData) {
                                  alert(strings.alert, strings.please_login);
                                  return;
                                }
                                if (isLiked) {
                                  this.props.unlikeComment(item.id);
                                } else {
                                  this.props.likeComment(item.id);
                                }
                              }}
                            >
                              <Text
                                style={{
                                  fontFamily: 'helveticaneue',
                                  fontSize: 4 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224 * 1.1,
                                  fontWeight: 'bold',
                                  color: isLiked ? '#477bdd' : 'gray'
                                }}
                              >
                                {isLiked ? 'Đã thích' : 'Thích'}
                              </Text>
                            </STouchableOpacity>
                          </View>
                        </View>
                      </View>
                    );
                  }}
                />
              </View>
              {!this.state.hideAddCommentTextInput && <View
                style={{
                  flexDirection: 'row',
                  marginBottom: 6 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224
                }}
              >
                <TextInput
                  ref={ref => {
                    this.textInutComment = ref;
                  }}
                  placeholder={strings.write_comment}
                  placeholderTextColor="gray"
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  multiline
                  onSubmitEditing={() => this.handleAddComment()}
                  style={styles.textInput}
                  value={this.state.txtComment}
                  onChangeText={txtComment => this.setState({ txtComment })}
                />
                <STouchableOpacity
                  style={{
                    justifyContent: 'center',
                    padding: 3 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224
                  }}
                  onPress={() => this.handleAddComment()}
                >
                  <MaterialIcons
                    name="send"
                    size={8 * SCALE_RATIO_WIDTH_BASIS -  DEVICE_HEIGHT / 224}
                    color="#707070"
                  />
                </STouchableOpacity>
              </View>
              }
            </KeyboardAvoidingView>
          </Animated.View>
        </Modal> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue',
    fontSize: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: 'bold',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  },
  textInput: {
    margin: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginBottom: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    padding: 3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    color: 'black',
    fontFamily: 'helveticaneue',
    fontSize: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderColor: '#e3e9f2',
    borderWidth: 0.9,
    backgroundColor: '#f9f9f9',
    borderRadius: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    maxHeight: 50 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  }
});

const mapStateToProps = state => ({
  listFavouritePost: state.profilePost.listFavouritePost,
  listFavouritePostPages: state.profilePost.listFavouritePostPages,
  listFavouritePostOutOfData: state.profilePost.listFavouritePostOutOfData,
  currentUserId: state.user.userData.id,
  // listPosts: state.post.listPosts,
  userData: state.user.userData,
  listLikeCmt: state.user.listLikeCmt,
  listFriend: state.user.listFriend
});

const mapActionCreators = {
  loadMorePost,
  refreshPosts,
  getListComment,
  commentPost,
  deleteComment,
  editComment,
  getListLikeComment,
  likeComment,
  unlikeComment,
  loadPosts,
  loadListFriend,
  loadListFollowings,
  loadFavouritePostForUser,

  reloadFavouriteList,
  loadMoreFavouriteList,
  outOfDataFavouriteList
};

export default connect(
  mapStateToProps,
  mapActionCreators,
  null,
  { withRef: true }
)(PostComponent);
