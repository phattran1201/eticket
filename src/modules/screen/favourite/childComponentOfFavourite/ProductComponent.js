import React from 'react';
import { ActivityIndicator, Dimensions, FlatList, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../../../constants/Constants';
import strings from '../../../../constants/Strings';
import ListProductsItem from '../../../view/ListProductsItem';
import MyComponent from '../../../view/MyComponent';
import {
  loadMoreFavouriteList,
  outOfDataFavouriteList,
  reloadFavouriteList
} from '../FavouriteAction';

const { height, width } = Dimensions.get('window');

const TYPE = 'PRODUCT';
const LIMIT = 5;
class ProductComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      listFavProduct: []
    };
  }
  renderItem = ({ item }) => {
    const newItem = { ...item, product: { ...item.product, user: item.user } };
    //Used for Share feature, it requires a user object inside product object
    return (
      <ListProductsItem
        id={item && item.product ? item.product.id : ''}
        data={newItem.product}
        navigation={this.props.navigation}
      />
    );
  };

  componentDidMount() {
    const temp = [];
    if (this.props.listFavouriteProductPages === 1) {
      this.props.reloadFavouriteList(
        TYPE,
        () => {
          //doFunc
          this.props.listFavouriteProduct
            .filter(e => e && e.product)
            .forEach((value, index) => {
              if (index < this.props.listFavouriteProductPages * LIMIT) {
                if (value) {
                  temp.push(value);
                }
              }
            });
        },
        () => {
          //doneFunc
          this.setState({ listFavProduct: temp });
        }
      );
    } else {
      this.props.listFavouriteProduct
        .filter(e => e && e.product)
        .forEach((value, index) => {
          if (index < this.props.listFavouriteProductPages * LIMIT) {
            if (value) {
              temp.push(value);
            }
          }
        });
      this.setState({ listFavProduct: temp });
    }
  }

  handleLoadMore = () => {
    if (this.props.listFavouriteProductOutOfData || this.state.loading) return;
    this.setState({ loading: true });
    const temp = [];
    this.props.loadMoreFavouriteList(
      TYPE,
      () => {
        //doFunc
        this.props.listFavouriteProduct
          .filter(e => e && e.product)
          .forEach((value, index) => {
            if (index < this.props.listFavouriteProductPages * LIMIT) {
              if (value) {
                temp.push(value);
              }
            }
          });
      },
      () => {
        //doneFunc
        if (temp.length < this.props.listFavouriteProductPages * LIMIT) {
          this.props.outOfDataFavouriteList(TYPE);
        }
        setTimeout(() => {
          this.setState({
            listFavProduct: temp,
            loading: false
          });
        });
      }
    );
  };

  onRefresh = () => {
    this.setState({ refreshing: true });
    const temp = [];
    this.props.reloadFavouriteList(
      TYPE,
      () => {
        //doFunc
        this.props.listFavouriteProduct
          .filter(e => e && e.product)
          .forEach((value, index) => {
            if (index < this.props.listFavouriteProductPages * LIMIT) {
              if (value) {
                temp.push(value);
              }
            }
          });
      },
      () => {
        //doneFunc
        this.setState({ listFavProduct: temp, refreshing: false });
      }
    );
  };

  renderFooter = () => {
    if (!this.props.listFavouriteProductOutOfData || this.state.listFavProduct.length === 0) {
      return (
        <Text
          style={{
            alignSelf: 'center',
            fontFamily: 'helveticaneue',
            fontSize: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
            fontStyle: 'normal',
            textAlign: 'center',
            marginTop: height / 2 - 25 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
          }}
        >
          {strings.no_data}
        </Text>
      );
    }
    return (
      !this.props.listFavouriteProductOutOfData &&
      this.props.listFavProduct.length !== 0 &&
      this.state.loading && (
        <View
          style={{
            paddingTop: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
            paddingBottom: 15 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
          }}
        >
          <ActivityIndicator animating size="small" />
        </View>
      )
    );
  };

  render() {
    const listFavProductData = this.props.listFavouriteProduct.filter(
      e => this.state.listFavProduct.includes(e) === true
    );
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          data={listFavProductData}
          keyExtractor={item => item.id}
          renderItem={this.renderItem}
          style={{ zIndex: 1, flex: 1 }}
          initialNumToRender={LIMIT}
          // onEndReached={this.handleLoadMore}
          // onEndReachedThreshold={0.01}
          // refreshing={this.state.refreshing}
          // onRefresh={this.onRefresh}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  listFavouriteProduct: state.profileProduct.listFavouriteProduct,
  listFavouriteProductPages: state.profileProduct.listFavouriteProductPages,
  listFavouriteProductOutOfData: state.profileProduct.listFavouriteProductOutOfData
});

const mapActionCreators = {
  reloadFavouriteList,
  loadMoreFavouriteList,
  outOfDataFavouriteList
};

export default connect(
  mapStateToProps,
  mapActionCreators,
  null,
  { withRef: true }
)(ProductComponent);
