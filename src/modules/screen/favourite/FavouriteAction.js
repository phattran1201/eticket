import {
    BASE_URL,
    BILL_API,
    LOAD_TRANSACTION_LIST,
    LOAD_TRANSACTION_LIST_SELL,
    LOAD_TRANSACTION_LIST_BUY,
    LOAD_MORE_TRANSACTION_LIST_SELL,
    LOAD_MORE_TRANSACTION_LIST_BUY,
    OUT_OF_DATA_TRANSACTION_LIST_SELL,
    OUT_OF_DATA_TRANSACTION_LIST_BUY,
    REFRESH_LIST_FAVOURITE_POST,
    REFRESH_LIST_FAVOURITE_PRODUCT,
    UPDATE_LIST_FAVOURITE_POST_PAGES,
    UPDATE_LIST_FAVOURITE_PRODUCT_PAGES,
    OUT_OF_DATA_LIST_FAVOURITE_POST_PAGES,
    OUT_OF_DATA_LIST_FAVOURITE_PRODUCT_PAGES
} from '../../../constants/Constants';
import request from '../../../utils/request';

const LIMIT = 3;

export function reloadFavouriteList(type, onDoFunc = () => {}, onDoneFunc = () => { }) {
    if (type === 'POST') {
        return (dispatch) => {
            dispatch({
                type: REFRESH_LIST_FAVOURITE_POST,
            });
            setTimeout(() => {
                onDoFunc();
                onDoneFunc();
            });
        };
    } else if (type === 'PRODUCT') {
        return (dispatch) => {
            dispatch({
                type: REFRESH_LIST_FAVOURITE_PRODUCT,
            });
            setTimeout(() => {
                onDoFunc();
                onDoneFunc();
            });
        };
    }
}

export function loadMoreFavouriteList(type, onDoFunc = () => {}, onDoneFunc = () => { }) {
    if (type === 'POST') {
        return (dispatch) => {
            dispatch({
                type: UPDATE_LIST_FAVOURITE_POST_PAGES
            });
            setTimeout(() => {
                onDoFunc();
                onDoneFunc();
            });
        };
    } else if (type === 'PRODUCT') {
        return (dispatch) => {
            dispatch({
                type: UPDATE_LIST_FAVOURITE_PRODUCT_PAGES
            });
            setTimeout(() => {
                onDoFunc();
                onDoneFunc();
            });
        };
    }
}

export function outOfDataFavouriteList(type, onDoFunc = () => {}, onDoneFunc = () => { }) {
    if (type === 'POST') {
        return (dispatch) => {
            dispatch({
                type: OUT_OF_DATA_LIST_FAVOURITE_POST_PAGES
            });
            setTimeout(() => {
                onDoFunc();
                onDoneFunc();
            });
        };
    } else if (type === 'PRODUCT') {
        return (dispatch) => {
            dispatch({
                type: OUT_OF_DATA_LIST_FAVOURITE_PRODUCT_PAGES
            });
            setTimeout(() => {
                onDoFunc();
                onDoneFunc();
            });
        };
    }
}
