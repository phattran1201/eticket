import React from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
  Animated,
  Dimensions,
  TextInput
} from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import FastImage from 'react-native-fast-image';
import Feather from 'react-native-vector-icons/dist/Feather';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import { connect } from 'react-redux';
import { DEVICE_WIDTH, DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS, ROUTE_KEY, FS } from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style, { APP_COLOR, FONT } from '../../../constants/style';
import { alert } from '../../../utils/alert';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import { getNumberWithCommas } from '../../../utils/numberUtils';
import PopupNotification from '../../view/PopupNotification';
import PopupDetailProduct from '../../view/PopupDetailProduct';
import { SwipeListView } from 'react-native-swipe-list-view';

class MyProductItem extends React.Component {
  render() {
    const { item } = this.props;
    return (
      <TouchableOpacity onPress={this.props.onPress} style={[style.shadow, styles.productContainer]} activeOpacity={1}>
        <View style={styles.productItemLeftContainer}>
          <FastImage
            style={{
              borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
              width: 70 * SCALE_RATIO_WIDTH_BASIS,
              height: 70 * SCALE_RATIO_WIDTH_BASIS
            }}
            resizeMode={FastImage.resizeMode.contain}
            source={{ uri: item.backgroundImage }}
          />
        </View>
        <View style={styles.productItemMiddleContainer}>
          <Text numberOfLines={1} style={style.text}>
            {item.name}
          </Text>
          <Text style={[style.titleHeader, { fontSize: FS(14), textAlign: 'right', color: APP_COLOR }]}>
            {getNumberWithCommas(item.finalPrice)}
            {'   '}
            <Text
              style={[
                style.text,
                {
                  fontSize: FS(10),
                  textDecorationLine: 'line-through',
                  fontFamily: FONT.Light
                }
              ]}
            >
              {item.discount > 0 ? getNumberWithCommas(item.price) : ''}
            </Text>
          </Text>
        </View>
        <View style={styles.productItemRightContainer}>
          <AntDesign name="heart" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#D3B574" />
        </View>
      </TouchableOpacity>
    );
  }
}

class FavouriteComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.totalCostWithDiscount = 0;
    this.totalCostWithoutDiscount = 0;
    this.state = {
      activeSections: [0],
      data: [
        {
          id: 0,
          index: 0,
          name: 'Milk tea',
          products: [
            {
              id: 0,
              name: 'Trà sữa Panda',
              price: 42000,
              discount: 7,
              finalPrice: 39000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/20177316527-tra-sua-tran-chau-soi.jpg'
            },
            {
              id: 1,
              name: 'Hồng trà việt quất',
              price: 38000,
              discount: 0,
              finalPrice: 38000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/201773165018-hong-tra-viet-quat.jpg'
            },
            {
              id: 2,
              name: 'Trà sữa kim cương đen Okinawa',
              price: 46000,
              discount: 0,
              finalPrice: 46000,
              backgroundImage:
                'https://images.foody.vn/res/g28/274442/s120x120/201773165348-tra-sua-kim-cuong-den-okinawa.jpg'
            },
            {
              id: 3,
              name: 'Trà sữa trân châu sợi',
              price: 45000,
              discount: 0,
              finalPrice: 45000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/20177316527-tra-sua-tran-chau-soi.jpg'
            },
            {
              id: 4,
              name: 'Trà sữa ba anh em',
              price: 48000,
              discount: 50,
              finalPrice: 24000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/201773165440-tra-sua-ba-anh-em.jpg'
            },
            {
              id: 5,
              name: 'Trà sữa matcha',
              price: 40000,
              discount: 0,
              finalPrice: 40000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/20171024144955-tra-sua-matcha.jpg'
            },
            {
              id: 6,
              name: 'Trà sữa bánh pudding',
              price: 45000,
              discount: 20,
              finalPrice: 36000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/201773165532-tra-sua-banh-pudding.jpg'
            },
            {
              id: 7,
              name: 'Sữa tươi trân châu đường hổ',
              price: 49000,
              discount: 0,
              finalPrice: 49000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/201891194154-st.jpg'
            }
          ]
        },
        {
          id: 1,
          index: 1,
          name: 'Fresh fruit tea',
          products: [
            {
              id: 0,
              name: 'Trà sữa Panda 2',
              price: 82000,
              discount: 7,
              finalPrice: 39000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/20177316527-tra-sua-tran-chau-soi.jpg'
            },
            {
              id: 1,
              name: 'Hồng trà việt quất 2',
              price: 76000,
              discount: 0,
              finalPrice: 38000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/201773165018-hong-tra-viet-quat.jpg'
            }
          ]
        }
      ],
      selectedItem: []
    };
  }

  renderSectionTitle = section => <Text>{section.name}</Text>;

  renderHeader = (section, index, isActive) => (
    <View
      style={{
        width: '100%',
        flexDirection: 'row',
        marginVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
        paddingLeft: 20 * SCALE_RATIO_WIDTH_BASIS
      }}
    >
      <View style={{ flex: 1 }}>
        <Text style={{ color: '#373737', fontSize: 16 * SCALE_RATIO_WIDTH_BASIS, fontWeight: 'bold' }}>
          {section.name}
        </Text>
      </View>
      <View
        style={{
          width: 30 * SCALE_RATIO_WIDTH_BASIS,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        {isActive ? (
          <Feather name="chevron-down" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#ABABAB" />
        ) : (
          <Feather name="chevron-right" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#ABABAB" />
        )}
      </View>
    </View>
  );

  renderItem = ({ item }) => (
    <MyProductItem
      item={item}
      style={{ overflow: 'visible' }}
      onPress={() => this.props.navigation.navigate(ROUTE_KEY.DETAIL_PRODUCT)}
    />
  );

  deleteRow(rowId) {
    console.log('dauphaiphat: FavouriteComponent -> deleteRow -> log', rowId.item.name);
  }

  renderContent = section => (
    <SwipeListView
      useFlatList
      data={section.products}
      renderItem={this.renderItem}
      renderHiddenItem={data => (
        <TouchableOpacity
          style={{
            alignItems: 'center',
            bottom: 0,
            justifyContent: 'center',
            position: 'absolute',
            top: 0,
            width: 40,
            right: 0
          }}
          onPress={() => this.deleteRow(data)}
        >
          <Feather name="trash" size={30 * SCALE_RATIO_WIDTH_BASIS} color="#EB5F5F" />
        </TouchableOpacity>
      )}
      disableRightSwipe
      stopRightSwipe={-40}
      rightOpenValue={-40}
      previewOpenValue={-40}
      previewOpenDelay={-10}
    />
  );

  updateSections = sections => {
    this.setState({
      activeSections: sections.includes(undefined) ? [] : sections
    });
  };
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <BaseHeader
          noShadow
          children={
            <TextInput
              style={[
                style.textCaption,
                {
                  width: (DEVICE_WIDTH * 75) / 100,
                  borderRadius: 30 * SCALE_RATIO_WIDTH_BASIS,
                  backgroundColor: '#fbfbfb',
                  paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
                  paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
                  fontSize: FS(14),
                  fontFamily: 'FontAwesome'
                }
              ]}
              ref="searchBar"
              clearButtonMode="always"
              placeholder=" Tìm kiếm sản phẩm"
              placeholderTextColor="#00000050"
              underlineColorAndroid="transparent"
              selectionColor="#C7AE6D"
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={this.onSearchBarChangeText}
            />
          }
          style={{ zIndex: 99, backgroundColor: '#fff' }}
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
          rightIcon="group-add"
          rightIconType="MaterialIcons"
          onRightPress={() => alert(strings.alert, strings.this_feature_is_in_development)}
        />
        <ScrollView style={{ flex: 1 }}>
          <View style={styles.contentContainer}>
            <Accordion
              duration={100}
              underlayColor={'#fff'}
              activeSections={this.state.activeSections}
              expandMultiple
              sections={this.state.data}
              renderHeader={this.renderHeader}
              renderContent={this.renderContent}
              onChange={this.updateSections}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const mapActionCreators = {};

export default connect(
  mapStateToProps,
  mapActionCreators,
  null,
  { withRef: true }
)(FavouriteComponent);

const styles = StyleSheet.create({
  paymentButtonStyle: {
    width: 140 * SCALE_RATIO_WIDTH_BASIS,
    height: 40 * SCALE_RATIO_WIDTH_BASIS,
    backgroundColor: '#D3B574',
    borderRadius: 20 * SCALE_RATIO_WIDTH_BASIS,
    justifyContent: 'center',
    alignItems: 'center'
  },
  productItemRightContainer: {
    paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
    // width: 50 * SCALE_RATIO_WIDTH_BASIS,
    justifyContent: 'center',
    alignItems: 'center'
  },
  productItemMiddleContainer: {
    paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  productItemLeftContainer: { paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS },
  productContainer: {
    marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS,
    marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
    // padding: 10 * SCALE_RATIO_WIDTH_BASIS,
    paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center'
  },
  paymentButtonContainer: {
    paddingVertical: 20 * SCALE_RATIO_WIDTH_BASIS,
    width: 150 * SCALE_RATIO_WIDTH_BASIS,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  contentContainer: {
    flex: 1,
    paddingRight: 20 * SCALE_RATIO_WIDTH_BASIS,
    backgroundColor: '#fff'
  },
  cartContainer: {
    backgroundColor: '#ffffff',
    position: 'absolute',
    height: 100 * SCALE_RATIO_WIDTH_BASIS,
    padding: 20 * SCALE_RATIO_WIDTH_BASIS,
    bottom: 10 * SCALE_RATIO_WIDTH_BASIS,
    width: DEVICE_WIDTH
  }
});
