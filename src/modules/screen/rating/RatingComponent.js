import LottieView from 'lottie-react-native';
import React from 'react';
import { Alert, StyleSheet, Text, View } from 'react-native';
import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';
import { DEVICE_HEIGHT, DEVICE_WIDTH, SCALE_RATIO_WIDTH_BASIS } from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import MySpinner from '../../view/MySpinner';
import PartnerInRatingPageListItem from '../../view/PartnerInRatingPageListItem';
import { loadListRating, rateFunction } from './RatingActions';

class RatingComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      showCompleteAnimation: false
    };
    this.page = 1;
    this.outOfData = false;
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    this.props.loadListRating(1, () => {
      this.outOfData = true;
    });
  }

  loadMore() {
    if (this.outOfData) return;
    this.setState({ showCompleteAnimation: true });
    this.timeOut = setTimeout(() => this.setState({ showCompleteAnimation: false }), 3000);
    this.page++;
    this.props.loadListRating(this.page, () => {
      this.outOfData = true;
    });
  }

  componentWillUnmount() {
    clearTimeout(this.timeOut);
  }

  render() {
    const { listRating } = this.props;
    return (
      <View style={styles.container}>
        <BaseHeader
          children={<Text style={style.titleHeader}>{strings.rating.toUpperCase()}</Text>}
          rightIcon="envelope"
          navigation={this.props.navigation}
        />
        {this.state.showCompleteAnimation ? (
          <View
            style={{
              position: 'absolute',
              width: DEVICE_WIDTH,
              height: DEVICE_HEIGHT,
              justifyContent: 'center',
              alignItems: 'center',
              zIndex: 999,
              elevation: 2
            }}
          >
            <LottieView
              source={require('../../../assets/rating_complete.json')}
              autoPlay
              loop
              hardwareAccelerationAndroid
              style={{
                width: 241 * SCALE_RATIO_WIDTH_BASIS,
                height: 236 * SCALE_RATIO_WIDTH_BASIS,
                alignSelf: 'center'
              }}
              resizeMode="cover"
            />
          </View>
        ) : (
          <View />
        )}
        <View style={{ flex: 1 }}>
          {listRating.length > 0 ? (
            <Swiper showsPagination={false}>
              {listRating
                .filter(e => {
                  const isBlocked = this.props.listBlocked.find(i => i.receiver.id === e.id);
                  return !isBlocked;
                })
                .map(e => (
                  <PartnerInRatingPageListItem
                    key={e.id}
                    item={e.user}
                    navigation={this.props.navigation}
                    onHideItem={rate => {
                      Alert.alert(
                        strings.alert,
                        `${strings.confirm_rating} ${rate}`,
                        [
                          { text: strings.no, onPress: () => {} },
                          {
                            text: strings.confirm_rating,
                            onPress: () => {
                              MySpinner.show();
                              this.props.rateFunction(
                                e.id,
                                rate,
                                MySpinner.hide,
                                listRating.length === 1
                                  ? this.loadMore
                                  : () => {
                                      this.setState({ showCompleteAnimation: true });
                                      this.timeOut = setTimeout(
                                        () => this.setState({ showCompleteAnimation: false }),
                                        2000
                                      );
                                    }
                              );
                            }
                          }
                        ],
                        { cancelable: true }
                      );
                    }}
                  />
                ))}
            </Swiper>
          ) : (
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
              <LottieView
                source={require('../../../assets/isempty.json')}
                autoPlay
                loop
                hardwareAccelerationAndroid
                style={{
                  width: 241 * SCALE_RATIO_WIDTH_BASIS,
                  height: 236 * SCALE_RATIO_WIDTH_BASIS,
                  alignSelf: 'center'
                }}
                resizeMode="cover"
              />
              <Text style={[style.text, { textAlign: 'center' }]}>
                You can rate for friends that you have called as a gift to them
              </Text>
              {/* <MyButton
                outline
                isDisabled
                style={{
                  marginHorizontal: 51 * SCALE_RATIO_WIDTH_BASIS,
                  marginTop: 51 * SCALE_RATIO_WIDTH_BASIS
                }}
                textStyle={{ fontSize: FS(16) }}
              >
                {strings.rating_complete}
              </MyButton> */}
            </View>
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  listRating: state.rating.listRating,
  listBlocked: state.blocking.listBlocked
});

const mapActionCreators = {
  loadListRating,
  rateFunction
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(RatingComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  titleStyle: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue-Bold',
    fontSize: 18 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  }
});
