import { Alert } from 'react-native';
import request from '../../../utils/request';
import {
  BASE_URL, RATE_API,
  UPDATE_LIST_RATING,
  REMOVE_ITEM_LIST_RATING
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';

export function loadListRating(page, onOutOfData = () => { }) {
  return (dispatch, store) => {
    const { id } = store().user.userData;

    const filter = `filter={"rater_user_id": "${id}", "type": "PENDING"}&limit=10&page=${page}`;

    request.get(`${BASE_URL}${RATE_API}?fields=["$all", {"user": ["$all"]}]&${filter}`)
      .finish((err, res) => {
        if (!err && res &&
          res.body &&
          res.body.results &&
          res.body.results.objects &&
          res.body.results.objects.rows
        ) {
          if (res.body.results.objects.rows.length === 0) onOutOfData();
          dispatch({
            type: UPDATE_LIST_RATING,
            payload: res.body.results.objects.rows
          });
        }
      });
  };
}

export function rateFunction(id, rate, onFinish = () => { }, onReload = () => { }) {
  return (dispatch, store) => {
    request.put(`${BASE_URL}${RATE_API}/${id}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send({
        rate,
        type: 'DONE'
      })
      .finish((err, res) => {
        onFinish();
        if (!err && res &&
          res.body &&
          res.body.code === 200
        ) {
          onReload();
          dispatch({
            type: REMOVE_ITEM_LIST_RATING,
            payload: id
          });
        } else {
          Alert.alert(
            strings.alert,
            strings.rating_fail,
            [
              { text: strings.yes, onPress: () => { } },
            ],
            { cancelable: true }
          );
        }
      });
  };
}

export function checkRatingExist({ rater, receiver }) {
  return new Promise((resolve, reject) => {
    const filter = `filter={"rater_user_id": "${rater}", "user_id": "${receiver}"}`;
    request.get(`${BASE_URL}${RATE_API}?fields=["$all"]&${filter}`)
      .finish((err, res) => {
        if (!err && res &&
          res.body &&
          res.body.results &&
          res.body.results.objects &&
          res.body.results.objects.rows
        ) {
          resolve(res.body.results.objects.rows.length > 0);
        } else reject();
      });
  });
}

export function createRating(receiver) {
  return (dispatch, store) => {
    const { user, rating } = store();

    request.post(`${BASE_URL}${RATE_API}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${user.token}`)
      .send({
        rater_user_id: user.userData.id,
        user_id: receiver.id,
        rate: 0,
        content: '...'
      })
      .finish((err, res) => {
        if (!err && res &&
          res.body &&
          res.body.code === 200
        ) {
          dispatch({
            type: UPDATE_LIST_RATING,
            payload: [...rating.listRating, { ...res.body.results.object, user: receiver }]
          });
        }
      });
  };
}
