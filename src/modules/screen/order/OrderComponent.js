import React from 'react';
import { FlatList, Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Carousel from 'react-native-snap-carousel';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import {
  DEVICE_WIDTH,
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  TYPE_TOP_10_RANK,
  ROUTE_KEY
} from '../../../constants/Constants';
import { DATA_STORE, DATA_TEST, DATA_MILK_TEA } from '../../../constants/dataTest';
import strings from '../../../constants/Strings';
import style, { APP_COLOR, APP_COLOR_TEXT, APP_COLOR_BLUE, FONT } from '../../../constants/style';
import { alert } from '../../../utils/alert';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import TabBar from '../../../style/Tabbar/MyTabbar';
import StoreComponent from '../store/StoreComponent';
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';
import { getNumberWithCommas } from '../../../utils/numberUtils';
import MyButton from '../../../style/MyButton';
import LinearGradient from 'react-native-linear-gradient';
import CustomTabBar from '../../../style/Tabbar/CustomTabBar';
import HeaderWithBackButtonComponent from '../../view/HeaderWithBackButtonComponent';

class OrderComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      showAllItem: false
    };
    this.store = DATA_STORE;
    this.data = DATA_TEST;
    this.orderCode = '#123456';
  }

  componentDidMount() {}
  renderItem = label => (
    <View
      tabLabel={label}
      style={[
        style.shadow,
        {
          // marginHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
          marginTop: 10 * SCALE_RATIO_WIDTH_BASIS
        }
      ]}
    >
      <View
        style={{
          // borderRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
          backgroundColor: '#fff',
          padding: 10 * SCALE_RATIO_WIDTH_BASIS
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
            borderBottomColor: APP_COLOR,
            paddingBottom: 5 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          <Image
            style={{
              width: 30 * SCALE_RATIO_WIDTH_BASIS,
              height: 30 * SCALE_RATIO_WIDTH_BASIS,
              justifyContent: 'center',
              alignSelf: 'center'
            }}
            resizeMode="contain"
            source={require('../../../assets/imgs/icons/confirmation.png')}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              flex: 1,
              marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <View style={{ justifyContent: 'flex-start' }}>
              <Text style={[style.text, { fontSize: FS(16), color: APP_COLOR, textAlign: 'left' }]}>
                {this.orderCode}
              </Text>
              <Text
                style={[
                  style.text,
                  {
                    fontSize: FS(12),
                    textAlign: 'left'
                  }
                ]}
              >
                Đang đợi xác nhận
              </Text>
            </View>
            <View style={{ justifyContent: 'flex-end' }}>
              <Text style={[style.text, { fontSize: FS(16), color: APP_COLOR, textAlign: 'right' }]}>
                146,000đ{'  '}
                <Text
                  style={[
                    style.text,
                    {
                      fontSize: FS(12),
                      textDecorationLine: 'line-through',
                      textAlign: 'right'
                    }
                  ]}
                >
                  210,000đ
                </Text>
              </Text>
              <Text
                style={[
                  style.text,
                  {
                    fontSize: FS(12),
                    textAlign: 'right'
                  }
                ]}
              >
                Số lượng:{' '}
                <Text
                  style={[
                    style.text,
                    {
                      fontSize: FS(14),
                      textAlign: 'right'
                    }
                  ]}
                >
                  5{' '}
                </Text>
                cốc
              </Text>
            </View>
          </View>
        </View>
        {/* body */}
        <View
          style={{
            height: DATA_MILK_TEA.length < 4 || this.state.showAllItem ? null : 150 * SCALE_RATIO_HEIGHT_BASIS,
            overflow: 'hidden'
          }}
        >
          {DATA_MILK_TEA.map((item, i) => (
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 5 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <View style={{ width: '60%' }}>
                <Text style={[style.text, { fontSize: FS(12), textAlign: 'left' }]}>{item.name}</Text>
                <Text
                  style={[
                    style.text,
                    {
                      color: '#707070',
                      fontSize: FS(10),
                      textAlign: 'left'
                    }
                  ]}
                >
                  {item.fill}
                </Text>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center', width: '10%', alignSelf: 'center' }}>
                <Text
                  style={[
                    style.text,
                    {
                      fontSize: FS(12)
                    }
                  ]}
                >
                  {item.sl}
                </Text>
              </View>
              <View style={{ alignSelf: 'flex-start', width: '30%' }}>
                <Text style={[style.text, { fontSize: FS(14), textAlign: 'right' }]}>
                  {getNumberWithCommas(item.price + 1000000)}
                </Text>
                <Text
                  style={[
                    style.text,
                    {
                      color: '#707070',
                      fontSize: FS(10),
                      textDecorationLine: 'line-through',
                      textAlign: 'right'
                    }
                  ]}
                >
                  {getNumberWithCommas(item.price + 100000)}
                </Text>
              </View>
            </View>
          ))}
        </View>
        {DATA_MILK_TEA.length > 3 ? (
          <LinearGradient
            locations={[0.1, 0.3]}
            colors={['#ffffff99', '#fff']}
            style={{
              width: DEVICE_WIDTH,
              // backgroundColor: 'transparent',
              zIndex: 9,
              marginTop: this.state.showAllItem ? 0 : -20 * SCALE_RATIO_HEIGHT_BASIS,
              alignSelf: 'center',
              // top: this.state.showAllItem ? 0 : -25 * SCALE_RATIO_HEIGHT_BASIS,
              height: this.state.showAllItem ? 30 * SCALE_RATIO_HEIGHT_BASIS : 40 * SCALE_RATIO_HEIGHT_BASIS,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row'
            }}
          >
            <MaterialIcons
              onPress={() => this.setState({ showAllItem: !this.state.showAllItem })}
              name={this.state.showAllItem ? 'keyboard-arrow-up' : 'keyboard-arrow-down'}
              size={FS(13)}
              color={APP_COLOR_TEXT}
              style={{
                marginTop: this.state.showAllItem ? 5 * SCALE_RATIO_HEIGHT_BASIS : 25 * SCALE_RATIO_HEIGHT_BASIS,
                marginRight: 3 * SCALE_RATIO_WIDTH_BASIS
              }}
            />
            <Text
              style={[
                style.text,
                { fontSize: FS(11), marginTop: this.state.showAllItem ? 0 : 20 * SCALE_RATIO_HEIGHT_BASIS }
              ]}
              onPress={() => this.setState({ showAllItem: !this.state.showAllItem })}
            >
              {this.state.showAllItem ? 'Thu gọn' : 'Xem Thêm'}
            </Text>
          </LinearGradient>
        ) : null}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            borderTopWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
            borderTopColor: APP_COLOR,
            paddingTop: 10 * SCALE_RATIO_WIDTH_BASIS,
            marginTop: 5 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          <MyButton
            outline
            textStyle={{ color: '#fff' }}
            width={DEVICE_WIDTH / 2.5}
            styleContent={{
              height: 31 * SCALE_RATIO_WIDTH_BASIS,
              backgroundColor: APP_COLOR_BLUE,
              borderColor: APP_COLOR_BLUE
            }}
          >
            Hủy đơn hàng
          </MyButton>
          <MyButton
            width={DEVICE_WIDTH / 2.5}
            style={{ height: 31 * SCALE_RATIO_WIDTH_BASIS }}
            onPress={() => {
              this.props.navigation.navigate(ROUTE_KEY.DETAIL_ORDER, {
                orderCode: this.orderCode,
                listProduct: DATA_MILK_TEA
              });
            }}
          >
            Xem chi tiết
          </MyButton>
        </View>
      </View>
    </View>
  );
  // shouldComponentUpdate(nextState) {
  //   if (this.state.showAllItem !== nextState.showAllItem) {
  //     return true;
  //   }
  //   return false;
  // }

  render() {
    return (
      <View
        style={{
          backgroundColor: '#fff',
          flex: 1
        }}
      >
        <BaseHeader
          noShadow
          translucent
          children={<Text style={[style.titleHeader, { fontSize: FS(18) }]}>ĐƠN HÀNG CỦA TÔI</Text>}
          leftIcon="arrow-left"
          leftIconType="MaterialCommunityIcons"
          onLeftPress={() => this.props.navigation.goBack()}
        />

        <ScrollableTabView
          initialPage={0}
          renderTabBar={() => <DefaultTabBar style={{ height: 30 }} />}
          // tabBarBackgroundColor='red'
          tabBarUnderlineStyle={{ backgroundColor: APP_COLOR, height: 2 }}
          // renderTabBar={() => <CustomTabBar />}
          tabBarBackgroundColor="#fff"
          tabBarActiveTextColor={APP_COLOR}
          tabBarInactiveTextColor="#707070"
          tabBarTextStyle={{
            fontSize: FS(12),
            fontFamily: FONT.Medium,
            backgroundColor: 'transparent'
          }}
        >
          {this.renderItem('Đang đến')}
          {this.renderItem('Hoàn tất')}
          {this.renderItem('Đơn nháp')}
        </ScrollableTabView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 30
  },
  icon: {
    width: 300,
    height: 300,
    alignSelf: 'center'
  },
  markerImageStyle: {
    width: 40 * SCALE_RATIO_WIDTH_BASIS,
    height: 40 * SCALE_RATIO_WIDTH_BASIS
  },
  searchInputContainer: {
    paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
    position: 'absolute',
    top: 20 * SCALE_RATIO_WIDTH_BASIS,
    width: DEVICE_WIDTH,
    height: 50 * SCALE_RATIO_WIDTH_BASIS
  },
  searchInput: {
    borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS,
    flex: 1,
    height: 50 * SCALE_RATIO_WIDTH_BASIS,
    flexDirection: 'row',
    backgroundColor: '#FFFFFF'
  },
  searchNameContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchNameTextStyle: {
    fontSize: 14 * SCALE_RATIO_WIDTH_BASIS,
    color: '#767676',
    fontWeight: '500'
  },
  searchIconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 40 * SCALE_RATIO_WIDTH_BASIS,
    height: '100%'
  },
  carouselItemContainer: {
    overflow: 'visible',
    borderRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
    width: 150 * SCALE_RATIO_WIDTH_BASIS,
    height: 180 * SCALE_RATIO_WIDTH_BASIS,
    backgroundColor: 'white'
  },
  carouselItemMoreInfo: {
    textAlign: 'center',
    fontSize: 10 * SCALE_RATIO_WIDTH_BASIS
  }
});
const mapActionCreators = {};

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData
});

// export default connect(
//   mapStateToProps,
//   mapActionCreators
// )(OrderComponent);
export default OrderComponent;
