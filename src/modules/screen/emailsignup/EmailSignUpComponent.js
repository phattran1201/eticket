import React from 'react';
import {
  Dimensions,
  ImageBackground,
  KeyboardAvoidingView,
  Platform,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import MyTextInputFloat from '../../../style/MyTextInputFloat';
import { alert } from '../../../utils/alert';
import MyComponent from '../../view/MyComponent';
import MySpinner from '../../view/MySpinner';
import { updateUserInfo } from '../editprofile/EditProfieActions';
import { loadConversation } from '../message/ListMessageAction';
import { registerPromise, registerSuccess } from './EmailSignUpActions';

const width = Dimensions.get('window').width;

class EmailSignUpComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      confirmpassword: '',
      isLoading: false
    };
  }
  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  registerFunct() {
    if (!this.validateEmail(this.state.email)) {
      alert(strings.alert, this.state.email + strings.is_invalid_email);
    } else if (this.state.password.length > 5) {
      if (this.state.password === this.state.confirmpassword) {
        MySpinner.show();
        registerPromise(this.state.email, this.state.password)
          .then(res => {
            this.props.registerSuccess(res, this, () => {
              const temp = { ...this.props.userData };
              temp.imei = DeviceInfo.getUniqueID();
              this.props.updateUserInfo(temp, () => {
                console.log('Hoang log uniqueId', temp.imei);
              });
            });
          })
          .catch(e => {
            MySpinner.hide();
            console.log('Hoang log err res register email', e);
            alert(strings.alert, e);
          });
      } else {
        alert(strings.alert, strings.password_match);
      }
    } else {
      alert(strings.alert, strings.password_lenght);
    }
  }
  render() {
    return (
      <ImageBackground
        style={{
          width,
          height: DEVICE_HEIGHT,
          alignContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff'
        }}
        resizeMode="contain"
        source={{ uri: 'http://channel.mediacdn.vn/prupload/879/2018/08/img20180820172941151.jpg' }}
      >
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          keyboardVerticalOffset={128}
          enabled
        >
          <View
            style={{
              height: DEVICE_HEIGHT / 4,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Text
              style={{
                color: '#2A2826',
                fontSize: FS(20),
                fontWeight: '500'
              }}
            >
              ĐĂNG KÝ
            </Text>
          </View>
          <View style={{ marginHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS }}>
            <View
              style={{
                paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
                paddingVertical: 10 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              <MyTextInputFloat
                label={'Họ và tên'}
                onChangeText={name => this.setState({ name })}
                value={this.state.name}
                returnKeyType="next"
                onSubmitEditing={() => this.txtEmail.focus()}
              />
            </View>
            <View
              style={{
                paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
                paddingVertical: 10 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              <MyTextInputFloat
                label={'Email'}
                onChangeText={email => this.setState({ email })}
                value={this.state.email}
                keyboardType="email-address"
                returnKeyType="next"
                ref={instance => (this.txtEmail = instance)}
                onSubmitEditing={() => this.txtPassword.focus()}
              />
            </View>
            <View
              style={{
                paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
                paddingVertical: 10 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              <MyTextInputFloat
                label={'Mật khẩu'}
                onChangeText={password => this.setState({ password })}
                value={this.state.password}
                keyboardType="email-address"
                returnKeyType="next"
                secureTextEntry
                ref={instance => (this.txtPassword = instance)}
                onSubmitEditing={() => this.txtConfirmPassword.focus()}
              />
            </View>
            <View
              style={{
                paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
                paddingVertical: 10 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              <MyTextInputFloat
                label={'Nhập lại mật khẩu'}
                onChangeText={confirmpassword => this.setState({ confirmpassword })}
                value={this.state.confirmpassword}
                keyboardType="email-address"
                returnKeyType="go"
                secureTextEntry
                ref={instance => (this.txtConfirmPassword = instance)}
                onSubmitEditing={() => this.registerFunct()}
              />
            </View>
          </View>
          <TouchableOpacity
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: 40 * SCALE_RATIO_HEIGHT_BASIS,
              width: DEVICE_WIDTH - 40 * SCALE_RATIO_WIDTH_BASIS,
              backgroundColor: '#BAA165',
              marginTop: 20 * SCALE_RATIO_HEIGHT_BASIS,
              borderRadius: 20 * SCALE_RATIO_HEIGHT_BASIS
            }}
            isDisabled={
              this.state.isLoading ||
              this.state.name === '' ||
              this.state.email === '' ||
              this.state.password === '' ||
              this.state.confirmpassword === ''
            }
            onPress={() => {
              this.registerFunct();
            }}
          >
            <Text
              style={{
                alignSelf: 'center',
                color: '#FFF',
                fontSize: FS(12),
                textAlign: 'center',
                fontFamily: 'helveticaneue',
                fontWeight: '600',
                backgroundColor: 'transparent',
                letterSpacing: 1
              }}
            >
              {strings.next}
            </Text>
          </TouchableOpacity>
          <View style={{ marginTop: 20 * SCALE_RATIO_HEIGHT_BASIS, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#E2D9C8', fontSize: FS(12) }}>
              Bạn đã có tài khoản?
                <Text style={{ color: '#120D0D', fontSize: FS(12), fontWeight: '500' }}> Đăng nhập</Text>
            </Text>

          </View>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

const mapActionCreators = {
  loadConversation,
  registerSuccess,
  updateUserInfo
};
const mapStateToProps = state => ({
  userData: state.user.userData
});
export default connect(
  mapStateToProps,
  mapActionCreators
)(EmailSignUpComponent);
