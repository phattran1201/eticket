import { Body, Button, Form, Header, Icon, Input, Item, Left, Right } from 'native-base';
import React from 'react';
import {
  Image,
  Text,
  View,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';
import Modal from 'react-native-modal';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import MyComponent from '../../view/MyComponent';
import PopupChangePass from '../../view/PopupChangePass';
// import HeaderWithBackButtonComponent from '../../view/HeaderWithBackButtonComponent';
import { changePassword, verifyFBAccountKITResetPass } from './ForgotPassActions';
import STouchableOpacity from '../../view/STouchableOpacity';

class ForgotPassComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      phoneNum: '',
      isModalVisible: false
    };
    this.onPressForgetPassword = this.onPressForgetPassword.bind(this);
  }

  onPressForgetPassword() {
    verifyFBAccountKITResetPass(this.state.phoneNum, this);
  }
  render() {
    return (
      <View style={style.content}>
        <Header style={{ backgroundColor: '#FFF' }} hasTabs>
          <Left>
            <Button onPress={() => this.props.navigation.goBack()} transparent>
              <Icon type="Ionicons" name="ios-arrow-back" style={{ color: 'rgb(109,119,247)' }} />
            </Button>
          </Left>
          <Body>{/* <Text>{strings.forget_password}</Text> */}</Body>
          <Right />
        </Header>
        <ScrollView>
          <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
            <View>
              <Image source={require('../../../assets/imgs/logo.png')} style={style.imgLogo} />
              <Form style={style.form}>
                {/* <Label
                style={{
                  // marginTop: 20,
                  // marginLeft: 20,
                  // marginRight: 20,
                  textAlign: 'center'
                }}
              >
                Vui lòng xác thực số điện thoại để lấy lại mật khẩu
                </Label> */}
                <Item rounded style={style.itemTextBox}>
                  <Icon style={{ color: 'rgb(102,123,157)' }} active type="Ionicons" name="call" />
                  <Input
                    value={this.state.phoneNum}
                    onChangeText={phoneNum => this.setState({ phoneNum })}
                    autoCorrect={false}
                    autoCapitalize="none"
                    placeholder={strings.insert_phone_number}
                    placeholderTextColor="rgb(102,123,157)"
                    style={style.textBox}
                    onSubmitEditing={this.onPressForgetPassword}
                    keyboardType='numeric'
                  />
                </Item>
                <STouchableOpacity
                  ref={instance => (this.btnForgetPassword = instance)}
                  rounded
                  primary
                  style={[
                    style.button,
                    {
                      borderRadius: 50,
                      alignItems: 'center',
                      justifyContent: 'center'
                    }
                  ]}
                  onPress={this.onPressForgetPassword}
                >
                  <Text style={{ color: '#FFF' }}> {strings.get_back_password} </Text>
                </STouchableOpacity>
                <Modal
                  isVisible={this.state.isModalVisible}
                  style={{ justifyContent: 'center', alignItems: 'center' }}
                  onRequestClose={() => { }}
                >
                  <PopupChangePass
                    onPress={(pass, onDoneFunc) =>
                      changePassword(
                        this.state.phoneNum,
                        pass,
                        this,
                        this.props.navigation,
                        onDoneFunc
                      )
                    }
                    onBackPress={() => this.setState({ isModalVisible: false })}
                  />
                </Modal>
              </Form>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }
}
export default ForgotPassComponent;
