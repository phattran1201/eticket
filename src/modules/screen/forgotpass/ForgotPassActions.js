import RNAccountKit from 'react-native-facebook-account-kit';
import { VIETNAM_MOBILE_PHONE_PREFIX, BASE_URL, AUTH_API, accountKitThemeForIOS } from '../../../constants/Constants';
import request from '../../../utils/request';
import { alert } from '../../../utils/alert';
import strings from '../../../constants/Strings';
import md5 from 'react-native-md5';

export const verifyFBAccountKITResetPass = (phoneNum, self) => {
  RNAccountKit.configure({
    initialPhoneCountryPrefix: VIETNAM_MOBILE_PHONE_PREFIX,
    initialPhoneNumber: phoneNum,
    theme: accountKitThemeForIOS
  });
  RNAccountKit.loginWithPhone().then(token => {
    if (!token) {
      // console.log('Login cancelled');
    } else {
      RNAccountKit.getCurrentAccount().then(account => {
        self.setState({
          isModalVisible: true,
          phoneNum: `(+${account.phoneNumber.countryCode})${account.phoneNumber.number}`
        });
      });
    }
  });
};

export const changePassword = (phone, password, self, navigation, onDoneFunc) => {
  request
    .put(`${BASE_URL}${AUTH_API}/forgetpass/`)
    .set('Content-Type', 'application/json')
    .send({
      phone,
      password: md5.hex_md5(password)
    })
    .finish((err, res) => {
      if (err) {
        // console.log(res);
        alert(
          strings.alert,
          res && res.body && res.body.error ? res.body.error : strings.network_require_fail,
          onDoneFunc
        );
      } else if (res.body.code === 200) {
        alert(strings.alert, strings.change_password_success, () => {
          self.setState({ isModalVisible: false });
          navigation.goBack();
          navigation.state.params.changeUserPass({ password });
        });
      }
    });
};
