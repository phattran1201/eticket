import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import ListCloseFriendMessageComponent from './ListCloseFriendMessageComponent';
import { deleteAllConversation, readAllConversation } from './ListMessageAction';
import ListNormalMessageComponent from './ListNormalMessageComponent';

const TabNavigator = createMaterialTopTabNavigator(
  {
    [strings.message_list]: {
      screen: ({ screenProps }) => <ListNormalMessageComponent navigation={screenProps} />
    },
    [strings.close_friend]: {
      screen: ({ screenProps }) => <ListCloseFriendMessageComponent navigation={screenProps} />
    }
  },
  {
    tabBarOptions: {
      labelStyle: {
        color: '#fff',
        fontSize: FS(12),
        textAlign: 'center',
        fontFamily: 'helveticaneue',
        fontWeight: 'bold',
        backgroundColor: 'transparent'
      },
      tabStyle: {
        backgroundColor: '#C7AE6D',
        margin: 5 * SCALE_RATIO_WIDTH_BASIS,
        borderRadius: 33 * SCALE_RATIO_HEIGHT_BASIS,
        paddingVertical: 4 * SCALE_RATIO_HEIGHT_BASIS,
        paddingHorizontal: 25 * SCALE_RATIO_WIDTH_BASIS
      },
      style: {
        backgroundColor: '#fff',
        borderWidth: 0
      },
      activeBackgroundColor: '#C7AE6D',
      activeTintColor: '#fff',
      inactiveTintColor: '#C7AE6D',
      indicatorStyle: {
        backgroundColor: 'transparent'
      }
    }
  }
);

class MessageComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <BaseHeader
          styleContent={{ shadowColor: 'transparent', elevation: 0 }}
          children={<Text style={style.titleHeader}>{strings.message.toUpperCase()}</Text>}
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
        />
        <TabNavigator screenProps={this.props.navigation} style={{ flex: 1 }} />
        {/* <DraggableButtonComponent
          title={strings.message_menu}
          buttons={[
            {
              title: strings.read_all,
              onPress: () => {
                setTimeout(() => {
                  MySpinner.show();
                  this.props.readAllConversation(ok => {
                    MySpinner.hide();
                    setTimeout(() => {
                      if (!ok) {
                        Alert.alert(
                          strings.alert,
                          strings.delete_conversation_fail,
                          [{ text: strings.yes, onPress: () => { } }],
                          { cancelable: false }
                        );
                      } else {
                        reloadUnreadNotificationNumber(
                          this.props.listConversation,
                          this.props.userData.id,
                          this.props.listConnectedUserIds
                        );
                        this.forceUpdate();
                      }
                    }, 300);
                  });
                }, 300);
              }
            },
            {
              title: strings.delete_all,
              onPress: () => {
                PopupNotification.showComponent(
                  <PopupNotificationChildComponent
                    title={strings.delete_all_messages}
                    content={strings.delete_all_messages_confirm}
                    button1Text={strings.cancel}
                    onButton1Press={PopupNotification.hide}
                    button2Text={strings.confirm}
                    onButton2Press={() => {
                      PopupNotification.hide();
                      setTimeout(() => {
                        MySpinner.show();
                        this.props.deleteAllConversation(ok => {
                          MySpinner.hide();
                          setTimeout(() => {
                            if (!ok) {
                              Alert.alert(
                                strings.alert,
                                strings.delete_conversation_fail,
                                [{ text: strings.yes, onPress: () => { } }],
                                { cancelable: false }
                              );
                            } else {
                              reloadUnreadNotificationNumber(
                                this.props.listConversation,
                                this.props.userData.id,
                                this.props.listConnectedUserIds
                              );
                              this.forceUpdate();
                            }
                          }, 300);
                        });
                      }, 300);
                    }}
                    subContent={strings.delete_all_messages_description}
                  />
                );
              }
            }
          ]}
        /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  titleStyle: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue-Bold',
    fontSize: 18 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  }
});

const mapStateToProps = state => ({
  userData: state.user.userData,
  listConversation: state.conversation.list_conversation || [],
  listConnectedUserIds: state.conversation.list_connected_user_id
});

const mapActionCreators = {
  deleteAllConversation,
  readAllConversation
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(MessageComponent);
