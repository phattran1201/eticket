import React from 'react';
import { ActivityIndicator, DeviceEventEmitter, FlatList, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import {
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  DEVICE_WIDTH,
  DEVICE_HEIGHT
} from '../../../constants/Constants';
import { playMessageReceivedSound } from '../../../utils/audioUtils';
import { getListDataFiltered, reloadUnreadNotificationNumber } from '../../../utils/chatManager';
import { formatDate } from '../../../utils/dateUtils';
import MyComponent from '../../view/MyComponent';
import {
  addNewConversation,
  addNewInterestConversation,
  addNewMessageToConversation,
  deleteConversation,
  loadConversation,
  loadInterestedConversation,
  loadMoreConversation,
  loadMoreInterestedConversation,
  refreshConversation
} from './ListMessageAction';
import ListMessageItem from './ListMessageItem';

class ListCloseFriendMessageComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      isSearchBarVisible: false,
      search_keyword: '',
      listFriend: [],
      showMenu: false,
      isLoadMore: false
    };
    this.page = 1;
    this.stillHaveData = true;
    this.renderOnRefresh = this.renderOnRefresh.bind(this);
  }

  onConnectionStateChangedFunc = data => {
    // console.log('Socket connection_state', data);
    this.onConnectionStateChangedFuncTimeout = setTimeout(() => {
      this.forceUpdate();
    }, 100);
  };

  onConversationUpdateFunc = data => {
    let conversationExist = false;
    this.props.listConversation.forEach(e => {
      if (e.id === data.message.conversation_id) {
        conversationExist = true;
      }
    });
    if (conversationExist) {
      this.props.addNewMessageToConversation(data.message.conversation_id, data.message);
    } else {
      const conversation = data.conversation;
      if (conversation) {
        if (!conversation.messages) {
          conversation.messages = [data.message];
        }
        this.props.addNewConversation(conversation);
      }
    }

    let isRead = false;
    data.message.read_user_ids.forEach(userId => {
      if (userId === this.props.userData.id) {
        isRead = true;
      }
    });
    if (!isRead && this.props.userData && data.message.sender_id !== this.props.userData.id) {
      playMessageReceivedSound();
    }
    this.onConversationUpdateFuncTimeout = setTimeout(() => {
      this.forceUpdate();
    }, 100);

    reloadUnreadNotificationNumber(
      this.props.listInterestedConversation,
      this.props.userData.id,
      this.props.listConnectedUserIds
    );
  };

  addNewMessageToConversationFunc = () => {
    this.forceUpdate();
  };

  componentWillUnmount() {
    clearInterval(this.updateTimeInterval);
    clearTimeout(this.onConnectionStateChangedFuncTimeout);
    clearTimeout(this.onConversationUpdateFuncTimeout);
    clearTimeout(this.didmountTimeout);
    clearTimeout(this.showSearchBarTimeout);
    clearTimeout(this.hideSearchBarTimeout);
    global.isChatScreenForeground = false;

    DeviceEventEmitter.removeListener(
      'addNewMessageToConversation',
      this.addNewMessageToConversationFunc
    );
    DeviceEventEmitter.removeListener('onConversationUpdate', this.onConversationUpdateFunc);
    DeviceEventEmitter.removeListener(
      'onConnectionStateChanged',
      this.onConnectionStateChangedFunc
    );
  }

  componentDidMount() {
    DeviceEventEmitter.addListener(
      'addNewMessageToConversation',
      this.addNewMessageToConversationFunc
    );
    DeviceEventEmitter.addListener('onConversationUpdate', this.onConversationUpdateFunc);
    DeviceEventEmitter.addListener('onConnectionStateChanged', this.onConnectionStateChangedFunc);

    global.isChatScreenForeground = true;

    this.props.loadConversation(() => {
      reloadUnreadNotificationNumber(
        this.props.listConversation,
        this.props.userData.id,
        this.props.listConnectedUserIds
      );
    });

    // To update the relative time, there will be an offset that's is maxium 59 seconds, accept it
    this.updateTimeInterval = setInterval(() => {
      this.forceUpdate();
    }, 60000);
  }

  renderOnRefresh() {
    this.setState({ refreshing: true });
    this.props.loadInterestedConversation(1, () => {
      this.outOfData = false;
      this.setState({ refreshing: false });
    });
  }

  handleLoadMore = () => {
    if (!this.stillHaveData || this.state.isLoadMore) return;
    this.page++;
    this.props.loadMoreInterestedConversation(this.page, stillHaveData => {
      this.stillHaveData = stillHaveData;
      this.setState({
        isLoadMore: false
      });
    });
  };

  renderFooter(data) {
    if (this.state.outOfData) return null;
    if (!this.state.refreshing && !data.length) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 30 * SCALE_RATIO_HEIGHT_BASIS - DEVICE_HEIGHT / 224
          }}
        />
      );
    }

    if (this.state.loading) {
      return (
        <View style={{ paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125 }}>
          <ActivityIndicator animating size="small" />
        </View>
      );
    }

    return null;
  }

  renderItem = ({ item }) => {
    let isRead = false;
    let lastMessage = '';
    let timeStamp = '';
    const id_interested = this.props.listInterestedConversation.filter(
      e => e.conversation_id === item.id
    );
    if (item.messages[0]) {
      item.messages[0].read_user_ids.forEach(userId => {
        if (userId === this.props.userData.id) {
          isRead = true;
        }
      });

      if (item.messages[0].type === 'TEXT') {
        lastMessage = item.messages[0].content;
      } else if (item.messages[0].type === 'IMAGE') {
        lastMessage =
          item.messages[0].sender_id === this.props.userData.id
            ? `${strings.you}: ${strings.has_sent_an_image}`
            : strings.has_sent_an_image;
      } else {
        lastMessage =
          item.messages[0].sender_id === this.props.userData.id
            ? `${strings.you}: ${strings.has_sent_an_emotion}`
            : strings.has_sent_an_emotion;
      }
      timeStamp = formatDate(item.messages[0].created_at);
    }
    console.log('Hoang log id', id_interested[0].id);
    return (
      <ListMessageItem
        item={{ ...item, isRead, lastMessage, timeStamp, id_interested }}
        isCloseFriend
        navigation={this.props.navigation}
      />
    );
  };

  isInterestConversation(conversation) {
    let isexist = false;
    this.props.listInterestedConversation.forEach(e => {
      if (e.conversation_id === conversation.id) {
        isexist = true;
        return;
      }
    });
    return isexist;
  }
  render() {
    const { userData } = this.props;
    if (!userData) return null;
    const listInterested = this.props.listConversation.filter(e => {
      if (this.isInterestConversation(e)) {
        return e;
      }
    });
    const listData = getListDataFiltered(
      listInterested,
      this.props.userData.id,
      this.props.listConnectedUserIds
    );
    console.log('Hoang log list Interested Conversation', listData);
    return (
      <View style={styles.container}>
        <FlatList
          data={listData.filter(e => {
            const isBlocked = this.props.listBlocked.find(
              i => i.receiver.id === e.conversationUserId
            );
            return !isBlocked;
          })}
          keyExtractor={item => item.id}
          renderItem={this.renderItem}
          style={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
          onRefresh={this.renderOnRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={0.01}
          ListFooterComponent={this.renderFooter(listData)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
});

const mapStateToProps = state => ({
  listInterestedConversation: state.conversation.list_interested_conversation,
  userData: state.user.userData,
  listConversation: state.conversation.list_conversation || [],
  listConnectedUserIds: state.conversation.list_connected_user_id,
  listBlocked: state.blocking.listBlocked
});

const mapActionCreators = {
  loadInterestedConversation,
  loadConversation,
  addNewConversation,
  refreshConversation,
  loadMoreConversation,
  addNewMessageToConversation,
  deleteConversation,
  addNewInterestConversation,
  loadMoreInterestedConversation
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(ListCloseFriendMessageComponent);
