import request from '../../../utils/request';
import {
  CLEAR_CONNECTION_STATE,
  UPDATE_CONVERSATION_CONNECTION_STATE,
  ADD_A_CONVERSATION,
  MESSAGE_API,
  REMOVE_A_CONVERSATION,
  UPDATE_LAST_MESSAGE_OF_A_CONVERSATION,
  UPDATE_LIST_CONVERSATION,
  BASE_URL,
  CONVERSATION_API,
  REFRESH_LIST_CONVERSATION,
  USER_API,
  REMOVE_ALL_CONVERSATION,
  READ_ALL_MESSAGE,
  GET_LIST_INTERESTED_CONVERSATION,
  ADD_A_INTERESTED_CONVERSATION,
  ADD_TO_LIST_INTERESTED_CONVERSATION
} from '../../../constants/Constants';

export function loadMoreConversation(page, self, onOutOfDate) {
  self.setState({ loading: true });
  return (dispatch, store) => {
    request
      .post(`${BASE_URL}${CONVERSATION_API}/get_conversation?limit=10&page=${page}&fields=["$all"]`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send()
      .finish((err, res) => {
        if (err) {
          self.setState({ loading: false });
        } else {
          //Save products list to reducer
          if (res.body.results.object && res.body.results.object.rows.length === 0) {
            onOutOfDate();
          }
          dispatch({
            type: UPDATE_LIST_CONVERSATION,
            payload: res.body.results.object ? res.body.results.object.rows : []
          });
          self.setState({ loading: false });
        }
      });
  };
}
export function refreshConversation(self, onSuccess) {
  return (dispatch, store) => {
    request
      .post(`${BASE_URL}${CONVERSATION_API}/get_conversation?limit=10&page=1&fields=["$all"]`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send()
      .finish((err, res) => {
        if (!err) {
          //Save posts post to reducer
          dispatch({
            type: REFRESH_LIST_CONVERSATION,
            payload: res.body.results.object ? res.body.results.object.rows : []
          });
          onSuccess();
          self.setState({
            refreshing: false
          });
        } else {
          self.setState({
            refreshing: false
          });
        }
      });
  };
}

export function deleteConversation(conversationId, onDoneFunc = () => { }) {
  return (dispatch, store) => {
    request
      .post(`${BASE_URL}${CONVERSATION_API}/${conversationId}/set_invisible`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send()
      .finish((err, res) => {
        onDoneFunc(!err);
        if (!err && res && res.body.code === 200) {
          dispatch({
            type: REMOVE_A_CONVERSATION,
            payload: { conversationId }
          });
        }
      });
  };
}

export function deleteAllConversation(onDoneFunc = () => { }) {
  return (dispatch, store) => {
    console.log(`${BASE_URL}${CONVERSATION_API}/delete_all_conversation`);
    console.log(`${store().user.token}`);
    request
      .delete(`${BASE_URL}${CONVERSATION_API}/delete_all_conversation`)
      // .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send()
      .finish((err, res) => {
        onDoneFunc(!err);
        if (!err && res && res.body.code === 200) {
          dispatch({
            type: REMOVE_ALL_CONVERSATION
          });
        } else {
          console.log('Hoang log err delete all conversation', err);
        }
      });
  };
}

export function readAllConversation(onDoneFunc = () => { }) {
  return (dispatch, store) => {
    request
      .post(`${BASE_URL}message/read_all_message`)
      // .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send()
      .finish((err, res) => {
        onDoneFunc(!err);
        if (!err && res && res.body.code === 200) {
          const tempListConversation = store().conversation.list_conversation;
          tempListConversation.forEach(e => {
            if (e.messages && e.messages.length > 0 && e.messages[0].read_user_ids.length === 1 && e.messages[0].read_user_ids[0] !== store().user.userData.id) {
              e.messages[0].read_user_ids.push(store().user.userData.id);
            }
          })
          console.log('Hoang log listConseravtion', tempListConversation);
          console.log('Hoang log token', store().user.token);
          dispatch({
            type: READ_ALL_MESSAGE,
            payload: tempListConversation
          });
        } else {
          console.log('Hoang log err read all conversation', err);
        }
      });
  };
}

export function loadConversation() {
  return (dispatch, store) => {
    request
      .post(`${BASE_URL}${CONVERSATION_API}/get_conversation?limit=10&page=1&fields=["$all"]`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send()
      .finish((err, res) => {
        if (!err) {
          dispatch({
            type: REFRESH_LIST_CONVERSATION,
            payload: res.body.results.object ? res.body.results.object.rows : []
          });
        }
      });
  };
}

export function loadInterestedConversation(page, onDoneFunc = () => { }) {
  return (dispatch, store) => {
    const limit = 20;
    request
      .get(`${BASE_URL}conversation_interest?fields=["$all"]&filter=["user_id":"${store().user.userData.id}"]&page=${page}&limit=${limit}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .finish((err, res) => {
        if (!err) {
          dispatch({
            type: GET_LIST_INTERESTED_CONVERSATION,
            payload: res.body.results.objects.rows
          })
          onDoneFunc();
        }

      })
  }
}

export function loadMoreInterestedConversation(page, onDone = () => { }) {
  return (dispatch, store) => {
    const limit = 20;
    request
      .get(`${BASE_URL}conversation_interest?fields=["$all"]&filter=["user_id":"${store().user.userData.id}"]&page=${page}&limit=${limit}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .finish((err, res) => {
        if (res &&
          res.body &&
          res.body.results &&
          res.body.results.objects &&
          res.body.results.objects.rows
        ) {
          if (res.body.results.objects.rows.length === 0) {
            onDone(false);
          } else {
            onDone(true);
          }
          dispatch({
            type: ADD_TO_LIST_INTERESTED_CONVERSATION,
            payload: res.body.results.objects.rows
          });
        } else {
          onDone(true);
        }
      });
  };
}

export function addNewInterestConversation(conversationId, user_id) {
  console.log('Hoang vo day khong');
  return (dispatch, store) => {
    request
      .post(`${BASE_URL}conversation_interest`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send({ conversationId, user_id })
      .finish((err, res) => {
        if (!err) {
          console.log('Hoang log res interested Conversation', res.body.results.object);
          dispatch({
            type: ADD_A_INTERESTED_CONVERSATION,
            payload: res.body.results.object ? res.body.results.object : {}
          })
        }
        else {
          console.log('Hoang log err interested Conversation', err);
        }

      })
  }
}

export function addNewMessageToConversation(conversationId, message) {
  return dispatch => {
    dispatch({
      type: UPDATE_LAST_MESSAGE_OF_A_CONVERSATION,
      payload: { conversationId, message }
    });
  };
}

export function addNewConversation(conversation) {
  return dispatch => {
    dispatch({
      type: ADD_A_CONVERSATION,
      payload: { conversation }
    });
  };
}

function setMessageReadReduxOnly(dispatch, store, message) {
  const myUserId = store().user.userData.id;
  message.read_user_ids.filter(element => element !== myUserId);
  message.read_user_ids.push(myUserId);
  dispatch({
    type: UPDATE_LAST_MESSAGE_OF_A_CONVERSATION,
    payload: { conversationId: message.conversation_id, message }
  });
}

export function setMessageRead(message, onDoneFunc) {
  return (dispatch, store) => {
    setMessageReadReduxOnly(dispatch, store, message);
    request
      .post(`${BASE_URL}${MESSAGE_API}/${message.id}/readmessage`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send()
      .finish((err, res) => {
        if (!err) {
          setMessageReadReduxOnly(dispatch, store, message);
          onDoneFunc();
        }
      });
  };
}

export function getListFriendToChat(key, onDoneFunc) {
  return (dispatch, store) => {
    request
      .post(`${BASE_URL}${USER_API}/search_friend?fields=["$all"]`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send({
        keyword: key
      })
      .finish((err, res) => {
        if (!err && res.body.results.objects.rows) {
          onDoneFunc(res.body.results.objects.rows);
        }
      });
  };
}

export function setConnectionState(userIds, isConnected) {
  return dispatch => {
    dispatch({
      type: UPDATE_CONVERSATION_CONNECTION_STATE,
      payload: { userIds, isConnected }
    });
  };
}

export function clearConnectionState() {
  return dispatch => {
    dispatch({
      type: CLEAR_CONNECTION_STATE
    });
  };
}
