import React from 'react';
import { ActivityIndicator, DeviceEventEmitter, FlatList, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import {
  SCALE_RATIO_HEIGHT_BASIS,
  DEVICE_HEIGHT,
  SCALE_RATIO_WIDTH_BASIS,
  DEVICE_WIDTH
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import { playMessageReceivedSound } from '../../../utils/audioUtils';
import { getListDataFiltered, reloadUnreadNotificationNumber } from '../../../utils/chatManager';
import { formatDate } from '../../../utils/dateUtils';
import global from '../../../utils/globalUtils';
import MyComponent from '../../view/MyComponent';
import {
  addNewConversation,
  addNewInterestConversation,
  addNewMessageToConversation,
  deleteConversation,
  loadConversation,
  loadMoreConversation,
  refreshConversation
} from './ListMessageAction';
import ListMessageItem from './ListMessageItem';

class ListNormalMessageComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      isSearchBarVisible: false,
      search_keyword: '',
      listFriend: [],
      showMenu: false
    };
    this.page = 1;
    this.outOfData = false;
    this.goBack = this.goBack.bind(this);
    this.renderOnRefresh = this.renderOnRefresh.bind(this);
  }

  onConnectionStateChangedFunc = data => {
    // console.log('Socket connection_state', data);
    this.onConnectionStateChangedFuncTimeout = setTimeout(() => {
      this.forceUpdate();
    }, 100);
  };

  onConversationUpdateFunc = data => {
    let conversationExist = false;
    this.props.listConversation.forEach(e => {
      if (e.id === data.message.conversation_id) {
        conversationExist = true;
      }
    });
    if (conversationExist) {
      this.props.addNewMessageToConversation(data.message.conversation_id, data.message);
    } else {
      const conversation = data.conversation;
      if (conversation) {
        if (!conversation.messages) {
          conversation.messages = [data.message];
        }
        this.props.addNewConversation(conversation);
      }
    }

    let isRead = false;
    data.message.read_user_ids.forEach(userId => {
      if (userId === this.props.userData.id) {
        isRead = true;
      }
    });
    if (!isRead && this.props.userData && data.message.sender_id !== this.props.userData.id) {
      playMessageReceivedSound();
    }
    this.onConversationUpdateFuncTimeout = setTimeout(() => {
      this.forceUpdate();
    }, 100);

    reloadUnreadNotificationNumber(
      this.props.listConversation,
      this.props.userData.id,
      this.props.listConnectedUserIds
    );
  };

  addNewMessageToConversationFunc = () => {
    this.forceUpdate();
  };

  componentWillUnmount() {
    clearInterval(this.updateTimeInterval);
    clearTimeout(this.onConnectionStateChangedFuncTimeout);
    clearTimeout(this.onConversationUpdateFuncTimeout);
    clearTimeout(this.didmountTimeout);
    clearTimeout(this.showSearchBarTimeout);
    clearTimeout(this.hideSearchBarTimeout);
    global.isChatScreenForeground = false;

    DeviceEventEmitter.removeListener(
      'addNewMessageToConversation',
      this.addNewMessageToConversationFunc
    );
    DeviceEventEmitter.removeListener('onConversationUpdate', this.onConversationUpdateFunc);
    DeviceEventEmitter.removeListener(
      'onConnectionStateChanged',
      this.onConnectionStateChangedFunc
    );
  }

  componentDidMount() {
    DeviceEventEmitter.addListener(
      'addNewMessageToConversation',
      this.addNewMessageToConversationFunc
    );
    DeviceEventEmitter.addListener('onConversationUpdate', this.onConversationUpdateFunc);
    DeviceEventEmitter.addListener('onConnectionStateChanged', this.onConnectionStateChangedFunc);

    global.isChatScreenForeground = true;

    this.props.loadConversation(() => {
      reloadUnreadNotificationNumber(
        this.props.listConversation,
        this.props.userData.id,
        this.props.listConnectedUserIds
      );
    });

    // To update the relative time, there will be an offset that's is maxium 59 seconds, accept it
    this.updateTimeInterval = setInterval(() => {
      this.forceUpdate();
    }, 60000);
  }

  handleLoadMore = () => {
    if (this.outOfData) return;

    this.page++;
    this.props.loadMoreConversation(this.page, this, () => {
      this.outOfData = true;
    });
  };

  renderFooter(data) {
    if (this.state.outOfData) return null;
    if (!this.state.refreshing && !data.length) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 30 * SCALE_RATIO_HEIGHT_BASIS - DEVICE_HEIGHT / 224
          }}
        />
      );
    }

    if (this.state.loading) {
      return (
        <View style={{ paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125 }}>
          <ActivityIndicator animating size="small" />
        </View>
      );
    }

    return null;
  }

  renderItem = ({ item }) => {
    let isRead = false;
    let lastMessage = '';
    let timeStamp = '';

    if (item.messages[0]) {
      item.messages[0].read_user_ids.forEach(userId => {
        if (userId === this.props.userData.id) {
          isRead = true;
        }
      });

      if (item.messages[0].type === 'TEXT') {
        lastMessage = item.messages[0].content;
      } else if (item.messages[0].type === 'IMAGE') {
        lastMessage =
          item.messages[0].sender_id === this.props.userData.id
            ? `${strings.you}: ${strings.has_sent_an_image}`
            : strings.has_sent_an_image;
      } else {
        lastMessage =
          item.messages[0].sender_id === this.props.userData.id
            ? `${strings.you}: ${strings.has_sent_an_emotion}`
            : strings.has_sent_an_emotion;
      }
      timeStamp = formatDate(item.messages[0].created_at);
    }

    return (
      <ListMessageItem
        navigation={this.props.navigation}
        item={{ ...item, isRead, lastMessage, timeStamp }}
      />
    );
  };

  goBack() {
    this.props.navigation.goBack();
  }

  renderOnRefresh() {
    this.setState({ refreshing: true });
    this.props.refreshConversation(this, () => {
      this.page = 1;
      this.outOfData = false;
    });
  }

  isExistInterestConversation(id) {
    isexist = false;
    this.props.listInterestedConversation.filter(e => {
      if (e.conversation_id === id) {
        isexist = true;
        return;
      }
    });
    return isexist;
  }

  render() {
    const { userData } = this.props;
    if (!userData) return null;
    const listConversation = getListDataFiltered(
      this.props.listConversation,
      this.props.userData.id,
      this.props.listConnectedUserIds
    );
    const listData = listConversation.filter(e => !this.isExistInterestConversation(e.id));
    return (
      <View style={styles.container}>
        <FlatList
          data={listData.filter(e => {
            const isBlocked = this.props.listBlocked.find(
              i => i.receiver.id === e.conversationUserId
            );
            return !isBlocked;
          })}
          keyExtractor={item => item.id}
          renderItem={this.renderItem}
          style={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
          onRefresh={this.renderOnRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={0.01}
          ListFooterComponent={this.renderFooter(listData)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
});

const mapStateToProps = state => ({
  userData: state.user.userData,
  listConversation: state.conversation.list_conversation || [],
  listConnectedUserIds: state.conversation.list_connected_user_id,
  listBlocked: state.blocking.listBlocked,
  listInterestedConversation: state.conversation.list_interested_conversation || []
});

const mapActionCreators = {
  loadConversation,
  addNewConversation,
  refreshConversation,
  loadMoreConversation,
  addNewMessageToConversation,
  deleteConversation,
  addNewInterestConversation
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(ListNormalMessageComponent);
