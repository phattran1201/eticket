import React from 'react';
import { Alert, Dimensions, Platform, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  FS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import MyImage from '../../../modules/view/MyImage';
import { alert } from '../../../utils/alert';
import getGenderImage from '../../../utils/getGenderImage';
import MyComponent from '../../view/MyComponent';
import Dropdown from '../../view/MyDropDown/dropdown';
import MySpinner from '../../view/MySpinner';
import PopupNotification from '../../view/PopupNotification';
import PopupNotificationChildComponent from '../../view/PopupNotificationChildComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import {
  addNewInterestConversation,
  deleteConversation,
  disInterestConversation
} from '../detailMessage/ListMessageAction';

const { width } = Dimensions.get('window');

class ListMessageItem extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      avatar,
      lastMessage,
      isRead,
      timeStamp,
      id_interested,
      status,
      conversationName,
      conversationAge,
      conversationFlag,
      conversationGender,
      visibility,
      otherUser,
      id
    } = this.props.item;
    return (
      <View
        style={[
          styles.container,
          style.shadow,
          { borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0 }
        ]}
        // underlayColor="rgba(192,192,192,0.6)"
        // onLongPress={() => {
        //   this.dropdown.onPress();
        // }}
        // onPress={() => {
        //   console.log('this.props.item', this.props.item);
        //   this.props.navigation.navigate(ROUTE_KEY.DETAIL_MESSAGE_COMPONENT, {
        //     conversation: this.props.item
        //   });
        // }}
      >
        <View
          style={{
            position: 'absolute',
            top: 15 * SCALE_RATIO_WIDTH_BASIS,
            right: 5 * SCALE_RATIO_WIDTH_BASIS,
            zIndex: 99,
            width: (width * 1) / 5,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        />
        <STouchableOpacity
          onPress={() =>
            this.props.navigation.navigate(ROUTE_KEY.PARTNER_DETAIL_COMPONENT, {
              item: otherUser
            })
          }
        >
          <MyImage
            source={{ uri: avatar }}
            size={60 * SCALE_RATIO_WIDTH_BASIS}
            status={visibility}
          />
        </STouchableOpacity>
        <Dropdown
          ref={ref => {
            this.dropdown = ref;
          }}
          data={[
            {
              value: this.props.isCloseFriend ? strings.dismiss_interest : strings.interest,
              onSelect: onDone => {
                if (this.props.isCloseFriend) {
                  MySpinner.show();
                  this.props.disInterestConversation(id_interested[0].id, () => {
                    MySpinner.hide();
                    this.forceUpdate();
                  });
                } else {
                  let isExist = false;
                  this.props.listInterestedConversation.forEach(e => {
                    if (e.conversation_id === id) {
                      isExist = true;
                      return;
                    }
                  });

                  if (isExist) {
                    alert(strings.alert, strings.already_interested);
                  } else {
                    MySpinner.show();
                    this.props.addNewInterestConversation(id, this.props.userData.id, () => {
                      MySpinner.hide();
                      this.forceUpdate();
                    });
                  }
                }
              }
            },
            {
              value: strings.delete,
              onSelect: onDone => {
                setTimeout(
                  () =>
                    PopupNotification.showComponent(
                      <PopupNotificationChildComponent
                        title={strings.delete_conversation_title}
                        content={strings.delete_conversation_content}
                        button1Text={strings.cancel}
                        onButton1Press={PopupNotification.hide}
                        button2Text={strings.confirm}
                        onButton2Press={() => {
                          PopupNotification.hide();
                          setTimeout(() => {
                            MySpinner.show();
                            this.props.deleteConversation(id, ok => {
                              MySpinner.hide();
                              setTimeout(() => {
                                if (!ok) {
                                  Alert.alert(
                                    strings.alert,
                                    strings.delete_conversation_fail,
                                    [{ text: strings.yes, onPress: () => {} }],
                                    { cancelable: false }
                                  );
                                } else {
                                  this.forceUpdate();
                                }
                              }, 300);
                            });
                          }, 300);
                        }}
                      />
                    ),
                  300
                );
              }
            }
          ]}
          pickerStyle={{
            borderColor: Platform.OS === 'ios' ? '#AE92D330' : '#70707010',
            borderWidth: 1,
            borderRadius: (31 * SCALE_RATIO_HEIGHT_BASIS) / 2,
            borderTopRightRadius: 0
          }}
          textColor="#C7AE6D"
          fontSize={15 * SCALE_RATIO_WIDTH_BASIS}
          dropdownPosition={1}
          itemTextStyle={{
            fontSize: 4.5 * SCALE_RATIO_WIDTH_BASIS,
            fontFamily: 'helveticaneue',
            color: '#C7AE6D'
          }}
          itemPadding={5}
          dropdownOffset={{
            top: 45 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
            left: 49 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
          }}
        />
        <STouchableOpacity
          // style={{  width }}
          underlayColor="rgba(192,192,192,0.6)"
          onLongPress={() => {
            this.dropdown.onPress();
          }}
          onPress={() => {
            console.log('this.props.item', this.props.item);
            this.props.navigation.navigate(ROUTE_KEY.DETAIL_MESSAGE_COMPONENT, {
              conversation: this.props.item,
              idInterested: id_interested ? id_interested[0].id : '',
              id
            });
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              width: (width * 3.6) / 5,
              marginRight: 5 * SCALE_RATIO_HEIGHT_BASIS,
              justifyContent: 'space-between',
              marginBottom: 8 * SCALE_RATIO_HEIGHT_BASIS
            }}
          >
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={[
                  style.textCaption,
                  {
                    color: isRead ? '#C7AE6D' : '#282828',
                    fontSize: isRead ? FS(11) : FS(13),
                    fontWeight: isRead ? '600' : 'bold',
                    marginRight: 5 * SCALE_RATIO_WIDTH_BASIS
                  }
                ]}
              >
                {conversationName}
              </Text>
              {getGenderImage(conversationGender)}
              <Text
                style={[
                  style.textCaption,
                  {
                    color: isRead ? '#C7AE6D' : '#282828',
                    fontSize: isRead ? FS(11) : FS(13),
                    fontWeight: isRead ? '600' : 'bold'
                  }
                ]}
              >
                {conversationAge}
              </Text>
            </View>
            <Text
              style={[
                style.textCaption,
                {
                  color: isRead ? '#C7AE6D' : '#282828',
                  fontSize: isRead ? FS(10) : FS(12),
                  fontWeight: isRead ? 'normal' : 'bold',
                  textAlign: 'right',
                  alignSelf: 'flex-end',
                  justifyContent: 'flex-end'
                }
              ]}
            >
              {timeStamp}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: (width * 3.6) / 5,
              marginRight: 5 * SCALE_RATIO_HEIGHT_BASIS,
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
          >
            <Text
              style={[
                style.textCaption,
                {
                  color: isRead ? '#C7AE6D' : '#282828',
                  fontSize: isRead ? FS(10) : FS(12),
                  fontWeight: isRead ? 'normal' : 'bold',
                  width: (width * 2.5) / 5
                }
              ]}
              numberOfLines={1}
            >
              {lastMessage}
            </Text>
            {!isRead && (
              <View
                style={{
                  // height: 12 * SCALE_RATIO_HEIGHT_BASIS,
                  borderRadius: 6 * SCALE_RATIO_HEIGHT_BASIS,
                  paddingHorizontal: 6 * SCALE_RATIO_WIDTH_BASIS,
                  paddingVertical: 3 * SCALE_RATIO_HEIGHT_BASIS,
                  backgroundColor: '#fff',
                  borderColor: '#EB5F5F',
                  borderWidth: 1.3 * SCALE_RATIO_WIDTH_BASIS,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text
                  style={{
                    fontFamily: 'helveticaneue',
                    fontSize: FS(10),
                    fontWeight: '600',
                    textAlign: 'center',
                    color: '#EB5F5F'
                  }}
                >
                  N
                </Text>
              </View>
            )}
          </View>

          <View style={{ width: (width * 2.5) / 5 }} />
        </STouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    marginBottom: 15 * SCALE_RATIO_WIDTH_BASIS,
    flexDirection: 'row',
    padding: 10 * SCALE_RATIO_WIDTH_BASIS
  },
  userInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  flagInfoImage: {
    width: 15 * SCALE_RATIO_WIDTH_BASIS,
    height: 10 * SCALE_RATIO_WIDTH_BASIS
  },
  gender: {
    width: 10 * SCALE_RATIO_WIDTH_BASIS,
    height: 16 * SCALE_RATIO_WIDTH_BASIS,
    marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS
  }
});
const mapStateToProps = state => ({
  listInterestedConversation: state.conversation.list_interested_conversation,
  userData: state.user.userData
});

const mapActionCreators = {
  deleteConversation,
  addNewInterestConversation,
  disInterestConversation
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(ListMessageItem);
