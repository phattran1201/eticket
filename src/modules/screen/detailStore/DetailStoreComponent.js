import React from 'react';
import {
  Animated,
  Dimensions,
  Easing,
  Image,
  PanResponder,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  Geolocation,
  FlatList,
  Share
} from 'react-native';

import MapView, { Circle, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FastImage from 'react-native-fast-image';
import MyComponent from '../../view/MyComponent';
import BaseHeader from '../../view/BaseHeader';
import style, { APP_COLOR, FONT } from '../../../constants/style';
import strings from '../../../constants/Strings';
import { alert } from '../../../utils/alert';
import { SCALE_RATIO_WIDTH_BASIS, DEVICE_WIDTH, ROUTE_KEY, FS } from '../../../constants/Constants';
import { getLocationDelta } from '../../../utils/mapUtils';
import DetailViewableImage from '../../view/DetailViewableImage';

const logoIcon = require('../../../assets/imgs/icons/logo.png');

export default class DetailStoreComponent extends MyComponent {
  constructor(props) {
    super(props);

    const { params } = this.props.navigation.state;
    this.storeInfo = params && params.detailStore ? params.detailStore : {};
    // this.storeInfo = {
    //   id: 0,
    //   name: 'TocoToco Ngô Thời Nhiệm',
    //   address: '87 Ngô Thời Nhiệm',
    //   district: 6,
    //   ward: 3,
    //   coordinate: {
    //     latitude: 10.784768,
    //     longitude: 106.6934272
    //   },
    //   backgroundImage:
    //     'https://lh5.googleusercontent.com/p/AF1QipP7nllayHsKIwgQHBWa53wDjndONOOh7PVgelfb=w284-h160-k-no',
    //   distance: 0.4,
    //   phone: '0912 345 678',
    //   openTime: '08:30 - 22:00 Hàng ngày'
    // };
    this.currentRadius = 0.5 * 1000;
    const locationDelta = getLocationDelta(this.storeInfo.latitude, this.storeInfo.longitude, this.currentRadius);
    console.log('poi poi:', this.storeInfo);
    this.state = {
      region: {
        latitude: this.storeInfo.latitude,
        longitude: this.storeInfo.longitude,
        latitudeDelta: locationDelta.latitudeDelta,
        longitudeDelta: locationDelta.longitudeDelta
      }
    };
  }

  onShare = async storeInfo => {
    try {
      const result = await Share.share({
        message: `${storeInfo.name} - ${storeInfo.address} - https://www.google.com/maps/search/?api=1&query=${
          storeInfo.latitude
        },${storeInfo.longitude}`
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    console.log('dauphaiphat: DetailStoreComponent -> render -> this.storeInfo;', this.storeInfo);
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#fff'
        }}
      >
        <BaseHeader
          children={<Text style={style.titleHeader}>{this.storeInfo.name}</Text>}
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
          rightIcon="directions"
          rightIconType="MaterialIcons"
          onRightPress={() => this.onShare(this.storeInfo)}
          styleContent={{ backgroundColor: '#fff' }}
        />
        <View style={{ flex: 1 }}>
          <View
            style={{
              width: '100%'
              // marginBottom: (40 * SCALE_RATIO_WIDTH_BASIS) / 1.5
            }}
          >
            {/* <FastImage
              style={{ width: '100%', height: 160 * SCALE_RATIO_WIDTH_BASIS }}
              resizeMode="cover"
              source={{
                uri: 'https://lh5.googleusercontent.com/p/AF1QipP7nllayHsKIwgQHBWa53wDjndONOOh7PVgelfb=w284-h160-k-no'
              }}
            /> */}
            <DetailViewableImage 
              style={{ width: '100%', height: 160 * SCALE_RATIO_WIDTH_BASIS }}
              resizeMode="cover"
              source={{
                uri: 'https://lh5.googleusercontent.com/p/AF1QipP7nllayHsKIwgQHBWa53wDjndONOOh7PVgelfb=w284-h160-k-no'
              }}
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              height: 100 * SCALE_RATIO_WIDTH_BASIS,
              paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,

              borderBottomWidth: 3 * SCALE_RATIO_WIDTH_BASIS,
              borderBottomColor: '#F0F0F0'
            }}
          >
            <View
              style={{
                width: 50 * SCALE_RATIO_WIDTH_BASIS,
                height: '100%',
                alignItems: 'center'
              }}
            >
              <FastImage
                source={logoIcon}
                resizeMode={FastImage.resizeMode.contain}
                style={{
                  width: 20 * SCALE_RATIO_WIDTH_BASIS,
                  height: 20 * SCALE_RATIO_WIDTH_BASIS
                }}
              />
            </View>
            <View
              style={{
                // flex: 1,
                justifyContent: 'space-between'
              }}
            >
              <Text
                style={{
                  fontSize: FS(16),
                  color: '#323232',
                  textAlign: 'justify',
                  fontFamily: FONT.Medium
                }}
              >
                {this.storeInfo.name}
              </Text>
              <Text
                numberOfLines={1}
                style={{
                  fontSize: FS(13),
                  textAlign: 'justify',
                  fontFamily: FONT.Regular
                }}
              >
                {this.storeInfo.address}
              </Text>
              <Text
                style={{
                  fontSize: FS(13),
                  textAlign: 'justify',
                  fontFamily: FONT.Medium
                }}
              >
                Cách bạn {parseFloat(this.storeInfo.distance).toFixed(1)} km
              </Text>
            </View>
          </View>

          <View
            style={{
              height: 70 * SCALE_RATIO_WIDTH_BASIS,
              paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <View style={{ flexDirection: 'row' }}>
              <View
                style={{
                  width: 50 * SCALE_RATIO_WIDTH_BASIS,
                  height: 25 * SCALE_RATIO_WIDTH_BASIS,
                  alignItems: 'center'
                }}
              >
                <MaterialIcons name="watch-later" color="#D3B574" size={20 * SCALE_RATIO_WIDTH_BASIS} />
              </View>
              <View
                style={{
                  flex: 1
                }}
              >
                <Text
                  style={{
                    fontSize: FS(15),
                    textAlign: 'justify',
                    fontFamily: FONT.Regular
                  }}
                >
                  {this.storeInfo.openTime}
                </Text>
              </View>
            </View>

            <View style={{ flexDirection: 'row' }}>
              <View
                style={{
                  width: 50 * SCALE_RATIO_WIDTH_BASIS,
                  height: 25 * SCALE_RATIO_WIDTH_BASIS,
                  alignItems: 'center'
                }}
              >
                <MaterialIcons name="phone" color="#D3B574" size={20 * SCALE_RATIO_WIDTH_BASIS} />
              </View>
              <View
                style={{
                  flex: 1
                }}
              >
                <Text
                  style={{
                    fontSize: FS(15),
                    textAlign: 'justify',
                    fontFamily: FONT.Regular
                  }}
                >
                  {this.storeInfo.phoneNumber}
                </Text>
              </View>
            </View>
          </View>

          <View style={{ flex: 1 }}>
            <MapView
              style={{ flex: 1 }}
              provider={PROVIDER_GOOGLE}
              ref={instance => (this.map = instance)}
              region={this.state.region}
            >
              <Marker coordinate={{ latitude: this.storeInfo.latitude, longitude: this.storeInfo.longitude }}>
                <FastImage
                  style={styles.markerImageStyle}
                  source={logoIcon}
                  resizeMode={FastImage.resizeMode.contain}
                />
              </Marker>
            </MapView>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  markerImageStyle: {
    width: 40 * SCALE_RATIO_WIDTH_BASIS,
    height: 40 * SCALE_RATIO_WIDTH_BASIS
  }
});
