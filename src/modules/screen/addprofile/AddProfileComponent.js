import React from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native';
import FastImage from 'react-native-fast-image';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import {
  DEVICE_WIDTH,
  FS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  DEVICE_HEIGHT
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import MyTextInputFloat from '../../../style/MyTextInputFloat';
import { alert } from '../../../utils/alert';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import Dropdown from '../../view/MyDropDown/dropdown';
import MySpinner from '../../view/MySpinner';
import PopupNotification from '../../view/PopupNotification';
import PopupNotificationEditProfileImageComponent from '../../view/PopupNotificationEditProfileImageComponent';
import { updateUserInfo } from '../editprofile/EditProfieActions';

const width = Dimensions.get('window').width;

class AddProfileComponent extends MyComponent {
  constructor(props) {
    super(props);
    const { params } = props.navigation.state;
    const userData =
      params && params.userData ? params.userData : { age: 18, introduction: '', nickname: '', sex: 'MALE' };
    const listAgetem = [];
    for (let i = 18; i < 61; i++) {
      listAgetem.push({ value: i });
    }
    this.state = {
      totalInterest: 0,
      selected1: -1,
      selected2: -1,
      selected3: -1,
      nickname: userData.nickname,
      age: userData.age,
      sex: userData.sex,
      introduction: userData.introduction,
      interested: '',
      isLoading: false,
      value: 0,
      listAge: listAgetem,
      area: '',
      dataInterest: [
        {
          selected: false,
          key: 'Travel',
          url: require('../../../assets/imgs/favou/travel.png')
        },
        {
          selected: false,
          key: 'Games',
          url: require('../../../assets/imgs/favou/game.png')
        },
        {
          selected: false,
          key: 'Blind date',
          url: require('../../../assets/imgs/favou/blind_date.png')
        },
        {
          selected: false,
          key: 'Chat',
          url: require('../../../assets/imgs/favou/chat.png')
        },
        {
          selected: false,
          key: 'Pet',
          url: require('../../../assets/imgs/favou/pet.png')
        },
        {
          selected: false,
          key: 'Exercise',
          url: require('../../../assets/imgs/favou/excercise.png')
        },
        {
          selected: false,
          key: 'Music',
          url: require('../../../assets/imgs/favou/music.png')
        },
        {
          selected: false,
          key: 'Movie',
          url: require('../../../assets/imgs/favou/movie.png')
        },
        {
          selected: false,
          key: 'Videochat',
          url: require('../../../assets/imgs/favou/video_chat.png')
        },
        {
          selected: false,
          key: 'Food',
          url: require('../../../assets/imgs/favou/food.png')
        },
        {
          selected: false,
          key: 'Comics',
          url: require('../../../assets/imgs/favou/comics.png')
        },
        {
          selected: false,
          key: 'Fashion',
          url: require('../../../assets/imgs/favou/fashion.png')
        }
      ]
    };

    this.renderItem = this.renderItem.bind(this);
  }

  renderItem = ({ item, index }) => (
    <TouchableOpacity
      onPress={() => {
        if (this.state.totalInterest === 3) {
          if (item.selected) {
            item.selected = !item.selected;
            const temp = [...this.state.dataInterest];
            temp[index].selected = item.selected;
            this.setState({ dataInterest: temp });
            this.setState({ totalInterest: this.state.totalInterest - 1 });
          } else {
            alert(strings.alert, strings.maximun_select);
          }
        } else {
          item.selected = !item.selected;
          const temp = [...this.state.dataInterest];
          temp[index].selected = item.selected;
          this.setState({ dataInterest: temp });
          if (item.selected) {
            this.setState({ totalInterest: this.state.totalInterest + 1 });
          } else {
            this.setState({ totalInterest: this.state.totalInterest - 1 });
          }
        }
      }}
      style={{ justifyContent: 'center', alignItems: 'center', width: DEVICE_WIDTH / 3 }}
    >
      <View
        style={[
          styles.eachInterest,
          {
            backgroundColor: item.selected ? '#C7AE6D' : '#fff'
          }
        ]}
      >
        <Image
          style={{
            height: 28 * SCALE_RATIO_HEIGHT_BASIS,
            width: 28 * SCALE_RATIO_HEIGHT_BASIS
          }}
          resizeMode={'cover'}
          source={item.url}
        />
        <Text
          style={{
            fontFamily: 'Helvetica Neue',
            color: item.selected ? '#fff' : '#C7AE6D',
            fontSize: 10 * SCALE_RATIO_HEIGHT_BASIS
          }}
        >
          {item.key}
        </Text>
      </View>
    </TouchableOpacity>
  );

  // componentDidMount() {
  //   BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  // }
  // componentWillUnmount() {
  //   BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  // }

  // handleBackButton() {
  //   // Alert.alert(strings.alert, strings.please_input_all_info);
  //   Alert.alert('Exit', 'Exit app?', [
  //     { text: 'Cancel', style: 'cancel' },
  //     { text: 'OK', onPress: () => BackHandler.exitApp() }
  //   ]);
  //   return true;
  // }
  render() {
    if (!this.props.userData) return null;
    console.log('Hoang log list area', this.props.listAreas);
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
          <BaseHeader children={<Text style={style.titleHeader}>{strings.add_profile.toUpperCase()}</Text>} />
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null}
            keyboardVerticalOffset={64}
            style={{ flex: 1 }}
          >
            <View style={{ alignSelf: 'center', marginVertical: 28 * SCALE_RATIO_HEIGHT_BASIS }}>
              <TouchableOpacity
                onPress={() => {
                  PopupNotification.showComponent(<PopupNotificationEditProfileImageComponent />);
                }}
              >
                <FastImage
                  style={styles.avatarImage}
                  removeClippedSubviews
                  source={{
                    uri: this.props.userData.avatar
                      ? this.props.userData.avatar
                      : 'https://www.fancyhands.com/images/default-avatar-250x250.png'
                  }}
                  defaultSource={null}
                />
              </TouchableOpacity>
            </View>

            <View
              style={[
                {
                  borderColor: Platform.OS === 'ios' ? '#AE92D330' : '#70707010',
                  borderWidth: 1,
                  backgroundColor: '#fff',
                  borderRadius: 16 * SCALE_RATIO_WIDTH_BASIS,
                  shadowColor: '#C7AE6D',
                  shadowOffset: {
                    width: 0,
                    height: 3
                  },
                  shadowOpacity: 0.5,
                  shadowRadius: 5,
                  elevation: 3,
                  marginHorizontal: 50 * SCALE_RATIO_WIDTH_BASIS,
                  alignSelf: 'center',
                  flexDirection: 'column',
                  paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
                  paddingVertical: 15 * SCALE_RATIO_HEIGHT_BASIS
                }
              ]}
            >
              <MyTextInputFloat
                label={'Nickname'}
                iconColor={'#282828'}
                iconClass={FontAwesomeIcon}
                iconName={'gift'}
                onChangeText={nickname => this.setState({ nickname })}
                value={this.state.nickname}
                returnKeyType="next"
                height={35 * SCALE_RATIO_HEIGHT_BASIS}
                inputStyle={{ height: 20 * SCALE_RATIO_HEIGHT_BASIS }}
              />
              <View
                style={{
                  height: 35 * SCALE_RATIO_HEIGHT_BASIS,
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  alignItems: 'center'
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                  }}
                >
                  <Dropdown
                    pickerStyle={{
                      width: 100 * SCALE_RATIO_WIDTH_BASIS,
                      borderColor: Platform.OS === 'ios' ? '#9297D330' : '#70707010',
                      borderWidth: 1,
                      flexDirection: 'row',
                      borderRadius: (31 * SCALE_RATIO_HEIGHT_BASIS) / 2
                    }}
                    overlayStyle={{ borderTopRightRadius: 0 }}
                    textColor="#282828"
                    fontSize={FS(12)}
                    value={this.state.cash}
                    itemTextStyle={(style.textInput, { color: '#282828', marginBottom: SCALE_RATIO_WIDTH_BASIS })}
                    dropdownOffset={{
                      top: 30 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
                      left: 30 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
                    }}
                    data={[
                      {
                        value: 'MALE',
                        onSelect: onDone => {
                          this.setState({ sex: 'MALE' });
                        }
                      },
                      {
                        value: 'FEMALE',
                        onSelect: onDone => {
                          this.setState({ sex: 'FEMALE' });
                        }
                      },
                      {
                        value: 'ALL',
                        onSelect: onDone => {
                          this.setState({ sex: 'ALL' });
                        }
                      }
                    ]}
                  >
                    <View style={[styles.dropdownContainer, { width: 100 * SCALE_RATIO_WIDTH_BASIS }]}>
                      <Text
                        style={[
                          style.textInput,
                          {
                            color: '#282828',
                            fontSize: 13 * SCALE_RATIO_WIDTH_BASIS,
                            marginRight: 4 * SCALE_RATIO_WIDTH_BASIS
                          }
                        ]}
                      >
                        {strings.gender}
                      </Text>
                      <View
                        style={[
                          style.viewInput,
                          {
                            height: 25 * SCALE_RATIO_HEIGHT_BASIS,
                            width: 80 * SCALE_RATIO_WIDTH_BASIS,
                            paddingVertical: 1 * SCALE_RATIO_HEIGHT_BASIS,
                            paddingHorizontal: 4 * SCALE_RATIO_WIDTH_BASIS,
                            justifyContent: 'space-between',
                            alignItems: 'center'
                          }
                        ]}
                      >
                        <Text
                          style={[
                            style.textInput,
                            {
                              color: '#282828',
                              fontSize: 13 * SCALE_RATIO_WIDTH_BASIS,
                              paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                            }
                          ]}
                          numberOfLines={1}
                        >
                          {this.state.sex}
                        </Text>
                        <MaterialIcons name="arrow-drop-down" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#282828" />
                      </View>
                    </View>
                  </Dropdown>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                  }}
                >
                  <Dropdown
                    pickerStyle={{
                      width: 100 * SCALE_RATIO_WIDTH_BASIS,
                      borderColor: Platform.OS === 'ios' ? '#9297D330' : '#70707010',
                      borderWidth: 1,
                      flexDirection: 'row',
                      borderRadius: (31 * SCALE_RATIO_HEIGHT_BASIS) / 2
                    }}
                    overlayStyle={{ borderTopRightRadius: 0 }}
                    textColor="#282828"
                    fontSize={FS(12)}
                    value={this.state.age}
                    itemTextStyle={(style.textInput, { color: '#282828', marginBottom: SCALE_RATIO_WIDTH_BASIS })}
                    dropdownOffset={{
                      top: 30 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
                      left: 30 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
                    }}
                    data={this.state.listAge.map(e => ({
                      value: e.value,
                      onSelect: () => {
                        this.setState({ age: e.value });
                      }
                    }))}
                  >
                    <View style={[styles.dropdownContainer, { width: 100 * SCALE_RATIO_WIDTH_BASIS }]}>
                      <Text
                        style={[
                          style.textInput,
                          {
                            color: '#282828',
                            fontSize: 13 * SCALE_RATIO_WIDTH_BASIS,
                            marginRight: 4 * SCALE_RATIO_WIDTH_BASIS
                          }
                        ]}
                      >
                        {strings.age}
                      </Text>
                      <View
                        style={[
                          style.viewInput,
                          {
                            height: 25 * SCALE_RATIO_HEIGHT_BASIS,
                            width: 80 * SCALE_RATIO_WIDTH_BASIS,
                            paddingVertical: 1 * SCALE_RATIO_HEIGHT_BASIS,
                            paddingHorizontal: 4 * SCALE_RATIO_WIDTH_BASIS,
                            justifyContent: 'space-between',
                            alignItems: 'center'
                          }
                        ]}
                      >
                        <Text
                          style={[
                            style.textInput,
                            {
                              color: '#282828',
                              fontSize: 13 * SCALE_RATIO_WIDTH_BASIS,
                              paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                            }
                          ]}
                          numberOfLines={1}
                        >
                          {this.state.age}
                        </Text>
                        <MaterialIcons name="arrow-drop-down" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#282828" />
                      </View>
                    </View>
                  </Dropdown>
                </View>
              </View>
              <View>
                <Text
                  style={{
                    color: '#282828',
                    fontFamily: 'Helvetica Neue',
                    fontSize: 13 * SCALE_RATIO_WIDTH_BASIS
                  }}
                >
                  {strings.introduce_myself}
                </Text>
                <TextInput
                  style={{
                    height: 80 * SCALE_RATIO_HEIGHT_BASIS,
                    width: 270 * SCALE_RATIO_WIDTH_BASIS,
                    color: '#282828',
                    borderColor: '#C7AE6D',
                    borderWidth: 1 * SCALE_RATIO_HEIGHT_BASIS,
                    borderRadius: 5 * SCALE_RATIO_HEIGHT_BASIS
                  }}
                  placeholder={'Introduction...'}
                  placeholderTextColor={'#282828'}
                  multiline
                  onChangeText={introduction => this.setState({ introduction })}
                  value={this.state.introduction}
                />
              </View>
            </View>
            <Text
              style={{
                color: '#282828',
                fontFamily: 'Helvetica Neue',
                fontSize: 16 * SCALE_RATIO_WIDTH_BASIS,
                marginTop: 10 * SCALE_RATIO_HEIGHT_BASIS,
                marginBottom: 3 * SCALE_RATIO_HEIGHT_BASIS,
                marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS,
                textDecorationLine: 'underline'
              }}
            >
              {strings.favourite}{' '}
            </Text>
            <Text
              style={{
                color: '#282828',
                fontFamily: 'Helvetica Neue',
                fontSize: 10 * SCALE_RATIO_WIDTH_BASIS,
                marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS,
                marginBottom: 5 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              {strings.favourite_description}
            </Text>
            <FlatList
              numColumns={3}
              style={{
                // marginLeft: 35 * SCALE_RATIO_WIDTH_BASIS,
                marginBottom: 53 * SCALE_RATIO_HEIGHT_BASIS,
                alignSelf: 'center'
              }}
              contentContainerStyle={{
                width: DEVICE_WIDTH,
                justifyContent: 'center',
                alignSelf: 'center'
              }}
              data={this.state.dataInterest}
              renderItem={this.renderItem}
            />
          </KeyboardAvoidingView>
        </ScrollView>
        <TouchableOpacity
          onPress={() => {
            if (
              !this.state.nickname ||
              !this.state.introduction ||
              this.state.nickname.length <= 0 ||
              this.state.age <= 0 ||
              this.state.introduction.length <= 0
            ) {
              alert(strings.alert, strings.fill_all_info);
            } else {
              const temp = { ...this.props.userData };
              console.log('Hoang log 1', temp);
              temp.nickname = this.state.nickname;

              temp.sex = this.state.sex === 'ALL' ? 'OTHER' : this.state.sex;

              temp.age = this.state.age;
              temp.introduction = this.state.introduction;
              temp.interests = '';
              this.state.dataInterest.forEach(e => {
                if (e.selected) {
                  temp.interests = `${temp.interests} #${e.key}`;
                }
              });
              MySpinner.show();
              this.props.updateUserInfo(temp, () => {
                MySpinner.hide();
                this.props.navigation.navigate(ROUTE_KEY.MAIN);
              });
            }
          }}
          style={styles.btnNext}
        >
          <Text style={[style.textButton, { textAlign: 'center' }]}>{strings.confirm.toUpperCase()}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userData: state.user.userData,
  token: state.user,
  listAreas: state.area.listAreas
});

const mapActionCreators = {
  updateUserInfo
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(AddProfileComponent);

const styles = StyleSheet.create({
  text: {
    color: '#282828',
    fontSize: 14 * SCALE_RATIO_WIDTH_BASIS,
    fontFamily: 'Helvetica Neue'
  },
  avatarArea: {
    height: 50 * SCALE_RATIO_HEIGHT_BASIS,
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatarImage: {
    width: width / 2,
    height: width / 2,
    borderRadius: width / 4
  },
  avatar: {
    height: 30 * SCALE_RATIO_HEIGHT_BASIS,
    width: 30 * SCALE_RATIO_HEIGHT_BASIS,
    borderRadius: 15 * SCALE_RATIO_HEIGHT_BASIS,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red'
  },
  fillInfo: {
    height: 172 * SCALE_RATIO_HEIGHT_BASIS,
    marginHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS
  },
  favourite: {
    flexDirection: 'row',
    height: 55 * SCALE_RATIO_HEIGHT_BASIS,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 5 * SCALE_RATIO_HEIGHT_BASIS
  },
  favouritebtn: {
    height: 15 * SCALE_RATIO_HEIGHT_BASIS,
    width: 15 * SCALE_RATIO_HEIGHT_BASIS,
    borderRadius: 7.5 * SCALE_RATIO_HEIGHT_BASIS,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red'
  },
  btnNext: {
    position: 'absolute',
    height: 50 * SCALE_RATIO_HEIGHT_BASIS,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#C7AE6D',
    bottom: 0,
    width: DEVICE_WIDTH
  },
  eachInfo: {
    height: 40 * SCALE_RATIO_HEIGHT_BASIS,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 2 * SCALE_RATIO_HEIGHT_BASIS
  },
  eachInterest: {
    height: 70 * SCALE_RATIO_HEIGHT_BASIS,
    width: 70 * SCALE_RATIO_HEIGHT_BASIS,
    borderRadius: (70 * SCALE_RATIO_HEIGHT_BASIS) / 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20 * SCALE_RATIO_WIDTH_BASIS,
    marginHorizontal: 18 * SCALE_RATIO_WIDTH_BASIS,
    borderWidth: 1.5 * SCALE_RATIO_WIDTH_BASIS,
    borderColor: '#C7AE6D'
  },
  dropdownContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 30 * SCALE_RATIO_HEIGHT_BASIS
  },
  flagInfoImage: {
    width: 37.38 * SCALE_RATIO_WIDTH_BASIS,
    height: 24.92 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 4 * SCALE_RATIO_WIDTH_BASIS,
    marginLeft: 10.92 * SCALE_RATIO_WIDTH_BASIS
  }
});
