import { LOAD_STORE_INFORMATION_FOR_USER, BASE_URL, STORE_API } from '../../../constants/Constants';
import request from '../../../utils/request';
import { alert } from '../../../utils/alert';
import strings from '../../../constants/Strings';

export function updateStoreInfo(newInfo, onDoneFunc, onErrFunc) {
    return (dispatch, store) => {
        request.put(`${BASE_URL}${STORE_API}/${store().user.storeInfo.id}`)
            .set('Content-Type', 'application/json')
            .set('Authorization', `Bearer ${store().user.token}`)
            .send(newInfo)
            .finish((err, res) => {
                if (err) {
                    alert(
                        strings.alert,
                        strings.alert_edit_info_fail,
                        onErrFunc()
                    );
                } else {
                    dispatch({
                        type: EDIT_PROFILE,
                        payload: newInfo,
                    });
                    onDoneFunc();
                }
            });
    };
}
