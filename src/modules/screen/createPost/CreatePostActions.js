import request from '../../../utils/request';
import { BASE_URL, POST_API } from '../../../constants/Constants';

export function createPost({ title, content, list_image }, onFinish = () => { }, onSuccess = () => { }, onFail = () => { }) {
    return (dispatch, store) => {
        request.post(`${BASE_URL}/${POST_API}`)
            .set('Content-Type', 'application/json')
            .set('Authorization', `Bearer ${store().user.token}`)
            .send({
                title,
                content,
                list_image,
            })
            .finish((err, res) => {
                onFinish();
                if (!err && res.body.code === 200) {
                    onSuccess();
                } else {
                    onFail();
                    console.log('Hoang log err post', err);
                }
            });
    };
}
