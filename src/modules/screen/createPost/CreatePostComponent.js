/* eslint-disable max-line-length */
import React from 'react';
import { Alert, Image, StyleSheet, Text, TextInput, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  FS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import MyButton from '../../../style/MyButton';
import getFlag from '../../../utils/getFlag';
import getGenderImage from '../../../utils/getGenderImage';
import { launchCamera, launchImageLibrary } from '../../../utils/imagePicker';
import { uploadImage } from '../../../utils/uploadImage';
import HeaderWithBackButtonComponent from '../../view/HeaderWithBackButtonComponent';
import MyComponent from '../../view/MyComponent';
import MyImage from '../../view/MyImage';
import MySpinner from '../../view/MySpinner';
import { createPost } from './CreatePostActions';

const defaultImage = require('../../../assets/imgs/default_product_image.jpg');

class CreatePostComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      listImage: null,
      image: null,
      content: '',
      title: ''
    };
    this.launchImageLibrary = this.launchImageLibrary.bind(this);
    this.launchCamera = this.launchCamera.bind(this);
    this.createPost = this.createPost.bind(this);
  }

  launchImageLibrary() {
    launchImageLibrary()
      .then(response => {
        this.setState({ image: response });
      })
      .catch(() => {});
  }

  launchCamera() {
    launchCamera()
      .then(response => {
        this.setState({ image: response });
      })
      .catch(() => {});
  }

  onUploadFail() {
    MySpinner.hide();
    Alert.alert(
      strings.alert,
      strings.create_post_fail,
      [{ text: strings.yes, onPress: () => {} }],
      { cancelable: true }
    );
  }

  createPost() {
    MySpinner.show();
    if (this.state.image) {
      uploadImage([this.state.image], this.props.token)
        .then(res => {
          this.setState({ listImage: res });
          console.log('Hoang log res upload Image', res);
          this.props.createPost(
            {
              title: this.state.title,
              content: this.state.content,
              list_image: res
            },
            MySpinner.hide,
            () => {
              this.props.navigation.replace(ROUTE_KEY.MY_POST_COMPONENT);
            },
            this.onUploadFail
          );
        })
        .catch(err => {
          console.log('Hoang log err upload Image', err);
        });
    } else {
      this.props.createPost(
        {
          title: this.state.title,
          content: this.state.content
        },
        MySpinner.hide,
        () => {
          this.props.navigation.replace(ROUTE_KEY.MY_POST_COMPONENT);
        },
        this.onUploadFail
      );
    }

    // if (!this.state.listImage) {
    //   console.log('Hoang log list_Image to Post', this.state.listImage);
    // } else {
    // }
  }

  render() {
    const { flag, nickname, email, age, interests, sex } = this.props.userData;

    return (
      <KeyboardAwareScrollView style={styles.container}>
        <HeaderWithBackButtonComponent
          bodyTitle={strings.post_now}
          onPress={() => this.props.navigation.goBack()}
        />
        <View style={[styles.bodyContainer, style.shadow]}>
          <MyImage
            source={{
              uri: this.state.image
                ? this.state.image.uri
                : 'http://www.rangerwoodperiyar.com/images/joomlart/demo/default.jpg'
            }}
            style={styles.image}
            styleContent={{ borderWidth: 0 }}
            resizeMode="cover"
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginVertical: 16.67 * SCALE_RATIO_HEIGHT_BASIS,
              marginHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <MyButton
              leftIcon="camera"
              leftIconType="Feather"
              textStyle={{ fontSize: FS(10) }}
              outline
              onPress={this.launchCamera}
            >
              {strings.take_photo_button_title}
            </MyButton>
            <MyButton
              leftIcon="image"
              leftIconType="Feather"
              textStyle={{ fontSize: FS(10) }}
              onPress={this.launchImageLibrary}
            >
              {strings.choose_from_library_button_title}
            </MyButton>
          </View>
          <View style={styles.infoContainer}>
            <Image source={getFlag(flag)} style={styles.flagInfoImage} />
            <Text
              style={[
                style.textNormal,
                styles.textInfo,
                { marginHorizontal: 8 * SCALE_RATIO_WIDTH_BASIS }
              ]}
            >
              {nickname || email}
            </Text>
            {getGenderImage(sex, 1.5)}
            <Text style={[style.textNormal, styles.textInfo]}>{age}</Text>
          </View>
          {interests ? (
            <Text
              style={[
                style.textNormal,
                styles.textInfo,
                {
                  marginTop: 7 * SCALE_RATIO_HEIGHT_BASIS,
                  marginHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
            >
              {interests}
            </Text>
          ) : null}
          <TextInput
            placeholder={strings.title}
            placeholderTextColor="rgb(156, 159, 170)"
            underlineColorAndroid="transparent"
            autoCapitalize="none"
            autoCorrect={false}
            style={{
              ...style.textNormal,
              fontSize: FS(12),
              textAlignVertical: 'top',
              borderRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
              borderWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
              borderColor: '#C7AE6D',
              paddingTop: 10 * SCALE_RATIO_WIDTH_BASIS,
              paddingLeft: 14 * SCALE_RATIO_WIDTH_BASIS,
              paddingRight: 14 * SCALE_RATIO_WIDTH_BASIS,
              marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
              marginTop: 15 * SCALE_RATIO_WIDTH_BASIS,
              height: 40 * SCALE_RATIO_HEIGHT_BASIS
            }}
            multiline
            value={this.state.title}
            onChangeText={title => this.setState({ title })}
          />
          <TextInput
            placeholder={strings.whats_on_your_mind}
            placeholderTextColor="rgb(156, 159, 170)"
            underlineColorAndroid="transparent"
            autoCapitalize="none"
            autoCorrect={false}
            style={{
              ...style.textNormal,
              fontSize: FS(12),
              textAlignVertical: 'top',
              borderRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
              borderWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
              borderColor: '#C7AE6D',
              paddingTop: 14 * SCALE_RATIO_WIDTH_BASIS,
              paddingBottom: 14 * SCALE_RATIO_WIDTH_BASIS,
              paddingLeft: 14 * SCALE_RATIO_WIDTH_BASIS,
              paddingRight: 14 * SCALE_RATIO_WIDTH_BASIS,
              margin: 15 * SCALE_RATIO_WIDTH_BASIS,
              height: 120 * SCALE_RATIO_HEIGHT_BASIS
            }}
            multiline
            value={this.state.content}
            onChangeText={content => this.setState({ content })}
          />
        </View>
        <Text style={[style.textNormal, styles.descriptionText]}>
          {strings.create_post_description}
        </Text>
        <MyButton
          style={{ alignSelf: 'center' }}
          width={135 * SCALE_RATIO_WIDTH_BASIS}
          leftIcon="edit-2"
          leftIconType="Feather"
          textStyle={{ fontSize: FS(12) }}
          onPress={this.createPost}
        >
          {strings.post_now.toLocaleUpperCase()}
        </MyButton>
      </KeyboardAwareScrollView>
    );
  }
}

const mapStateToProps = state => ({
  userData: state.user.userData,
  token: state.user.token
});

const mapActionCreators = {
  createPost
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(CreatePostComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  userInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 100 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },

  bodyContainer: {
    backgroundColor: '#fff',
    borderRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
    overflow: 'hidden',
    marginHorizontal: 28 * SCALE_RATIO_WIDTH_BASIS,
    marginVertical: 10 * SCALE_RATIO_HEIGHT_BASIS
  },
  image: {
    width: 320 * SCALE_RATIO_WIDTH_BASIS,
    height: 170 * SCALE_RATIO_WIDTH_BASIS,
    borderTopLeftRadius: 15 * SCALE_RATIO_WIDTH_BASIS,
    borderTopRightRadius: 15 * SCALE_RATIO_WIDTH_BASIS
  },
  descriptionText: {
    fontSize: FS(9),
    marginTop: 15 * SCALE_RATIO_HEIGHT_BASIS,
    marginBottom: 10 * SCALE_RATIO_HEIGHT_BASIS,
    textAlign: 'center',
    marginHorizontal: 41 * SCALE_RATIO_WIDTH_BASIS
  },
  infoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
    marginBottom: 10 * SCALE_RATIO_HEIGHT_BASIS
  },
  flagInfoImage: {
    width: 24.4 * SCALE_RATIO_WIDTH_BASIS,
    height: 16.26 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 4 * SCALE_RATIO_WIDTH_BASIS
  },
  textInfo: {
    fontSize: FS(14)
  }
});
