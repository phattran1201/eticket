import { BASE_URL } from '../../../constants/Constants';
import request from '../../../utils/request';

const LIMIT = 10;

export const loadCashHistory = id =>
  new Promise((resolve, reject) => {
    request
      .get(
        `${BASE_URL}cash_history?fields=["$all"]&filter={"user_id":"${id}"}&limit=${LIMIT}&page=1`
      )
      .finish((err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res.body.results.objects.rows);
        }
      });
  });

export const loadMoreCashHistory = (id, page) =>
  new Promise((resolve, reject) => {
    request
      .get(
        `${BASE_URL}cash_history?fields=["$all"]&filter={"user_id":"${id}"}&limit=${LIMIT}&page=${page}`
      )
      .finish((err, res) => {
        // console.log('loadMoreWalletExport', res);
        if (err) {
          reject(err);
        } else {
          resolve(res.body.results.objects.rows);
        }
      });
  });

export const refreshCashHistory = id =>
  new Promise((resolve, reject) => {
    request
      .get(
        `${BASE_URL}cash_history?fields=["$all"]&filter={"user_id":"${id}"}&limit=${LIMIT}&page=1`
      )
      .finish((err, res) => {
        // console.log('refreshWalletExport', res);
        if (!err) {
          resolve(res.body.results.objects.rows);
        } else {
          reject(err);
        }
      });
  });
//------------------------------------------------------------------------------------HeartHistory---------------------------------------------------------------------------------------------------
export const loadHeartHistory = id =>
  new Promise((resolve, reject) => {
    request
      .get(
        `${BASE_URL}heart_history?fields=["$all"]&filter={"user_id":"${id}"}&limit=${LIMIT}&page=1`
      )
      .finish((err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res.body.results.objects.rows);
        }
      });
  });

export const loadMoreHeartHistory = (id, page) =>
  new Promise((resolve, reject) => {
    request
      .get(
        `${BASE_URL}heart_history?fields=["$all"]&filter={"user_id":"${id}"}&limit=${LIMIT}&page=${page}`
      )
      .finish((err, res) => {
        // console.log('loadMoreHeartHistory', res);
        if (err) {
          reject(err);
        } else {
          resolve(res.body.results.objects.rows);
        }
      });
  });

export const refreshHeartHistory = id =>
  new Promise((resolve, reject) => {
    request
      .get(
        `${BASE_URL}heart_history?fields=["$all"]&filter={"user_id":"${id}"}&limit=${LIMIT}&page=1`
      )
      .finish((err, res) => {
        // console.log('refreshHeartHistory', res);
        if (!err) {
          resolve(res.body.results.objects.rows);
        } else {
          reject(err);
        }
      });
  });
