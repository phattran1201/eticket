/* eslint-disable no-unused-vars */
/* eslint-disable max-line-length */
/* eslint-disable import/imports-first */
import LottieView from 'lottie-react-native';
import moment from 'moment';
import React from 'react';
import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import MySpinner from '../../view/MySpinner';
import {
  loadCashHistory,
  loadHeartHistory,
  loadMoreCashHistory,
  loadMoreHeartHistory,
  refreshCashHistory,
  refreshHeartHistory
} from './HistoryActions';

const { height, width } = Dimensions.get('window');
class HistoryComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      loading: false,
      outOfData: false,
      refreshing: false,
      page2: 1,
      loading2: false,
      outOfData2: false,
      refreshing2: false
    };
    this.listCashHistory = [];
    this.listHeartHistory = [];
  }

  renderItem = ({ item }) => {
    const { params } = this.props.navigation.state;
    const type = params && params.type ? params.type : '';
    return (
      <View
        style={{
          width: DEVICE_WIDTH,
          flexDirection: 'row',
          borderBottomWidth: 1 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
          borderColor: '#F4F0F9',
          marginTop: 5 * SCALE_RATIO_HEIGHT_BASIS
        }}
      >
        <View
          style={{
            flex: 3,
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10 * SCALE_RATIO_HEIGHT_BASIS,
            borderRightWidth: 1 * SCALE_RATIO_HEIGHT_BASIS,
            borderColor: '#F4F0F9'
          }}
        >
          <Text style={style.textCaption}>{moment(item.created_at).format('DD/MM/YYYY')}</Text>
        </View>
        <View
          style={{
            flex: 4,
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10 * SCALE_RATIO_HEIGHT_BASIS,
            borderRightWidth: 1 * SCALE_RATIO_HEIGHT_BASIS,
            borderColor: '#F4F0F9'
          }}
        >
          <Text
            style={[style.textCaption, { padding: 3 * SCALE_RATIO_WIDTH_BASIS }]}
            numberOfLines={1}
          >
            {item.history}
          </Text>
        </View>
        <View
          style={{
            flex: 3,
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10 * SCALE_RATIO_HEIGHT_BASIS,
            borderRightWidth: 1 * SCALE_RATIO_HEIGHT_BASIS,
            borderColor: '#F4F0F9',
            flexDirection: 'row'
          }}
        >
          <Text style={[style.textCaption, { marginRight: 2 * SCALE_RATIO_WIDTH_BASIS }]}>
            {type === 'HEART' ? item.heart : item.cash}
          </Text>
          {type === 'HEART' ? (
            <Icon name={'heart'} size={12 * SCALE_RATIO_WIDTH_BASIS} color={'#C7AE6D'} />
          ) : (
            <Image
              source={require('../../../assets/imgs/icons/coin.png')}
              resizeMode={'contain'}
              style={{
                height: 11 * SCALE_RATIO_HEIGHT_BASIS,
                width: 11 * SCALE_RATIO_WIDTH_BASIS
              }}
            />
          )}
        </View>
      </View>
    );
  };
  handleLoadMore = () => {
    const { params } = this.props.navigation.state;
    const type = params && params.type ? params.type : [];
    if (this.state.outOfData || this.state.loading) return;
    if (this.state.outOfData2 || this.state.loading2) return;
    if (type === 'HEART') {
      this.setState({ loading2: true });
      loadMoreHeartHistory(this.props.userData.id, this.state.page2)
        .then(listHeartHistory => {
          if (listHeartHistory.length === 0) {
            this.setState({ outOfData2: true });
          } else {
            listHeartHistory.forEach(post => {
              this.listHeartHistory.push(post);
            });
            this.setState(previousState2 => ({
              loading2: false,
              page2: previousState2.page2 + 1
            }));
          }
        })
        .catch(err => this.setState({ loading2: false }));
    }
    if (type === 'CASH') {
      this.setState({ loading: true });
      loadMoreCashHistory(this.props.userData.id, this.state.page)
        .then(listCashHistory => {
          if (listCashHistory.length === 0) {
            this.setState({ outOfData: true });
          } else {
            listCashHistory.forEach(post => {
              this.listCashHistory.push(post);
            });
            this.setState(previousState => ({
              loading: false,
              page: previousState.page + 1
            }));
          }
        })
        .catch(err => this.setState({ loading: false }));
    }
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const type = params && params.type ? params.type : [];
    if (type === 'HEART') {
      MySpinner.show();
      this.setState({ loading2: true });
      loadHeartHistory(this.props.userData.id)
        .then(listHeartHistory => {
          listHeartHistory.forEach(post => {
            this.listHeartHistory.push(post);
          });
          this.setState(previousState => ({
            loading2: false,
            page2: 2
          }));
          MySpinner.hide();
        })
        .catch(err => {
          MySpinner.hide();
          this.setState({ loading2: false });
        });
    }
    if (type === 'CASH') {
      MySpinner.show();
      this.setState({ loading: true });
      loadCashHistory(this.props.userData.id)
        .then(listCashHistory => {
          listCashHistory.forEach(post => {
            this.listCashHistory.push(post);
          });
          this.setState(previousState => ({
            loading: false,
            page: 2
          }));
          MySpinner.hide();
        })
        .catch(err => {
          MySpinner.hide();
          this.setState({ loading: false });
        });
    }
  }

  renderFooter = () => {
    const { params } = this.props.navigation.state;
    const type = params && params.type ? params.type : [];
    if (type === 'HEART') {
      if (this.state.outOfData2) {
        return null;
      }
      if (!this.state.refreshing2 && this.listHeartHistory.length === 0) {
        return (
          <LottieView
            source={require('../../../assets/isempty.json')}
            autoPlay
            loop
            hardwareAccelerationAndroid
            style={{
              width: 240 * SCALE_RATIO_WIDTH_BASIS,
              height: 300 * SCALE_RATIO_WIDTH_BASIS,
              alignSelf: 'center'
            }}
          />
        );
      }

      return (
        !this.state.outOfData2 &&
        this.listHeartHistory.length !== 0 &&
        this.state.loading2 && (
          <View
            style={{
              paddingTop: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              paddingBottom: 15 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
            }}
          >
            <ActivityIndicator animating size="small" />
          </View>
        )
      );
    }
    if (type === 'CASH') {
      if (this.state.outOfData) {
        return null;
      }
      if (!this.state.refreshing && this.listCashHistory.length === 0) {
        return (
          <LottieView
            source={require('../../../assets/isempty.json')}
            autoPlay
            loop
            hardwareAccelerationAndroid
            style={{
              width: 240 * SCALE_RATIO_WIDTH_BASIS,
              height: 300 * SCALE_RATIO_WIDTH_BASIS,
              alignSelf: 'center'
            }}
          />
        );
      }

      return (
        !this.state.outOfData &&
        this.listCashHistory.length !== 0 &&
        this.state.loading && (
          <View
            style={{
              paddingTop: 5 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
              paddingBottom: 15 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
            }}
          >
            <ActivityIndicator animating size="small" />
          </View>
        )
      );
    }
  };

  onRefresh = () => {
    this.refs.listRef.scrollToOffset({ x: 0, y: 0, animated: true });
    const { params } = this.props.navigation.state;
    const type = params && params.type ? params.type : [];
    if (type === 'HEART') {
      this.setState({ refreshing2: true });
      refreshHeartHistory(this.props.userData.id)
        .then(res => {
          if (res.length === 0) {
            this.listHeartHistory = [];
            this.setState({ refreshing2: false });
          } else {
            this.listHeartHistory = [];
            res.forEach(post => {
              this.listHeartHistory.push(post);
            });
            this.setState(previousState => ({
              page2: 2,
              outOfData2: false,
              refreshing2: false,
              loading2: false
            }));
          }
        })
        .catch(err => {
          this.setState({
            refreshing2: false
          });
        });
    }
    if (type === 'CASH') {
      this.setState({ refreshing: true });
      refreshCashHistory(this.props.userData.id)
        .then(res => {
          if (res.length === 0) {
            this.listCashHistory = [];
            this.setState({ refreshing: false });
          } else {
            this.listCashHistory = [];
            res.forEach(post => {
              this.listCashHistory.push(post);
            });
            this.setState(previousState => ({
              page: 2,
              outOfData: false,
              refreshing: false,
              loading: false
            }));
          }
        })
        .catch(err => {
          this.setState({
            refreshing: false
          });
        });
    }
  };

  getDataForFlatList = () => {
    const { params } = this.props.navigation.state;
    const type = params && params.type ? params.type : [];
    if (type === 'HEART') {
      let currentYear = null;
      const listHeartHistory = this.listHeartHistory.map(e => {
        if (moment(e.created_at).format('YYYY') === currentYear) {
          return e;
        }
        currentYear = moment(e.created_at).format('YYYY');
        return {
          ...e,
          time: currentYear
        };
      });
      return listHeartHistory;
    }
    if (type === 'CASH') {
      let currentYear = null;
      const listCashHistory = this.listCashHistory.map(e => {
        if (moment(e.created_at).format('YYYY') === currentYear) {
          return e;
        }
        currentYear = moment(e.created_at).format('YYYY');
        return {
          ...e,
          time: currentYear
        };
      });
      return listCashHistory;
    }
  };

  render() {
    const { params } = this.props.navigation.state;
    const title = params && params.title ? params.title : '';
    console.log('​render -> title', title);
    // const subTitle = params && params.subTitle ? params.subTitle : '';
    // const value = params && params.value ? params.value : '';
    const type = params && params.type ? params.type : [];
    const unit = params && params.unit ? params.unit : [];

    const listCashHistory = this.listCashHistory;
    const listHeartHistory = this.listHeartHistory;

    console.log('​render -> listHeartHistory', listHeartHistory);
    console.log('​render -> listCashHistory', listCashHistory);

    return (
      <View style={styles.container}>
        <BaseHeader
          children={<Text style={style.titleHeader}>{title}</Text>}
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
          rightIcon="refresh-cw"
          rightIconType="Feather"
          onRightPress={this.onRefresh}
        />
        <View style={styles.showheart}>
          <Text style={[style.text, { color: '#282828', fontSize: 14 * SCALE_RATIO_WIDTH_BASIS }]}>
            {type === 'HEART' ? strings.current_heart : strings.current_cash}
          </Text>
          <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <Text
              style={{
                marginRight: 5 * SCALE_RATIO_WIDTH_BASIS,
                color: '#282828',
                fontSize: 30 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              {type === 'HEART' ? this.props.userData.heart : this.props.userData.cash}
            </Text>

            {type === 'HEART' ? (
              <Icon name={'heart'} size={28 * SCALE_RATIO_WIDTH_BASIS} color={'#C7AE6D'} />
            ) : (
              <Image
                resizeMode={'contain'}
                source={require('../../../assets/imgs/icons/coin.png')}
                style={{
                  height: 25 * SCALE_RATIO_HEIGHT_BASIS,
                  width: 25 * SCALE_RATIO_WIDTH_BASIS
                }}
              />
            )}
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center',
            height: 40 * SCALE_RATIO_HEIGHT_BASIS,
            width: 350 * SCALE_RATIO_WIDTH_BASIS,
            borderRadius: (40 * SCALE_RATIO_HEIGHT_BASIS) / 2,
            borderWidth: 1.5 * SCALE_RATIO_HEIGHT_BASIS,
            borderColor: '#C7AE6D'
          }}
        >
          <Text
            style={[
              style.text,
              {
                color: '#282828',
                flex: 1,
                textAlign: 'center'
              }
            ]}
          >
            {strings.date}
          </Text>
          <Text style={[style.text, { color: '#282828', flex: 1, textAlign: 'center' }]}>
            {strings.history}
          </Text>
          <Text style={[style.text, { color: '#282828', flex: 1, textAlign: 'center' }]}>
            {unit}
          </Text>
        </View>
        <FlatList
          ref="listRef"
          data={this.getDataForFlatList()}
          keyExtractor={item => item.id}
          renderItem={this.renderItem}
          style={{ zIndex: 1, flex: 1 }}
          ListFooterComponent={this.renderFooter}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={0.01}
          onRefresh={this.onRefresh}
          refreshing={this.state.refreshing}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userData: state.user.userData
});

const mapActionCreators = {
  loadCashHistory,
  loadMoreCashHistory,
  refreshCashHistory,
  loadHeartHistory,
  loadMoreHeartHistory,
  refreshHeartHistory
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(HistoryComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  textLabelContainer: {
    padding: 15 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    borderBottomWidth: 1 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    borderColor: '#000'
  },
  textLabel: {
    fontSize: 18 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },
  itemContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    borderColor: '#a7aaaf',
    padding: 15 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  itemLeftContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  showheart: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15 * SCALE_RATIO_HEIGHT_BASIS,
    marginBottom: 20 * SCALE_RATIO_HEIGHT_BASIS
  },
  text: {
    color: '#282828',
    fontFamily: 'Helvetica Neue',
    fontSize: 14 * SCALE_RATIO_WIDTH_BASIS
  }
});
