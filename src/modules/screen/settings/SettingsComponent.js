import React from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import RNRestart from 'react-native-restart';
import Icons from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  APP_COLOR,
  FS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import { alert } from '../../../utils/alert';
import HeaderWithBackButtonComponent from '../../view/HeaderWithBackButtonComponent';
import MyComponent from '../../view/MyComponent';
import MySpinner from '../../view/MySpinner';
import PopupNotification from '../../view/PopupNotification';
import PopupNotificationChildComponent from '../../view/PopupNotificationChildComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import SwitchComponent from '../../view/SwitchComponent';
import { deleteUser } from '../editprofile/EditProfieActions';
import { changeUserSetting, getUserSettings, updatePhoneNumber } from './SettingsActions';
import { ListItem } from 'react-native-elements';
import style, { FONT } from '../../../constants/style';
import BaseHeader from '../../view/BaseHeader';

class SettingsComponent extends MyComponent {
  constructor(props) {
    super(props);
    const { currentSettings } = this.props;
    this.state = {
      notification_sound:
        currentSettings && currentSettings.notification_sound ? currentSettings.notification_sound : false,
      vibration_alert: currentSettings && currentSettings.vibration_alert ? currentSettings.vibration_alert : false,
      incomming_video_call_alert:
        currentSettings && currentSettings.incomming_video_call_alert
          ? currentSettings.incomming_video_call_alert
          : false,
      push_notification:
        currentSettings && currentSettings.push_notification ? currentSettings.push_notification : false
    };
    this.listSetting = [
      {
        title: 'Thay đổi mật khẩu',
        icon: 'email',
        route: ROUTE_KEY
      },
      {
        title: 'Email thông báo',
        icon: 'lock',
        route: ROUTE_KEY
      },
      {
        title: 'Đánh giá ứng dụng',
        icon: 'star',
        iconImg: require('../../../assets/imgs/menu/menu_2.png'),
        route: ROUTE_KEY
      }
    ];
  }
  // componentDidMount() {
  //   this.props.getUserSettings();
  // }
  deleteAccount() {
    console.log('Hoang');
    this.props.deleteUser(this.props.token, () => {
      this.props.navigation.navigate(ROUTE_KEY.PRELOGIN);
    });
  }
  render() {
    // const { currentSettings } = this.props;
    // console.log(
    //   '​SettingsComponent -> render -> 1-2-3-4',
    //   this.state.notification_sound,
    //   this.state.vibration_alert,
    //   this.state.incomming_video_call_alert,
    //   this.state.push_notification
    // );
    // console.log('​SettingsComponent -> render -> currentSettings', this.props.currentSettings);
    return (
      <View style={styles.container}>
        <BaseHeader
          translucent
          children={<Text style={style.titleHeader}>{strings.settings.toUpperCase()}</Text>}
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
        />
        <View
          style={{
            paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          {this.listSetting.map((item, i) => (
            <ListItem
              containerStyle={{
                // padding: 5 * SCALE_RATIO_WIDTH_BASIS,
                paddingHorizontal: 30 * SCALE_RATIO_WIDTH_BASIS,
                borderBottomColor: `${APP_COLOR}80`,
                borderBottomWidth: 1 * SCALE_RATIO_HEIGHT_BASIS
              }}
              key={i}
              onPress={() => this.props.navigation.navigate(`${item.route}`)}
              title={item.title}
              // leftAvatar={{
              //   source: item.iconImg,
              //   avatarStyle: {
              //     width: 18 * SCALE_RATIO_WIDTH_BASIS,
              //     height: 18 * SCALE_RATIO_WIDTH_BASIS,
              //     resizeMode: 'contain'
              //   },
              //   containerStyle: { backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' },
              //   overlayContainerStyle: { backgroundColor: 'transparent' }
              // }}
              leftIcon={{ name: item.icon }}
              titleStyle={[
                style.text,
                {
                  fontFamily: FONT.Medium,
                  fontSize: FS(16)
                }
              ]}
              chevron
            />
          ))}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  currentSettings: state.user.currentSettings,
  userData: state.user.userData,
  token: state.user.token
});

const mapActionCreators = {
  changeUserSetting,
  getUserSettings,
  deleteUser,
  updatePhoneNumber
};
export default connect(
  mapStateToProps,
  mapActionCreators
)(SettingsComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  textLabelContainer: {
    padding: 10 * SCALE_RATIO_HEIGHT_BASIS
  },
  textLabel: {
    fontSize: 18 * SCALE_RATIO_HEIGHT_BASIS,
    fontFamily: 'Helvetica Neue',
    color: '#282828',
    borderBottomWidth: 0.5 * SCALE_RATIO_HEIGHT_BASIS,
    borderColor: '#C7AE6D'
  },
  itemContainer: {
    flexDirection: 'row',
    padding: 10 * SCALE_RATIO_HEIGHT_BASIS,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  itemLeftContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  }
});
