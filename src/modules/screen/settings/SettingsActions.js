import {
  BASE_URL,
  UPDATE_USER_SETTINGS,
  USER_SETTING_API,
  VIETNAM_MOBILE_PHONE_PREFIX,
  UPDATE_USER_INFO,
  accountKitThemeForIOS
} from '../../../constants/Constants';
import request from '../../../utils/request';
import MySpinner from '../../view/MySpinner';
import { alert } from '../../../utils/alert';
import strings from '../../../constants/Strings';
import RNAccountKit from 'react-native-facebook-account-kit';
import { loadUserProfile } from '../splash/SplashActions';
import PopupNotification from '../../view/PopupNotification';

export function updatePhoneNumber(onDoneFunc = () => {}) {
  return (dispatch, store) => {
    RNAccountKit.configure({
      initialPhoneCountryPrefix: VIETNAM_MOBILE_PHONE_PREFIX,
      initialPhoneNumber: '',
      theme: accountKitThemeForIOS
    });
    RNAccountKit.loginWithPhone().then(token => {
      if (!token) {
        console.log('Login cancelled');
      } else {
        const acckit_token = token.token;
        console.log('Hoang log token', `${store().user.token}`);

        console.log('Hoang log request', `${BASE_URL}user/update_phone_number`);
        console.log('Hoang log access token', `${store().user.token}`);
        onDoneFunc();
        request
          .post(`${BASE_URL}user/update_phone_number`)
          .set('Authorization', `bearer ${store().user.token}`)
          .set('Content-Type', 'application/json')
          .send({ token: acckit_token })
          .finish((err, res) => {
            console.log('Hoang log finish');
            if (!err) {
              MySpinner.show();
              console.log('Hoang log update phone number');
              loadUserProfile(store, dispatch, () => {
                MySpinner.hide();
              });
            } else {
              console.log('Hoang log update phone number err', err);
              alert(strings.alert, 'Update phone number failed');
            }
          });
      }
    });
  };
}

export function changeUserSetting(key, value, onDoneFunc = () => {}, onErrorFunc = () => {}) {
  return (dispatch, store) => {
    // MySpinner.show();
    const obj = {};
    obj[key] = value;
    request
      .put(`${BASE_URL}${USER_SETTING_API}/${store().user.currentSettings.id}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${store().user.token}`)
      .send(obj)
      .finish((err, res) => {
        if (!err) {
          dispatch({
            type: UPDATE_USER_SETTINGS,
            payload: res.body.results.object
          });
          onDoneFunc();
        } else {
          onErrorFunc();
        }
      });
  };
}

export function getUserSettings() {
  return (dispatch, store) => {
    getUserSettingsPublic(dispatch, store);
  };
}

export function getUserSettingsPublic(dispatch, store) {
  request
    .get(`${BASE_URL}${USER_SETTING_API}?fields=["$all"]&filter={"user_id": "${store().user.userData.id}"}`)
    .finish((err, res) => {
      if (!err) {
        dispatch({
          type: UPDATE_USER_SETTINGS,
          payload: res.body.results.objects.rows[0]
        });
      }
    });
}
