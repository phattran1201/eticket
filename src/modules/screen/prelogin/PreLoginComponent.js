/* eslint-disable max-line-length */
import React from 'react';
import { ImageBackground, Image, PermissionsAndroid, Text, View, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  FS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style, { APP_COLOR, APP_COLOR_TEXT } from '../../../constants/style';
import MyButton from '../../../style/MyButton';
import { isFirstTimeUseApp, setFirstTimeUseApp } from '../../../utils/asyncStorage';
import { onRequestPermission } from '../../../utils/permissionUtils';
import FollowAccessPermission from '../../FollowAccessPermission';
import IntroductionComponent from '../../view/IntroductionComponent';
import MyComponent from '../../view/MyComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import { registerSuccess } from '../emailsignup/EmailSignUpActions';
import { loadConversation } from '../message/ListMessageAction';
import { loginSuccessWithFaceBookAccountKit } from '../signin/SignInActions';
import {
  loginWithFacebookSDK,
  loginWithGoogleSDK,
  loginWithKaKaoSDK,
  loginWithFacebookAccountKit,
  loginWithPhone
} from './PreLoginActions';
import SplashScreen from 'react-native-splash-screen';
import { alert } from '../../../utils/alert';
import global from '../../../utils/globalUtils';

const loginBackground = require('../../../assets/imgs/login_background.png');

class PreLoginComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isLoading: false,
      isShowingIntroduction: false,
      isShowMoreOption: true,
      showAccessPermission: false
    };
    const { params } = this.props.navigation.state;
    this.previousScreenName = params && params.fromScreen ? params.fromScreen : '';
  }

  componentDidMount() {
    SplashScreen.hide();
    // isFirstTimeUseApp().then(isFirstTime => {
    //   if (isFirstTime) {
    //     setTimeout(() => {
    //       this.setState({ isShowingIntroduction: true, showAccessPermission: true });
    //     }, 300);
    //   }
    // });
  }
  showMoreOption() {
    this.setState({ isShowMoreOption: !this.state.isShowMoreOption });
    if (this.state.isShowMoreOption) {
      this.refs.showMoreOption.scrollToEnd();
    } else {
      this.refs.showMoreOption.scrollTo(0);
    }
  }
  flowAccessPermission(self) {
    onRequestPermission(
      [
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      ],
      self,
      () => {
        setFirstTimeUseApp(false);
        self.setState({ showAccessPermission: false });
      },
      true,
      false
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.state.isShowingIntroduction ? (
          <View
            style={{
              width: DEVICE_WIDTH,
              height: DEVICE_HEIGHT,
              position: 'absolute',
              zIndex: 99999999
            }}
          >
            <IntroductionComponent
              style={{
                zIndex: 99999999
              }}
              onDone={() => {
                this.setState({ isShowingIntroduction: false });
              }}
            />
          </View>
        ) : this.state.showAccessPermission ? (
          <FollowAccessPermission onDoneFunc={() => this.flowAccessPermission(this)} />
        ) : (
          <View style={{ flex: 1 }}>
            <StatusBar backgroundColor={'#ffffff60'} barStyle="dark-content" translucent />
            <ImageBackground
              source={loginBackground}
              style={{
                flex: 1,
                alignContent: 'center',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Image
                style={{
                  zIndex: 999,
                  width: 400 * SCALE_RATIO_WIDTH_BASIS,
                  height: 110 * SCALE_RATIO_WIDTH_BASIS,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  marginBottom: 60 * SCALE_RATIO_WIDTH_BASIS
                }}
                resizeMode="contain"
                source={require('../../../assets/imgs/logo.png')}
              />
              <View
                style={{
                  zIndex: 99,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                {/* <MyButton
                outline
                width={(85 / 100) * DEVICE_WIDTH}
                style={[
                  style.button,
                  style.shadow,
                  {
                    borderWidth: 0,
                    backgroundColor: '#fff',
                    marginBottom: 10
                  }
                ]}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_KEY.MAIN);
                }}
              >
                Đăng nhập nhanh để test
              </MyButton> */}
                <MyButton
                  outline
                  width={(85 / 100) * DEVICE_WIDTH}
                  style={[
                    style.button,
                    style.shadow,
                    {
                      borderWidth: 0,
                      backgroundColor: '#fff'
                    }
                  ]}
                  onPress={() => {
                    // loginWithPhone(() => {
                    //   console.log('poi this.props.navigation:', this.props.navigation);
                    //   const { params } = this.props.navigation.state;
                    //   console.log('poi previousScreenName:', this.previousScreenName);
                    //   if (this.previousScreenName === '') {
                    //     this.props.navigation.replace(ROUTE_KEY.MAIN);
                    //   } else {
                    //     this.props.navigation.navigate(this.previousScreenName);
                    //   }
                    // });
                    loginWithPhone()
                      .then(res => {
                        //res.accessToken
                        alert(strings.alert, 'login success');
                        this.props.loginSuccessWithFaceBookAccountKit(res, () => {
                          if (this.previousScreenName === '') {
                            this.props.navigation.replace(ROUTE_KEY.MAIN);
                          } else {
                            this.props.navigation.navigate(this.previousScreenName);
                          }
                        });
                      })
                      .catch(err => {
                        // alert(strings.alert, err);
                      });
                  }}
                  leftIcon="phone"
                  leftIconType="MaterialCommunityIcons"
                >
                  Đăng nhập bằng điện thoại
                </MyButton>

                <View
                  style={{
                    paddingVertical: 20 * SCALE_RATIO_HEIGHT_BASIS,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: (70 / 100) * DEVICE_WIDTH
                  }}
                >
                  <View
                    style={{
                      height: SCALE_RATIO_WIDTH_BASIS,
                      backgroundColor: APP_COLOR_TEXT,
                      flex: 1
                    }}
                  />
                  <Text
                    style={[
                      style.textCaption,
                      {
                        textAlign: 'center',
                        fontSize: FS(13),
                        color: APP_COLOR_TEXT,
                        flex: 3
                      }
                    ]}
                  >
                    hoặc đăng nhập bằng
                  </Text>
                  <View
                    style={{
                      height: SCALE_RATIO_WIDTH_BASIS,
                      backgroundColor: APP_COLOR_TEXT,
                      flex: 1
                    }}
                  />
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: (85 / 100) * DEVICE_WIDTH
                  }}
                >
                  <MyButton
                    style={[
                      style.button,
                      style.shadow,
                      {
                        paddingHorizontal: 35 * SCALE_RATIO_WIDTH_BASIS,
                        backgroundColor: '#3b5998',
                        marginBottom: 10 * SCALE_RATIO_HEIGHT_BASIS
                      }
                    ]}
                    onPress={() => {
                      loginWithFacebookSDK(this);
                    }}
                    leftIcon="facebook"
                    leftIconType="MaterialCommunityIcons"
                  >
                    Facebook
                  </MyButton>
                  <MyButton
                    style={[
                      style.button,
                      style.shadow,
                      {
                        paddingHorizontal: 35 * SCALE_RATIO_WIDTH_BASIS,
                        backgroundColor: '#CB4036',
                        marginBottom: 10 * SCALE_RATIO_HEIGHT_BASIS
                      }
                    ]}
                    onPress={() => {
                      loginWithGoogleSDK(this);
                    }}
                    leftIcon="google-plus"
                    leftIconType="MaterialCommunityIcons"
                  >
                    Google+
                  </MyButton>
                </View>
                <MyButton
                  width={(85 / 100) * DEVICE_WIDTH}
                  style={[
                    style.button,
                    style.shadow,
                    {
                      backgroundColor: APP_COLOR,
                      marginBottom: 10 * SCALE_RATIO_HEIGHT_BASIS
                    }
                  ]}
                  onPress={() => {
                    this.props.navigation.navigate(ROUTE_KEY.SIGNIN, {
                      fromScreen: this.previousScreenName
                    });
                  }}
                  leftIcon="email"
                  leftIconType="MaterialCommunityIcons"
                >
                  Email
                </MyButton>
              </View>
            </ImageBackground>
          </View>
        )}
      </View>
    );
  }
}

const mapActionCreators = {
  loadConversation,
  registerSuccess,
  loginSuccessWithFaceBookAccountKit
};
const mapStateToProps = state => ({
  userData: state.user.userData
});
export default connect(
  mapStateToProps,
  mapActionCreators
)(PreLoginComponent);
