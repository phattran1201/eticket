import React from 'react';
import { StyleSheet, View, WebView } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { connect } from 'react-redux';
import { SCALE_RATIO_HEIGHT_BASIS, DEVICE_WIDTH } from '../../../constants/Constants';
import HeadedWithBackButtonComponent from '../../view/HeaderWithBackButtonComponent';
import MyComponent from '../../view/MyComponent';
import MySpinner from '../../view/MySpinner';
import AutoHeightWebView from 'react-native-autoheight-webview/autoHeightWebView';

class TermsConditionsWebViewComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      nickname: '',
      age: '',
      introduce: '',
      value: 0
    };
  }
  componentDidMount() {
    MySpinner.show();
  }
  render() {
    const { navigation } = this.props;
    const { params } = this.props.navigation.state;
    const name = params && params.name ? params.name : '';
    const uri = params && params.uri ? params.uri : '';

    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <HeadedWithBackButtonComponent
          translucent
          styleContent={{
            backgroundColor: 'transparent'
          }}
          bodyTitle={name}
          onPress={() => {
            this.props.navigation.goBack();
          }}
        />

        <AutoHeightWebView
          style={{ width: DEVICE_WIDTH }}
          onLoad={() => {
            MySpinner.hide();
          }}
          source={{ uri }}
        />
      </View>
    );
  }
}

const mapActionCreators = {};

export default connect(
  null,
  mapActionCreators
)(TermsConditionsWebViewComponent);

const styles = StyleSheet.create({});
