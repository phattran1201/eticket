import { BASE_URL } from '../../../constants/Constants';
import request from '../../../utils/request';

export function exportRequest(payload, onDoneFunction, OnErrorFunc) {
  return (dispatch, store) => {
    request
      .post(`${BASE_URL}export_request/request`)
      .set('Authorization', `Bearer ${store().user.token}`)
      .set('Content-Type', 'application/json')
      .send(payload)
      .finish((err, res) => {
        if (err) {
          OnErrorFunc(err);
        } else {
          onDoneFunction(res);
        }
      });
  };
}
