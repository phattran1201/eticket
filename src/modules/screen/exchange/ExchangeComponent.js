import AnimatedLottieView from 'lottie-react-native';
import React from 'react';
import { Image, Platform, StatusBar, StyleSheet, Text, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import MyButton from '../../../style/MyButton';
import MyTextInput from '../../../style/MyTextInput';
import { alert } from '../../../utils/alert';
import { formatDate } from '../../../utils/dateUtils';
import { getNumberWithCommas } from '../../../utils/numberUtils';
import MyComponent from '../../view/MyComponent';
import Dropdown from '../../view/MyDropDown/dropdown';
import PopupNotification from '../../view/PopupNotification';
import SuccessWithdrawComponent from '../successwithdraw/SuccessWithdrawComponent';
import { exportRequest } from './ExchangeActions';

class ExchangeComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      bankName: '',
      accountNo: '',
      accountHolder: '',
      idNumber: '',
      cash: 0,
      isLoading: false
    };
    this.exchangeUnit = [200, 500, 1000, 5000, 10000];
  }
  componentDidMount() {
    if (this.props.userData && this.props.userData.cash < 200) {
      setTimeout(() => {
        alert(strings.alert, strings.least_200_cash_to_exchange);
      }, 300);
    }
  }
  renderItem = ({ item }) => (
    <View
      style={{ padding: 10 * SCALE_RATIO_WIDTH_BASIS, flexDirection: 'row', alignItems: 'center' }}
    >
      <AnimatedLottieView
        source={require('../../../assets/check.json')}
        autoPlay
        loop
        hardwareAccelerationAndroid
        style={{
          position: 'absolute',
          width: 70 * SCALE_RATIO_WIDTH_BASIS,
          height: 70 * SCALE_RATIO_WIDTH_BASIS
        }}
        resizeMode="cover"
      />
      <View style={{ paddingLeft: 55 * SCALE_RATIO_WIDTH_BASIS }}>
        <Text style={[style.text, {}]}>{item.description}</Text>
        <Text
          style={[
            style.textCaption,
            {
              fontSize: FS(8)
            }
          ]}
        >
          {formatDate(item.start_date)} -> {formatDate(item.end_date)}
        </Text>
      </View>
    </View>
  );

  render() {
    const { bankName, accountNo, accountHolder, idNumber, cash, isLoading } = this.state;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#ffff" barStyle="dark-content" />
        <KeyboardAwareScrollView style={{ flex: 1 }}>
          <View style={{ paddingRight: 10 * SCALE_RATIO_WIDTH_BASIS }}>
            <Carousel
              ref={c => (this._sliderRef = c)}
              data={this.props.listEvent}
              renderItem={this.renderItem}
              hasParallaxImages
              sliderWidth={DEVICE_WIDTH}
              itemWidth={DEVICE_WIDTH}
              // itemWidth={Math.round((60 * DEVICE_WIDTH) / 100)}
              firstItem={1}
              // inactiveSlideScale={0.94}
              // inactiveSlideOpacity={0.7}
              // inactiveSlideShift={5 * SCALE_RATIO_WIDTH_BASIS}
              enableMomentum
              activeSlideAlignment={'start'}
              containerCustomStyle={{
                overflow: 'visible'
              }}
              // contentContainerCustomStyle={{ paddingVertical: SCALE_RATIO_WIDTH_BASIS }}
              loop
              loopClonesPerSide={2}
              autoplay
              autoplayDelay={6000}
              autoplayInterval={3000}
              onSnapToItem={index => this.setState({ activeSlide: index })}
            />
            <Pagination
              dotsLength={this.props.listEvent.length}
              activeDotIndex={this.state.activeSlide}
              containerStyle={{ paddingVertical: 0.5 * SCALE_RATIO_WIDTH_BASIS }}
              dotColor="#70707090"
              dotStyle={{
                width: 6 * SCALE_RATIO_WIDTH_BASIS,
                height: 6 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS
              }}
              inactiveDotColor="#242424"
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
              carouselRef={this._sliderRef}
              tappableDots={!!this._sliderRef}
            />
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                paddingTop: 5 * SCALE_RATIO_WIDTH_BASIS,
                paddingBottom: 10 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              <Image
                resizeMode={'contain'}
                source={require('../../../assets/imgs/icons/coin.png')}
                style={{
                  height: 28 * SCALE_RATIO_HEIGHT_BASIS,
                  width: 28 * SCALE_RATIO_WIDTH_BASIS,
                  marginRight: 15 * SCALE_RATIO_WIDTH_BASIS
                }}
              />
              <Text
                style={{
                  color: '#282828',
                  fontSize: 16 * SCALE_RATIO_WIDTH_BASIS,
                  fontFamily: 'Helvetica Neue'
                }}
              >
                {this.props.userData.cash} {strings.cash}
              </Text>
            </View>
            {/* <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  marginTop: 8 * SCALE_RATIO_HEIGHT_BASIS
                }}
              >
                <Image
                  resizeMode={'contain'}
                  source={require('../../../assets/imgs/icons/won.png')}
                  style={{
                    height: 32 * SCALE_RATIO_HEIGHT_BASIS,
                    width: 32 * SCALE_RATIO_WIDTH_BASIS,
                    marginRight: 23 * SCALE_RATIO_WIDTH_BASIS
                  }}
                />
                <Text
                  style={{
                    color: '#282828',
                    fontSize: 17 * SCALE_RATIO_WIDTH_BASIS,
                    fontFamily: 'Helvetica Neue'
                  }}
                >
                  35200 {strings.won}
                </Text>
              </View> */}
            <View
              style={{
                height: 1 * SCALE_RATIO_HEIGHT_BASIS,
                backgroundColor: '#C7AE6D',
                opacity: 0.2,
                width: (DEVICE_WIDTH * 8) / 10
              }}
            />
          </View>
          <View
            style={{
              alignItems: 'center',
              marginHorizontal: 40 * SCALE_RATIO_WIDTH_BASIS,
              marginTop: 10 * SCALE_RATIO_HEIGHT_BASIS
            }}
          >
            <MyTextInput
              styleContent={{ elevation: 2 }}
              leftText={strings.bank}
              placeholder={strings.bank_name}
              value={this.state.bankName}
              onChangeText={bankName => this.setState({ bankName })}
              returnKeyType="next"
              onSubmitEditing={() => this.accountNo.focus()}
            />
            <MyTextInput
              styleContent={{ elevation: 2 }}
              leftText={strings.account_no}
              placeholder={strings.account_no}
              value={this.state.accountNo}
              onChangeText={accountNo => this.setState({ accountNo })}
              ref={instance => (this.accountNo = instance)}
              keyboardType="number-pad"
              returnKeyType="next"
              onSubmitEditing={() => this.accountHolder.focus()}
            />
            <MyTextInput
              styleContent={{ elevation: 2 }}
              leftText={strings.account_holder}
              placeholder={strings.account_holder}
              value={this.state.accountHolder}
              onChangeText={accountHolder => this.setState({ accountHolder })}
              ref={instance => (this.accountHolder = instance)}
              returnKeyType="next"
              onSubmitEditing={() => this.idNumber.focus()}
            />
            <MyTextInput
              ref={instance => (this.idNumber = instance)}
              styleContent={{ elevation: 2 }}
              leftText={strings.id_number}
              placeholder={strings.id_number}
              value={this.state.idNumber}
              onChangeText={idNumber => this.setState({ idNumber })}
              keyboardType="number-pad"
            />
          </View>
          <View
            style={{
              marginHorizontal: 40 * SCALE_RATIO_WIDTH_BASIS,
              marginVertical: 20 * SCALE_RATIO_HEIGHT_BASIS
            }}
          >
            <View style={{}}>
              <Text
                style={{
                  color: '#282828',
                  fontSize: 16 * SCALE_RATIO_WIDTH_BASIS,
                  fontFamily: 'Helvetica Neue',
                  fontWeight: '500',
                  marginBottom: 10 * SCALE_RATIO_HEIGHT_BASIS,
                  textDecorationLine: 'underline',
                  textDecorationColor: '#282828'
                }}
              >
                {strings.top_up}
              </Text>

              <View style={[style.viewInput, { justifyContent: 'center', alignItems: 'center' }]}>
                <Image
                  resizeMode={'contain'}
                  source={require('../../../assets/imgs/icons/coin.png')}
                  style={{
                    height: 26 * SCALE_RATIO_HEIGHT_BASIS,
                    width: 26 * SCALE_RATIO_WIDTH_BASIS,
                    marginRight: 16 * SCALE_RATIO_WIDTH_BASIS
                  }}
                />
                <Dropdown
                  pickerStyle={{
                    width: 169 * SCALE_RATIO_WIDTH_BASIS,
                    borderColor: Platform.OS === 'ios' ? '#AE92D330' : '#70707010',
                    borderWidth: 1,
                    flexDirection: 'row',
                    borderRadius: (31 * SCALE_RATIO_HEIGHT_BASIS) / 2
                  }}
                  overlayStyle={{ borderTopRightRadius: 0 }}
                  textColor="#C7AE6D"
                  fontSize={FS(14)}
                  value={this.state.cash}
                  itemTextStyle={(style.textInput, { color: '#C7AE6D' })}
                  dropdownOffset={{
                    top: 30 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
                    left: 30 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
                  }}
                  data={[
                    {
                      value: '200',
                      onSelect: onDone => {
                        this.setState({ cash: 200 });
                      }
                    },
                    {
                      value: '500',
                      onSelect: onDone => {
                        this.setState({ cash: 500 });
                      }
                    },
                    {
                      value: '1000',
                      onSelect: onDone => {
                        this.setState({ cash: 1000 });
                      }
                    },
                    {
                      value: '5000',
                      onSelect: onDone => {
                        this.setState({ cash: 5000 });
                      }
                    },
                    {
                      value: '10000',
                      onSelect: onDone => {
                        this.setState({ cash: 10000 });
                      }
                    }
                  ]}
                >
                  <View
                    style={[styles.dropdownContainer, { width: 180 * SCALE_RATIO_WIDTH_BASIS }]}
                  >
                    <Text style={[style.textInput, { color: '#C7AE6D' }]} numberOfLines={1}>
                      {this.state.cash}
                    </Text>
                    <MaterialIcons
                      name="arrow-drop-down"
                      size={35 * SCALE_RATIO_WIDTH_BASIS}
                      color="#C7AE6D"
                    />
                  </View>
                </Dropdown>
              </View>
              <View
                style={[
                  style.viewInput,
                  {
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10 * SCALE_RATIO_HEIGHT_BASIS
                  }
                ]}
              >
                <Image
                  resizeMode={'contain'}
                  source={require('../../../assets/imgs/icons/won.png')}
                  style={{
                    height: 26 * SCALE_RATIO_HEIGHT_BASIS,
                    width: 26 * SCALE_RATIO_WIDTH_BASIS,
                    marginRight: 25 * SCALE_RATIO_WIDTH_BASIS
                  }}
                />
                <Text style={(style.textInput, { color: '#282828' })}>
                  {getNumberWithCommas(this.state.cash * 100)}
                </Text>
              </View>
            </View>
          </View>
          <MyButton
            style={{ alignSelf: 'center', marginBottom: 10 * SCALE_RATIO_HEIGHT_BASIS }}
            width={162 * SCALE_RATIO_WIDTH_BASIS}
            outline={
              this.state.bankName === '' ||
              this.state.accountNo === '' ||
              this.state.accountHolder === '' ||
              this.state.idNumber === ''
            }
            isDisabled={
              this.state.isLoading ||
              this.state.bankName === '' ||
              this.state.accountNo === '' ||
              this.state.accountHolder === '' ||
              this.state.idNumber === ''
            }
            isLoading={this.state.isLoading}
            onPress={() => {
              //FIXME for testing, should uncomment the below code for the project
              if (cash < 200) {
                alert(strings.alert, strings.please_topup_at_least_200_cash);
                return;
              }
              if (
                this.state.isLoading ||
                this.state.bankName === '' ||
                this.state.accountNo === '' ||
                this.state.accountHolder === '' ||
                this.state.idNumber === ''
              ) {
                alert(strings.alert, strings.please_input_all_info);
                return;
              }
              this.setState({ isLoading: true });
              this.props.exportRequest(
                {
                  amount: cash,
                  bank_user_name: accountHolder,
                  bank_name: bankName,
                  bank_serial: accountNo,
                  bank_id: 'BankId',
                  type: 'BANK',
                  bank_branch: 'BankBranch',
                  bank_province: 'BankProvince'
                },
                () => {
                  PopupNotification.showComponent(
                    <SuccessWithdrawComponent
                      onButtonClick={() => {
                        PopupNotification.hide();
                        this.setState({ isLoading: false });
                      }}
                    />
                  );
                },
                () => this.setState({ isLoading: false })
              );
            }}
          >
            {strings.request.toLocaleUpperCase()}
          </MyButton>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData,
  listEvent: state.event.listEvent
});

const mapActionCreators = {
  exportRequest
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(ExchangeComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  dropdownContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 31 * SCALE_RATIO_HEIGHT_BASIS
  }
});
