import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { connect } from 'react-redux';
import { FS, SCALE_RATIO_HEIGHT_BASIS, SCALE_RATIO_WIDTH_BASIS } from '../../../constants/Constants';
import style, { FONT } from '../../../constants/style';
import CustomTabBar from '../../../style/Tabbar/CustomTabBar';
import BaseHeader from '../../view/BaseHeader';
import AtStoreComponent from './AtStoreComponent';
import DeliveryComponent from './DeliveryComponent';

class CheckOutComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTab: 0
    };
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <BaseHeader
          noShadow
          children={<Text style={style.titleHeader}>THANH TOÁN</Text>}
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
        />
        <ScrollableTabView
          initialPage={0}
          renderTabBar={() => <CustomTabBar style={{ marginBottom: 3 * SCALE_RATIO_WIDTH_BASIS }} />}
        >
          <DeliveryComponent tabLabel="Giao hàng tận nơi" navigation={this.props.navigation} />
          <AtStoreComponent tabLabel="Nhận tại cửa hàng" navigation={this.props.navigation} />
        </ScrollableTabView>
      </View>
    );
  }
}

const mapStateToProps = state => ({});

const mapActionCreators = {};

export default connect(
  mapStateToProps,
  mapActionCreators
)(CheckOutComponent);
