import React from 'react';
import {
  Dimensions,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  Platform,
  FlatList,
  Image,
  ScrollView
} from 'react-native';
import FastImage from 'react-native-fast-image';
import MyComponent from '../../view/MyComponent';
import { alert } from '../../../utils/alert';
import BaseHeader from '../../view/BaseHeader';
import strings from '../../../constants/Strings';
import { SCALE_RATIO_WIDTH_BASIS, FS, ROUTE_KEY } from '../../../constants/Constants';
import style, { APP_COLOR } from '../../../constants/style';

const tocotoco_star_img = require('../../../assets/imgs/tocotoco_star_img.png');

export default class CompletedOrderComponent extends MyComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <BaseHeader
          children={<Text />}
          leftIcon="x"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
          rightIcon="home"
          rightIconType="Feather"
          onRightPress={() => alert(strings.alert, strings.this_feature_is_in_development)}
        />
        <View
          style={{
            flex: 1,
            padding: 10 * SCALE_RATIO_WIDTH_BASIS,
            borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          <View
            style={[
              Platform.OS === 'ios' ? style.shadowIOS : style.shadowAndroid,
              {
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
                padding: 10 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
          >
            <FastImage
              source={tocotoco_star_img}
              resizeMode={FastImage.resizeMode.contain}
              style={{
                width: 100 * SCALE_RATIO_WIDTH_BASIS,
                height: 100 * SCALE_RATIO_WIDTH_BASIS
              }}
            />
          </View>

          <View
            style={{
              height: 50 * SCALE_RATIO_WIDTH_BASIS,
              padding: 10 * SCALE_RATIO_WIDTH_BASIS,
              backgroundColor: 'white'
            }}
          >
            <Text style={[style.text, { fontSize: FS(22), textAlign: 'center' }]}>Đặt hàng thành công</Text>
          </View>

          <View
            style={{
              backgroundColor: 'white',
              height: 80 * SCALE_RATIO_WIDTH_BASIS,
              padding: 10 * SCALE_RATIO_WIDTH_BASIS,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Text
              numberOfLines={2}
              style={[
                style.text,
                {
                  fontSize: FS(15),
                  textAlign: 'center'
                }
              ]}
            >
              Cảm ơn bạn đã mua hàng, mọi thông tin chi tiết về đơn hàng bạn có thể theo dõi qua ứng dụng này!
            </Text>
          </View>

          <View
            style={{
              marginTop: 1 * SCALE_RATIO_WIDTH_BASIS,
              height: 50 * SCALE_RATIO_WIDTH_BASIS,
              padding: 10 * SCALE_RATIO_WIDTH_BASIS,
              backgroundColor: 'white'
            }}
          >
            <Text style={[style.text, { fontSize: FS(18), textAlign: 'center', fontWeight: '500' }]}>
              Mã đơn hàng:{' '}
              <Text style={[style.text, { fontSize: FS(18), color: APP_COLOR, fontWeight: '400' }]}>#001234</Text>
            </Text>
          </View>

          <View
            style={{
              marginTop: 1 * SCALE_RATIO_WIDTH_BASIS,
              height: 100 * SCALE_RATIO_WIDTH_BASIS,
              padding: 10 * SCALE_RATIO_WIDTH_BASIS,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <TouchableOpacity onPress={() => this.props.navigation.navigate(ROUTE_KEY.ORDER_COMPONENT)}>
              <View
                style={{
                  width: 300 * SCALE_RATIO_WIDTH_BASIS,
                  height: 40 * SCALE_RATIO_WIDTH_BASIS,
                  padding: 10 * SCALE_RATIO_WIDTH_BASIS,
                  borderRadius: 25 * SCALE_RATIO_WIDTH_BASIS,
                  backgroundColor: APP_COLOR,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text
                  style={[
                    style.text,
                    {
                      color: 'white',
                      fontSize: FS(18),
                      textAlign: 'center',
                      fontWeight: '600'
                    }
                  ]}
                >
                  Đi tới Đơn hàng của tôi
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{ flex: 1 }} />
        </View>
      </View>
    );
  }
}
