import React from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native';
import Collapsible from 'react-native-collapsible';
import { CheckBox } from 'react-native-elements';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Menu, MenuOptions, MenuTrigger, renderers } from 'react-native-popup-menu';
import Feather from 'react-native-vector-icons/dist/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import {
  DEVICE_WIDTH,
  FS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  DEVICE_HEIGHT
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style, { APP_COLOR, APP_COLOR_BLUE, APP_COLOR_TEXT, FONT } from '../../../constants/style';
import { alert } from '../../../utils/alert';
import MyComponent from '../../view/MyComponent';
import { getPriceWithCommas, getNumberWithCommas } from '../../../utils/numberUtils';
import { loadCurrentLocation } from '../main/MainActions';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const markerIcon = require('../../..//assets/imgs/icons/logo.png');

const { Popover } = renderers;

class DeliveryComponent extends MyComponent {
  constructor(props) {
    super(props);
    // const locationDelta = this.getLocationDelta(10.7725451, 106.6980413, this.currentRadius * 1000);
    this.state = {
      showInfoShip: false,
      showInfoOrder: false,
      showInfoPay: false,
      store: 'TocoToco - 12 Thái Hà',
      offline: true,
      online: false,
      selectedStore: 0,
      showAllItem: false,
      currentPosition: null,
      // region: {
      //   latitude: 10.7725451,
      //   longitude: 106.6980413,
      //   latitudeDelta: locationDelta.latitudeDelta,
      //   longitudeDelta: locationDelta.longitudeDelta
      // },
      dataOrder: [
        {
          id: 1,
          name: 'Trà sữa Panda',
          image: 'http://tocotocotea.com/wp-content/uploads/2018/03/panda-1.jpg',
          detail:
            ' 30% đường, 30% đá, trân châu trắng 30% đường, 30% đá, trân châu trắng 30% đường, 30% đá, trân châu trắng',
          price: 30000,
          amount: 2
        },
        {
          id: 2,
          name: 'Trà sữa Panda',
          image: 'http://tocotocotea.com/wp-content/uploads/2018/03/panda-1.jpg',
          detail: '30% đường, 30% đá, trân châu trắng',
          price: 30000,
          amount: 2
        },
        {
          id: 3,
          name: 'Trà sữa Panda',
          image: 'http://tocotocotea.com/wp-content/uploads/2018/03/panda-1.jpg',
          detail: '30% đường, 30% đá, trân châu trắng',
          price: 30000,
          amount: 2
        },
        {
          id: 4,
          name: 'Trà sữa Panda',
          image: 'http://tocotocotea.com/wp-content/uploads/2018/03/panda-1.jpg',
          detail: '30% đường, 30% đá, trân châu trắng',
          price: 30000,
          amount: 2
        },
        {
          id: 5,
          name: 'Trà sữa Panda',
          image: 'http://tocotocotea.com/wp-content/uploads/2018/03/panda-1.jpg',
          detail: '30% đường, 30% đá, trân châu trắng',
          price: 30000,
          amount: 2
        }
      ],
      listStore: [
        {
          id: 0,
          name: 'TocoToco Nguyễn Tri Phương',
          coordinate: {
            latitude: 10.7629044,
            longitude: 106.6334002
          },
          region: {
            latitude: 10.7629044,
            longitude: 106.6334002,
            latitudeDelta: 0.00922,
            longitudeDelta: 0.00421
          },
          image: 'https://static.vietnammm.com/images/restaurants/vn/NPPPQ77/products/machiatohongtra.png'
        },
        {
          id: 1,
          name: 'TocoToco Phan Xích Long',
          coordinate: {
            latitude: 10.798286,
            longitude: 106.6540424
          },
          region: {
            latitude: 10.798286,
            longitude: 106.6540424,
            latitudeDelta: 0.00922,
            longitudeDelta: 0.00421
          },
          image: 'https://static.vietnammm.com/images/restaurants/vn/NONR35Q/products/machiatomatcha.png'
        },
        {
          id: 2,
          name: 'TocoToco Hồ Tùng Mậu',
          coordinate: {
            latitude: 10.7732321,
            longitude: 106.6683471
          },
          region: {
            latitude: 10.7732321,
            longitude: 106.6683471,
            latitudeDelta: 0.00922,
            longitudeDelta: 0.00421
          },
          image: 'https://static.vietnammm.com/images/restaurants/vn/NRQOR37/products/machiatotraxanh.png'
        }
      ]
    };
  }

  distance(lo1, la1, lo2, la2) {
    const dLat = (la2 - la1) * (Math.PI / 180);
    const dLon = (lo2 - lo1) * (Math.PI / 180);
    const la1ToRad = la1 * (Math.PI / 180);
    const la2ToRad = la2 * (Math.PI / 180);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(la1ToRad) * Math.cos(la2ToRad) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return 6371 * c;
  }

  renderListStore = ({ item, index }) => {
    item.distance = this.distance(
      item.longitude,
      item.latitude,
      this.props.currentLocation.longitude,
      this.props.currentLocation.latitude
    );
    const addressArray = item && item.address ? item.address.split(',') : [];
    item.district = null;
    if (addressArray) {
      item.district = addressArray[2];
    }
    return (
      <TouchableOpacity
        onSelect={() => this.setState({ selectedStore: item.id })}
        style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
      >
        <Text style={[style.text, { paddingVertical: 10 }]}>{item.name}</Text>
        <Text
          style={[
            style.text,
            {
              color: APP_COLOR_TEXT,
              fontSize: FS(10),
              fontFamily: FONT.Thin,
              paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
          numberOfLines={1}
        >
          {parseFloat(item.distance).toFixed(1)} km
        </Text>
      </TouchableOpacity>
    );
  };
  renderListGift = ({ item, index }) => (
    <View
      style={{
        flexDirection: 'row',
        paddingVertical: 10,
        alignItems: 'center',
        borderBottomColor: '#EFEFEF',
        borderBottomWidth: 1
      }}
    >
      <CheckBox
        center
        containerStyle={{ margin: 0, padding: 0 }}
        checkedColor={APP_COLOR}
        checked={index % 2 ? this.state.offline : !this.state.offline}
        checkedIcon="circle"
        uncheckedIcon="circle-o"
        onPress={() => this.setState({ offline: !this.state.offline })}
      />
      <Image
        source={{ uri: item.image }}
        style={{
          marginLeft: -10 * SCALE_RATIO_WIDTH_BASIS,
          height: 60 * SCALE_RATIO_HEIGHT_BASIS,
          width: 60 * SCALE_RATIO_HEIGHT_BASIS
        }}
      />
      <View
        style={{
          width: DEVICE_WIDTH - 80 * SCALE_RATIO_HEIGHT_BASIS
        }}
      >
        <Text
          style={[
            style.text,
            {
              fontSize: FS(12),
              fontFamily: FONT.SemiBold
            }
          ]}
        >
          {item.name}
        </Text>
      </View>
    </View>
  );
  renderListOrder = ({ item, index }) => (
    <View
      style={{
        flexDirection: 'row',
        paddingVertical: 10,
        alignItems: 'center',
        borderBottomColor: '#EFEFEF',
        borderBottomWidth: 1
      }}
    >
      <Image
        source={{ uri: item.image }}
        style={{ height: 60 * SCALE_RATIO_HEIGHT_BASIS, width: 60 * SCALE_RATIO_HEIGHT_BASIS }}
      />
      <View
        style={{
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
          width: DEVICE_WIDTH - 80 * SCALE_RATIO_HEIGHT_BASIS
        }}
      >
        <Text
          style={[
            style.text,
            {
              fontSize: FS(12),
              fontFamily: FONT.SemiBold
            }
          ]}
        >
          {item.name}
        </Text>
        <Text
          style={[
            style.text,
            {
              fontSize: FS(12),
              color: '#707070'
            }
          ]}
        >
          {item.detail}
        </Text>
        <Text
          style={[
            style.text,
            {
              fontSize: FS(12),
              color: APP_COLOR
            }
          ]}
        >
          {getPriceWithCommas(item.price)} x {getNumberWithCommas(item.amount)} ={' '}
          {getPriceWithCommas(item.price * item.amount)}
        </Text>
      </View>
    </View>
  );

  renderHeader = (section, show) => (
    <TouchableOpacity
      onPress={() =>
        show === 'showInfoShip'
          ? this.setState({ showInfoShip: !this.state.showInfoShip })
          : show === 'showInfoOrder'
          ? this.setState({ showInfoOrder: !this.state.showInfoOrder })
          : this.setState({ showInfoPay: !this.state.showInfoPay })
      }
      style={{
        width: '100%',
        flexDirection: 'row',
        marginTop: 15 * SCALE_RATIO_WIDTH_BASIS,
        marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
        paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS
      }}
    >
      <View style={{ flex: 1 }}>
        <Text
          style={{
            color: '#373737',
            fontSize: 16 * SCALE_RATIO_WIDTH_BASIS,
            fontWeight: 'bold'
          }}
        >
          {section}
        </Text>
      </View>
      <View
        style={{
          width: 30 * SCALE_RATIO_WIDTH_BASIS,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        {show ? (
          <Feather name="chevron-down" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#ABABAB" />
        ) : (
          <Feather name="chevron-right" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#ABABAB" />
        )}
      </View>
    </TouchableOpacity>
  );

  componentDidMount() {
    loadCurrentLocation()
      .then(posInfo => {
        const locationDelta = this.getLocationDelta(
          posInfo.coords.latitude,
          posInfo.coords.longitude,
          this.currentRadius * 1000
        );
        this.setState({
          region: {
            latitude: posInfo.coords.latitude,
            longitude: posInfo.coords.longitude,
            latitudeDelta: locationDelta.latitudeDelta,
            longitudeDelta: locationDelta.longitudeDelta
          }
        });
        this.setState({
          currentPosition: {
            latitude: posInfo.coords.latitude,
            longitude: posInfo.coords.longitude
          }
        });
      })
      .catch(err => {
        console.log('poi pos err', err);
      });
  }

  render() {
    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        {this.renderHeader('Thông tin giao hàng', 'showInfoShip')}
        <Collapsible
          collapsed={this.state.showInfoShip}
          style={{ backgroundColor: '#fff', padding: 10 * SCALE_RATIO_WIDTH_BASIS }}
        >
          <View style={[styles.shadow]}>
            <Text
              style={[
                style.text,
                {
                  paddingVertical: 8 * SCALE_RATIO_HEIGHT_BASIS,
                  borderBottomColor: '#70707030',
                  borderBottomWidth: 0.5
                }
              ]}
            >
              Bạn đã có tài khoản ?{' '}
              <Text
                style={[
                  style.text,
                  {
                    fontFamily: FONT.SemiBold,
                    paddingVertical: 8 * SCALE_RATIO_HEIGHT_BASIS,
                    borderBottomColor: '#70707030',
                    borderBottomWidth: 0.5
                  }
                ]}
              >
                Đăng nhập
              </Text>{' '}
              hoặc{' '}
              <Text
                style={[
                  style.text,
                  {
                    fontFamily: FONT.SemiBold,
                    paddingVertical: 8 * SCALE_RATIO_HEIGHT_BASIS,
                    borderBottomColor: '#70707030',
                    borderBottomWidth: 0.5
                  }
                ]}
              >
                Đăng ký
              </Text>
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingVertical: 8 * SCALE_RATIO_HEIGHT_BASIS,
                borderBottomColor: '#70707030',
                borderBottomWidth: 0.5
              }}
            >
              <Icon name="person" size={FS(16)} color={APP_COLOR} />
              <Text
                style={{
                  marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
                  fontSize: FS(14),
                  fontFamily: FONT.Regular,
                  color: '#707070'
                }}
              >
                Tên người nhận
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingVertical: 8 * SCALE_RATIO_HEIGHT_BASIS,
                borderBottomColor: '#70707030',
                borderBottomWidth: 0.5
              }}
            >
              <Icon name="call" size={FS(16)} color={APP_COLOR} />
              <Text
                style={{
                  marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
                  fontSize: FS(14),
                  fontFamily: FONT.Regular,
                  color: '#707070'
                }}
              >
                Số điện thoại người nhận
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                paddingVertical: 8 * SCALE_RATIO_HEIGHT_BASIS,
                borderBottomColor: '#70707030',
                borderBottomWidth: 0.5
              }}
            >
              <MapView
                style={{ width: 100 * SCALE_RATIO_WIDTH_BASIS, height: 100 * SCALE_RATIO_WIDTH_BASIS }}
                provider={PROVIDER_GOOGLE}
                region={this.state.listStore[this.state.selectedStore].region}
              >
                {/* <Marker coordinate={this.state.listStore[this.state.selectedStore].coordinate}>
                  <Image
                    style={styles.markerImageStyle}
                    source={{ uri: this.state.listStore[this.state.selectedStore].image }}
                    resizeMode="cover"
                  />
                </Marker> */}
              </MapView>

              <View style={{ paddingLeft: 5 * SCALE_RATIO_WIDTH_BASIS, flex: 1, marginRight: 10 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Icons name="map-marker-outline" size={FS(16)} color={APP_COLOR} />
                  <Text
                    numberOfLines={2}
                    style={{
                      paddingLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
                      fontSize: FS(12),
                      fontFamily: FONT.SemiBold
                    }}
                  >
                    Toà nhà GP Invest, 170 Đê La Thành, Ô Chợ Dừa, Đống Đa, Hà Nội
                  </Text>
                </View>
                <TouchableOpacity style={{ alignSelf: 'flex-end' }}>
                  <Text
                    style={{ fontSize: FS(12), fontFamily: FONT.SemiBold, color: APP_COLOR_BLUE, textAlign: 'right' }}
                  >
                    Thay đổi
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingVertical: 8 * SCALE_RATIO_HEIGHT_BASIS,
                borderBottomColor: '#70707030',
                borderBottomWidth: 0.5
              }}
            >
              <Text style={style.text}>Giao hàng 18:00 - hôm nay 05/03/2019</Text>
              <TouchableOpacity style={{ alignSelf: 'flex-end' }}>
                <Text
                  style={{ fontSize: FS(12), fontFamily: FONT.SemiBold, color: APP_COLOR_BLUE, textAlign: 'right' }}
                >
                  Sửa
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Collapsible>

        {this.renderHeader('Thông tin đơn hàng', 'showInfoOrder')}
        <Collapsible
          collapsed={this.state.showInfoOrder}
          style={{ backgroundColor: '#fff', padding: 10 * SCALE_RATIO_WIDTH_BASIS }}
        >
          <View style={[styles.shadow, {}]}>
            <Menu renderer={Popover} rendererProps={{ preferredPlacement: 'bottom' }}>
              <MenuTrigger>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name="store" size={FS(16)} color={APP_COLOR_TEXT} />
                    <Text
                      style={[
                        {
                          color: APP_COLOR_TEXT,
                          fontWeight: '500',
                          fontSize: FS(12),
                          fontFamily: FONT.SemiBold,
                          paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                        }
                      ]}
                      numberOfLines={1}
                    >
                      {this.state.listStore[this.state.selectedStore].name}
                    </Text>
                  </View>
                  <Text
                    style={[
                      {
                        color: APP_COLOR_TEXT,
                        fontSize: FS(12),
                        fontFamily: FONT.Thin,
                        paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                      }
                    ]}
                    numberOfLines={1}
                  >
                    2.8 km
                  </Text>
                </View>
              </MenuTrigger>
              <MenuOptions
                style={{
                  paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS,
                  paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
                  maxHeight: DEVICE_HEIGHT / 2.15
                }}
              >
                <TextInput
                  style={[
                    style.textCaption,
                    {
                      width: (DEVICE_WIDTH * 80) / 100,
                      paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
                      // paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
                      fontSize: FS(14),
                      fontFamily: 'FontAwesome'
                    }
                  ]}
                  ref="searchBar"
                  clearButtonMode="always"
                  placeholder=" Tìm cửa hàng"
                  placeholderTextColor="#00000050"
                  underlineColorAndroid="transparent"
                  selectionColor="#C7AE6D"
                  autoCapitalize="none"
                  autoCorrect={false}
                  onChangeText={this.onSearchBarChangeText}
                />
                <View style={{ marginBottom: 10, height: 1, backgroundColor: '#70707050' }} />
                <FlatList
                  data={this.props.listStores}
                  renderItem={this.renderListStore}
                  style={{ maxHeight: DEVICE_HEIGHT / 2.15 }}
                />
              </MenuOptions>
            </Menu>

            {/* <Dropdown
              pickerStyle={{
                width: DEVICE_WIDTH - 60 * SCALE_RATIO_WIDTH_BASIS,
                borderColor: Platform.OS === 'ios' ? '#9297D330' : '#70707010',
                borderWidth: 1,
                flexDirection: 'row',
                borderRadius: (31 * SCALE_RATIO_HEIGHT_BASIS) / 2
              }}
              overlayStyle={{ borderTopRightRadius: 0 }}
              textColor={'#000'}
              fontSize={FS(12)}
              value={this.state.cash}
              itemTextStyle={{ color: '#000' }}
              dropdownOffset={{
                top: 120 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
                left: 40 * SCALE_RATIO_WIDTH_BASIS
              }}
              data={this.state.listStore.map(e => ({
                value: e.name,
                onSelect: () => {
                  this.setState({ selectedStore: e.id });
                }
              }))}
            >
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Icon name="store" size={FS(16)} color={APP_COLOR_TEXT} />
                  <Text
                    style={[
                      {
                        color: APP_COLOR_TEXT,
                        fontWeight: '500',
                        fontSize: FS(12),
                        fontFamily: FONT.SemiBold,
                        paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                      }
                    ]}
                    numberOfLines={1}
                  >
                    {this.state.listStore[this.state.selectedStore].name}
                  </Text>
                </View>
                <Text
                  style={[
                    {
                      color: APP_COLOR_TEXT,
                      fontSize: FS(12),
                      fontFamily: FONT.Thin,
                      paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                    }
                  ]}
                  numberOfLines={1}
                >
                  2.8 km
                </Text>
              </View>
            </Dropdown> */}
          </View>
          <FlatList
            data={this.state.dataOrder}
            keyExtractor={item => item.id}
            renderItem={this.renderListOrder}
            numColumns={1}
            scrollEnabled={!!(this.state.dataOrder.length < 4 || this.state.showAllItem)}
            style={{
              height: this.state.dataOrder.length < 4 || this.state.showAllItem ? null : 240 * SCALE_RATIO_HEIGHT_BASIS
            }}
          />
          {!this.state.showAllItem && (
            <View style={styles.line}>
              <Text
                style={{
                  fontFamily: FONT.Regular,
                  fontSize: FS(12),
                  color: APP_COLOR_BLUE
                }}
                onPress={() => this.setState({ showAllItem: true })}
              >
                Còn {getNumberWithCommas(this.state.dataOrder.length - 3)} cốc nữa
              </Text>
            </View>
          )}
          <View style={styles.line}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingVertical: 10
              }}
            >
              <MaterialCommunityIcons name="ticket-percent" size={FS(16)} color={APP_COLOR_TEXT} />
              <Text
                style={[
                  {
                    color: APP_COLOR_TEXT,
                    fontWeight: '500',
                    fontSize: FS(12),
                    fontFamily: FONT.SemiBold,
                    paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                  }
                ]}
                numberOfLines={1}
              >
                Mã khuyến mãi
              </Text>
            </View>
            <View
              style={[
                {
                  borderColor: '#70707050',
                  borderWidth: 1,
                  width: '100%',
                  backgroundColor: '#fff',
                  height: 35 * SCALE_RATIO_WIDTH_BASIS,
                  flexDirection: 'row',
                  borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
            >
              <TextInput
                clearButtonMode="while-editing"
                placeholder="Nhập mã khuyến mãi của bạn..."
                placeholderTextColor="#9c9faa"
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                autoCorrect={false}
                style={{
                  color: APP_COLOR_TEXT,
                  flex: 7,
                  height: 35 * SCALE_RATIO_WIDTH_BASIS,
                  fontFamily: 'helveticaneue',
                  fontSize: FS(14),
                  marginLeft: 10 * SCALE_RATIO_WIDTH_BASIS
                }}
              />
              <View
                onPress={() => alert(strings.alert, 'test')}
                style={{
                  borderTopRightRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                  borderBottomRightRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                  height: 35 * SCALE_RATIO_WIDTH_BASIS,
                  padding: 3 * SCALE_RATIO_WIDTH_BASIS,
                  backgroundColor: APP_COLOR_TEXT,
                  flex: 2,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text
                  style={{
                    color: 'white',
                    fontFamily: 'helveticaneue',
                    fontSize: FS(14)
                  }}
                >
                  Sử dụng
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.line}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingBottom: 5
              }}
            >
              <MaterialCommunityIcons name="gift" size={FS(16)} color={APP_COLOR_TEXT} />
              <Text
                style={[
                  {
                    color: APP_COLOR_TEXT,
                    fontWeight: '500',
                    fontSize: FS(12),
                    fontFamily: FONT.SemiBold,
                    paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                  }
                ]}
                numberOfLines={1}
              >
                Quà tặng
              </Text>
            </View>
            <Text style={style.text}>Hãy lựa chọn một trong các phần quà từ ToCoToCo dành cho bạn:</Text>
          </View>
          <FlatList data={this.state.dataOrder} keyExtractor={item => item.id} renderItem={this.renderListGift} />
          <View style={{ paddingVertical: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={{ flex: 1, alignItems: 'flex-start' }}>
              <Text style={style.text}>
                Số lượng:{' '}
                <Text style={[style.textCaption, { fontSize: FS(16), fontWeight: 'bold' }]}>
                  {getNumberWithCommas(this.state.dataOrder.length)}
                </Text>{' '}
                cốc
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-start' }}>
              <Text style={[style.text, { textAlign: 'right', alignSelf: 'flex-end' }]}>Tổng:</Text>
              <Text style={[style.text, { textAlign: 'right', alignSelf: 'flex-end' }]}>Phí vận chuyển:</Text>
              <Text style={[style.text, { textAlign: 'right', alignSelf: 'flex-end' }]}>Khuyến mãi:</Text>
              <Text style={[style.text, { textAlign: 'right', alignSelf: 'flex-end', fontWeight: 'bold' }]}>
                Tổng cộng:
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-start' }}>
              <Text style={[style.text, { textAlign: 'right', alignSelf: 'flex-end' }]}>
                {getPriceWithCommas(219000)}
              </Text>
              <Text style={[style.text, { textAlign: 'right', alignSelf: 'flex-end' }]}>
                {getPriceWithCommas(20000)}
              </Text>
              <Text style={[style.text, { textAlign: 'right', alignSelf: 'flex-end' }]}>{getPriceWithCommas(0)}</Text>
              <Text
                style={[
                  style.text,
                  { textAlign: 'right', alignSelf: 'flex-end', fontWeight: 'bold', color: APP_COLOR }
                ]}
              >
                {getPriceWithCommas(239000)}
              </Text>
            </View>
          </View>
          <View
            style={{
              // paddingTop: 10,
              borderTopColor: '#EFEFEF',
              borderTopWidth: 1,
              height: 50 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <TextInput
              clearButtonMode="while-editing"
              placeholder="Thêm ghi chú..."
              placeholderTextColor="#9c9faa"
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              autoCorrect={false}
              style={{
                color: APP_COLOR_TEXT,
                flex: 7,
                // height: 35 * SCALE_RATIO_WIDTH_BASIS,
                fontFamily: 'helveticaneue',
                fontSize: FS(14),
                marginLeft: 10 * SCALE_RATIO_WIDTH_BASIS
              }}
            />
          </View>
        </Collapsible>

        {this.renderHeader('Phương thức thanh toán', 'showInfoPay')}
        <Collapsible
          collapsed={this.state.showInfoPay}
          style={{ backgroundColor: '#fff', padding: 10 * SCALE_RATIO_WIDTH_BASIS, paddingVertical: 0 }}
        >
          <CheckBox
            title="Tiền mặt"
            checkedColor={APP_COLOR}
            checked={this.state.online}
            containerStyle={{
              paddingVertical: 10,
              borderBottomColor: '#EFEFEF',
              borderBottomWidth: 1,
              margin: 0,
              padding: 0,
              borderWidth: 0,
              backgroundColor: 'transparent'
            }}
            checkedColor={APP_COLOR}
            checkedIcon="circle"
            uncheckedIcon="circle-o"
            textStyle={[style.text, { fontWeight: 'normal' }]}
            size={FS(16)}
            onPress={() => {
              if (!this.state.offline) {
                this.setState({
                  offline: !this.state.offline,
                  online: !this.state.online
                });
              }
            }}
          />
          <CheckBox
            title="Cổng thanh toán online Vnpay"
            checkedColor={APP_COLOR}
            checked={this.state.online}
            containerStyle={{
              paddingVertical: 10,
              borderBottomColor: '#EFEFEF',
              borderBottomWidth: 1,
              margin: 0,
              padding: 0,
              borderWidth: 0,
              backgroundColor: 'transparent'
            }}
            checkedColor={APP_COLOR}
            checkedIcon="circle"
            uncheckedIcon="circle-o"
            textStyle={[style.text, { fontWeight: 'normal' }]}
            size={FS(16)}
            onPress={() => {
              if (this.state.offline) {
                this.setState({
                  offline: !this.state.offline,
                  online: !this.state.online
                });
              }
            }}
          />
          <CheckBox
            title="Cổng thanh toán online Napas"
            checkedColor={APP_COLOR}
            checked={this.state.online}
            containerStyle={{
              paddingVertical: 10,
              margin: 0,
              padding: 0,
              borderWidth: 0,
              backgroundColor: 'transparent'
            }}
            checkedColor={APP_COLOR}
            checkedIcon="circle"
            uncheckedIcon="circle-o"
            textStyle={[style.text, { fontWeight: 'normal' }]}
            size={FS(16)}
            onPress={() => {
              if (this.state.offline) {
                this.setState({
                  offline: !this.state.offline,
                  online: !this.state.online
                });
              }
            }}
          />
        </Collapsible>
        <TouchableOpacity
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            height: 40 * SCALE_RATIO_HEIGHT_BASIS,
            width: DEVICE_WIDTH - 40 * SCALE_RATIO_WIDTH_BASIS,
            backgroundColor: '#BAA165',
            marginVertical: 20 * SCALE_RATIO_HEIGHT_BASIS,
            marginBottom: 40 * SCALE_RATIO_WIDTH_BASIS,
            borderRadius: 20 * SCALE_RATIO_HEIGHT_BASIS
          }}
          onPress={() => {
            this.props.navigation.navigate(ROUTE_KEY.COMPLETED_ORDER_COMPONENT);
          }}
        >
          <Text
            style={{
              alignSelf: 'center',
              color: '#FFF',
              fontSize: FS(12),
              textAlign: 'center',
              fontFamily: FONT.SemiBold,
              fontWeight: '600',
              backgroundColor: 'transparent',
              letterSpacing: 1
            }}
          >
            ĐẶT HÀNG
          </Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

const mapActionCreators = {};
const mapStateToProps = state => ({
  listStores: state.store.listStores,
  currentLocation: state.user.currentLocation
});
export default connect(
  mapStateToProps,
  mapActionCreators
)(DeliveryComponent);
const styles = StyleSheet.create({
  line: {
    paddingVertical: 10,
    borderBottomColor: '#EFEFEF',
    borderBottomWidth: 1
  },
  container: {
    backgroundColor: '#FAFAFA',
    flex: 1,
    // paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
    paddingBottom: 20 * SCALE_RATIO_HEIGHT_BASIS
  },
  markerImageStyle: {
    width: 20 * SCALE_RATIO_WIDTH_BASIS,
    height: 20 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 10 * SCALE_RATIO_WIDTH_BASIS
  },
  viewBorder: {
    borderColor: Platform.OS === 'ios' ? '#C7AE6D30' : '#70707010',
    borderWidth: 1,
    backgroundColor: '#fff',
    borderRadius: 5 * SCALE_RATIO_HEIGHT_BASIS,
    paddingVertical: 5 * SCALE_RATIO_HEIGHT_BASIS,
    paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
    marginVertical: 10 * SCALE_RATIO_HEIGHT_BASIS,
    shadowColor: APP_COLOR,
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 3
  },
  dropdownContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#fff'
  }
});
