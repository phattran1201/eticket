import React from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
  TextInput,
  Image
} from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import FastImage from 'react-native-fast-image';
import Feather from 'react-native-vector-icons/dist/Feather';
import { connect } from 'react-redux';
import { DEVICE_WIDTH, DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS, ROUTE_KEY, FS } from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style, { FONT, APP_COLOR, APP_COLOR_TEXT } from '../../../constants/style';
import { alert } from '../../../utils/alert';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import { getNumberWithCommas } from '../../../utils/numberUtils';
import PopupNotification from '../../view/PopupNotification';
import PopupDetailProduct from '../../view/PopupDetailProduct';
import MyTextInput from '../../../style/MyTextInput';
import MyButton from '../../../style/MyButton';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import STouchableOpacity from '../../view/STouchableOpacity';
import { getBottomSpace, getStatusBarHeight } from 'react-native-iphone-x-helper';
import DetailViewableImage from '../../view/DetailViewableImage';
import Modal from 'react-native-modal';
import { Menu, MenuOptions, MenuOption, MenuTrigger, renderers } from 'react-native-popup-menu';
import { DATA_MILK_TEA, DATA_CATEGORY } from '../../../constants/dataTest';
import Drawer from 'react-native-draggable-view';

const { Popover } = renderers;

class MyProductItem1Column extends React.Component {
  constructor(props) {
    super(props);
    this.state = { total: 0 };
    // this.onPressMinus = this.onPressMinus.bind(this);
    // this.onPressPlus = this.onPressPlus.bind(this);
  }
  // onPressMinus() {
  //   this.setState({ total: this.state.total + 1 });
  // }
  // onPressPlus() {
  //   this.setState({ total: this.state.total - 1 });
  // }
  render() {
    const { item } = this.props;
    return (
      <View style={[style.shadow, styles.productContainer, { paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS }]}>
        <DetailViewableImage
          style={{
            width: 70 * SCALE_RATIO_WIDTH_BASIS,
            height: 70 * SCALE_RATIO_WIDTH_BASIS
          }}
          source={{ uri: item.backgroundImage }}
        />

        <STouchableOpacity
          style={[styles.productItemMiddleContainer]}
          onPress={() => this.props.navigation.navigate(ROUTE_KEY.DETAIL_PRODUCT)}
        >
          <View style={{ flex: 1, maxWidth: (DEVICE_WIDTH * 50) / 100 }}>
            <Text
              numberOfLines={2}
              style={{
                color: '#3C3C3C',
                fontSize: 15 * SCALE_RATIO_WIDTH_BASIS,
                fontWeight: '600'
              }}
            >
              {item.name}
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <Text
              style={{
                fontWeight: '600',
                color: '#D3B574',
                fontSize: 13 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              {item.finalPrice}đ{' '}
              <Text
                style={{
                  color: '#A7A7A7',
                  fontSize: 10 * SCALE_RATIO_WIDTH_BASIS,
                  textDecorationLine: 'line-through'
                }}
              >
                {item.discount > 0 ? `${item.price} đ` : ''}
              </Text>
            </Text>
          </View>
        </STouchableOpacity>
        {this.state.total > 0 ? (
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.productItemRightContainer}>
              <TouchableOpacity onPress={() => this.setState({ total: this.state.total - 1 })}>
                <MaterialCommunityIcons name="minus-circle" size={24 * SCALE_RATIO_WIDTH_BASIS} color="#7f9dd4" />
              </TouchableOpacity>
            </View>
            <Text
              style={[
                style.textHeader,
                { fontFamily: FONT.SemiBold, alignSelf: 'center', paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS }
              ]}
            >
              {this.state.total}
            </Text>
          </View>
        ) : null}
        <View style={styles.productItemRightContainer}>
          <TouchableOpacity
            onPress={() => this.setState({ total: this.state.total + 1 })}
            // onPress={() => {
            //   const temp = this.state.selectedItem.find(e => e.id === item.id);
            //   if (temp) {
            //     return;
            //   }
            //   this.totalCostWithoutDiscount += item.price;
            //   this.totalCostWithDiscount += item.finalPrice;
            //   this.setState({ selectedItem: [...this.state.selectedItem, item] });
            //   PopupNotification.showComponent(PopupDetailProduct, true);
            // }}
          >
            <MaterialCommunityIcons name="plus-circle" size={24 * SCALE_RATIO_WIDTH_BASIS} color="#7f9dd4" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

class MyProductItem2Column extends React.Component {
  constructor(props) {
    super(props);
    this.state = { total: 0 };
  }
  render() {
    const { item, index } = this.props;
    return (
      <View
        style={[
          style.shadow,
          {
            backgroundColor: '#fff',
            flex: 1,
            maxWidth: (DEVICE_WIDTH * 50) / 100,
            margin: 10 * SCALE_RATIO_WIDTH_BASIS,
            marginRight: index % 2 ? 10 * SCALE_RATIO_WIDTH_BASIS : 0,
            borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            paddingBottom: 5 * SCALE_RATIO_WIDTH_BASIS
          }
        ]}
      >
        <View
          style={{
            borderTopLeftRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            borderTopRightRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            overflow: 'hidden'
          }}
        >
          <DetailViewableImage
            style={{
              width: '100%',
              height: 150 * SCALE_RATIO_WIDTH_BASIS
            }}
            source={{ uri: item.backgroundImage }}
          />
        </View>

        <STouchableOpacity
          onPress={() => this.props.navigation.navigate(ROUTE_KEY.DETAIL_PRODUCT)}
          style={[styles.productItemMiddleContainer, { marginTop: 5 * SCALE_RATIO_WIDTH_BASIS }]}
        >
          <Text
            numberOfLines={2}
            style={{
              color: '#3C3C3C',
              fontSize: 15 * SCALE_RATIO_WIDTH_BASIS,
              fontWeight: '600'
            }}
          >
            {item.name}
          </Text>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  fontWeight: '600',
                  color: '#D3B574',
                  fontSize: 13 * SCALE_RATIO_WIDTH_BASIS
                }}
              >
                {item.finalPrice}đ{' '}
                <Text
                  style={{
                    color: '#A7A7A7',
                    fontSize: 10 * SCALE_RATIO_WIDTH_BASIS,
                    textDecorationLine: 'line-through'
                  }}
                >
                  {item.discount > 0 ? `${item.price} đ` : ''}
                </Text>
              </Text>
            </View>
            <View style={styles.productItemRightContainer}>
              <TouchableOpacity onPress={() => this.props.onPress()}>
                <MaterialCommunityIcons name="plus-circle" size={24 * SCALE_RATIO_WIDTH_BASIS} color="#7f9dd4" />
              </TouchableOpacity>
            </View>
          </View>
        </STouchableOpacity>
      </View>
    );
  }
}
class MenuComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.totalCostWithDiscount = 0;
    this.totalCostWithoutDiscount = 0;
    this.changLayout1Column = this.changLayout1Column.bind(this);
    this.changLayout2Column = this.changLayout2Column.bind(this);
    this.state = {
      showMyCart: false,
      activeLayout: true,
      activeSections: [0],
      data: [
        {
          id: 0,
          index: 0,
          name: 'Milk tea',
          products: [
            {
              id: 0,
              name: 'Trà sữa Panda',
              price: 42000,
              discount: 7,
              finalPrice: 39000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/20177316527-tra-sua-tran-chau-soi.jpg',
              total: 0
            },
            {
              id: 1,
              name: 'Hồng trà việt quất',
              price: 38000,
              discount: 0,
              finalPrice: 38000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/201773165018-hong-tra-viet-quat.jpg',
              total: 0
            },
            {
              id: 2,
              name: 'Trà sữa kim cương đen Okinawa',
              price: 46000,
              discount: 0,
              finalPrice: 46000,
              backgroundImage:
                'https://images.foody.vn/res/g28/274442/s120x120/201773165348-tra-sua-kim-cuong-den-okinawa.jpg',
              total: 0
            },
            {
              id: 3,
              name: 'Trà sữa trân châu sợi',
              price: 45000,
              discount: 0,
              finalPrice: 45000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/20177316527-tra-sua-tran-chau-soi.jpg',
              total: 0
            },
            {
              id: 4,
              name: 'Trà sữa ba anh em',
              price: 48000,
              discount: 50,
              finalPrice: 24000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/201773165440-tra-sua-ba-anh-em.jpg',
              total: 0
            },
            {
              id: 5,
              name: 'Trà sữa matcha',
              price: 40000,
              discount: 0,
              finalPrice: 40000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/20171024144955-tra-sua-matcha.jpg',
              total: 0
            },
            {
              id: 5,
              name: 'Trà sữa bánh pudding',
              price: 45000,
              discount: 20,
              finalPrice: 36000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/201773165532-tra-sua-banh-pudding.jpg',
              total: 0
            },
            {
              id: 6,
              name: 'Sữa tươi trân châu đường hổ',
              price: 49000,
              discount: 0,
              finalPrice: 49000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/201891194154-st.jpg',
              total: 0
            }
          ]
        },
        {
          id: 1,
          index: 1,
          name: 'Fresh fruit tea',
          products: [
            {
              id: 0,
              name: 'Trà sữa Panda',
              price: 42000,
              discount: 7,
              finalPrice: 39000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/20177316527-tra-sua-tran-chau-soi.jpg',
              total: 0
            },
            {
              id: 1,
              name: 'Hồng trà việt quất',
              price: 38000,
              discount: 0,
              finalPrice: 38000,
              backgroundImage: 'https://images.foody.vn/res/g28/274442/s120x120/201773165018-hong-tra-viet-quat.jpg',
              total: 0
            }
          ]
        }
      ],
      selectedItem: []
    };
  }

  renderSectionTitle = section => (
    <View>
      <Text>{section.name}</Text>
    </View>
  );

  renderHeader = (section, index, isActive) => (
    <View
      style={{
        width: '100%',
        flexDirection: 'row',
        marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS
      }}
    >
      <View style={{ flex: 1 }}>
        <Text
          style={{
            color: '#373737',
            fontSize: 16 * SCALE_RATIO_WIDTH_BASIS,
            fontWeight: 'bold'
          }}
        >
          {section.name}
        </Text>
      </View>
      <View
        style={{
          width: 30 * SCALE_RATIO_WIDTH_BASIS,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        {isActive ? (
          <Feather name="chevron-down" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#ABABAB" />
        ) : (
          <Feather name="chevron-right" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#ABABAB" />
        )}
      </View>
    </View>
  );

  onItemPress = item => {
    const temp = this.state.selectedItem.find(e => e.id === item.id);
    // PopupNotification.showComponentOnBottom(PopupDetailProduct, true);
    if (temp) {
      return;
    }
    this.totalCostWithoutDiscount += item.price;
    this.totalCostWithDiscount += item.finalPrice;
    this.setState({ selectedItem: [...this.state.selectedItem, item] });
    // this.props.navigation.navigate(ROUTE_KEY.DETAIL_PRODUCT);
    // return (
    //   <PopupDetailProduct
    //     // onDone={}
    //     // product={}
    //   />
    // );
  };

  renderItem = ({ item, index }) => {
    if (this.state.activeLayout) {
      return (
        <MyProductItem2Column
          item={item}
          index={index}
          style={{ overflow: 'visible' }}
          onPress={() => this.onItemPress(item)}
          navigation={this.props.navigation}
        />
      );
    }
    return (
      <MyProductItem1Column
        item={item}
        index={index}
        style={{ overflow: 'visible' }}
        onPress={() => this.onItemPress(item)}
        navigation={this.props.navigation}
      />
    );
  };

  renderContent = section => (
    <FlatList
      key={this.state.activeLayout ? 2 : 1}
      numColumns={this.state.activeLayout ? 2 : 1}
      data={section.products}
      keyExtractor={item => item.id}
      removeClippedSubviews
      renderItem={(item, index) => this.renderItem(item, index)}
    />
  );

  updateSections = sections => {
    this.setState({
      activeSections: sections.includes(undefined) ? [] : sections
    });
  };
  changLayout2Column() {
    this.setState({ activeLayout: true });
  }
  changLayout1Column() {
    this.setState({ activeLayout: false });
  }
  render() {
    return (
      <Drawer
        finalDrawerHeight={10 * SCALE_RATIO_WIDTH_BASIS + getStatusBarHeight()}
        initialDrawerSize={0.1}
        renderContainerView={() => (
          <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <BaseHeader
              noShadow
              children={
                <TextInput
                  style={[
                    style.textCaption,
                    {
                      width: (DEVICE_WIDTH * 80) / 100,
                      borderRadius: 30 * SCALE_RATIO_WIDTH_BASIS,
                      backgroundColor: '#fbfbfb',
                      paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
                      paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
                      fontSize: FS(14),
                      fontFamily: 'FontAwesome'
                    }
                  ]}
                  ref="searchBar"
                  clearButtonMode="always"
                  placeholder=" Tìm kiếm sản phẩm"
                  placeholderTextColor="#00000050"
                  underlineColorAndroid="transparent"
                  selectionColor="#C7AE6D"
                  autoCapitalize="none"
                  autoCorrect={false}
                  onChangeText={this.onSearchBarChangeText}
                />
              }
              style={{ zIndex: 99, backgroundColor: '#fff' }}
              leftIcon="arrow-left"
              leftIconType="Feather"
              onLeftPress={() => this.props.navigation.goBack()}
            />
            <Menu
              renderer={Popover}
              rendererProps={{ placement: 'top' }}
              style={{
                position: 'absolute',
                zIndex: 999,
                bottom: 75 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace(),
                left: 5 * SCALE_RATIO_WIDTH_BASIS,
                width: 30 * SCALE_RATIO_WIDTH_BASIS,
                height: 30 * SCALE_RATIO_WIDTH_BASIS,
                padding: 5 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <MenuTrigger
                style={{
                  width: 30 * SCALE_RATIO_WIDTH_BASIS,
                  height: 30 * SCALE_RATIO_WIDTH_BASIS,
                  backgroundColor: APP_COLOR,
                  borderRadius: (30 * SCALE_RATIO_WIDTH_BASIS) / 2,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <MaterialCommunityIcons name="view-sequential" size={FS(20)} color={'#fff'} />
              </MenuTrigger>
              <MenuOptions style={{ margin: 10 * SCALE_RATIO_WIDTH_BASIS }}>
                <View
                  style={{
                    borderBottomColor: '#F4F4F4',
                    borderBottomWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
                    paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS
                  }}
                >
                  <Text style={style.textModal}>Danh mục</Text>
                </View>
                {DATA_CATEGORY.map(item => (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      borderBottomColor: '#F4F4F4',
                      borderBottomWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
                      padding: 10 * SCALE_RATIO_WIDTH_BASIS
                    }}
                  >
                    <Text style={style.text}>{item}</Text>
                    <Text style={[style.textCaption, { marginLeft: 10 * SCALE_RATIO_WIDTH_BASIS }]}>
                      {Math.floor(Math.random() * 30) + 1}
                    </Text>
                  </View>
                ))}
              </MenuOptions>
            </Menu>
            <View
              style={[
                style.shadow,
                {
                  zIndex: 98,
                  flexDirection: 'row',
                  backgroundColor: '#fff',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
                  paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
            >
              <MyButton
                textStyle={[style.text, { color: '#fff' }]}
                styleContent={{
                  height: 30 * SCALE_RATIO_WIDTH_BASIS,
                  alignSelf: 'center',
                  paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS,
                  paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS
                }}
              >
                Tạo nhóm
              </MyButton>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <STouchableOpacity
                  style={{
                    borderWidth: 1,
                    borderColor: this.state.activeLayout ? APP_COLOR : '#B8B8B8',
                    backgroundColor: this.state.activeLayout ? APP_COLOR : 'transparent',
                    padding: 4 * SCALE_RATIO_WIDTH_BASIS,
                    borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS
                  }}
                  onPress={() => this.changLayout2Column()}
                >
                  <MaterialCommunityIcons
                    name="view-grid"
                    size={FS(12)}
                    color={this.state.activeLayout ? '#fff' : '#B8B8B8'}
                  />
                </STouchableOpacity>
                <STouchableOpacity
                  style={{
                    marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
                    borderWidth: 1,
                    borderColor: !this.state.activeLayout ? APP_COLOR : '#B8B8B8',
                    backgroundColor: !this.state.activeLayout ? APP_COLOR : 'transparent',
                    padding: 4 * SCALE_RATIO_WIDTH_BASIS,
                    borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS
                  }}
                  onPress={() => this.changLayout1Column()}
                >
                  <MaterialCommunityIcons
                    name="view-sequential"
                    size={FS(12)}
                    color={!this.state.activeLayout ? '#fff' : '#B8B8B8'}
                  />
                </STouchableOpacity>
              </View>
            </View>

            <ScrollView style={{ flex: 1 }}>
              <View
                style={{
                  flex: 1,
                  paddingTop: 20 * SCALE_RATIO_WIDTH_BASIS,
                  paddingBottom: 100 * SCALE_RATIO_WIDTH_BASIS
                }}
              >
                <Accordion
                  duration={100}
                  underlayColor={'#fff'}
                  activeSections={this.state.activeSections}
                  expandMultiple
                  sections={this.state.data}
                  renderHeader={this.renderHeader}
                  renderContent={this.renderContent}
                  onChange={this.updateSections}
                />
              </View>
            </ScrollView>
          </View>
        )}
        renderDrawerView={() => (
          <View
            style={[
              {
                backgroundColor: '#fff',
                paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
          >
            {/* <View
              style={{
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.2,
                borderColor: '#70707020',
                alignItems: 'center',
                paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
                marginBottom: 5 * SCALE_RATIO_WIDTH_BASIS,
                paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <MaterialCommunityIcons
                onPress={() => this.setState({ showMyCart: false })}
                name="arrow-down-circle"
                size={18 * SCALE_RATIO_WIDTH_BASIS}
                color="#808080"
                style={{ flex: 1, alignSelf: 'center' }}
              />
              <View style={{ flex: 3, alignSelf: 'center' }}>
                <Text style={[style.titleHeader, { textAlign: 'center' }]}>Giỏ hàng của tôi</Text>
              </View>
              <View style={{ flex: 1, alignSelf: 'center' }}>
                <Text style={[style.text, { textAlign: 'right' }]}>Xóa tất cả</Text>
              </View>
            </View> */}
            {DATA_MILK_TEA.map((item, i) => {
              {
                /* console.log('dauphaiphat: render -> i', i);
              console.log('dauphaiphat: render -> totalItem', item.total); */
              }
              if (item.total === 0) return true;
              return (
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 5 * SCALE_RATIO_WIDTH_BASIS
                  }}
                >
                  <View style={{ maxWidth: '70%' }}>
                    <Text style={[style.titleHeader, { fontSize: FS(12), color: '#707070', textAlign: 'left' }]}>
                      {item.name}
                    </Text>
                    <Text
                      style={[
                        style.text,
                        {
                          fontSize: FS(10),
                          textAlign: 'left'
                        }
                      ]}
                    >
                      {item.fill}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={[style.textCaption, { fontSize: FS(10) }]}>{getNumberWithCommas(item.price)}</Text>
                      <Text
                        style={[style.textCaption, { fontSize: FS(10), marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS }]}
                      >
                        x
                      </Text>
                      <Text style={[style.textCaption, { fontSize: FS(10) }]}>{item.total}</Text>
                      <Text
                        style={[style.textCaption, { fontSize: FS(10), marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS }]}
                      >
                        =
                      </Text>
                      <Text
                        style={[
                          style.textCaption,
                          {
                            fontSize: FS(10),
                            marginLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                            // textDecorationLine: 'line-through'
                          }
                        ]}
                      >
                        {getNumberWithCommas(item.price * item.total)}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    {item.total > 0 ? (
                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={styles.productItemRightContainer}>
                          <TouchableOpacity onPress={() => (item.total -= 1)}>
                            <MaterialCommunityIcons
                              name="minus-circle"
                              size={24 * SCALE_RATIO_WIDTH_BASIS}
                              color="#7f9dd4"
                            />
                          </TouchableOpacity>
                        </View>
                        <Text
                          style={[
                            style.textHeader,
                            {
                              fontFamily: FONT.SemiBold,
                              alignSelf: 'center',
                              paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS
                            }
                          ]}
                        >
                          {item.total}
                        </Text>
                      </View>
                    ) : null}
                    <View style={styles.productItemRightContainer}>
                      <TouchableOpacity onPress={() => item.total + 1}>
                        <MaterialCommunityIcons
                          name="plus-circle"
                          size={24 * SCALE_RATIO_WIDTH_BASIS}
                          color="#7f9dd4"
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        )}
        renderInitDrawerView={() => (
          <View
            style={{
              backgroundColor: 'white',
              height: 60 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace()
            }}
          >
            <View
              activeOpacity={1}
              style={[
                style.shadow,
                {
                  backgroundColor: '#ffffff',
                  height: 60 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace(),
                  padding: 5 * SCALE_RATIO_WIDTH_BASIS,
                  paddingTop: 0,
                  paddingBottom: getBottomSpace(),
                  width: DEVICE_WIDTH
                }
              ]}
              // onPress={() => this.setState({ showMyCart: !this.state.showMyCart })}
            >
              <View style={{ height: 1, width: DEVICE_WIDTH, backgroundColor: APP_COLOR }} />
              <View
                style={{
                  width: 40,
                  height: 2,
                  backgroundColor: APP_COLOR,
                  alignSelf: 'center',
                  borderRadius: 1,
                  marginTop: 5
                }}
              />
              <View
                style={{
                  width: 40,
                  height: 2,
                  backgroundColor: APP_COLOR,
                  alignSelf: 'center',
                  borderRadius: 2,
                  marginVertical: 2
                }}
              />
              <View
                style={{ width: 40, height: 2, backgroundColor: APP_COLOR, alignSelf: 'center', borderRadius: 1 }}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS
                }}
              >
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image
                    source={require('../../../assets/imgs/icons/milkshake.png')}
                    resizeMode="contain"
                    style={{
                      width: 20 * SCALE_RATIO_WIDTH_BASIS,
                      alignSelf: 'center',
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}
                  />
                  <Text
                    style={[
                      style.textHeader,
                      { fontFamily: FONT.SemiBold, marginHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS }
                    ]}
                  >
                    x
                  </Text>
                  <Text style={[style.textHeader, { fontFamily: FONT.SemiBold, color: APP_COLOR }]}>
                    {/* {this.state.selectedItem.length} */}
                  </Text>
                  <Text
                    style={[
                      style.textHeader,
                      { fontFamily: FONT.SemiBold, marginHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS }
                    ]}
                  >
                    =
                  </Text>
                  <Text style={[style.textHeader, { fontFamily: FONT.SemiBold, color: APP_COLOR }]}>
                    {this.totalCostWithDiscount}đ{' '}
                  </Text>
                  {this.totalCostWithoutDiscount ? (
                    <Text
                      style={[
                        style.textHeader,
                        {
                          fontFamily: FONT.SemiBold,
                          color: '#A7A7A7',
                          textDecorationLine: 'line-through',
                          fontSize: FS(11)
                        }
                      ]}
                    >
                      {this.totalCostWithoutDiscount}đ
                    </Text>
                  ) : null}
                </View>

                <MyButton
                  styleContent={{ height: 38 * SCALE_RATIO_WIDTH_BASIS, alignSelf: 'center' }}
                  onPress={() => {
                    if (this.props.token !== '' && this.props.userData && this.props.userData.id !== '') {
                      this.props.navigation.navigate(ROUTE_KEY.CHECKOUT);
                    } else {
                      alert(strings.alert, strings.please_login, () => {
                        this.props.navigation.navigate(ROUTE_KEY.PRELOGIN, { fromScreen: 'CHECKOUT' });
                      });
                    }
                  }}
                  textStyle={{
                    textAlign: 'center',
                    fontSize: FS(14)
                  }}
                  outline={this.totalCostWithDiscount === 0}
                >
                  THANH TOÁN
                </MyButton>
              </View>
            </View>
          </View>
        )}
      />
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const mapActionCreators = {};

export default connect(
  mapStateToProps,
  mapActionCreators,
  null,
  { withRef: true }
)(MenuComponent);

const styles = StyleSheet.create({
  paymentButtonStyle: {
    width: 140 * SCALE_RATIO_WIDTH_BASIS,
    height: 40 * SCALE_RATIO_WIDTH_BASIS,
    backgroundColor: '#D3B574',
    borderRadius: 20 * SCALE_RATIO_WIDTH_BASIS,
    justifyContent: 'center',
    alignItems: 'center'
  },
  productItemRightContainer: {
    // paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
    justifyContent: 'center',
    alignItems: 'center'
  },
  productItemMiddleContainer: {
    paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  productContainer: {
    // marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
    // padding: 10 * SCALE_RATIO_WIDTH_BASIS,
    paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
    // overflow: 'hidden',
    // borderRadius: 5,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row'
  },
  paymentButtonContainer: {
    paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
    width: 120 * SCALE_RATIO_WIDTH_BASIS,
    justifyContent: 'center',
    alignItems: 'center'
  },
  contentContainer: {
    flex: 1,
    padding: 20 * SCALE_RATIO_WIDTH_BASIS,
    backgroundColor: '#fff'
  },
  cartContainer: {
    backgroundColor: '#ffffff',
    position: 'absolute',
    height: 60 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace(),
    padding: 5 * SCALE_RATIO_WIDTH_BASIS,
    bottom: 0,
    paddingBottom: getBottomSpace(),
    width: DEVICE_WIDTH
  }
});
