import React from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  ScrollView,
  Share,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import { getBottomSpace } from 'react-native-iphone-x-helper';
import Modal from 'react-native-modal';
import {
  Menu,
  MenuOptions,
  MenuTrigger,
  renderers
} from 'react-native-popup-menu';
import Feather from 'react-native-vector-icons/dist/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  FS,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import {
  DATA_CATEGORY,
  DATA_MILK_TEA,
  DATA_TEST
} from '../../../constants/dataTest';
import strings from '../../../constants/Strings';
import style, { APP_COLOR, FONT } from '../../../constants/style';
import MyButton from '../../../style/MyButton';
import { alert } from '../../../utils/alert';
import {
  getPriceWithCommas,
  getNumberWithCommas
} from '../../../utils/numberUtils';
import BaseHeader from '../../view/BaseHeader';
import DetailViewableImage from '../../view/DetailViewableImage';
import MyComponent from '../../view/MyComponent';
import MyImage from '../../view/MyImage';
import MySpinner from '../../view/MySpinner';
import STouchableOpacity from '../../view/STouchableOpacity';
import { loadListProduct, loadMoreListProduct } from './MenuAction';
import FastImage from 'react-native-fast-image';
import CartDetailComponent from '../../view/CartDetailComponent';
import CartGroupDetailComponent from '../../view/CartGroupDetailComponent';

const { Popover } = renderers;

class MyProductItem1Column extends React.Component {
  constructor(props) {
    super(props);
    this.state = { total: 0 };
    // this.onPressMinus = this.onPressMinus.bind(this);
    // this.onPressPlus = this.onPressPlus.bind(this);
  }
  // onPressMinus() {
  //   this.setState({ total: this.state.total + 1 });
  // }
  // onPressPlus() {
  //   this.setState({ total: this.state.total - 1 });
  // }
  render() {
    const { item } = this.props;
    const image = item.images ? item.images[0] : '';
    const title = item.name;
    const priceFormat = getPriceWithCommas(item.minPrice);
    const compareAtPriceFormat = getPriceWithCommas(item.maxPrice);
    const sale = item.sale;
    return (
      <STouchableOpacity
        onPress={() => {
          this.props.navigation.navigate({
            key: 'DETAIL_PRODUCT_001',
            routeName: ROUTE_KEY.DETAIL_PRODUCT,
            params: {
              product: item
            }
          });
        }}
        style={[
          style.shadow,
          styles.productContainer,
          { paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS }
        ]}
      >
        <FastImage
          style={{
            width: 70 * SCALE_RATIO_WIDTH_BASIS,
            height: 70 * SCALE_RATIO_WIDTH_BASIS
          }}
          source={
            image
              ? { uri: image }
              : require('../../../assets/imgs/tocotoco_star_img.png')
          }
          defaultSource={require('../../../assets/imgs/tocotoco_star_img.png')}
        />
        <STouchableOpacity
          style={[styles.productItemMiddleContainer]}
          onPress={() => {
            this.props.navigation.navigate({
              key: 'DETAIL_PRODUCT_001',
              routeName: ROUTE_KEY.DETAIL_PRODUCT,
              params: {
                product: item
              }
            });
          }}
        >
          <View style={{ flex: 1, maxWidth: (DEVICE_WIDTH * 50) / 100 }}>
            <Text
              numberOfLines={2}
              style={{
                color: '#3C3C3C',
                fontSize: 15 * SCALE_RATIO_WIDTH_BASIS,
                fontWeight: '600'
              }}
            >
              {title}
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <Text
              style={{
                fontWeight: '600',
                color: '#D3B574',
                fontSize: 13 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              {/* {item.price}đ{' '} */}
              {priceFormat}
              {'  '}
              <Text
                style={{
                  color: '#A7A7A7',
                  fontSize: 10 * SCALE_RATIO_WIDTH_BASIS,
                  textDecorationLine: 'line-through'
                }}
              >
                {/* {item.discount > 0 ? `${item.price} đ` : ''} */}
                {/* {sale > 0 ? compareAtPriceFormat : ''} */}
                {compareAtPriceFormat}
              </Text>
            </Text>
          </View>
        </STouchableOpacity>
        {this.state.total > 0 ? (
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.productItemRightContainer}>
              <TouchableOpacity
                onPress={() => this.setState({ total: this.state.total - 1 })}
              >
                <MaterialCommunityIcons
                  name='minus-circle'
                  size={24 * SCALE_RATIO_WIDTH_BASIS}
                  color='#7f9dd4'
                />
              </TouchableOpacity>
            </View>
            <Text
              style={[
                style.textHeader,
                {
                  fontFamily: FONT.SemiBold,
                  alignSelf: 'center',
                  paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
            >
              {this.state.total}
            </Text>
          </View>
        ) : null}
        <View style={styles.productItemRightContainer}>
          <TouchableOpacity
            onPress={() => this.setState({ total: this.state.total + 1 })}
            // onPress={() => {
            //   const temp = this.state.selectedItem.find(e => e.id === item.id);
            //   if (temp) {
            //     return;
            //   }
            //   this.totalCostWithoutDiscount += item.price;
            //   this.totalCostWithDiscount += item.finalPrice;
            //   this.setState({ selectedItem: [...this.state.selectedItem, item] });
            //   PopupNotification.showComponent(PopupDetailProduct, true);
            // }}
          >
            <MaterialCommunityIcons
              name='plus-circle'
              size={24 * SCALE_RATIO_WIDTH_BASIS}
              color='#7f9dd4'
            />
          </TouchableOpacity>
        </View>
      </STouchableOpacity>
    );
  }
}

class MyProductItem2Column extends React.Component {
  constructor(props) {
    super(props);
    this.state = { total: 0 };
  }
  render() {
    const { item, index } = this.props;
    const image = item.images ? item.images[0] : '';
    const title = item.name;
    const priceFormat = getPriceWithCommas(item.minPrice);
    const compareAtPriceFormat = getPriceWithCommas(item.maxPrice);
    const sale = item.sale;
    return (
      <STouchableOpacity
        onPress={() => {
          this.props.navigation.navigate({
            key: 'DETAIL_PRODUCT_001',
            routeName: ROUTE_KEY.DETAIL_PRODUCT,
            params: {
              product: item
            }
          });
        }}
        style={[
          style.shadow,
          {
            backgroundColor: '#fff',
            flex: 1,
            maxWidth: (DEVICE_WIDTH * 46) / 100,
            marginVertical: (DEVICE_WIDTH * 2) / 100,
            marginLeft: index % 2 ? 0 : (DEVICE_WIDTH * 2.5) / 100,
            marginRight: index % 2 ? 0 : (DEVICE_WIDTH * 3) / 100,
            borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            paddingBottom: 5 * SCALE_RATIO_WIDTH_BASIS
          }
        ]}
      >
        <View
          style={{
            borderTopLeftRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            borderTopRightRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            overflow: 'hidden'
          }}
        >
          <FastImage
            style={{
              width: '100%',
              height: 150 * SCALE_RATIO_WIDTH_BASIS
            }}
            source={
              image
                ? { uri: image }
                : require('../../../assets/imgs/tocotoco_star_img.png')
            }
            defaultSource={require('../../../assets/imgs/tocotoco_star_img.png')}
          />
        </View>

        <STouchableOpacity
          onPress={() => {
            this.props.navigation.navigate({
              key: 'DETAIL_PRODUCT_001',
              routeName: ROUTE_KEY.DETAIL_PRODUCT,
              params: {
                product: item
              }
            });
          }}
          style={[
            styles.productItemMiddleContainer,
            { marginTop: 5 * SCALE_RATIO_WIDTH_BASIS }
          ]}
        >
          <Text
            numberOfLines={2}
            style={{
              color: '#3C3C3C',
              fontSize: 15 * SCALE_RATIO_WIDTH_BASIS,
              fontWeight: '600'
            }}
          >
            {/* {item.name} */}
            {title}
          </Text>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  fontWeight: '600',
                  color: '#D3B574',
                  fontSize: 13 * SCALE_RATIO_WIDTH_BASIS
                }}
              >
                {/* {item.finalPrice}đ{' '} */}
                {priceFormat}
                {'  '}
                <Text
                  style={{
                    color: '#A7A7A7',
                    fontSize: 10 * SCALE_RATIO_WIDTH_BASIS,
                    textDecorationLine: 'line-through'
                  }}
                >
                  {/* {item.discount > 0 ? `${item.price} đ` : ''} */}
                  {compareAtPriceFormat}
                  {/* {sale > 0 ? compareAtPriceFormat : ''} */}
                </Text>
              </Text>
            </View>
            <View style={styles.productItemRightContainer}>
              <TouchableOpacity onPress={() => this.props.onPress()}>
                <MaterialCommunityIcons
                  name='plus-circle'
                  size={24 * SCALE_RATIO_WIDTH_BASIS}
                  color='#7f9dd4'
                />
              </TouchableOpacity>
            </View>
          </View>
        </STouchableOpacity>
      </STouchableOpacity>
    );
  }
}

class MenuComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.totalCostWithDiscount = 0;
    this.totalCostWithoutDiscount = 0;
    this.changLayout1Column = this.changLayout1Column.bind(this);
    this.changLayout2Column = this.changLayout2Column.bind(this);
    const { params } = this.props.navigation.state;
    this.selectedCollection =
      params && params.selectedCollection ? params.selectedCollection : {};
    const index = this.props.listCollections.findIndex(
      e => e.col_title === this.selectedCollection.col_title
    );
    this.state = {
      searchedList: [],
      searchText: '',
      showMyCart: false,
      showGroupOrder: false,
      activeLayout: true,
      activeSections: index !== -1 ? [index] : [],
      products: this.props.listCollections,
      selectedItem: [],
      loading: false
    };
    this.isGroupOrder = true;
    if (this.isGroupOrder) {
      this.isGroupOwner = false;
    }
  }

  renderHeader = (section, index, isActive) => (
    <View
      style={{
        width: '100%',
        flexDirection: 'row',
        paddingTop: index === 0 ? 0 : 20 * SCALE_RATIO_WIDTH_BASIS,
        paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
        paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS
      }}
    >
      <View style={{ flex: 1 }}>
        <Text style={[style.text, { fontSize: FS(18) }]}>
          {section.col_title}
        </Text>
      </View>
      {section.products && section.products.length > 0 ? (
        <View
          style={{
            width: 30 * SCALE_RATIO_WIDTH_BASIS,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          {isActive ? (
            <Feather
              name='chevron-down'
              size={20 * SCALE_RATIO_WIDTH_BASIS}
              color='#ABABAB'
            />
          ) : (
            <Feather
              name='chevron-right'
              size={20 * SCALE_RATIO_WIDTH_BASIS}
              color='#ABABAB'
            />
          )}
        </View>
      ) : null}
    </View>
  );

  onItemPress = item => {
    const temp = this.state.selectedItem.find(e => e.id === item.id);
    // PopupNotification.showComponentOnBottom(PopupDetailProduct, true);
    if (temp) {
      return;
    }
    this.totalCostWithoutDiscount += item.price;
    this.totalCostWithDiscount += item.finalPrice;
    this.setState({ selectedItem: [...this.state.selectedItem, item] });
    // this.props.navigation.navigate(ROUTE_KEY.DETAIL_PRODUCT);
    // return (
    //   <PopupDetailProduct
    //     // onDone={}
    //     // product={}
    //   />
    // );
  };

  renderItem = ({ item, index }) => {
    if (this.state.activeLayout) {
      return (
        <MyProductItem2Column
          item={item}
          index={index}
          style={{ overflow: 'visible' }}
          onPress={() => this.onItemPress(item)}
          navigation={this.props.navigation}
        />
      );
    }
    return (
      <MyProductItem1Column
        item={item}
        index={index}
        style={{ overflow: 'visible' }}
        onPress={() => this.onItemPress(item)}
        navigation={this.props.navigation}
      />
    );
  };

  handleLoadmore = category => {
    if (this.state.loading || category.outOfData) return;
    this.setState({ loading: true });
    this.props.loadMoreListProduct(category, isSuccess => {
      this.setState({ loading: false });
    });
  };

  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }

    return (
      <View style={{ paddingVertical: 10 * SCALE_RATIO_HEIGHT_BASIS }}>
        <ActivityIndicator animating size='small' />
      </View>
    );
  };

  renderContent = section => (
    <FlatList
      key={this.state.activeLayout ? 2 : 1}
      numColumns={this.state.activeLayout ? 2 : 1}
      data={section && section.products ? section.products : []}
      keyExtractor={item => item.id}
      removeClippedSubviews
      renderItem={(item, index) => this.renderItem(item, index)}
      // onEndReachedThreshold={0.005}
      // onEndReached={() => this.handleLoadmore(section)}
      // ListFooterComponent={this.renderFooter}
    />
  );

  componentDidMount() {
    MySpinner.show();
    this.props.listCollections.map((e, i) => {
      if (e && e.products && e.products.length) {
        if (i === this.props.listCollections.length - 1) {
          MySpinner.hide();
        }
      } else {
        this.props.loadListProduct(e, (isSuccess, data) => {
          this.setState({ loading: false });
          if (i === this.props.listCollections.length - 1) {
            MySpinner.hide();
          }
        });
      }
    });
  }
  updateSections = newActiveSections => {
    clearTimeout(this.timeOutActiveSections);
    this.timeOutActiveSections = setTimeout(
      () =>
        this.setState({
          activeSections: newActiveSections
        }),
      500
    );
    // this.props.listCollections.map((e1, i1) => {
    //   const found = newActiveSections.find(e2 => e2 === i1);
    //   if (found !== undefined && !e1.products) {
    //     MySpinner.show();
    //     this.setState({ loading: true });
    //     this.props.loadListProduct(e1, (isSuccess, data) => {
    //       this.setState({ loading: false });
    //       // const newUpdatedList = this.state.products.map(e => {
    //       //   if (e.id === e1.id) {
    //       //       return { ...e, products: [...data] };
    //       //   }
    //       //   return e;
    //       // });
    //       // this.setState({ products: newUpdatedList });
    //       MySpinner.hide();
    //     });
    //   }
    //   if (i1 === this.props.listCollections.length - 1) {
    //     this.timeOutActiveSections = setTimeout(
    //       () =>
    //         this.setState({
    //           activeSections: newActiveSections
    //         }),
    //       500
    //     );
    //   }
    // });
  };
  changLayout2Column() {
    this.setState({ activeLayout: true });
  }
  changLayout1Column() {
    this.setState({ activeLayout: false });
  }

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          'Join my group order Trà Sữa TocotocoFC - 55 Hoàng Hoa Thám - 55 Đường Hoàng Hoa Thám, Phường 13, Tân Bình, Hồ Chí Minh '
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  componentWillUnmount() {
    clearTimeout(this.timeOutActiveSections);
  }

  onSearchBarChangeText = text => {
    // searchedList
    this.setState({ searchText: text });
    const tempList = [...this.props.listCollections]
      .filter(e => e.products)
      .map(collectionHaveProducts => {
        const listProductsMatchcedSearchText = collectionHaveProducts.products.filter(
          product => product && product.name && product.name.includes(text)
        );
        return {
          ...collectionHaveProducts,
          products: listProductsMatchcedSearchText || []
        };
      });
    this.setState({
      searchedList: tempList.filter(e => e.products.length > 0)
    });
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <BaseHeader
          noShadow
          children={
            <TextInput
              style={[
                style.textCaption,
                {
                  width: (DEVICE_WIDTH * 80) / 100,
                  borderRadius: 30 * SCALE_RATIO_WIDTH_BASIS,
                  backgroundColor: '#fbfbfb',
                  paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
                  paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
                  fontSize: FS(14),
                  fontFamily: 'FontAwesome'
                }
              ]}
              ref='searchBar'
              clearButtonMode='always'
              placeholder=' Tìm kiếm sản phẩm'
              placeholderTextColor='#00000050'
              underlineColorAndroid='transparent'
              selectionColor='#C7AE6D'
              autoCapitalize='none'
              autoCorrect={false}
              value={this.state.searchText}
              onChangeText={this.onSearchBarChangeText}
            />
          }
          styleContent={{ zIndex: 99, backgroundColor: '#fff' }}
          leftIcon='arrow-left'
          leftIconType='Feather'
          onLeftPress={() => this.props.navigation.goBack()}
          // onLeftPress={() => this.setState({ showGroupOrder: !this.state.showGroupOrder })}
        />
        <View
          style={[
            {
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
              paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS,
              borderBottomWidth: 1,
              borderBottomColor: '#70707030'
            }
          ]}
        >
          <MyButton
            onPress={() =>
              this.setState({ showGroupOrder: !this.state.showGroupOrder })
            }
            textStyle={[style.text, { color: '#fff' }]}
            styleContent={{
              height: 30 * SCALE_RATIO_WIDTH_BASIS,
              alignSelf: 'center',
              paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS,
              paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            Tạo nhóm
          </MyButton>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
          >
            <STouchableOpacity
              style={{
                borderWidth: 1,
                borderColor: this.state.activeLayout ? APP_COLOR : '#B8B8B8',
                backgroundColor: this.state.activeLayout
                  ? APP_COLOR
                  : 'transparent',
                padding: 4 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS
              }}
              onPress={() => this.changLayout2Column()}
            >
              <MaterialCommunityIcons
                name='view-grid'
                size={FS(12)}
                color={this.state.activeLayout ? '#fff' : '#B8B8B8'}
              />
            </STouchableOpacity>
            <STouchableOpacity
              style={{
                marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
                borderWidth: 1,
                borderColor: !this.state.activeLayout ? APP_COLOR : '#B8B8B8',
                backgroundColor: !this.state.activeLayout
                  ? APP_COLOR
                  : 'transparent',
                padding: 4 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS
              }}
              onPress={() => this.changLayout1Column()}
            >
              <MaterialCommunityIcons
                name='view-sequential'
                size={FS(12)}
                color={!this.state.activeLayout ? '#fff' : '#B8B8B8'}
              />
            </STouchableOpacity>
          </View>
        </View>
        {!this.state.showMyCart && (
          <Menu
            renderer={Popover}
            rendererProps={{ placement: 'top' }}
            style={{
              position: 'absolute',
              zIndex: 999,
              bottom: 75 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace(),
              left: 5 * SCALE_RATIO_WIDTH_BASIS,
              width: 50 * SCALE_RATIO_WIDTH_BASIS,
              height: 50 * SCALE_RATIO_WIDTH_BASIS,
              borderRadius: (50 * SCALE_RATIO_WIDTH_BASIS) / 2,
              padding: 5 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <MenuTrigger
              style={{
                width: 40 * SCALE_RATIO_WIDTH_BASIS,
                height: 40 * SCALE_RATIO_WIDTH_BASIS,
                backgroundColor: APP_COLOR,
                borderRadius: (40 * SCALE_RATIO_WIDTH_BASIS) / 2,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <MaterialCommunityIcons
                name='view-sequential'
                size={FS(20)}
                color={'#fff'}
              />
            </MenuTrigger>
            <MenuOptions style={{ margin: 10 * SCALE_RATIO_WIDTH_BASIS }}>
              <View
                style={{
                  borderBottomColor: '#F4F4F4',
                  borderBottomWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
                  paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS
                }}
              >
                <Text style={style.textModal}>Danh mục</Text>
              </View>
              {this.props.listCollections.map(item => (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    borderBottomColor: '#F4F4F4',
                    borderBottomWidth: 0.5 * SCALE_RATIO_WIDTH_BASIS,
                    padding: 10 * SCALE_RATIO_WIDTH_BASIS
                  }}
                >
                  <Text style={style.text}>{item.col_title}</Text>
                  <Text
                    style={[
                      style.textCaption,
                      { marginLeft: 10 * SCALE_RATIO_WIDTH_BASIS }
                    ]}
                  >
                    {/* {Math.floor(Math.random() * 30) + 1} */}
                    {item.products && item.products.length
                      ? item.products.length
                      : 0}
                  </Text>
                </View>
              ))}
            </MenuOptions>
          </Menu>
        )}
        <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              paddingTop: 20 * SCALE_RATIO_WIDTH_BASIS,
              paddingBottom: 100 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <Accordion
              duration={500}
              underlayColor={'#fff'}
              activeSections={this.state.activeSections}
              expandMultiple
              sections={
                this.state.searchText === ''
                  ? this.props.listCollections
                  : this.state.searchedList
              }
              renderHeader={this.renderHeader}
              renderContent={this.renderContent}
              onChange={this.updateSections}
            />
          </View>
        </ScrollView>
        {/* showGroupOrder */}
        <Modal
          deviceWidth={DEVICE_WIDTH}
          deviceHeight={DEVICE_HEIGHT}
          onBackdropPress={() => this.setState({ showGroupOrder: false })}
          onSwipe={() => this.setState({ showGroupOrder: false })}
          swipeDirection={'down'}
          backdropOpacity={0.3}
          isVisible={this.state.showGroupOrder}
          style={[
            {
              width: DEVICE_WIDTH,
              height: DEVICE_HEIGHT,
              position: 'absolute',
              bottom: 0,
              margin: 0
            }
          ]}
        >
          <View
            style={[
              {
                // maxHeight: (DEVICE_HEIGHT * 90) / 100,
                minHeight: (DEVICE_HEIGHT * 30) / 100,
                borderTopLeftRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
                borderTopRightRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
                width: DEVICE_WIDTH,
                backgroundColor: '#fff',
                position: 'absolute',
                paddingTop: 10 * SCALE_RATIO_WIDTH_BASIS,
                paddingBottom: getBottomSpace(),
                bottom: 0,
                paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
          >
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                alignItems: 'center',
                paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
                marginBottom: 5 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <View style={{ flex: 1 }}>
                <MaterialCommunityIcons
                  onPress={() => this.setState({ showGroupOrder: false })}
                  name='arrow-down-circle'
                  size={18 * SCALE_RATIO_WIDTH_BASIS}
                  color='#808080'
                  style={{ textAlign: 'left' }}
                />
              </View>

              <View style={{ flex: 3 }}>
                <Text style={[style.titleHeader, { textAlign: 'center' }]}>
                  Đặt nhóm
                </Text>
              </View>
              <STouchableOpacity onPress={this.onShare} style={{ flex: 1 }}>
                <Text style={[style.text, { textAlign: 'right' }]}>
                  Gửi link
                </Text>
              </STouchableOpacity>
            </View>
            <TextInput
              style={[
                style.textCaption,
                {
                  borderRadius: 30 * SCALE_RATIO_WIDTH_BASIS,
                  backgroundColor: '#fbfbfb',
                  paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
                  paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
                  fontSize: FS(14),
                  fontFamily: 'FontAwesome'
                }
              ]}
              ref='searchBar'
              clearButtonMode='always'
              placeholder=' Tìm kiếm nguoi'
              placeholderTextColor='#00000050'
              underlineColorAndroid='transparent'
              selectionColor='#C7AE6D'
              autoCapitalize='none'
              autoCorrect={false}
              onChangeText={this.onSearchBarChangeText}
            />
            <ScrollView
              style={{
                maxHeight: (DEVICE_HEIGHT * 80) / 100,
                minHeight: (DEVICE_HEIGHT * 25) / 100
              }}
            >
              {DATA_TEST.map(item => (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 5 * SCALE_RATIO_WIDTH_BASIS,
                    borderBottomWidth: 0.2,
                    borderColor: '#70707020',
                    paddingVertical: 8 * SCALE_RATIO_WIDTH_BASIS
                  }}
                >
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center'
                    }}
                  >
                    <MyImage
                      source={{
                        uri: item.image
                      }}
                      size={40 * SCALE_RATIO_WIDTH_BASIS}
                      styleContent={{
                        alignSelf: 'center',
                        marginRight: 5 * SCALE_RATIO_WIDTH_BASIS
                      }}
                      borderDisabled
                    />
                    <Text style={[style.text]}>{item.name}</Text>
                  </View>
                  <STouchableOpacity
                    style={{
                      borderRadius: (33 * SCALE_RATIO_WIDTH_BASIS) / 2,
                      paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
                      paddingVertical: 2 * SCALE_RATIO_WIDTH_BASIS,
                      alignSelf: 'center',
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: '#7F9DD4'
                    }}
                    onPress={() => {
                      this.isGroupOrder = true;
                      console.log('poi flow groupOrder start groupOrder');
                      this.forceUpdate();
                    }}
                  >
                    <Text style={[style.text, { color: '#fff' }]}>Mời</Text>
                  </STouchableOpacity>
                </View>
              ))}
            </ScrollView>
          </View>
        </Modal>
        {/* <Modal
          backdropOpacity={0}
          deviceWidth={DEVICE_WIDTH}
          deviceHeight={DEVICE_HEIGHT}
          onBackdropPress={() => this.setState({ showMyCart: false })}
          onSwipe={() => this.setState({ showMyCart: false })}
          swipeDirection={'down'}
          swipeThreshold={20}
          isVisible={this.state.showMyCart}
          style={[
            {
              width: DEVICE_WIDTH,
              height: DEVICE_HEIGHT,
              position: 'absolute',
              bottom: 0,
              margin: 0
            }
          ]}
        >
          <View
            style={[
              {
                borderTopLeftRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
                borderTopRightRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
                width: DEVICE_WIDTH,
                backgroundColor: '#fff',
                position: 'absolute',
                paddingTop: 10 * SCALE_RATIO_WIDTH_BASIS,
                paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
                bottom: 100 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace(),
                paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
          >
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.2,
                borderColor: '#70707020',
                alignItems: 'center',
                paddingBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
                marginBottom: 5 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <View style={{ flex: 1 }}>
                <MaterialCommunityIcons
                  onPress={() => this.setState({ showMyCart: false })}
                  name="arrow-down-circle"
                  size={18 * SCALE_RATIO_WIDTH_BASIS}
                  color="#808080"
                  style={{ textAlign: 'left' }}
                />
              </View>
              <View style={{ flex: 3 }}>
                <Text style={[style.titleHeader, { textAlign: 'center' }]}>Giỏ hàng của tôi</Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={[style.text, { textAlign: 'right' }]}>Xóa tất cả</Text>
              </View>
            </View>
            {DATA_MILK_TEA.map((item, i) => {
              if (item.total === 0) return true;
              return (
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 5 * SCALE_RATIO_WIDTH_BASIS
                  }}
                >
                  <View style={{ maxWidth: '70%' }}>
                    <Text
                      style={[
                        style.titleHeader,
                        {
                          fontSize: FS(12),
                          color: '#707070',
                          textAlign: 'left'
                        }
                      ]}
                    >
                      {item.name}
                    </Text>
                    <Text
                      style={[
                        style.text,
                        {
                          fontSize: FS(10),
                          textAlign: 'left'
                        }
                      ]}
                    >
                      {item.fill}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={[style.textCaption, { fontSize: FS(10) }]}>{getPriceWithCommas(item.price)}</Text>
                      <Text
                        style={[
                          style.textCaption,
                          {
                            fontSize: FS(10),
                            marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS
                          }
                        ]}
                      >
                        x
                      </Text>
                      <Text style={[style.textCaption, { fontSize: FS(10) }]}>{item.total}</Text>
                      <Text
                        style={[
                          style.textCaption,
                          {
                            fontSize: FS(10),
                            marginHorizontal: 3 * SCALE_RATIO_WIDTH_BASIS
                          }
                        ]}
                      >
                        =
                      </Text>
                      <Text
                        style={[
                          style.textCaption,
                          {
                            fontSize: FS(10),
                            marginLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                            // textDecorationLine: 'line-through'
                          }
                        ]}
                      >
                        {getPriceWithCommas(item.price * item.total)}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}
                  >
                    {item.total > 0 ? (
                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={styles.productItemRightContainer}>
                          <TouchableOpacity onPress={() => (item.total -= 1)}>
                            <MaterialCommunityIcons
                              name="minus-circle"
                              size={24 * SCALE_RATIO_WIDTH_BASIS}
                              color="#7f9dd4"
                            />
                          </TouchableOpacity>
                        </View>
                        <Text
                          style={[
                            style.textHeader,
                            {
                              fontFamily: FONT.SemiBold,
                              alignSelf: 'center',
                              paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS
                            }
                          ]}
                        >
                          {item.total}
                        </Text>
                      </View>
                    ) : null}
                    <View style={styles.productItemRightContainer}>
                      <TouchableOpacity onPress={() => item.total + 1}>
                        <MaterialCommunityIcons
                          name="plus-circle"
                          size={24 * SCALE_RATIO_WIDTH_BASIS}
                          color="#7f9dd4"
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        </Modal> */}
        {/* {this.state.selectedItem.length >= 0 ? ( */}

        {this.state.showMyCart ? (
          this.isGroupOrder ? (
            <CartGroupDetailComponent
              onClose={() =>
                this.setState({ showMyCart: !this.state.showMyCart })
              }
              isGroupOwner={this.isGroupOwner}
            />
          ) : (
            <CartDetailComponent
              onClose={() =>
                this.setState({ showMyCart: !this.state.showMyCart })
              }
            />
          )
        ) : null}
        {this.state.showMyCart &&
        this.isGroupOrder &&
        !this.isGroupOwner ? null : (
          <STouchableOpacity
            activeOpacity={1}
            style={[style.shadow, styles.cartContainer]}
            onPress={() =>
              this.setState({ showMyCart: !this.state.showMyCart })
            }
            // onPress={() => PopupNotification.showComponentOnBottom(<PopupCartComponent item={this.props.item} />)}
          >
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image
                  source={require('../../../assets/imgs/icons/milkshake.png')}
                  resizeMode='contain'
                  style={{
                    width: 20 * SCALE_RATIO_WIDTH_BASIS,
                    alignSelf: 'center',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                />
                <Text
                  style={[
                    style.textHeader,
                    {
                      fontFamily: FONT.SemiBold,
                      marginHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS
                    }
                  ]}
                >
                  x
                </Text>
                <Text
                  style={[
                    style.textHeader,
                    { fontFamily: FONT.SemiBold, color: APP_COLOR }
                  ]}
                >
                  {getNumberWithCommas(this.state.selectedItem.length)}
                </Text>
                <Text
                  style={[
                    style.textHeader,
                    {
                      fontFamily: FONT.SemiBold,
                      marginHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS
                    }
                  ]}
                >
                  =
                </Text>
                <Text
                  style={[
                    style.textHeader,
                    { fontFamily: FONT.SemiBold, color: APP_COLOR }
                  ]}
                >
                  {getPriceWithCommas(this.totalCostWithDiscount)}{' '}
                </Text>
                {this.totalCostWithoutDiscount ? (
                  <Text
                    style={[
                      style.textHeader,
                      {
                        fontFamily: FONT.SemiBold,
                        color: '#A7A7A7',
                        textDecorationLine: 'line-through',
                        fontSize: FS(11)
                      }
                    ]}
                  >
                    {getPriceWithCommas(this.totalCostWithoutDiscount)}
                  </Text>
                ) : null}
              </View>

              <MyButton
                styleContent={{
                  height: 38 * SCALE_RATIO_WIDTH_BASIS,
                  alignSelf: 'center'
                }}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_KEY.CHECKOUT);
                  // if (this.props.token !== '' && this.props.userData && this.props.userData.id !== '') {
                  //   this.props.navigation.navigate(ROUTE_KEY.CHECKOUT);
                  // } else {
                  //   alert(strings.alert, strings.please_login, () => {
                  //     this.props.navigation.navigate(ROUTE_KEY.PRELOGIN, {
                  //       fromScreen: 'CHECKOUT'
                  //     });
                  //   });
                  // }
                }}
                textStyle={{
                  textAlign: 'center',
                  fontSize: FS(14)
                }}
                outline={this.totalCostWithDiscount === 0}
              >
                {this.isGroupOrder && !this.isGroupOwner
                  ? 'XONG'
                  : 'THANH TOÁN'}
              </MyButton>
            </View>
          </STouchableOpacity>
        )}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    listCollections: state.menu.listCollections
  };
}

const mapActionCreators = {
  loadListProduct,
  loadMoreListProduct
};

export default connect(
  mapStateToProps,
  mapActionCreators,
  null,
  { withRef: true }
)(MenuComponent);

const styles = StyleSheet.create({
  paymentButtonStyle: {
    width: 140 * SCALE_RATIO_WIDTH_BASIS,
    height: 40 * SCALE_RATIO_WIDTH_BASIS,
    backgroundColor: '#D3B574',
    borderRadius: 20 * SCALE_RATIO_WIDTH_BASIS,
    justifyContent: 'center',
    alignItems: 'center'
  },
  productItemRightContainer: {
    // paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
    justifyContent: 'center',
    alignItems: 'center'
  },
  productItemMiddleContainer: {
    paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  productContainer: {
    // marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
    // padding: 10 * SCALE_RATIO_WIDTH_BASIS,
    paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
    // overflow: 'hidden',
    // borderRadius: 5,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row'
  },
  paymentButtonContainer: {
    paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
    width: 120 * SCALE_RATIO_WIDTH_BASIS,
    justifyContent: 'center',
    alignItems: 'center'
  },
  contentContainer: {
    flex: 1,
    padding: 20 * SCALE_RATIO_WIDTH_BASIS,
    backgroundColor: '#fff'
  },
  cartContainer: {
    borderTopColor: '#70707030',
    borderTopWidth: 0.5,
    backgroundColor: '#ffffff',
    position: 'absolute',
    height: 60 * SCALE_RATIO_WIDTH_BASIS + getBottomSpace(),
    padding: 5 * SCALE_RATIO_WIDTH_BASIS,
    bottom: 0,
    paddingBottom: getBottomSpace(),
    width: DEVICE_WIDTH
  }
});
