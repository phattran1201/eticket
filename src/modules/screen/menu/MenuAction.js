import request from '../../../utils/request';
import {
  BASE_URL_TOCOTOCO,
  COLLECTIONS,
  THEMEID,
  UPDATE_LIST_COLLECTION,
  PRODUCT_API,
  PRODUCT,
  PRODUCTS,
  UPDATE_PRODUCT_INFO,
  UPDATE_PRODUCT_TOPPING,
  BASE_URL_FBS
} from '../../../constants/Constants';
import { mergerTwoArray } from '../../../utils/numberUtils';

const LIMIT = 10;
export const loadListCollection = (onDoneFunc = isSuccess => {}) => {
  // return (dispatch, store) => {
  //     // {{baseUrl}}/collections/all?view=list.json&themeid={{themeId}}
  //     // menu_screen
  //     const url = `${BASE_URL_TOCOTOCO}/${COLLECTIONS}/all?view=list.json&themeid=${THEMEID}`;
  //     request.get(url)
  //     .set('X-Tocotoco-Region', store().user.currentLocation.region)
  //     .finish((err, res) => {
  //         const response = JSON.parse(res && res.text ? res.text : undefined);
  //         console.log('poi 273 loadListCollection res', res);
  //         if (!err && response && response.list_collections) {
  //             dispatch({
  //                 type: UPDATE_LIST_COLLECTION,
  //                 payload: response.list_collections
  //             });
  //             onDoneFunc(true);
  //         } else {
  //             onDoneFunc(false);
  //         }
  //     });
  // };
};

export const loadListProduct = (collection, onDoneFunc = (isSuccess, newData) => {}) => {
    return (dispatch, store) => {
         // {{baseUrl}}/collections/1001604564/products?limit=10&offset=0&sort=price-asc
        // const url = `${BASE_URL_FBS}/${COLLECTIONS}/${collection.id}/products?limit=10&offset=0&sort=price-asc`;
        const url = `${BASE_URL_FBS}/${COLLECTIONS}/${collection.col_id}/${PRODUCTS}?limit=10&offset=0&sort=price-asc`;
        // console.log('poi 273 1 loadListProduct url', url);
        request.get(url)
        .set('X-Tocotoco-Region', store().user.currentLocation.region)
        .finish((err, res) => {
            // console.log('poi 273 1 loadListProduct res', res);
            if (!err && res && res.body && res.body.products) {
                const newUpdatedList = store().menu.listCollections.map(e => {
                    if (e.col_id === collection.col_id) {
                        return { ...e, products: [...res.body.products] };
                    }
                    return e;
                }); 
                dispatch({
                    type: UPDATE_LIST_COLLECTION,
                    payload: newUpdatedList
                });
                onDoneFunc(true, res.body.products);
            } else {
                onDoneFunc(false, []);
            }
          });
        };
  };

export const loadMoreListProduct = (collection, onDoneFunc = (isSuccess, newData) => {}) => (dispatch, store) => {
    // {{baseUrl}}/collections/:handle?view=detail.json&themeid={{themeId}}
    // {{baseUrl}}/collections/1001602784/products?limit=10&offset=0&sort=price-asc
    // const url = `${BASE_URL_TOCOTOCO}/${COLLECTIONS}/${collection.handle}?view=detail.json&themeid=${THEMEID}`;
    const url = `${BASE_URL_FBS}/${COLLECTIONS}/${collection.id}/products?limit=10&offset=${
      collection.offset
    }&sort=price-asc`;

    request
      .get(url)
      .set('X-Tocotoco-Region', store().user.currentLocation.region)
      .finish((err, res) => {
        if (!err && res && res.body && res.body.products) {
          const newUpdatedList = store().menu.listCollections.map(e => {
            if (e.id === collection.id) {
              // const tempProducts = [...e.products, ...res.body.products];
              const tempProducts = mergerTwoArray(e.products, res.body.products);
              return {
                ...e,
                products: tempProducts,
                offset: tempProducts.length,
                outOfData: res.body.products.length < 10
              };
            }
            return e;
          });
          dispatch({
            type: UPDATE_LIST_COLLECTION,
            payload: newUpdatedList
          });
          onDoneFunc(true);
        } else {
          onDoneFunc(false);
        }
      });
  };

export const getImagesAndDescriptionProductInfoData = (handle, onDoneFunc = (isSuccess) => {}) => (dispatch, store) => {
    const url = `${handle}?view=detail.json&themeid=${THEMEID}`;
    // const url = `${BASE_URL_TOCOTOCO}/${PRODUCTS}/${handle}?view=detail.json&themeid=${THEMEID}`;
    
    console.log('poi flow product getImagesAndDescriptionProductInfoData url:', url);
    request
      .get(url)
      .set('X-Tocotoco-Region', store().user.currentLocation.region)
      .finish((err, res) => {
        console.log('poi 283 getImagesAndDescriptionProductInfoData res', res);
        const response = JSON.parse(res && res.text ? res.text : undefined);
        if (!err && response && response.product) {
          const temp = {
            images: response.product.images ? response.product.images : [],
            description: response.product.description
          };
          dispatch({
            type: UPDATE_PRODUCT_INFO,
            payload: { ...store().menu.detailProductInfo, ...temp } //we will only get images and description
          });
          onDoneFunc(true);
        } else {
          onDoneFunc(false);
        }
      });
  };

export const getProductData = (id, onDoneFunc = (isSuccess) => {}) => (dispatch, store) => {
    const url = `${BASE_URL_FBS}/${PRODUCTS}/${id}/variants`;
    request
      .get(url)
      .set('X-Tocotoco-Region', store().user.currentLocation.region)
      .finish((err, res) => {
        console.log('poi flow product getProductData detail res:', res);
        
        if (!err && res && res.body && res.body.variants) {
          const variants = res.body.variants;
          dispatch({
            type: UPDATE_PRODUCT_INFO,
            payload: { ...store().menu.detailProductInfo, id, variants }
          });
          onDoneFunc(true);
        } else {
          onDoneFunc(false);
        }
      });
  };
