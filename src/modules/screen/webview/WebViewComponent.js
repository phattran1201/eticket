import React from 'react';
import { Share, StyleSheet, Text, View } from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import { connect } from 'react-redux';
import { BASE_URL_TOCOTOCO, DEVICE_WIDTH, FS, SCALE_RATIO_WIDTH_BASIS } from '../../../constants/Constants';
import style from '../../../constants/style';
import { alert } from '../../../utils/alert';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';

class WebViewComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      nickname: '',
      age: '',
      introduce: '',
      value: 0,
      loading: true
    };
  }
  componentDidMount() {
    // MySpinner.show();
  }

  onShare = async (name, url, html) => {
    try {
      const result = await Share.share({
        message: `${name} - ${url}`
        // url: uri !== '' ? `${uri}` : `${html}`,
        // title: name
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    const { navigation } = this.props;
    const { params } = this.props.navigation.state;
    const name = params && params.name ? params.name : '';
    const html = params && params.html ? params.html : '';
    const uri = params && params.url ? BASE_URL_TOCOTOCO + params.url : '';
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <BaseHeader
          styleContent={{ backgroundColor: '#fff' }}
          children={
            <Text style={[style.titleHeader, { marginHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS }]} numberOfLines={1}>
              {name}
            </Text>
          }
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
          rightIcon="share-alt"
          rightIconType="SimpleLineIcons"
          rightIconStyle={{ width: FS(16) }}
          onRightPress={() => this.onShare(name, uri, html)}
        />
        <AutoHeightWebView
          onLoad={() => {
            this.setState({ loading: false });
          }}
          style={{ width: DEVICE_WIDTH }}
          source={{ html }}
        />
        {/* <MyButton
          style={{
            width: 300 * SCALE_RATIO_WIDTH_BASIS,
            height: 33,
            marginHorizontal: 30,
            position: 'absolute',
            zIndex: 2,
            bottom: 5 + getBottomSpace(),
            alignSelf: 'center'
          }}
        >
          Đặt hàng ngày
        </MyButton> */}
      </View>
    );
  }
}

const mapActionCreators = {};

export default connect(
  null,
  mapActionCreators
)(WebViewComponent);

const styles = StyleSheet.create({});
