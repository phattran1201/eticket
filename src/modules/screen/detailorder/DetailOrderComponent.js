import React from 'react';
import {
  Dimensions,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  Platform,
  FlatList,
  Image,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import {
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  DEVICE_HEIGHT,
  SOFT_MENU_BAR_HEIGHT,
  DEVICE_WIDTH,
  ROUTE_KEY
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import MyComponent from '../../view/MyComponent';
import { alert } from '../../../utils/alert';
import MySpinner from '../../view/MySpinner';
import style, { APP_COLOR, FONT, APP_COLOR_TEXT } from '../../../constants/style';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import Dropdown from '../../view/MyDropDown/dropdown';
import { CheckBox } from 'react-native-elements'
import MapView, { Marker, PROVIDER_GOOGLE, Circle } from 'react-native-maps';
import BaseHeader from '../../view/BaseHeader';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const markerIcon = require('../../..//assets/imgs/icons/logo.png');

class DetailOrderComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      store: 'TocoToco - 12 Thái Hà',
      offline: true,
      online: false,
      selectedStore: 0,
      store: {
        id: 0,
        name: 'TocoToco Nguyễn Tri Phương',
        coordinate: {
          latitude: 10.7629044,
          longitude: 106.6334002
        },
        region: {
          latitude: 10.7629044,
          longitude: 106.6334002,
          latitudeDelta: 0.00922,
          longitudeDelta: 0.00421,
        },
        image: 'https://static.vietnammm.com/images/restaurants/vn/NPPPQ77/products/machiatohongtra.png'
      },
      dataOrder: [
        {
          id: 1,
          name: 'Trà sữa Panda',
          image: 'http://tocotocotea.com/wp-content/uploads/2018/03/panda-1.jpg',
          detail:
            ' 30% đường, 30% đá, trân châu trắng 30% đường, 30% đá, trân châu trắng 30% đường, 30% đá, trân châu trắng',
          price: 30000,
          amount: 2
        },
        {
          id: 2,
          name: 'Trà sữa Panda',
          image: 'http://tocotocotea.com/wp-content/uploads/2018/03/panda-1.jpg',
          detail: '30% đường, 30% đá, trân châu trắng',
          price: 30000,
          amount: 2
        },
        {
          id: 3,
          name: 'Trà sữa Panda',
          image: 'http://tocotocotea.com/wp-content/uploads/2018/03/panda-1.jpg',
          detail: '30% đường, 30% đá, trân châu trắng',
          price: 30000,
          amount: 2
        },
        {
          id: 4,
          name: 'Trà sữa Panda',
          image: 'http://tocotocotea.com/wp-content/uploads/2018/03/panda-1.jpg',
          detail: '30% đường, 30% đá, trân châu trắng',
          price: 30000,
          amount: 2
        },
        {
          id: 5,
          name: 'Trà sữa Panda',
          image: 'http://tocotocotea.com/wp-content/uploads/2018/03/panda-1.jpg',
          detail: '30% đường, 30% đá, trân châu trắng',
          price: 30000,
          amount: 2
        }
      ],
    };
  }

  renderListGift = ({ item, index }) => (
    <View
      style={{
        flexDirection: 'row',
        paddingVertical: 10,
        alignItems: 'center',
        borderBottomColor: '#EFEFEF',
        borderBottomWidth: 1
      }}
    >
      {/* <CheckBox
        center
        containerStyle={{ margin: 0, padding: 0 }}
        checkedColor={APP_COLOR}
        checked={index % 2 ? this.state.offline : !this.state.offline}
        checkedIcon="circle"
        uncheckedIcon="circle-o"
        onPress={() => this.setState({ offline: !this.state.offline })}
      /> */}
      <Image
        source={{ uri: item.image }}
        style={{
          marginLeft: -10 * SCALE_RATIO_WIDTH_BASIS,
          height: 60 * SCALE_RATIO_HEIGHT_BASIS,
          width: 60 * SCALE_RATIO_HEIGHT_BASIS
        }}
      />
      <View
        style={{
          width: DEVICE_WIDTH - 80 * SCALE_RATIO_HEIGHT_BASIS
        }}
      >
        <Text
          style={[
            style.text,
            {
              fontSize: FS(12),
              fontFamily: FONT.SemiBold
            }
          ]}
        >
          {item.name}
        </Text>
      </View>
    </View>
  );


  renderListOrder = ({ item, index }) => (
    <View style={{ flexDirection: 'row', height: 85 * SCALE_RATIO_HEIGHT_BASIS, alignItems: 'center', borderBottomColor: '#EFEFEF', borderBottomWidth: 1 * SCALE_RATIO_HEIGHT_BASIS }}>
      <Image source={{ uri: item.image }} style={{ height: 80 * SCALE_RATIO_HEIGHT_BASIS, width: 60 * SCALE_RATIO_HEIGHT_BASIS }} />
      <View style={{ marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
        <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12), color: '#000', fontWeight: '500' }}>{item.name}</Text>
        <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12), maxWidth: 220 * SCALE_RATIO_WIDTH_BASIS }}>{item.fill}</Text>
        <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12), color: APP_COLOR }}>{item.price}đ <Text style={{ fontSize: FS(12) }}>/ cốc</Text></Text>
        <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12) }}>Số lượng: {item.sl} cốc</Text>
      </View>
    </View>
  )
  render() {
    const { navigation } = this.props;
    const orderCode = navigation.getParam('orderCode');
    const listProduct = navigation.getParam('listProduct');
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <BaseHeader
          children={<Text style={style.titleHeader}>ĐƠN HÀNG {orderCode}</Text>}
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
        />
        <ScrollView style={styles.container}>

          <View style={[styles.viewBorder]}>
            <View style={{ flexDirection: 'row', paddingBottom: 2 * SCALE_RATIO_HEIGHT_BASIS, justifyContent: 'space-between', borderBottomColor: '#f0f0f0', borderBottomWidth: 0.5 * SCALE_RATIO_HEIGHT_BASIS }}>
              <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold }}>Mã đơn hàng: <Text style={{ color: APP_COLOR }}>{orderCode}</Text></Text>
              <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold }}>Giao hàng tận nơi</Text>
            </View>
            <View style={{ flexDirection: 'row', paddingTop: 10 * SCALE_RATIO_HEIGHT_BASIS, marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Icons name={'check-circle'} color={APP_COLOR} size={FS(16)} />
                <View style={{ width: 0.5 * SCALE_RATIO_WIDTH_BASIS, height: 30 * SCALE_RATIO_HEIGHT_BASIS, backgroundColor: APP_COLOR }} />
              </View>
              <View style={{ flex: 7 }}>
                <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold, color: '#000' }}>Đặt hàng thành công</Text>
                <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold, marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>08:30 SA 15/02/2019</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Icons name={'check-circle'} color={APP_COLOR} size={FS(16)} />
                <View style={{ width: 0.5 * SCALE_RATIO_WIDTH_BASIS, height: 30 * SCALE_RATIO_HEIGHT_BASIS, backgroundColor: APP_COLOR }} />
              </View>
              <View style={{ flex: 7 }}>
                <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold, color: '#000' }}>Xác nhận đơn hàng thành công</Text>
                <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold, marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>08:35 SA 15/02/2019</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Icons name={'check-circle'} color={APP_COLOR} size={FS(16)} />
                <View style={{ width: 0.5 * SCALE_RATIO_WIDTH_BASIS, height: 30 * SCALE_RATIO_HEIGHT_BASIS, backgroundColor: APP_COLOR }} />
              </View>
              <View style={{ flex: 7 }}>
                <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold, color: '#000' }}>Chuẩn bị đồ xong</Text>
                <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold, marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>08:50 SA 15/02/2019</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Icons name={'check-circle'} color={APP_COLOR} size={FS(16)} />
                <View style={{ width: 0.5 * SCALE_RATIO_WIDTH_BASIS, height: 50 * SCALE_RATIO_HEIGHT_BASIS, backgroundColor: APP_COLOR }} />
              </View>
              <View style={{ flex: 7 }}>
                <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold, color: '#000' }}>Shipper đã tới nhận đồ</Text>
                <View style={{ marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>
                  <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold }}>08:52 SA 15/02/2019</Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Image
                      style={{ height: 40 * SCALE_RATIO_WIDTH_BASIS, width: 40 * SCALE_RATIO_WIDTH_BASIS, borderRadius: 25 * SCALE_RATIO_WIDTH_BASIS }}
                      source={{ uri: 'https://cdn.pose.com.vn/assets/2018/08/1535086308-381-chi-em-phat-hon-voi-my-nam-so-huu-lan-da-dep-nhat-han-quoc-1-1535081318-width500height750.jpg' }}
                    />

                    <View style={{ marginLeft: 3 * SCALE_RATIO_WIDTH_BASIS, justifyContent: 'center' }}>
                      <Text style={{ fontSize: FS(10), fontFamily: FONT.SemiBold }}>
                        Nguyễn Văn An
                      </Text>
                      <Text style={{ fontSize: FS(10), fontFamily: FONT.SemiBold }}>
                        SĐT: 0123456789 - Biển số xe: 29HN-90434
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View style={{ flexDirection: 'row', paddingBottom: 10 * SCALE_RATIO_HEIGHT_BASIS, marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Icons name={'check-circle'} color={APP_COLOR} size={FS(16)} />
              </View>
              <View style={{ flex: 7 }}>
                <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold, color: '#000' }}>Đang giao hàng</Text>
                <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold, marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>08:54 SA 15/02/2019</Text>
              </View>
            </View>

          </View>

          <View style={[styles.viewBorder, { minHeight: 160 * SCALE_RATIO_HEIGHT_BASIS, maxHeight: 340 * SCALE_RATIO_HEIGHT_BASIS }]}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Icon name='store' size={FS(16)} color='#000' />
              <Text style={{
                marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS, fontSize: FS(12),
                fontFamily: FONT.SemiBold,
                color: '#000'
              }}>ToCoToCo 604 Trường Chinh</Text>
            </View>
            <FlatList
              data={listProduct}
              keyExtractor={item => item.id}
              renderItem={this.renderListOrder}
              style={{ minHeight: 85 * SCALE_RATIO_HEIGHT_BASIS, maxHeight: 255 * SCALE_RATIO_HEIGHT_BASIS }}
            />
            <View style={styles.line}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingBottom: 5
              }}
            >
              <MaterialCommunityIcons name="gift" size={FS(16)} color={APP_COLOR_TEXT} />
              <Text
                style={[
                  {
                    color: APP_COLOR_TEXT,
                    fontWeight: '500',
                    fontSize: FS(12),
                    fontFamily: FONT.SemiBold,
                    paddingLeft: 3 * SCALE_RATIO_WIDTH_BASIS
                  }
                ]}
                numberOfLines={1}
              >
                Quà tặng
              </Text>
            </View>
            {/* <Text style={style.text}>Hãy lựa chọn một trong các phần quà từ ToCoToCo dành cho bạn:</Text> */}
          </View>
          <FlatList data={this.state.dataOrder} keyExtractor={item => item.id} renderItem={this.renderListGift} />

            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12) }}>Số lượng: <Text style={{ color: '#000' }}>5</Text></Text>
              </View>
              <View style={{ flex: 3 }}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12), alignSelf: 'flex-end' }}>Tổng:</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12), alignSelf: 'flex-end' }}>219,000đ</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12), alignSelf: 'flex-end' }}>Phí vận chuyển:</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12), alignSelf: 'flex-end' }}>20,000đ</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12), alignSelf: 'flex-end' }}>Khuyến mãi:</Text>
                    <View style={{ flexDirection: 'row', alignSelf: 'flex-end' }}>
                      <Icons name='tag' size={FS(12)} color={APP_COLOR} />
                      <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(10), marginRight: 2 * SCALE_RATIO_WIDTH_BASIS }}>HOANGDEPTRAI</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12), alignSelf: 'flex-end' }}>-30,000đ</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12), alignSelf: 'flex-end' }}>Tổng cộng:</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12), alignSelf: 'flex-end' }}>209,000đ</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.viewBorder}>
            <Text style={{ fontFamily: FONT.SemiBold, fontSize: FS(12) }}>Ghi chú</Text>
          </View>

          <View style={[styles.viewBorder]}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 2 }}>
                <MapView
                  style={{ flex: 1 }}
                  provider={PROVIDER_GOOGLE}
                  region={this.state.store.region}
                >
                  <Marker coordinate={this.state.store.coordinate}>
                    <Image style={styles.markerImageStyle} source={{ uri: this.state.store.image }} resizeMode='cover' />
                  </Marker>
                </MapView>
              </View>
              <View style={{ flex: 5, marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text style={{ fontSize: FS(12), fontFamily: FONT.SemiBold }}>Giao đến</Text>
                </View>
                <View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name='person' size={FS(16)} color={'#000'} />
                    <Text style={{
                      marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
                      fontSize: FS(12),
                      fontFamily: FONT.SemiBold,
                      color: '#000',
                      fontWeight: '500'
                    }}
                    >Bùi Anh Khôi</Text>
                  </View>

                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name='call' size={FS(16)} color={'#000'} />
                    <Text style={{
                      marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS, fontSize: FS(12),
                      fontFamily: FONT.SemiBold,
                    }}>0916587543</Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icons name='map-marker-outline' size={FS(16)} color={'#000'} />
                    <Text
                      numberOfLines={2}
                      style={{
                        marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS, fontSize: FS(12),
                        fontFamily: FONT.SemiBold,
                      }}>170 Đê La Thành, Ô Chợ Dừa, Đống Đa, Hầ Nội</Text>
                  </View>
                </View>
              </View>
            </View>
            <Text style={{
              marginTop: 3 * SCALE_RATIO_HEIGHT_BASIS,
              fontSize: FS(12),
              fontFamily: FONT.SemiBold,
            }}>
              Giao hàng 18:00 - hôm nay 07/03/2019
            </Text>
          </View>
        </ScrollView >
      </View>
    );
  }
}

const mapActionCreators = {
};
const mapStateToProps = state => ({
});
export default connect(
  mapStateToProps,
  mapActionCreators
)(DetailOrderComponent);
const styles = StyleSheet.create({
  container: {
    // flex: 1,
    paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
    paddingBottom: 20 * SCALE_RATIO_HEIGHT_BASIS
  },
  markerImageStyle: {
    width: 20 * SCALE_RATIO_WIDTH_BASIS,
    height: 20 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 10 * SCALE_RATIO_WIDTH_BASIS
  },
  viewBorder: {
    borderColor: Platform.OS === 'ios' ? '#C7AE6D30' : '#70707010',
    borderWidth: 1,
    backgroundColor: '#fff',
    borderRadius: 5 * SCALE_RATIO_HEIGHT_BASIS,
    paddingVertical: 5 * SCALE_RATIO_HEIGHT_BASIS,
    paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
    marginVertical: 10 * SCALE_RATIO_HEIGHT_BASIS,
    shadowColor: APP_COLOR,
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 3
  },
  dropdownContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
})