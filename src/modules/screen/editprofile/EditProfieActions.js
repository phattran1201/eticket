import firebase from 'react-native-firebase';
import moment from 'moment';
import { GoogleSignin } from 'react-native-google-signin';
import RNKakaoLogins from 'react-native-kakao-logins';
import { LoginManager } from 'react-native-fbsdk';
import {
  BASE_URL,
  CLEAR_DATA,
  SET_LOGGED_IN,
  UPDATE_CURRENT_USER_DATA,
  UPDATE_GENDER_FILTER
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import { alert } from '../../../utils/alert';
import { closeSocketConnection } from '../../../utils/chatManager';
import request from '../../../utils/request';
import MySpinner from '../../view/MySpinner';
import { setLastTimeAttendanceCheck , setUserIdentity} from '../../../utils/asyncStorage';
import { loadUserProfile } from '../splash/SplashActions';
import { listGender } from '../../../config/redux/reducers/filter/genderFilterReducer';


export function checkBlockSystem(imei, last_ip, onDoneFunc = () => { }) {
  return (dispatch, store) => {
    request
      .post(`${BASE_URL}user/check_system_blocked`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `bearer ${store().user.token}`)
      .send({ imei, last_ip })
      .finish(
        (err, res) => {
          if (!err) {
            if (res.body.results.object.code === 201) {
              dispatch({
                type: SET_LOGGED_IN,
                payload: false
              });
              dispatch({
                type: CLEAR_DATA
              })
              onDoneFunc();
            }
          }
        }
      )
  }
}

export function updateUserInfo(userInfo, onDoneFun = () => { }) {
  return (dispatch, store) => {
    const userInfoToSend = { ...userInfo };
    delete userInfoToSend.cash;
    delete userInfoToSend.heart;
    request
      .put(`${BASE_URL}user/${store().user.userData.id}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `bearer ${store().user.token}`)
      .send(userInfoToSend)
      .finish((err, res) => {
        if (err) {
          MySpinner.hide();
          console.log('Hoang log Error edit userInfo', err);
          alert(strings.alert, strings.alert_edit_info_fail);
        } else {
          dispatch({
            type: UPDATE_CURRENT_USER_DATA,
            payload: userInfo
          });
          setUserIdentity({ token: store().user.token, userData: userInfo });
          let genderFilter = listGender[0];
          if (userInfo.sex === 'OTHER') {
            genderFilter = listGender[2];
          } if (userInfo.sex === 'MALE') {
            genderFilter = listGender[1];
          }

          dispatch({
            type: UPDATE_GENDER_FILTER,
            payload: genderFilter
          });
          onDoneFun();
        }
      });
  };
}

export function deleteUser(onDoneFunc) {
  return (dispatch, store) => {
    MySpinner.show();
    request
      .delete(`${BASE_URL}/user/delete_account`)
      .set('Authorization', `bearer ${store().user.token}`)
      .finish(async (err, res) => {
        if (err) {
          MySpinner.hide();
          console.log('Hoang log Error delete user', err);
          alert(strings.alert, strings.alert_edit_info_fail);
        } else {
          const loginType = store().user.userData.login_type;
          firebase.messaging().unsubscribeFromTopic(store().user.userData.id);
          dispatch({
            type: SET_LOGGED_IN,
            payload: false
          });
          setTimeout(() => {
            closeSocketConnection();
            dispatch({
              type: CLEAR_DATA
            });
            onDoneFunc();
          }, 200);
          console.log('hinodi loginType', loginType);
          if (loginType === 'FACEBOOK') {
            LoginManager.logOut();
          } else if (loginType === 'GOOGLE') {
            await GoogleSignin.configure({
              iosClientId: '434659415477-8goq0p4i1884a0os7k1h7kvbo259suks.apps.googleusercontent.com', // only for iOS
            });
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
          } else {
            RNKakaoLogins.logout((err2, result) => {
              console.log('hinodi logout kakao', err2, result);
            });
          }
        }
      });
  };
}

export const attendanceCheck = (user_id, onDoneFunc) => (dispatch, store) => {
  MySpinner.show();
  request
    .post(`${BASE_URL}/attendance_check`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `Bearer ${store().user.token}`)
    .send({ user_id })
    .finish((err, res) => {
      if (err) {
        MySpinner.hide();
        console.log('Hoang log err attendace check', err);
        alert(strings.alert, err.message);
      } else {
        onDoneFunc();
        loadUserProfile(store, dispatch);
        console.log('Hoang log res attendance check', res);
      }
    });
};

export const getListAttendanceCheck = (onDoneFunc, checked) => (dispatch, store) => {
  request
    .get(
      `${BASE_URL}attendance_check?fields=["$all"]&filter={"user_id":"${store().user.userData.id}"}`
    )
    .set('Authorization', `Bearer ${store().user.token}`)
    .finish((err, res) => {
      if (err) {
        console.log('Hoang log err getListAttendanceCheck', err);
      }
      else if (
        res.body && res.body.results &&
        res.body.results.objects &&
        res.body.results.objects.count > 0 &&
        res.body.results.objects.rows.length > 0 &&
        moment.utc(res.body.results.objects.rows[0].created_at).format('DD-MM-YYYY') === moment().format('DD-MM-YYYY')) {
        checked();
      } else {
        onDoneFunc();
      }
    });
};

export const uploadImage = (listImages, token) =>
  new Promise((resolve, reject) => {
    MySpinner.show();
    console.log('Hoang log 1', listImages);
    const dataImage = [];
    if (listImages.length === 0) resolve([]);
    let index = -1;
    listImages.forEach(image => {
      if (image.uri.slice(0, 4) === 'http') {
        dataImage.push(image.uri);
        index++;
        if (index === listImages.length - 1) {
          resolve(dataImage);
        }
      } else {
        const body = new FormData();
        const indexOfDot = image.uri.lastIndexOf('.');
        const fileName = image.uri.slice(indexOfDot - 1, image.uri.length);
        body.append('image', {
          uri: image.uri,
          name: image.fileName ? image.fileName : fileName,
          type: 'multipart/form-data'
        });
        console.log('Hoang log body', body);
        fetch(`${BASE_URL}/image/upload`, {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${token}`
          },
          body
        })
          .then(res => res.json())
          .then(res => {
            console.log('Hoang log res upload Image', res);
            dataImage.push(res.results.object.url);
          })
          .catch(e => reject(e))
          .done(() => {
            index++;
            if (index === listImages.length - 1) {
              resolve(dataImage);
            }
          });
      }
    });
  });
