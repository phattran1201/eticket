import React from 'react';
import { Dimensions, Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import { FS, ROUTE_KEY, SCALE_RATIO_HEIGHT_BASIS, SCALE_RATIO_WIDTH_BASIS } from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import MyButton from '../../../style/MyButton';
import getGenderImage from '../../../utils/getGenderImage';
import MyComponent from '../../view/MyComponent';
import PopupNotification from '../../view/PopupNotification';
import PopupNotificationEditProfileImageComponent from '../../view/PopupNotificationEditProfileImageComponent';
import PopupNotificationEditProfileTextComponent from '../../view/PopupNotificationEditProfileTextComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import { updateUserInfo } from '../editprofile/EditProfieActions';
import { updatePhoneNumber } from '../settings/SettingsActions';

const hashtag = require('../../../assets/imgs/hashtag.png');

const { width } = Dimensions.get('window');

class EditProfileComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    console.log('Hoang log userData from Redux', this.props.userData);
  }
  replaceSource(source = '') {
    if (typeof source !== 'string') return '';
    return source
      ? source.replace('localhost', 'hitek.com.vn').replace('toi.innoway.info', 'hitek.com.vn')
      : '';
  }
  render() {
    return (
      <View style={styles.container}>
        <Feather
          onPress={() => {
            console.log('bambi ne');
            this.props.navigation.goBack();
          }}
          name="arrow-left"
          size={FS(20)}
          color="#C7AE6D"
          style={
            (style.shadow,
              {
                position: 'absolute',
                top: 16.5 * SCALE_RATIO_HEIGHT_BASIS + getStatusBarHeight(true),
                left: 16 * SCALE_RATIO_WIDTH_BASIS,
                zIndex: 99
              })
          }
        />
        <ScrollView style={{ flex: 1 }}>
          <View>
            <View
              style={{
                position: 'absolute',
                bottom: 16 * SCALE_RATIO_HEIGHT_BASIS,
                right: 12 * SCALE_RATIO_WIDTH_BASIS,
                zIndex: 2
              }}
            >
              <MyButton
                styleContent={{
                  height: 22 * SCALE_RATIO_HEIGHT_BASIS,
                  borderRadius: (22 * SCALE_RATIO_HEIGHT_BASIS) / 2,
                  paddingVertical: 5 * SCALE_RATIO_HEIGHT_BASIS,
                  paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS
                }}
                onPress={() => {
                  PopupNotification.showComponent(<PopupNotificationEditProfileImageComponent />);
                }}
                textStyle={{ fontSize: FS(8) }}
              >
                {strings.set_picture_profile}
              </MyButton>
            </View>
            <View
              style={
                (style.shadow,
                  {
                    borderBottomWidth: 2,
                    borderColor: '#C7AE6D',
                    marginBottom: 8 * SCALE_RATIO_HEIGHT_BASIS
                  })
              }
            >
              <FastImage
                style={styles.avatarImage}
                removeClippedSubviews
                source={
                  this.props.userData && this.props.userData.avatar
                    ? { uri: this.replaceSource(this.props.userData.avatar) }
                    : require('../../../assets/imgs/default_avatar.jpg')
                }
                defaultSource={null}
              />
            </View>
          </View>

          <View style={styles.infoContainer}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text numberOfLines={2} style={[style.textNormal, styles.textInfo]}>
                {this.props.userData && this.props.userData.nickname
                  ? this.props.userData.nickname
                  : ''}
              </Text>
              <STouchableOpacity
                style={{ padding: 5 * SCALE_RATIO_HEIGHT_BASIS }}
                onPress={() => {
                  PopupNotification.showComponent(
                    <PopupNotificationEditProfileTextComponent
                      title={strings.edit_nickname}
                      placeHolder={strings.nickname}
                      description={strings.edit_nickname_description}
                      icon="user"
                    />
                  );
                }}
              >
                <Feather name="edit" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
              </STouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 5 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              {getGenderImage(this.props.userData.sex)}
              <Text style={[style.textNormal, { fontSize: FS(15) }]}>
                {this.props.userData && this.props.userData.age ? this.props.userData.age : ''}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginLeft: 13 * SCALE_RATIO_WIDTH_BASIS
                }}
              >
                {[0, 1, 2, 3, 4].map(e => (
                  <MaterialIcons
                    key={e.toString()}
                    name={this.props.userData.rating > e ? 'star' : 'star-border'}
                    size={16 * SCALE_RATIO_WIDTH_BASIS}
                    color="#C7AE6D"
                    style={{ marginRight: 1 * SCALE_RATIO_WIDTH_BASIS }}
                  />
                ))}
              </View>
              <STouchableOpacity
                style={{ padding: 5 * SCALE_RATIO_HEIGHT_BASIS, justifyContent: 'flex-end' }}
                onPress={() =>
                  PopupNotification.showComponent(
                    <PopupNotificationEditProfileTextComponent
                      title={strings.edit_age}
                      placeHolder={strings.age}
                      description={strings.edit_age_description}
                      icon="info"
                    />
                  )
                }
              >
                <Feather name="edit" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
              </STouchableOpacity>
            </View>
            <View style={styles.moreDetailWrapper}>
              <View style={styles.moreDetailContainer}>
                <Feather name="info" size={23 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
                <Text
                  style={[
                    style.text,
                    {
                      color: '#282828',
                      marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
                      maxWidth: 200 * SCALE_RATIO_WIDTH_BASIS
                    }
                  ]}
                  numberOfLines={2}
                >
                  {this.props.userData && this.props.userData.introduction
                    ? this.props.userData.introduction
                    : ''}
                </Text>
              </View>
              <STouchableOpacity
                style={{ padding: 5 * SCALE_RATIO_HEIGHT_BASIS }}
                onPress={() =>
                  PopupNotification.showComponent(
                    <PopupNotificationEditProfileTextComponent
                      title={strings.edit_self_introduction}
                      placeHolder={strings.self_introduction}
                      description={strings.edit_self_introduction_description}
                      icon="info"
                    />
                  )
                }
              >
                <Feather name="edit" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
              </STouchableOpacity>
            </View>
            <View style={styles.moreDetailWrapper}>
              <View style={styles.moreDetailContainer}>
                <Feather name="phone" size={23 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
                <Text
                  style={[
                    style.text,
                    {
                      color: '#282828',
                      marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
                      maxWidth: 200 * SCALE_RATIO_WIDTH_BASIS
                    }
                  ]}
                  numberOfLines={2}
                >
                  {this.props.userData && this.props.userData.phone
                    ? this.props.userData.phone
                    : strings.press_here_to_update_phone}
                </Text>
              </View>
              <STouchableOpacity
                style={{ padding: 5 * SCALE_RATIO_HEIGHT_BASIS }}
                onPress={() =>
                  this.props.updatePhoneNumber(() => {
                  })
                }
              >
                <Feather name="edit" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
              </STouchableOpacity>
            </View>
            <View style={styles.moreDetailWrapper}>
              <View style={styles.moreDetailContainer}>
                <Feather name="map-pin" size={23 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
                <Text
                  style={[
                    style.text,
                    {
                      color: '#282828',
                      // width: 250 * SCALE_RATIO_WIDTH_BASIS,
                      marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
                      maxWidth: 200 * SCALE_RATIO_WIDTH_BASIS
                    }
                  ]}
                  numberOfLines={2}
                >
                  {this.props.userData && this.props.userData.address
                    ? this.props.userData.address
                    : ''}
                </Text>
              </View>
              <STouchableOpacity
                style={{ padding: 5 * SCALE_RATIO_HEIGHT_BASIS }}
                onPress={() =>
                  PopupNotification.showComponent(
                    <PopupNotificationEditProfileTextComponent
                      title={strings.edit_address}
                      placeHolder={strings.address}
                      description={strings.edit_address_description}
                      icon="info"
                    />
                  )
                }
              >
                <Feather name="edit" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
              </STouchableOpacity>
            </View>
            <View style={styles.moreDetailWrapper}>
              <View style={styles.moreDetailContainer}>
                <Image
                  source={hashtag}
                  style={{
                    width: 22 * SCALE_RATIO_WIDTH_BASIS,
                    height: 22 * SCALE_RATIO_WIDTH_BASIS
                  }}
                />
                <Text
                  style={[
                    style.text,
                    {
                      color: '#282828',
                      width: 250 * SCALE_RATIO_WIDTH_BASIS,
                      marginLeft: 5 * SCALE_RATIO_WIDTH_BASIS
                    }
                  ]}
                  numberOfLines={2}
                >
                  {this.props.userData && this.props.userData.interests
                    ? this.props.userData.interests
                    : ''}
                </Text>
              </View>
              <STouchableOpacity
                style={{ padding: 5 * SCALE_RATIO_HEIGHT_BASIS }}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_KEY.FAVOURITE_TOPIC_COMPONENT);
                }}
              >
                <Feather name="edit" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
              </STouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View >
    );
  }
}

const mapStateToProps = state => ({
  userData: state.user.userData
});

const mapActionCreators = {
  updateUserInfo,
  updatePhoneNumber
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(EditProfileComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  avatarImage: {
    width,
    height: width
  },
  infoContainer: {
    paddingHorizontal: 31 * SCALE_RATIO_WIDTH_BASIS
    // paddingVertical: 18 * SCALE_RATIO_WIDTH_BASIS
  },
  flagInfoImage: {
    width: 37.38 * SCALE_RATIO_WIDTH_BASIS,
    height: 24.92 * SCALE_RATIO_WIDTH_BASIS,
    borderRadius: 4 * SCALE_RATIO_WIDTH_BASIS,
    marginLeft: 25.92 * SCALE_RATIO_WIDTH_BASIS
  },
  textInfo: {
    fontSize: FS(20),
    maxWidth: 200 * SCALE_RATIO_WIDTH_BASIS
  },
  moreDetailWrapper: {
    marginTop: 10 * SCALE_RATIO_HEIGHT_BASIS,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  moreDetailContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  moreDetailText: {
    fontSize: FS(18),
    color: '#282828',
    marginLeft: 11 * SCALE_RATIO_WIDTH_BASIS
  }
});
