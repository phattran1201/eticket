import React from 'react';
import { Alert, AppState, BackHandler, NativeModules, Platform, Vibration, View, AsyncStorage } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import AndroidOpenSettings from 'react-native-android-open-settings';
import firebase from 'react-native-firebase';
// import VoipPushNotification from 'react-native-voip-push-notification';
import { connect } from 'react-redux';
import { persistStore } from 'redux-persist';
import store from '../../../config/redux/store';
import {
  FCM_CHAT_ACTION,
  FCM_MISSED_CALL,
  FCM_RESPONSE_PAYMENT_FAILED_ACTION,
  FCM_RESPONSE_PAYMENT_SUCCESS_ACTION,
  MIN_HEART_TO_MAKE_A_PHONE_CALL,
  ROUTE_KEY
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import { getListDataFiltered } from '../../../utils/chatManager';
import FCMSubscriber from '../../../utils/fcmSubscriber';
import global from '../../../utils/globalUtils';
import MyComponent from '../../view/MyComponent';
import PopupNotification from '../../view/PopupNotification';
import PopupNotificationCallComponent from '../../view/PopupNotificationCallComponent';
import { loadConversationById } from '../detailMessage/ListMessageAction';
import { clearConnectionState } from '../message/ListMessageAction';
import { loadAllData, loadUserById, shortenListData } from './SplashActions';
import { getLocation, loadLocation, loadListSettingData } from '../main/MainActions';
import { loadListStoreNearByUser } from '../store/StoreActions';
import DeviceInfo from 'react-native-device-info';

class SplashComponent extends MyComponent {
  state = {
    appState: AppState.currentState,
    isLoading: true,
    allowToLoadMainComponent: false,
    persistDone: false
  };

  constructor(props) {
    super(props);

    this.hideSplashScreenTimeout = 0;
    this.needToLoadAllDataWhenPersistDone = false;

    this.onGoBack = this.onGoBack.bind(this);
    this.exitApp = this.exitApp.bind(this);
    this.goToSetting = this.goToSetting.bind(this);
    this.addEventListener = this.addEventListener.bind(this);
    this.removeEventListener = this.removeEventListener.bind(this);
    this.handleBackPress = this.handleBackPress.bind(this);

    let locale = 'vi';
    if (Platform.OS === 'ios') {
      locale = NativeModules.SettingsManager.settings.AppleLocale;
    } else {
      locale = NativeModules.I18nManager.localeIdentifier;
    }
    strings.setLanguage(locale && locale.length > 2 ? locale.substring(0, 2) : locale);
  }

  componentWillMount() {
    persistStore(store, null, () => {
      this.props.clearConnectionState();
      this.props.shortenListData();
      setTimeout(() => {
        this.setState({ persistDone: true });
        this.props.getLocation((isSuccess, responseDataCurrentLocation) => {
          if (isSuccess) {
            //if get loacation success then continue load new data:
            console.log('poi ab cd e responseDataCurrentLocation:', responseDataCurrentLocation);
            this.props.loadListStoreNearByUser(responseDataCurrentLocation, isSuccess => {
              console.log('poi 999 listStores:', this.props.listStores);
            });
            this.props.loadListSettingData(responseDataCurrentLocation, isSuccess => {});
          } else if (this.props.currentLocation && this.props.currentLocation.region) {
            //incase we cannot get location NOW, we will get load new data depend on LAST location user have.
            this.props.loadListSettingData(responseDataCurrentLocation, isSuccess => {});
          }
        });
        if (this.needToLoadAllDataWhenPersistDone) {
          this.requestPermission();
        }
        if (!global.isAcceptCallFromBackground) {
          const MyBridge = NativeModules.MyBridge;
          if (MyBridge && false) {
            MyBridge.isOpenningIncommingCallActivity(isOpenning => {
              if (!isOpenning) {
                const { userData } = this.props;
                if (this.props.token !== '' && userData) {
                  if (
                    !userData.nickname ||
                    !userData.introduction ||
                    userData.nickname.length === 0 ||
                    userData.introduction.length === 0 ||
                    userData.age === 0 ||
                    userData.sex.length === 0
                  ) {
                    this.props.navigation.navigate(ROUTE_KEY.ADDPROFILE, { userData });
                  } else {
                    this.props.navigation.replace(ROUTE_KEY.MAIN);
                  }
                } else {
                  this.props.navigation.replace(ROUTE_KEY.PRELOGIN);
                }
              }
            });
          } else {
            this.props.navigation.replace(ROUTE_KEY.MAIN);
          }
          // } else if (this.props.token !== '' && this.props.userData) {
          //   this.props.navigation.replace(ROUTE_KEY.MAIN);
          // } else {
          //   this.props.navigation.replace(ROUTE_KEY.PRELOGIN);
          // }
        }
      }, 500); //Must wait for shorten list data to be done before setState
    });
  }

  handleBackPress() {
    Alert.alert(
      strings.close_app_title,
      strings.close_app_content,
      [
        {
          text: strings.back,
          onPress: null,
          style: 'cancel'
        },
        {
          text: strings.close,
          onPress: this.exitApp
        }
      ],
      {
        cancelable: false
      }
    );
    return true;
  }

  goToSetting() {
    Alert.alert(
      strings.notification,
      strings.turn_on_location_permission,
      [
        {
          text: strings.close,
          onPress: this.exitApp
        },
        {
          text: strings.go_to_device_setting,
          onPress: () => {
            setTimeout(() => {
              this.exitApp();
              AndroidOpenSettings.appDetailsSettings();
            });
          }
        }
      ],
      {
        cancelable: false
      }
    );
  }

  async requestPermission(onDoneFunc = () => {}) {
    if (Platform.OS === 'ios') {
      this.props.loadAllData(onDoneFunc);
      this.willMountTimeout = setTimeout(() => {
        this.setState({ isLoading: false });
      }, 3000);
      return;
    }
    this.props.loadAllData(() => {
      onDoneFunc();
    });
    this.willMountTimeout = setTimeout(() => {
      this.setState({ isLoading: false });
    }, 3000);
  }

  exitApp() {
    BackHandler.exitApp();
  }

  addEventListener() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  removeEventListener() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  async componentDidMount() {
    const language = DeviceInfo.getDeviceLocale();

    firebase
      .messaging()
      .getToken()
      .then(token => {
        this._onChangeToken(token, language);
      });

    firebase.messaging().onTokenRefresh(token => {
      this._onChangeToken(token, language);
    });
  }

  _onChangeToken = (token, language) => {
    const data = {
      device_token: token,
      device_type: Platform.OS,
      device_language: language
    };

    this._loadDeviceInfo(data).done();
  };

  _loadDeviceInfo = async deviceData => {
    const value = JSON.stringify(deviceData);
    try {
      await AsyncStorage.setItem('@deviceInfo:key', value);
    } catch (error) {
      console.log(error);
    }
    // this.props.getLocation((isSuccess, responseDataCurrentLocation) => {
    //   if (isSuccess) { //if get loacation success then continue load new data:
    //     console.log('poi ab cd e responseDataCurrentLocation:', responseDataCurrentLocation);
    //     this.props.loadListStoreNearByUser(responseDataCurrentLocation, (isSuccess) => {
    //         console.log('poi 999 listStores:', this.props.listStores);
    //     });
    //     this.props.loadListSettingData(responseDataCurrentLocation, isSuccess => {});
    //   } else if (this.props.currentLocation && this.props.currentLocation.region) {
    //     //incase we cannot get location NOW, we will get load new data depend on LAST location user have.
    //     this.props.loadListSettingData(responseDataCurrentLocation, isSuccess => {});
    //   }
    // });

    // Platform.OS === 'ios' && VoipPushNotification.requestPermissions();

    // Platform.OS === 'ios' &&
    //   VoipPushNotification.addEventListener('register', token => {
    //     // send token to your apn provider server
    //   });

    // Platform.OS === 'ios' &&
    //   VoipPushNotification.addEventListener('notification', notification => {
    //     // register your VoIP client, show local notification, etc.
    //     // e.g.
    //     this.doRegister();

    //     /* there is a boolean constant exported by this module called
    //      *
    //      * wakeupByPush
    //      *
    //      * you can use this constant to distinguish the app is launched
    //      * by VoIP push notification or not
    //      *
    //      * e.g.
    //      */
    //     if (VoipPushNotification.wakeupByPush) {
    //       // do something...

    //       // remember to set this static variable to false
    //       // since the constant are exported only at initialization time
    //       // and it will keep the same in the whole app
    //       VoipPushNotification.wakeupByPush = false;
    //     }

    //   /**
    //    * Local Notification Payload
    //    *
    //    * - `alertBody` : The message displayed in the notification alert.
    //    * - `alertAction` : The "action" displayed beneath an actionable notification. Defaults to "view";
    //    * - `soundName` : The sound played when the notification is fired (optional).
    //    * - `category`  : The category of this notification, required for actionable notifications (optional).
    //    * - `userInfo`  : An optional object containing additional notification data.
    //    */
    //   VoipPushNotification.presentLocalNotification({
    //     alertBody: `hello! ${notification.getMessage()}`
    //   });
    // });

    AppState.addEventListener('change', this.handleAppStateChange);
    AppState.addEventListener('memoryWarning', this.handleLowMemoryWarning);

    this.props.navigation.addListener('didFocus', this.addEventListener);
    this.props.navigation.addListener('willBlur', this.removeEventListener);

    //This is a trick to prevent the app from being stuck at the beginning
    //Splash component must be able to show the loading first, then after the loading is shown
    //it should load MainComponent
    this.didMountTimeout1 = setTimeout(() => {
      this.setState({ allowToLoadMainComponent: true });
    }, 100);

    firebase
      .messaging()
      .requestPermission()
      .then(() => {})
      .catch(() => {});

    this.messageListener = firebase.messaging().onMessage(message => {
      if (Platform.OS === 'ios') {
        if (this.state.appState === 'background' || message._collapseKey) {
          return;
        }
      }

      // console.log('Notification received internally ', message);
      //Only check for FCM if user logged in
      if (this.props.token === '' || !this.props.userData) return;

      if (message.data.action === FCM_RESPONSE_PAYMENT_SUCCESS_ACTION) {
        FCMSubscriber.getInstance().next(true);
        return;
      }

      if (message.data.action === FCM_RESPONSE_PAYMENT_FAILED_ACTION) {
        FCMSubscriber.getInstance().next(false);
        return;
      }

      if (message.data.action === FCM_CHAT_ACTION && global.isChatScreenForeground) {
        if (global.chattingConversationId === '' || global.chattingConversationId === message.data.entity_id) {
          return;
        }
      }

      if (message.data.type === 'background_call' || message.data.type === 'background_call_cancel') {
        return;
      }
      const channel = new firebase.notifications.Android.Channel(
        'TocoToco',
        'TocoToco',
        firebase.notifications.Android.Importance.Max
      ).setDescription('TocoToco');
      // Create the channel
      firebase.notifications().android.createChannel(channel);
      // Build a channel group
      const channelGroup = new firebase.notifications.Android.ChannelGroup('TocoToco', 'TocoToco');
      // Create the channel group
      firebase.notifications().android.createChannelGroup(channelGroup);
      const notification = new firebase.notifications.Notification()
        .setNotificationId(message.messageId)
        .android.setChannelId('TocoToco')
        .android.setAutoCancel(true)
        .setTitle('TocoToco')
        .setBody(message.data.message)
        .setData(message);
      if (
        message.data.action !== FCM_CHAT_ACTION &&
        this.props.currentSettings &&
        this.props.currentSettings.notification_sound
      ) {
        notification.setSound('default');
      }

      firebase.notifications().displayNotification(notification);
      if (
        message.data.action !== FCM_CHAT_ACTION &&
        this.props.currentSettings &&
        this.props.currentSettings.vibration_alert
      ) {
        Vibration.vibrate(1000);
      }
    });

    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const state = store.getState();
      const action = this.getActionFromNotification(notificationOpen.notification);
      if (action === FCM_CHAT_ACTION) {
        const entityId = this.getEntityIdFromNotification(notificationOpen.notification);
        const conversation = state.conversation.list_conversation.find(e => e.id === entityId);
        if (conversation) {
          global.showSpinnerOnMain = false;

          this.props.navigation.navigate(ROUTE_KEY.DETAIL_MESSAGE_COMPONENT, {
            conversation
          });
        } else {
          loadConversationById(entityId, state.user.token)
            .then(res => {
              const { id } = state.user.userData;
              const { list_connected_user_id } = state.conversation;
              const data = getListDataFiltered(res, id, list_connected_user_id)[0];
              global.showSpinnerOnMain = false;

              this.props.navigation.navigate(ROUTE_KEY.DETAIL_MESSAGE_COMPONENT, {
                conversation: data
              });
            })
            .catch(err => console.log('loadConversationById fail', err));
        }
      } else if (action === FCM_MISSED_CALL) {
        const entityId = this.getEntityIdFromNotification(notificationOpen.notification);
        global.showSpinnerOnMain = false;
        loadUserById(entityId)
          .then(res => {
            const name = res.nickname && res.nickname.length > 0 ? res.nickname : res.email;
            Alert.alert(
              strings.alert,
              `${strings.missed_call} ${name}. ${strings.call_back}`,
              [
                { text: strings.cancel, style: 'cancel' },
                {
                  text: strings.yes,
                  onPress: () => {
                    if (this.props.userData.heart < MIN_HEART_TO_MAKE_A_PHONE_CALL) {
                      Alert.alert(strings.alert, strings.out_of_heart, [{ text: strings.yes, onPress: () => {} }], {
                        cancelable: true
                      });
                      this.props.navigation.goBack();
                    } else {
                      PopupNotification.showComponent(
                        <PopupNotificationCallComponent item={res} navigation={this.props.navigation} />
                      );
                    }
                  }
                }
              ],
              { cancelable: false }
            );
          })
          .catch(err => console.log('hinodi load user by id', err));
      }
    } else if (this.state.persistDone) {
      this.requestPermission(() => {
        this.setState({ isLoading: false });
      });
    } else {
      this.needToLoadAllDataWhenPersistDone = true;
    }
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened(notificationOpen => {
      const action = this.getActionFromNotification(notificationOpen.notification);
      if (action === FCM_CHAT_ACTION) {
        const entityId = this.getEntityIdFromNotification(notificationOpen.notification);
        const conversation = store.getState().conversation.list_conversation.find(e => e.id === entityId);
        if (conversation) {
          global.showSpinnerOnMain = false;

          this.props.navigation.navigate(ROUTE_KEY.DETAIL_MESSAGE_COMPONENT, {
            conversation
          });
        } else {
          //không có conversation khi nhận fcm chat ở foreground
        }
      } else if (action === FCM_MISSED_CALL) {
        const entityId = this.getEntityIdFromNotification(notificationOpen.notification);
        global.showSpinnerOnMain = false;
        loadUserById(entityId)
          .then(res => {
            const name = res.nickname && res.nickname.length > 0 ? res.nickname : res.email;
            Alert.alert(
              strings.alert,
              `${strings.missed_call} ${name}. ${strings.call_back}`,
              [
                { text: strings.cancel, style: 'cancel' },
                {
                  text: strings.yes,
                  onPress: () => {
                    if (this.props.userData.heart < MIN_HEART_TO_MAKE_A_PHONE_CALL) {
                      Alert.alert(
                        strings.alert,
                        strings.out_of_heart,
                        [
                          {
                            text: strings.yes,
                            onPress: () => {
                              this.props.navigation.navigate(ROUTE_KEY.STORE);
                            }
                          }
                        ],
                        { cancelable: true }
                      );
                      this.props.navigation.goBack();
                    } else {
                      PopupNotification.showComponent(
                        <PopupNotificationCallComponent item={res} navigation={this.props.navigation} />
                      );
                    }
                  }
                }
              ],
              { cancelable: false }
            );
          })
          .catch(err => console.log('hinodi load user by id', err));
      }
    });
  };

  onGoBack() {
    this.setState({ isLoading: false });
  }

  getActionFromNotification(notification) {
    if (Platform.OS === 'ios') {
      return notification.ios.category ? notification.ios.category.split(',')[0] : notification._data._data.action;
    }

    return notification._data._data ? notification._data._data.action : notification._data.action;
  }

  getEntityIdFromNotification(notification) {
    if (Platform.OS === 'ios') {
      return notification.ios.category ? notification.ios.category.split(',')[1] : notification._data._data.entity_id;
    }

    return notification._data._data ? notification._data._data.entity_id : notification._data.entity_id;
  }

  componentWillUnmount() {
    SplashScreen.hide();
    //deep linking:
    // C. We delete the Linking listener on componentWillUnmount
    // Linking.removeEventListener('url', this.handleOpenURL);
    clearTimeout(this.cancelTheCallingTimeout);

    clearTimeout(this.willMountTimeout);
    clearTimeout(this.didMountTimeout1);
    // this.messageListener();
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange = nextAppState => {
    this.setState({ appState: nextAppState });
  };

  handleLowMemoryWarning = () => {
    console.log('memory low warning (due to big images)');
  };

  render() {
    // console.log('Splash this.state.isLoading:', this.state.isLoading);
    // console.log(
    //   'Splash this.state.allowToLoadMainComponent:',
    //   this.state.allowToLoadMainComponent
    // );
    return <View style={{ flex: 1, opacity: global.isCallingFromBackground ? 0 : 1 }} />;
  }
}

const mapActionCreators = {
  loadAllData,
  clearConnectionState,
  shortenListData,
  loadConversationById,
  loadListSettingData,
  getLocation,
  loadListStoreNearByUser
};

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData,
  list_conversation: state.conversation.list_conversation,
  currentSettings: state.user.currentSettings,
  currentLocation: state.user.currentLocation,
  listStores: state.store.listStores
});

export default connect(
  mapStateToProps,
  mapActionCreators
)(SplashComponent);
