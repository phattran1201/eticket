import React from 'react';
import { connect } from 'react-redux';
import { Image, ScrollView, Text, View } from 'react-native';
import { ListItem } from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  FS,
  ORDER_COMPONENT,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS,
  BASE_URL_TOCOTOCO
} from '../../../constants/Constants';
import { DATA_STORE, DATA_TEST } from '../../../constants/dataTest';
import strings from '../../../constants/Strings';
import style, { APP_COLOR, FONT } from '../../../constants/style';
import MyButton from '../../../style/MyButton';
import { alert } from '../../../utils/alert';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import MyImage from '../../view/MyImage';
import Barcode from 'react-native-barcode-builder';
import { deleteUser } from '../editprofile/EditProfieActions';
import DeviceInfo from 'react-native-device-info';

class ProfileComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {};
    this.store = DATA_STORE;
    this.data = DATA_TEST;
    const { page_content } = this.props.settingData;
    this.listMenu = [
      {
        title: 'Đơn hàng của tôi',
        icon: 'airport-shuttle',
        iconImg: require('../../../assets/imgs/menu/menu_1.png'),
        onPress: () => {
          this.props.navigation.navigate(ROUTE_KEY.ORDER_COMPONENT);
        },
        route: ROUTE_KEY.ORDER_COMPONENT
      },
      {
        title: 'Thông tin cá nhân',
        icon: 'person',
        iconImg: require('../../../assets/imgs/menu/menu_3.png'),
        route: ROUTE_KEY.PERSONALINFO,
        onPress: () => {
          this.props.navigation.navigate(ROUTE_KEY.PERSONALINFO);
        }
      },
      {
        title: 'Đồ uống yêu thích',
        icon: 'favorite',
        iconImg: require('../../../assets/imgs/menu/menu_4.png'),
        route: ROUTE_KEY.FAVOURITE,
        onPress: () => {
          this.props.navigation.navigate(ROUTE_KEY.FAVOURITE);
        }
      },
      {
        title:
          page_content && page_content.page_policy_suport && page_content.page_policy_suport.page_title
            ? page_content.page_policy_suport.page_title
            : 'Chính sách hổ trợ',
        icon: 'info',
        iconImg: require('../../../assets/imgs/menu/menu_5.png'),
        route: ROUTE_KEY.TERMSCONDITIONSWEBVIEW,
        url:
          page_content && page_content.page_policy_suport && page_content.page_policy_suport.page_url
            ? `${BASE_URL_TOCOTOCO}${page_content.page_policy_suport.page_url}`
            : '',
        onPress: item => {
          this.props.navigation.navigate(`${item.route}`, {
            name: 'Chính sách và hổ trợ',
            uri: 'http://tocotocotea.com/category/cong-bo/'
          });
        }
      },
      {
        title: 'Cài đặt',
        icon: 'settings',
        iconImg: require('../../../assets/imgs/menu/menu_6.png'),
        avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
        route: ROUTE_KEY.SETTINGS_COMPONENT
      },
      {
        title: 'Đăng xuất',
        icon: 'log_out',
        iconImg: require('../../../assets/imgs/menu/menu_7.png'),
        onPress: item => {
          alert(strings.alert, strings.log_out);
          this.deleteAccount();
        }
      }
    ];
  }

  componentDidMount() {
    // if (this.props.token !== '' && this.props.userData && this.props.userData.id !== '') {
    // } else {
    //   alert(strings.alert, strings.please_login);
    //   this.props.navigation.navigate(ROUTE_KEY.PRELOGIN);
    // }
  }

  deleteAccount() {
    console.log('Hoang');
    this.props.deleteUser(this.props.token, () => {});
  }

  render() {
    const nickname =
      this.props.userData && this.props.userData.nickname !== null ? this.props.userData.nickname : 'Đăng Nhập';
    const avatar = this.props.userData && this.props.userData.avatar ? this.props.userData.avatar : null;
    return (
      <ScrollView style={{ backgroundColor: '#fff', flex: 1 }}>
        <BaseHeader
          noShadow
          translucent
          styleContent={{
            backgroundColor: 'transparent'
          }}
          leftIcon="qr"
          onLeftPress={() => this.props.navigation.goBack()}
          rightIcon="bell"
          onRightPress={() => {}}
        />

        <FastImage
          style={{
            zIndex: -1,
            width: DEVICE_WIDTH,
            height: (20 / 100) * DEVICE_HEIGHT,
            position: 'absolute'
          }}
          removeClippedSubviews
          source={{
            uri: 'https://i.pinimg.com/originals/81/f1/8d/81f18dbd8bef71772d6db3b379101310.jpg'
          }}
          defaultSource={null}
        />

        <View
          style={[
            style.shadow,
            {
              backgroundColor: '#fff',
              borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
              marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
        >
          <View style={{ marginTop: (-80 * SCALE_RATIO_WIDTH_BASIS) / 2 }}>
            <View
              style={{
                backgroundColor: '#fff',
                width: 100 * SCALE_RATIO_WIDTH_BASIS,
                height: 100 * SCALE_RATIO_WIDTH_BASIS,
                padding: 20 * SCALE_RATIO_WIDTH_BASIS,
                borderRadius: (100 * SCALE_RATIO_WIDTH_BASIS) / 2,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center'
              }}
            >
              <MyImage
                source={{
                  uri: avatar
                }}
                size={80 * SCALE_RATIO_WIDTH_BASIS}
                styleContent={{
                  alignSelf: 'center'
                }}
                borderDisabled
              />
            </View>
            <Text
              style={[
                style.titleHeader,
                {
                  marginTop: 10 * SCALE_RATIO_WIDTH_BASIS
                }
              ]}
            >
              {nickname}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 10 * SCALE_RATIO_HEIGHT_BASIS
            }}
          >
            <View
              style={{
                flex: 1,
                borderColor: APP_COLOR,
                borderRightWidth: 0.1 * SCALE_RATIO_WIDTH_BASIS,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <View
                style={{
                  flexDirection: 'row'
                }}
              >
                <Image
                  source={require('../../../assets/imgs/icons/coin.png')}
                  style={{
                    width: 13 * SCALE_RATIO_WIDTH_BASIS,
                    height: 13 * SCALE_RATIO_WIDTH_BASIS,
                    alignSelf: 'center',
                    marginRight: 3 * SCALE_RATIO_WIDTH_BASIS
                  }}
                  resizeMode="contain"
                />
                <Text
                  style={[
                    style.text,
                    {
                      fontSize: FS(12),
                      marginBottom: 4 * SCALE_RATIO_HEIGHT_BASIS
                    }
                  ]}
                >
                  Điểm tích lũy
                </Text>
              </View>
              <Text style={[style.titleHeader, { fontSize: FS(24), color: APP_COLOR }]}>123,456</Text>
            </View>
            <View
              style={{
                flex: 1,
                borderColor: APP_COLOR,
                borderLeftWidth: 0.1 * SCALE_RATIO_WIDTH_BASIS,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <View
                style={{
                  flexDirection: 'row'
                }}
              >
                <Image
                  source={require('../../../assets/imgs/icons/start.png')}
                  style={{
                    width: 13 * SCALE_RATIO_WIDTH_BASIS,
                    height: 13 * SCALE_RATIO_WIDTH_BASIS,
                    alignSelf: 'center',
                    marginRight: 3 * SCALE_RATIO_WIDTH_BASIS
                  }}
                  resizeMode="contain"
                />
                <Text
                  style={[
                    style.text,
                    {
                      fontSize: FS(12),
                      marginBottom: SCALE_RATIO_HEIGHT_BASIS * 2
                    }
                  ]}
                >
                  Hạng thành viên
                </Text>
              </View>

              <Text style={[style.titleHeader, { fontSize: FS(24), color: APP_COLOR }]}>Vàng</Text>
            </View>
          </View>
          <View
            style={{
              borderTopColor: APP_COLOR,
              borderTopWidth: 0.2 * SCALE_RATIO_WIDTH_BASIS,
              marginTop: 10 * SCALE_RATIO_WIDTH_BASIS,
              marginHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            {!DeviceInfo.isEmulator() ? (
              <Barcode value="T1234565565" format="CODE128" text="T1234565565" height={50} width={1.5} />
            ) : null}
          </View>
        </View>
        <View
          style={[
            style.shadow,
            {
              marginTop: 20 * SCALE_RATIO_WIDTH_BASIS,
              backgroundColor: '#fff',
              paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
              paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS,
              borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
              marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
        >
          {this.listMenu.map((item, i) => (
            <ListItem
              containerStyle={{
                padding: 5 * SCALE_RATIO_WIDTH_BASIS,
                borderBottomColor: `${APP_COLOR}80`,
                borderBottomWidth: 0 * SCALE_RATIO_HEIGHT_BASIS
              }}
              key={i}
              onPress={() => {
                if (item.onPress) {
                  item.onPress(item);
                }
              }}
              title={item.title}
              leftAvatar={{
                source: item.iconImg,
                avatarStyle: {
                  width: 18 * SCALE_RATIO_WIDTH_BASIS,
                  height: 18 * SCALE_RATIO_WIDTH_BASIS,
                  resizeMode: 'contain'
                },
                containerStyle: {
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                  alignItems: 'center'
                },
                overlayContainerStyle: { backgroundColor: 'transparent' }
              }}
              // leftIcon={{ name: item.icon }}
              titleStyle={[style.text, { fontFamily: FONT.Medium, fontSize: FS(16), marginLeft: -15 }]}
              chevron
            />
          ))}
        </View>
        {/* <MyButton
          onPress={() => {
            alert(strings.alert, strings.log_out);
            this.deleteAccount();
          }}
          outline
          width={150 * SCALE_RATIO_WIDTH_BASIS}
          style={{
            // marginRight: 18 * SCALE_RATIO_WIDTH_BASIS,
            marginTop: 18 * SCALE_RATIO_WIDTH_BASIS,
            alignSelf: 'center'
          }}
        >
          {strings.log_out}
        </MyButton> */}
        {/* <Text
          style={[
            style.text,
            {
              marginTop: 18 * SCALE_RATIO_WIDTH_BASIS,
              color: '#b6bbbf',
              textAlign: 'center',
              paddingBottom: 40 * SCALE_RATIO_HEIGHT_BASIS
            }
          ]}
        >
          Phiên bản 1.1.23.34
        </Text> */}
      </ScrollView>
    );
  }
}
const mapActionCreators = {
  deleteUser
};

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData,
  settingData: state.setting.settingData
});

export default connect(
  mapStateToProps,
  mapActionCreators
)(ProfileComponent);
