import React from 'react';
import { FlatList, ScrollView, Text, View, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import { FS, SCALE_RATIO_HEIGHT_BASIS, SCALE_RATIO_WIDTH_BASIS, ROUTE_KEY } from '../../../constants/Constants';
import { DATA_STORE, DATA_TEST } from '../../../constants/dataTest';
import style from '../../../constants/style';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import MySpinner from '../../view/MySpinner';
import { loadListCollection } from '../menu/MenuAction';

class CategoryComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      area: this.props.currentLocation && this.props.currentLocation.city ? this.props.currentLocation.city : 'Hà Nội'
    };
    this.store = DATA_STORE;
    this.data = DATA_TEST;
    this.area = ['Hà Nội', 'Hồ Chí Minh', 'Hải Phòng'];
  }

  // componentDidMount() {
  //   this.props.loadListCollection();
  // }

  renderItem = ({ item, index }) => {
    const title = item.col_title;
    const image = item.col_img;
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.navigate({
            key: 'MENU_COMPONENT_001',
            routeName: ROUTE_KEY.MENU_COMPONENT,
            params: {
              selectedCollection: item
            }
          });
        }}
      >
        <View
          style={[
            style.shadow,
            {
              marginHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
              marginTop: 20,
              borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
        >
          <View
            style={{
              width: '100%',
              height: '100%',
              position: 'absolute',
              zIndex: 99,
              paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
              alignItems: 'center',
              justifyContent: 'center'
              // backgroundColor: '#C7AE6D80'
            }}
          >
            <Text
              style={[
                style.textHeader,
                {
                  zIndex: 999,
                  textShadowColor: 'rgba(0, 0, 0, 0.55)',
                  textShadowOffset: { width: -1, height: 1 },
                  textShadowRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                  color: '#fff'
                }
              ]}
            >
              {title}
            </Text>
          </View>

          <FastImage
            style={[
              {
                width: '100%',
                height: 150 * SCALE_RATIO_HEIGHT_BASIS,
                borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
            removeClippedSubviews
            source={{
              uri: image || 'https://www.fancyhands.com/images/default-avatar-250x250.png'
            }}
            defaultSource={null}
          />
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    console.log('poi 273 ', this.props.currentLocation.region);
    const { menu_screen } = this.props.regionData;
    const menuTitle = menu_screen && menu_screen.screen_title ? menu_screen.screen_title : 'ToCoToCo Menu';
    // const listMenuData = menu_screen && menu_screen.list_collections ? menu_screen.list_collections.filter(e => e.col_use) : [];
    const listMenuData = this.props.listCollections ? this.props.listCollections : [];
    return (
      <View style={{ backgroundColor: '#fff', flex: 1 }}>
        <BaseHeader
          children={<Text style={style.titleHeader}>{menuTitle}</Text>}
          leftIcon={require('../../../assets/imgs/tabbar/user.png')}
          leftIconType="Image"
          onLeftPress={() => this.props.navigation.goBack()}
          rightIconMenu="s"
          // rightIconTypeMenu="MaterialIcons"
          valueMenu={this.state.area}
          disabledDropdown
          dataMenu={this.area.map(e => ({
            value: e,
            onSelect: () => {
              this.setState({ area: e });
            }
          }))}
        />

        <FlatList
          numColumns={1}
          data={listMenuData.filter(e => e.col_use)}
          renderItem={this.renderItem}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }
}
const mapActionCreators = {
  loadListCollection
};

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData,
  regionData: state.setting.regionData,
  currentLocation: state.user.currentLocation,
  listCollections: state.menu.listCollections
});

export default connect(
  mapStateToProps,
  mapActionCreators
)(CategoryComponent);
