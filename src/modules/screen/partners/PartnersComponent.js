import React from 'react';
import { ActivityIndicator, FlatList, StatusBar, StyleSheet, Text, View } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import { ROUTE_KEY, SCALE_RATIO_HEIGHT_BASIS, SCALE_RATIO_WIDTH_BASIS } from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import global from '../../../utils/globalUtils';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import Dropdown from '../../view/MyDropDown/dropdown';
import MySpinner from '../../view/MySpinner';
import PartnersListItem from '../../view/PartnersListItem';
import { loadListBlocked } from '../blockfriends/BlockFriendActions';
import {
  loadListAllPartners,
  loadListPartners,
  loadMoreListPartners,
  updateGenderFilter,
  updateOrderFilter
} from './PartnersActions';

class PartnersComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      type: strings.recently_visited,
      refreshing: false,
      isLoadMore: false
    };
    this.page = 1;
    this.stillHaveData = true;
    this.renderItem = this.renderItem.bind(this);
  }

  renderItem = ({ item }) => <PartnersListItem item={item} navigation={this.props.navigation} />;

  async componentDidMount() {
    if (global.showSpinnerOnMain) MySpinner.show();
    this.props.loadListPartners({}, () => {
      setTimeout(() => MySpinner.hide(), 2000);
    });
    this.props.loadListBlocked(1);
    this.props.loadListAllPartners(() => {});
  }

  renderFooter = () => {
    if (!this.state.isLoadMore) {
      return null;
    }

    return (
      <View style={{ paddingVertical: 10 * SCALE_RATIO_HEIGHT_BASIS }}>
        <ActivityIndicator animating size="small" />
      </View>
    );
  };

  render() {
    const { userData } = this.props;
    if (!userData) return null;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white'
        }}
      >
        <StatusBar backgroundColor="#ffff" barStyle="dark-content" />
        <BaseHeader
          children={<Text style={style.titleHeader}>{strings.call.toUpperCase()}</Text>}
          rightIcon="envelope"
          navigation={this.props.navigation}
          leftIcon="search"
          leftIconType="Feather"
          onLeftPress={() => {
            this.props.navigation.navigate(ROUTE_KEY.SEARCH, {
              transition: 'slideFromRight'
            });
          }}
        />

        <View
          style={{
            zIndex: 99,
            position: 'absolute',
            bottom: 5 * SCALE_RATIO_HEIGHT_BASIS,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          <View style={{ flex: 1 }}>
            <Dropdown
              containerStyle={{
                alignItems: 'center',
                justifyContent: 'center'
              }}
              pickerStyle={{
                width: 170 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                backgroundColor: '#00000090',
                borderRadius: 5 * SCALE_RATIO_HEIGHT_BASIS
              }}
              textColor="#fff"
              fontSize={15 * SCALE_RATIO_WIDTH_BASIS}
              dropdownPosition={-1}
              itemTextStyle={{
                fontSize: 4.5 * SCALE_RATIO_WIDTH_BASIS,
                fontFamily: 'helveticaneue',
                color: '#fff'
              }}
              itemPadding={5}
              dropdownOffset={{
                top: -35 * SCALE_RATIO_HEIGHT_BASIS,
                left: 52 * SCALE_RATIO_WIDTH_BASIS
              }}
              data={this.props.listGender.map(e => ({
                value: e.name,
                onSelect: () => {
                  if (e.id === this.props.currentGender.id) return;
                  setTimeout(() => {
                    MySpinner.show();
                    this.props.updateGenderFilter(e);
                    this.page = 1;
                    this.stillHaveData = true;
                    this.props.loadListPartners({ gender: e.id }, MySpinner.hide);
                  }, 300);
                }
              }))}
            >
              <View style={[styles.dropdownContainer, { width: 170 * SCALE_RATIO_WIDTH_BASIS }]}>
                <Text style={[style.text, { color: '#fff' }]}>{this.props.currentGender.name}</Text>
                <MaterialIcons name="arrow-drop-down" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#fff" />
              </View>
            </Dropdown>
          </View>
          <View style={{ flex: 1 }}>
            <Dropdown
              containerStyle={{
                alignItems: 'center',
                justifyContent: 'center'
              }}
              pickerStyle={{
                width: 170 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                backgroundColor: '#00000090',
                borderRadius: 5 * SCALE_RATIO_HEIGHT_BASIS
              }}
              textColor="#fff"
              fontSize={15 * SCALE_RATIO_WIDTH_BASIS}
              dropdownPosition={-1}
              itemTextStyle={{
                fontSize: 4.5 * SCALE_RATIO_WIDTH_BASIS,
                fontFamily: 'helveticaneue',
                color: '#fff'
              }}
              itemPadding={5}
              dropdownOffset={{
                top: -35 * SCALE_RATIO_HEIGHT_BASIS,
                left: 52 * SCALE_RATIO_WIDTH_BASIS
              }}
              data={this.props.listOrder.map(e => ({
                value: e.name,
                onSelect: () => {
                  if (e.id === this.props.currentOrder.id) return;
                  setTimeout(() => {
                    MySpinner.show();
                    this.props.updateOrderFilter(e);
                    this.page = 1;
                    this.stillHaveData = true;
                    this.props.loadListPartners({ order: e.id }, MySpinner.hide);
                  }, 300);
                }
              }))}
            >
              <View style={[styles.dropdownContainer, { width: 170 * SCALE_RATIO_WIDTH_BASIS }]}>
                <Text style={[style.text, { color: '#fff' }]}>{this.props.currentOrder.name}</Text>
                <MaterialIcons name="arrow-drop-down" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#fff" />
              </View>
            </Dropdown>
          </View>
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.props.listPartners.filter(e => {
            const isBlocked = this.props.listBlocked.find(i => i.receiver.id === e.id);
            const isMine = e.id === this.props.userData.id;
            return !isBlocked && !isMine;
          })}
          keyExtractor={item => item.id}
          renderItem={this.renderItem}
          style={{}}
          numColumns={2}
          onRefresh={() => {
            this.page = 1;
            this.stillHaveData = true;
            this.setState({ refreshing: true });
            this.props.loadListPartners({}, () => {
              this.setState({ refreshing: false });
            });
          }}
          refreshing={this.state.refreshing}
          onEndReached={() => {
            if (!this.stillHaveData || this.state.isLoadMore) return;
            this.setState({ isLoadMore: true });
            this.props.loadMoreListPartners(this.page + 1, stillHaveData => {
              this.page = this.page + 1;
              this.stillHaveData = stillHaveData;
              this.setState({
                isLoadMore: false
              });
            });
          }}
          onEndReachedThreshold={0.01}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  listGender: state.filter.gender.listGender,
  currentGender: state.filter.gender.currentGender,
  listOrder: state.filter.order.listOrder,
  currentOrder: state.filter.order.currentOrder,
  listPartners: state.partners.listPartners,
  listBlocked: state.blocking.listBlocked,
  userData: state.user.userData
});

const mapActionCreators = {
  loadListPartners,
  loadListAllPartners,
  updateGenderFilter,
  updateOrderFilter,
  loadMoreListPartners,
  loadListBlocked
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(PartnersComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  titleStyle: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue-Bold',
    fontSize: 18 * SCALE_RATIO_WIDTH_BASIS,
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  },
  dropdownContainer: {
    justifyContent: 'space-between',
    paddingLeft: 5 * SCALE_RATIO_WIDTH_BASIS,
    alignItems: 'center',
    height: 31 * SCALE_RATIO_HEIGHT_BASIS,
    flexDirection: 'row',
    backgroundColor: '#00000090',
    borderRadius: 5 * SCALE_RATIO_HEIGHT_BASIS
  },
  dropdownText: {
    fontFamily: 'helveticaneue',
    fontSize: 15 * SCALE_RATIO_WIDTH_BASIS
  }
});
