import request from '../../../utils/request';
import {
    BASE_URL, USER_API,
    UPDATE_GENDER_FILTER,
    UPDATE_ORDER_FILTER,
    UPDATE_LIST_PARTNERS,
    ADD_TO_LIST_PARTNERS,
    UPDATE_LIST_ALL_PARTNERS
} from '../../../constants/Constants';

export function loadListPartners({ order, gender }, onDone = () => { }) {
    return (dispatch, store) => {
        console.log('hinodi loadListPartners', gender);
        const { filter } = store();
        let genderFilter = '';
        if (gender) {
            if (gender !== 'ALL') {
                genderFilter = `&filter=[{"sex": "${gender}"}]`;
            }
        } else {
            if (filter.gender.currentGender.id !== 'ALL') {
                genderFilter = `&filter=[{"sex": "${filter.gender.currentGender.id}"}]`;
            }
        }
        const orderURL = order ? order : filter.order.currentOrder.id;
        const page = 1;
        const limit = 30;

        request.get(`${BASE_URL}${USER_API}?fields=["$all"]&order=[${orderURL}]${genderFilter}&page=${page}&limit=${limit}`)
            .finish((err, res) => {
                onDone();
                if (res &&
                    res.body &&
                    res.body.results &&
                    res.body.results.objects &&
                    res.body.results.objects.rows) {
                    dispatch({
                        type: UPDATE_LIST_PARTNERS,
                        payload: res.body.results.objects.rows
                    });
                } else {

                }
            });
    };
}
export function loadListAllPartners(onDone = () => { }) {
    return (dispatch, store) => {

        request.get(`${BASE_URL}${USER_API}?fields=["$all"]`)
            .finish((err, res) => {
                onDone();
                if (res &&
                    res.body &&
                    res.body.results &&
                    res.body.results.objects &&
                    res.body.results.objects.rows) {
                    dispatch({
                        type: UPDATE_LIST_ALL_PARTNERS,
                        payload: res.body.results.objects.rows
                    });
                } else {

                }
            });
    };
}

export function loadMoreListPartners(page, onDone = () => { }) {
    return (dispatch, store) => {
        const { filter } = store();
        let genderFilter = '';
        if (filter.gender.currentGender.id !== 'ALL') {
            genderFilter = `&filter=[{"sex": "${filter.gender.currentGender.id}"}]`;
        }
        const orderURL = filter.order.currentOrder.id;
        const limit = 30;

        request.get(`${BASE_URL}${USER_API}?fields=["$all"]&order=[${orderURL}]${genderFilter}&page=${page}&limit=${limit}`)
            .finish((err, res) => {
                if (res &&
                    res.body &&
                    res.body.results &&
                    res.body.results.objects &&
                    res.body.results.objects.rows
                ) {
                    if (res.body.results.objects.rows.length === 0) {
                        onDone(false);
                    } else {
                        onDone(true);
                    }
                    dispatch({
                        type: ADD_TO_LIST_PARTNERS,
                        payload: res.body.results.objects.rows
                    });
                } else {
                    onDone(true);
                }
            });
    };
}

export function updateGenderFilter(gender) {
    return (dispatch, store) => {
        dispatch({
            type: UPDATE_GENDER_FILTER,
            payload: gender
        });
    };
}

export function updateOrderFilter(order) {
    return (dispatch, store) => {
        dispatch({
            type: UPDATE_ORDER_FILTER,
            payload: order
        });
    };
}
