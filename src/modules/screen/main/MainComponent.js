import React from 'react';
import { Alert, DeviceEventEmitter, Dimensions, Image, Platform, StyleSheet, View, Text } from 'react-native';
import CodePush from 'react-native-code-push';
import publicIP from 'react-native-public-ip';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import { playMessageReceivedSound } from '../../../utils/audioUtils';
import { reloadUnreadNotificationNumber, subscribeMessages } from '../../../utils/chatManager';
import MyComponent from '../../view/MyComponent';
import MySpinner from '../../view/MySpinner';
import { MyStatelessTabView } from '../../view/MyTabView';
import STouchableOpacity from '../../view/STouchableOpacity';
import { loadConversation } from '../detailMessage/ListMessageAction';
import { checkBlockSystem, updateUserInfo } from '../editprofile/EditProfieActions';
import {
  addNewConversation,
  addNewMessageToConversation,
  loadInterestedConversation
} from '../message/ListMessageAction';
import MoreComponent from '../more/MoreComponent';
import MapComponent from '../map/MapComponent';
import PartnersComponent from '../partners/PartnersComponent';
import PostingComponent from '../posting/PostingComponent';
import QuickMatchComponent from '../quickMatch/QuickMatchComponent';
import RatingComponent from '../rating/RatingComponent';
import {
  changeIsFilterByRadius,
  changeNewCategoryForMap,
  changeNewRadius,
  changeNewTradeTypeForMap,
  getUserLocale,
  loadCurrentLocation,
  loadProductsByFilterForMap,
  reloadProductByRadius,
  loadListSettingData,
  updateUserLocation,
  loadLocation
} from './MainActions';
import TabBar from '../../../style/Tabbar/MyTabbar';
import StoreComponent from '../store/StoreComponent';
import ProfileComponent from '../profile/ProfileComponent';

import MenuComponent from '../menu/MenuComponent';
import DetailStoreComponent from '../detailStore/DetailStoreComponent';
import CategoryComponent from '../category/CategoryComponent';
import { alert } from '../../../utils/alert';
import PreLoginComponent from '../prelogin/PreLoginComponent';
import RewardsComponent from '../reward/RewardsComponent';
import HomeComponent from '../../screens/home/HomeComponent';
import DetailEventComponent from '../../screens/detailEvent/DetailEventComponent';
import AsyncStore from '../store';
import AsyncReward from '../reward';
import AsyncCategory from '../category';
import AsyncMap from '../map';
import AsyncPreLogin from '../prelogin';
import AsyncProfile from '../profile';
import global from '../../../utils/globalUtils';

const home = require('../../../assets/imgs/tabbar/home.png');
const user = require('../../../assets/imgs/tabbar/user.png');
const rewards = require('../../../assets/imgs/tabbar/rewards.png');
const store = require('../../../assets/imgs/tabbar/store.png');

const homeSelected = require('../../../assets/imgs/tabbar/home_select.png');
const userSelected = require('../../../assets/imgs/tabbar/user_select.png');
const rewardsSelected = require('../../../assets/imgs/tabbar/rewards_select.png');
const storeSelected = require('../../../assets/imgs/tabbar/store_select.png');

const tabbar0 = require('../../../assets/imgs/tabbar/tabbar_1.png');
const tabbar1 = require('../../../assets/imgs/tabbar/tabbar_2.png');
const tabbar2 = require('../../../assets/imgs/tabbar/tabbar_3.png');
const tabbar3 = require('../../../assets/imgs/tabbar/tabbar_4.png');
const tabbar4 = require('../../../assets/imgs/tabbar/tabbar_5.png');

const tabbar0Selected = require('../../../assets/imgs/tabbar/tabbar_1_select.png');
const tabbar1Selected = require('../../../assets/imgs/tabbar/tabbar_2_select.png');
const tabbar2Selected = require('../../../assets/imgs/tabbar/tabbar_3_select.png');
const tabbar3Selected = require('../../../assets/imgs/tabbar/tabbar_4_select.png');
const tabbar4Selected = require('../../../assets/imgs/tabbar/tabbar_5_select.png');

const { width } = Dimensions.get('window');

class MainComponent extends MyComponent {
  constructor(props) {
    super(props);

    this.state = {
      tabProductShowMap: true,
      isFilterModalVisible: false,
      index: this.getSelectedIndex(this.props.navigation.state.params),
      ip: '',
      countryCode: ''
    };
    this.onFilterButtonPressed = this.onFilterButtonPressed.bind(this);
    this.onBuyButtonPressed = this.onBuyButtonPressed.bind(this);
    this.onSellButtonPressed = this.onSellButtonPressed.bind(this);
    this.setTabProductShowMap = this.setTabProductShowMap.bind(this);
    this.toggleFilterByRadius = this.toggleFilterByRadius.bind(this);
    this.onSliderValueChanged = this.onSliderValueChanged.bind(this);
    this.onConversationUpdateFunc = this.onConversationUpdateFunc.bind(this);
    // this.onChangeTab = this.onChangeTab.bind(this);
    this.refListProductsOnMap = this.refListProductsOnMap.bind(this);
    this.refListProducts = this.refListProducts.bind(this);
    this.toggleMenu = this.toggleMenu.bind(this);
    this.onFilterDone = this.onFilterDone.bind(this);
    this.onModalBackdropPress = this.onModalBackdropPress.bind(this);
    // this.navigateToProfileTab = this.navigateToProfileTab.bind(this);
    this.onShowSearchBarProductOnMap = this.onShowSearchBarProductOnMap.bind(this);
    this.pendingProductTabToShow = -1;
    this.userRegion = 'north';
  }

  onConversationUpdateFunc = data => {
    let conversationExist = false;
    this.props.listConversation.forEach(e => {
      if (e.id === data.message.conversation_id) {
        conversationExist = true;
      }
    });
    if (conversationExist) {
      this.props.addNewMessageToConversation(data.message.conversation_id, data.message);
    } else {
      const conversation = data.conversation;
      if (conversation) {
        if (!conversation.messages) {
          conversation.messages = [data.message];
        }
        this.props.addNewConversation(conversation);
      }
    }

    let isRead = false;
    data.message.read_user_ids.forEach(userId => {
      if (userId === this.props.userData.id) {
        isRead = true;
      }
    });
    if (!isRead && this.props.userData && data.message.sender_id !== this.props.userData.id) {
      playMessageReceivedSound();
    }
    this.onConversationUpdateFuncTimeout = setTimeout(() => {
      this.forceUpdate();
    }, 100);

    reloadUnreadNotificationNumber(
      this.props.listConversation,
      this.props.userData.id,
      this.props.listConnectedUserIds
    );
  };

  componentDidMount() {}

  getSelectedIndex(params) {
    const isFromFCMChat = params && params.isFromFCMChat ? params.isFromFCMChat : false;
    const isFromSuccessPostProduct =
      params && params.isFromSuccessPostProduct ? params.isFromSuccessPostProduct : false;
    const isFromFCMValidProduct = params && params.isFromFCMValidProduct ? params.isFromFCMValidProduct : false;
    const isFromFCMBannedProduct = params && params.isFromFCMBannedProduct ? params.isFromFCMBannedProduct : false;
    const isFromFCMExpiredProduct = params && params.isFromFCMExpiredProduct ? params.isFromFCMExpiredProduct : false;
    const isFromFCMReviewProduct = params && params.isFromFCMReviewProduct ? params.isFromFCMReviewProduct : false;
    const isFromSuccessPostDiary = params && params.isFromSuccessPostDiary ? params.isFromSuccessPostDiary : false;
    let index = 0; //TAB_PRODUCT
    if (isFromFCMChat) {
      index = 2;
    } else if (isFromSuccessPostDiary) {
      index = 1; //TAB_NEW_FEED
    } else if (
      isFromSuccessPostProduct ||
      isFromFCMValidProduct ||
      isFromFCMBannedProduct ||
      isFromFCMExpiredProduct ||
      isFromFCMReviewProduct
    ) {
      let productTabToShow = 0;
      if (isFromFCMBannedProduct) {
        productTabToShow = 1;
      } else if (isFromFCMExpiredProduct) {
        productTabToShow = 2;
      } else if (isFromSuccessPostProduct || isFromFCMReviewProduct) {
        productTabToShow = 3;
      }
      this.navigateToProfileTab(productTabToShow);
    }

    return index;
  }

  setTabProductShowMap(isShown) {
    this.setState({ tabProductShowMap: isShown });
  }

  onFilterButtonPressed() {
    this.setState({
      isFilterModalVisible: true
    });
  }

  // navigateToProfileTab(productTabToShow = -1) {
  //   if (this.MainTabView) {
  //     const index = 4;
  //     this.MainTabView.changeTab(index);

  //     this.ivGroup.setNativeProps({
  //       style: [styles.tab_icon, { opacity: index === 1 ? 0 : 1 }]
  //     });
  //     this.ivGroupSelected.setNativeProps({
  //       style: [styles.tab_icon, { position: 'absolute', opacity: index === 1 ? 1 : 0 }]
  //     });

  //     this.ivChat.setNativeProps({
  //       style: [styles.tab_icon, { opacity: index === 0 ? 0 : 1 }]
  //     });
  //     this.ivChatSelected.setNativeProps({
  //       style: [styles.tab_icon, { position: 'absolute', opacity: index === 0 ? 1 : 0 }]
  //     });

  //     this.ivPost.setNativeProps({
  //       style: [styles.tab_icon, { opacity: index === 2 ? 0 : 1 }]
  //     });
  //     this.ivPostSelected.setNativeProps({
  //       style: [styles.tab_icon, { position: 'absolute', opacity: index === 2 ? 1 : 0 }]
  //     });

  //     this.ivNotification.setNativeProps({
  //       style: [styles.tab_icon, { opacity: index === 3 ? 0 : 1 }]
  //     });
  //     this.ivNotificationSelected.setNativeProps({
  //       style: [styles.tab_icon, { position: 'absolute', opacity: index === 3 ? 1 : 0 }]
  //     });

  //     this.ivProfile.setNativeProps({
  //       style: [styles.tab_icon, { opacity: index === 4 ? 0 : 1 }]
  //     });
  //     this.ivProfileSelected.setNativeProps({
  //       style: [styles.tab_icon, { position: 'absolute', opacity: index === 4 ? 1 : 0 }]
  //     });

  //     if (productTabToShow !== -1) {
  //       DeviceEventEmitter.emit('changeTabMyProfileProduct', productTabToShow);
  //     }

  //     this.pendingProductTabToShow = -1;
  //   } else if (productTabToShow !== -1) {
  //     this.pendingProductTabToShow = productTabToShow;
  //     setTimeout(() => {
  //       this.navigateToProfileTab(this.pendingProductTabToShow);
  //     }, 100);
  //   }
  // }

  onBuyButtonPressed() {
    if (this.props.token !== '' && this.props.userData) {
      this.props.navigation.navigate(ROUTE_KEY.CHOOSE_ROLE_POST, {
        type: {
          key: 'BUY',
          name: strings.need_buy
        }
      });
    } else {
      Alert.alert(
        strings.login_app_title,
        strings.please_login,
        [
          {
            text: strings.back,
            onPress: null,
            style: 'cancel'
          },
          {
            text: strings.login,
            onPress: this.navigateToProfileTab
          }
        ],
        {
          cancelable: true
        }
      );
    }
  }

  onSellButtonPressed() {
    if (this.props.token !== '' && this.props.userData) {
      this.props.navigation.navigate(ROUTE_KEY.CHOOSE_ROLE_POST, {
        type: {
          key: 'SELL',
          name: strings.need_sell
        }
      });
    } else {
      Alert.alert(
        strings.login_app_title,
        strings.please_login,
        [
          {
            text: strings.back,
            onPress: null,
            style: 'cancel'
          },
          {
            text: strings.login,
            onPress: this.navigateToProfileTab
          }
        ],
        {
          cancelable: true
        }
      );
    }
  }

  toggleFilterByRadius(value) {
    this.props.toggleFilterByRadius(value);
  }

  onSliderValueChanged(value) {
    MySpinner.show();
    this.props.reloadProductByRadius(value, () => MySpinner.hide());
  }

  // onChangeTab = index => () => {
  //   this.MainTabView.changeTab(index);

  //   this.ivGroup.setNativeProps({
  //     style: [styles.tab_icon, { opacity: index === 1 ? 0 : 1 }]
  //   });
  //   this.ivGroupSelected.setNativeProps({
  //     style: [styles.tab_icon, { position: 'absolute', opacity: index === 1 ? 1 : 0 }]
  //   });

  //   this.ivChat.setNativeProps({
  //     style: [styles.tab_icon, { opacity: index === 0 ? 0 : 1 }]
  //   });
  //   this.ivChatSelected.setNativeProps({
  //     style: [styles.tab_icon, { position: 'absolute', opacity: index === 0 ? 1 : 0 }]
  //   });

  //   this.ivPost.setNativeProps({
  //     style: [styles.tab_icon, { opacity: index === 2 ? 0 : 1 }]
  //   });
  //   this.ivPostSelected.setNativeProps({
  //     style: [styles.tab_icon, { position: 'absolute', opacity: index === 2 ? 1 : 0 }]
  //   });

  //   this.ivNotification.setNativeProps({
  //     style: [styles.tab_icon, { opacity: index === 3 ? 0 : 1 }]
  //   });
  //   this.ivNotificationSelected.setNativeProps({
  //     style: [styles.tab_icon, { position: 'absolute', opacity: index === 3 ? 1 : 0 }]
  //   });

  //   this.ivProfile.setNativeProps({
  //     style: [styles.tab_icon, { opacity: index === 4 ? 0 : 1 }]
  //   });
  //   this.ivProfileSelected.setNativeProps({
  //     style: [styles.tab_icon, { position: 'absolute', opacity: index === 4 ? 1 : 0 }]
  //   });
  // };

  refListProductsOnMap(c) {
    if (c) {
      this.listProductsOnMapComponent = c.getWrappedInstance();
    }
  }

  refListProducts(c) {
    if (c) {
      this.listProductsComponent = c.getWrappedInstance();
    }
  }

  toggleMenu(value) {
    this.setState({ isFilterModalVisible: value });
  }

  onFilterDone(hideSpinner) {
    this.props.loadProductsByFilterForMap(() => {
      hideSpinner();
      this.setState({ isFilterModalVisible: false });
    });
  }

  onModalBackdropPress() {
    this.setState({ isFilterModalVisible: false });
  }

  updateCurrentLocation() {
    if (this.listProductsOnMapComponent) {
      this.listProductsOnMapComponent.reloadCurrentLocation();
    }
  }

  onShowSearchBarProductOnMap() {
    requestAnimationFrame(() => {
      this.setState({ tabProductShowMap: false });
      requestAnimationFrame(() => {
        this.listProductsComponent.showSearchBar();
      });
    });
  }

  render() {
    console.log('poi flow login global.logged:', this.props.token);
    return (
      <TabBar>
        <TabBar.Item icon={home} selectedIcon={homeSelected} title="Tin tức">
          <AsyncStore navigation={this.props.navigation} />
        </TabBar.Item>
        <TabBar.Item icon={rewards} selectedIcon={rewardsSelected} title="Điểm thưởng">
          <AsyncReward navigation={this.props.navigation} />
        </TabBar.Item>
        <TabBar.Item title="Đặt hàng">
          <AsyncCategory navigation={this.props.navigation} />
        </TabBar.Item>
        <TabBar.Item icon={store} selectedIcon={storeSelected} title="Cửa hàng">
          <AsyncMap navigation={this.props.navigation} />
        </TabBar.Item>
        <TabBar.Item icon={user} selectedIcon={userSelected} title="Tài khoản">
        {
          this.props.token === ''
          // || !this.props.userData
          ?
          <AsyncPreLogin navigation={this.props.navigation} isFromMainComponent /> :
          <AsyncProfile navigation={this.props.navigation} />
        }
        </TabBar.Item>
      </TabBar>
    );
  }
}

function mapStateToProps(state) {
  return {
    listConnectedUserIds: state.conversation.list_connected_user_id,
    token: state.user.token,
    userData: state.user.userData,
    radiusFilter: state.filter.radius,
    listConversation: state.conversation.list_conversation || [],
    listExport: state.wallet.listExport,
    listImport: state.wallet.listImport,
    listdrawmoney: state.drawmoney.listdrawmoney,
    listPosts: state.post.listPosts,
    transactionList: state.transaction.transactionList,
    buyTransactionList: state.transaction.buyTransactionList,
    sellTransactionList: state.transaction.sellTransactionList,
    listOtherGroups: state.group.listOtherGroups
  };
}

const mapActionCreators = {
  loadProductsByFilterForMap,
  changeNewRadius,
  loadConversation,
  addNewMessageToConversation,
  addNewConversation,
  changeNewTradeTypeForMap,
  changeIsFilterByRadius,
  reloadProductByRadius,
  changeNewCategoryForMap,
  loadInterestedConversation,
  updateUserInfo,
  checkBlockSystem
};

export default connect(
  mapStateToProps,
  mapActionCreators,
  null,
  { withRef: true }
)(MainComponent);
