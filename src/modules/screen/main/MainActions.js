import DeviceInfo from 'react-native-device-info';
import request from '../../../utils/request';
import {
  BASE_URL,
  USER_API,
  THEMEID,
  UPDATE_SETTING_DATA,
  BASE_URL_TOCOTOCO,
  UPDATE_CURRENT_LOCATION,
  UPDATE_REGION_DATA,
  BLOG,
  GOOGLE_API,
  BASE_URL_FBS,
  UPDATE_LIST_COLLECTION
} from '../../../constants/Constants';

// export function getUserLocale(ip, token) {
//     return new Promise((resolve, reject) => {
//         request
//             .post(`${BASE_URL}${USER_API}/get_geoip/${ip}`)
//             .set('Authorization', `Bearer ${token}`)
//             .finish((err, res) => {
//                 if (!err && res && res.body.code === 200) {
//                     resolve(res.body.results.object.country);
//                 } else {
//                     reject(err);
//                 }
//             });
//     });
// }

export function loadCurrentLocation() {
    return new Promise((resolve, reject) => {
        if (!DeviceInfo.isEmulator()) {
            try {
                navigator.geolocation.getCurrentPosition(
                    position => {
                        resolve({
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude
                        });
                    },
                    (err) => { reject(err); },
                    {
                        enableHighAccuracy: false
                    }
                );
            } catch (err) {
                reject(err);
            }
        } else {
            resolve({
                latitude: 0,
                longitude: 0,
            });
        }
    });
}

export function getUserLocale(token) {
  return new Promise((resolve, reject) => {
    request
      .get(`${BASE_URL}${USER_API}/get_geoip`)
      .set('Authorization', `Bearer ${token}`)
      .finish((err, res) => {
        if (!err && res && res.body.code === 200) {
          resolve(res.body.results.object);
          console.log('Hoang log get locale', res.body.results.object);
        } else {
          reject(err);
        }
      });
  });
}

export function loadListSettingData(currentLocation, onDoneFunc = isSuccess => {}) {
  return (dispatch, store) => {
    const url = `${BASE_URL_TOCOTOCO}/?view=settings.app.json&themeid=${THEMEID}`;
    request.get(url)
    .set('X-Tocotoco-Region', store().user.currentLocation.region)
    .finish((err, res) => {
      const response = JSON.parse(res && res.text ? res.text : undefined);
      // console.log('poi loadListSettingData res :', res);
      // console.log('poi loadListSettingData response :', response);
      if (!err && response && response.mobile) {
        dispatch({
          type: UPDATE_SETTING_DATA,
          payload: response
        });
        console.log('poi test: response.mobile[currentLocation.region]', response.mobile[currentLocation.region]);
        dispatch({
          type: UPDATE_REGION_DATA,
          payload: response.mobile[currentLocation.region]
        });
        dispatch({
          type: UPDATE_LIST_COLLECTION,
          payload: response.mobile[currentLocation.region].menu_screen.list_collections
        });
        onDoneFunc(true);
      } else {
        onDoneFunc(false);
      }
    });
  };
}

export function getLocation(onDoneFunc = (isSuccess, region) => {}) {
  return (dispatch, store) => {
    const url = `${BASE_URL_FBS}/location/check`;
    request.get(url)
    .finish((err, res) => {
      // console.log('poi loadlocation res', res);
      if (!err && res && res.body) {
        dispatch({
          type: UPDATE_CURRENT_LOCATION,
          payload: res.body
        });
        onDoneFunc(true, res.body);
      } else {
        onDoneFunc(false, undefined);
      }
    });
  };
}

export function updateUserLocation(currentLocation, onDoneFunc = () => {}) {
  return (dispatch, store) => {
    dispatch({
      type: UPDATE_CURRENT_LOCATION,
      payload: currentLocation
    });
    onDoneFunc();
  };
}
