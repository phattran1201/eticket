import React from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import HeadedWithBackButtonComponent from '../../view/HeaderWithBackButtonComponent';
import MyComponent from '../../view/MyComponent';
import STouchableOpacity from '../../view/STouchableOpacity';
import BaseHeader from '../../view/BaseHeader';

class TermsConditionsComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      nickname: '',
      age: '',
      introduce: '',
      isLoading: false,
      value: 0
    };
  }
  render() {
    return (
      <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
        <BaseHeader
          children={<Text style={style.titleHeader}>TERMS CONDITIONS</Text>}
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
        />
        <View style={styles.termsImageContainer}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Image
              style={{
                maxWidth: 327 * SCALE_RATIO_WIDTH_BASIS,
                height: 133 * SCALE_RATIO_WIDTH_BASIS,
                marginHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS - DEVICE_WIDTH / 125,
                marginBottom: 10 * SCALE_RATIO_HEIGHT_BASIS - DEVICE_HEIGHT / 224
              }}
              resizeMode="contain"
              source={require('../../../assets/imgs/intro/intro_1.png')}
            />
          </View>
          <View>
            <Text style={style.textCaption}>
              (Our service) connects you to a world with no boundaries, and brings people together and helps you build
              new connections (Our service) will be suspended for prostitution, sexual act , and Advertising other Apps
            </Text>
          </View>
        </View>
        <View style={{ flex: 1 }}>
          <STouchableOpacity
            style={styles.termsItemContainer}
            onPress={() =>
              this.props.navigation.navigate(ROUTE_KEY.TERMSCONDITIONSWEBVIEW, {
                name: 'Terms of Services',
                uri: 'https://termsfeed.com/privacy-policy/cdb387d60ff1377a43250ae682690806'
              })
            }
          >
            <View style={{ flex: 12 }}>
              <Text style={[style.text, { marginLeft: 15 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540 }]}>
                {strings.terms_of_services}
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Icon
                name={'chevron-right'}
                size={20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540}
                color={'#C7AE6D'}
              />
            </View>
          </STouchableOpacity>
          <STouchableOpacity
            style={styles.termsItemContainer}
            onPress={() =>
              this.props.navigation.navigate(ROUTE_KEY.TERMSCONDITIONSWEBVIEW, {
                name: 'Privacy Statement',
                uri: 'https://termsfeed.com/privacy-policy/cdb387d60ff1377a43250ae682690806'
              })
            }
          >
            <View style={{ flex: 12 }}>
              <Text style={[style.text, { marginLeft: 15 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540 }]}>
                {strings.privacy_statement}
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Icon
                name={'chevron-right'}
                size={20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540}
                color={'#C7AE6D'}
              />
            </View>
          </STouchableOpacity>
          <STouchableOpacity
            style={styles.termsItemContainer}
            onPress={() =>
              this.props.navigation.navigate(ROUTE_KEY.TERMSCONDITIONSWEBVIEW, {
                name: 'Location-Based Services Terms and Conditions',
                uri: 'https://termsfeed.com/privacy-policy/cdb387d60ff1377a43250ae682690806'
              })
            }
          >
            <View style={{ flex: 12 }}>
              <Text style={[style.text, { marginLeft: 15 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540 }]}>
                {strings.locations_based}
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Icon
                name={'chevron-right'}
                size={20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540}
                color={'#C7AE6D'}
              />
            </View>
          </STouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const mapActionCreators = {};

export default connect(
  null,
  mapActionCreators
)(TermsConditionsComponent);

const styles = StyleSheet.create({
  termsItemContainer: {
    flexDirection: 'row',
    borderBottomWidth: SCALE_RATIO_WIDTH_BASIS,
    borderColor: '#AE92D350',
    padding: 20 * SCALE_RATIO_HEIGHT_BASIS,
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  termsImageContainer: {
    borderBottomWidth: SCALE_RATIO_WIDTH_BASIS,
    borderColor: '#AE92D350',
    padding: 20 * SCALE_RATIO_HEIGHT_BASIS
  }
});
