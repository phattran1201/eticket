import React from 'react';
import { Dimensions, Image, Platform, StatusBar, StyleSheet, Text, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  FS,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import MyButton from '../../../style/MyButton';
import MyTextInput from '../../../style/MyTextInput';
import { alert } from '../../../utils/alert';
import { getNumberWithCommas } from '../../../utils/numberUtils';
import MyComponent from '../../view/MyComponent';
import Dropdown from '../../view/MyDropDown/dropdown';
import PopupNotification from '../../view/PopupNotification';
import { exportRequest } from '../exchange/ExchangeActions';
import SuccessWithdrawComponent from '../successwithdraw/SuccessWithdrawComponent';

const { width } = Dimensions.get('window');

class PaypalComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      paypalName: '',
      paypalId: '',
      cash: 0,
      isLoading: false
    };
    this.exchangeUnit = [200, 500, 1000, 5000, 10000];
  }

  render() {
    const { paypalName, paypalId, cash, isLoading } = this.state;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#ffff" barStyle="dark-content" />
        <KeyboardAwareScrollView style={{ flex: 1 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                paddingVertical: 15 * SCALE_RATIO_HEIGHT_BASIS
              }}
            >
              <Image
                resizeMode={'contain'}
                source={require('../../../assets/imgs/icons/coin.png')}
                style={{
                  height: 28 * SCALE_RATIO_HEIGHT_BASIS,
                  width: 28 * SCALE_RATIO_WIDTH_BASIS,
                  marginRight: 15 * SCALE_RATIO_WIDTH_BASIS
                }}
              />
              <Text
                style={{
                  color: '#282828',
                  fontSize: 16 * SCALE_RATIO_WIDTH_BASIS,
                  fontFamily: 'Helvetica Neue'
                }}
              >
                {this.props.userData.cash} {strings.cash}
              </Text>
            </View>
            {/* <View
                style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}
              >
                <Image
                  resizeMode={'contain'}
                  source={require('../../../assets/imgs/icons/won.png')}
                  style={{
                    height: 32 * SCALE_RATIO_HEIGHT_BASIS,
                    width: 32 * SCALE_RATIO_WIDTH_BASIS,
                    marginRight: 23 * SCALE_RATIO_WIDTH_BASIS
                  }}
                />
                <Text
                  style={{
                    color: '#282828',
                    fontSize: 17 * SCALE_RATIO_WIDTH_BASIS,
                    fontFamily: 'Helvetica Neue'
                  }}
                >
                  35200 {strings.won}
                </Text>
              </View> */}
            <View
              style={{
                height: 1 * SCALE_RATIO_HEIGHT_BASIS,
                backgroundColor: '#C7AE6D',
                opacity: 0.2,
                width: (DEVICE_WIDTH * 8) / 10
              }}
            />
          </View>
          <View
            style={{
              alignItems: 'center',
              marginHorizontal: 40 * SCALE_RATIO_WIDTH_BASIS,
              marginTop: 10 * SCALE_RATIO_HEIGHT_BASIS
            }}
          >
            <MyTextInput
              styleContent={{ elevation: 2 }}
              leftText={strings.name}
              placeholder={strings.name}
              value={this.state.paypalName}
              onChangeText={paypalName => this.setState({ paypalName })}
              returnKeyType="next"
              onSubmitEditing={() => this.paypalId.focus()}
            />
            <MyTextInput
              ref={instance => (this.paypalId = instance)}
              styleContent={{ elevation: 2 }}
              leftText={strings.id}
              placeholder={strings.id}
              value={this.state.paypalId}
              onChangeText={paypalId => this.setState({ paypalId })}
              keyboardType="number-pad"
            />
          </View>
          <View
            style={{
              marginHorizontal: 40 * SCALE_RATIO_WIDTH_BASIS,
              marginVertical: 20 * SCALE_RATIO_HEIGHT_BASIS
            }}
          >
            <Text
              style={{
                color: '#282828',
                fontSize: 16 * SCALE_RATIO_WIDTH_BASIS,
                fontFamily: 'Helvetica Neue',
                fontWeight: '500',
                marginBottom: 10 * SCALE_RATIO_HEIGHT_BASIS,
                textDecorationLine: 'underline',
                textDecorationColor: '#282828'
              }}
            >
              {strings.top_up}
            </Text>

            <View style={[style.viewInput, { justifyContent: 'center', alignItems: 'center' }]}>
              <Image
                resizeMode={'contain'}
                source={require('../../../assets/imgs/icons/coin.png')}
                style={{
                  height: 26 * SCALE_RATIO_HEIGHT_BASIS,
                  width: 26 * SCALE_RATIO_WIDTH_BASIS,
                  marginRight: 16 * SCALE_RATIO_WIDTH_BASIS
                }}
              />
              <Dropdown
                pickerStyle={{
                  width: 169 * SCALE_RATIO_WIDTH_BASIS,
                  borderColor: Platform.OS === 'ios' ? '#AE92D330' : '#70707010',
                  borderWidth: 1,
                  flexDirection: 'row',
                  borderRadius: (31 * SCALE_RATIO_HEIGHT_BASIS) / 2
                }}
                overlayStyle={{ borderTopRightRadius: 0 }}
                textColor="#C7AE6D"
                fontSize={FS(14)}
                value={this.state.cash}
                itemTextStyle={(style.text, { color: '#C7AE6D', marginBottom: SCALE_RATIO_WIDTH_BASIS })}
                dropdownOffset={{
                  top: 30 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
                  left: 30 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
                }}
                data={[
                  {
                    value: '200',
                    onSelect: onDone => {
                      this.setState({ cash: 200 });
                    }
                  },
                  {
                    value: '500',
                    onSelect: onDone => {
                      this.setState({ cash: 500 });
                    }
                  },
                  {
                    value: '1000',
                    onSelect: onDone => {
                      this.setState({ cash: 1000 });
                    }
                  },
                  {
                    value: '5000',
                    onSelect: onDone => {
                      this.setState({ cash: 5000 });
                    }
                  },
                  {
                    value: '10000',
                    onSelect: onDone => {
                      this.setState({ cash: 10000 });
                    }
                  }
                ]}
              >
                <View style={[styles.dropdownContainer, { width: 180 * SCALE_RATIO_WIDTH_BASIS }]}>
                  <Text style={[style.textInput, { color: '#C7AE6D' }]} numberOfLines={1}>
                    {this.state.cash}
                  </Text>
                  <MaterialIcons name="arrow-drop-down" size={35 * SCALE_RATIO_WIDTH_BASIS} color="#C7AE6D" />
                </View>
              </Dropdown>
            </View>
            <View
              style={[
                style.viewInput,
                {
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  marginTop: 10 * SCALE_RATIO_HEIGHT_BASIS
                }
              ]}
            >
              <Image
                resizeMode={'contain'}
                source={require('../../../assets/imgs/icons/won.png')}
                style={{
                  height: 26 * SCALE_RATIO_HEIGHT_BASIS,
                  width: 26 * SCALE_RATIO_WIDTH_BASIS,
                  marginRight: 25 * SCALE_RATIO_WIDTH_BASIS
                }}
              />
              <Text style={(style.textInput, { color: '#282828' })}>{getNumberWithCommas(this.state.cash * 100)}</Text>
            </View>
          </View>
          <MyButton
            style={{ alignSelf: 'center', marginBottom: 10 * SCALE_RATIO_HEIGHT_BASIS }}
            width={210 * SCALE_RATIO_WIDTH_BASIS}
            outline={this.state.paypalName === '' || this.state.paypalId === ''}
            isDisabled={this.state.paypalName === '' || this.state.paypalId === ''}
            isLoading={this.state.isLoading}
            onPress={() => {
              //FIXME for testing, should uncomment the below code for the project
              if (cash < 200) {
                alert(strings.alert, strings.please_topup_at_least_200_cash);
                return;
              }
              if (this.state.paypalName === '' || this.state.paypalId === '') {
                alert(strings.alert, strings.please_input_all_info);
                return;
              }
              this.setState({ isLoading: true });
              this.props.exportRequest(
                {
                  amount: cash,
                  type: 'PAYPAL',
                  paypal_name: paypalName,
                  paypal_id: paypalId
                },
                () => {
                  PopupNotification.showComponent(
                    <SuccessWithdrawComponent
                      onButtonClick={() => {
                        PopupNotification.hide();
                        this.setState({ isLoading: false });
                      }}
                    />
                  );
                },
                () => this.setState({ isLoading: false })
              );
            }}
          >
            {strings.refund_application.toLocaleUpperCase()}
          </MyButton>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userData: state.user.userData
});

const mapActionCreators = {
  exportRequest
};

export default connect(
  mapStateToProps,
  mapActionCreators
)(PaypalComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  avatarImage: {
    width,
    height: width
  },
  infoContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    position: 'absolute',
    bottom: 0
  },
  flagInfoImage: {
    width: 50 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    height: 40 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },
  textInfo: {
    color: '#fff',
    fontSize: 17 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },
  moreDetailContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },
  moreDetailText: {
    fontSize: 14 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    marginLeft: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },
  buttonContainer: {
    flex: 1,
    borderWidth: 1 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    borderColor: '#000',
    padding: 20 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    flexDirection: 'row',
    margin: 10 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    alignItems: 'center',
    justifyContent: 'center'
  },
  requestButtonContainer: {
    padding: 15 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    margin: 15 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#000',
    borderWidth: 1 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    borderRadius: 10 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
  },
  headerContainer: {
    backgroundColor: '#6d77f7',
    height:
      Platform.OS === 'ios'
        ? 50 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540
        : 18 * SCALE_RATIO_HEIGHT_BASIS -
          DEVICE_HEIGHT / 224 -
          getStatusBarHeight(true) -
          3 * SCALE_RATIO_HEIGHT_BASIS -
          DEVICE_HEIGHT / 224,
    paddingTop: Platform.OS === 'ios' ? getStatusBarHeight() : 0,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  headerText: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue',
    fontSize: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: 'bold',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  },
  dropdownContainer: {
    justifyContent: 'space-between',
    // paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 31 * SCALE_RATIO_HEIGHT_BASIS
  }
});
