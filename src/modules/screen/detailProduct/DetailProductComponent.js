import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import CheckBox from 'react-native-check-box';
import Accordion from 'react-native-collapsible/Accordion';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Feather from 'react-native-vector-icons/dist/Feather';
import {
  YouTubeStandaloneAndroid,
  YouTubeStandaloneIOS
} from 'react-native-youtube';
import { connect } from 'react-redux';
import {
  DEVICE_WIDTH,
  FS,
  SCALE_RATIO_WIDTH_BASIS,
  YOUTUBE_API
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style, { APP_COLOR, APP_COLOR_TEXT } from '../../../constants/style';
import { alert } from '../../../utils/alert';
import BaseHeader from '../../view/BaseHeader';
import {
  getImagesAndDescriptionProductInfoData,
  getProductData
} from '../menu/MenuAction';
// import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import RadioForm from '../../view/react-native-simple-radio-button/lib/SimpleRadioButton';
import {
  getPriceWithCommas,
  getNumberWithCommas
} from '../../../utils/numberUtils';
import MySpinner from '../../view/MySpinner';
import LazyLoadComponent from '../../view/LazyLoadComponent';

const ACTION_TYPE = {
  SELECT_TEMPERATURE: 'SELECT_TEMPERATURE',
  SELECT_SIZE: 'SELECT_SIZE',
  SELECT_ICE: 'SELECT_ICE',
  SELECT_SUGAR: 'SELECT_SUGAR'
};

const playYoutubeVideoImg = require('../../../assets/imgs/play_video.png');

export class DetailProductComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.product = params && params.product ? params.product : {};

    // const productTemp = this.chooseProductToShowDependOnInput(this.props.detailProductInfo.variants, 'M', 'C');
    this.state = {
      loading: true,
      productVariantsInfo: {},
      amount: 1,
      finalPrice: 0,
      activeSections: [0, 1, 2, 3, 4],
      activeSlide: 0,
      selectedIce: null,
      selectedSugar: null,
      selectedSize: null,
      selectedTemperature: null,
      dataExtraBonusForProduct: []
      // productVariantsInfo: productTemp,
      // amount: 1,
      // finalPrice: productTemp.price,
      // activeSections: [0, 1, 2, 3, 4],
      // activeSlide: 0,
      // selectedIce: this.getListIcesPicker(productTemp) && this.getListIcesPicker(productTemp)[0] ? this.getListIcesPicker(productTemp)[0].id : null,
      // selectedSugar: this.getListSugarsPicker(productTemp) && this.getListSugarsPicker(productTemp)[0] ? this.getListSugarsPicker(productTemp)[0].id : null,
      // selectedSize: 'M',
      // selectedTemperature: 'C',
      // dataExtraBonusForProduct: this.updateDataExtra(productTemp)
    };
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const product = params && params.product ? params.product : {};
    console.log('poi flow detail product 11this.props.detailProductInfo:', this.props.detailProductInfo);
    if (this.props.detailProductInfo.id === product.id) {
      const tempProduct =
        this.props.detailProductInfo &&
        this.props.detailProductInfo.variants &&
        this.props.detailProductInfo.variants.length
          ? this.props.detailProductInfo.variants[0]
          : {};
      this.setState({
        loading: false,
        productVariantsInfo: tempProduct,
        finalPrice:
          this.props.detailProductInfo &&
          this.props.detailProductInfo.variants &&
          this.props.detailProductInfo.variants[0]
            ? this.props.detailProductInfo.variants[0].price
            : undefined,
        amount: 1,
        selectedIce:
          this.getListIcesPicker(tempProduct) &&
          this.getListIcesPicker(tempProduct)[0]
            ? this.getListIcesPicker(tempProduct)[0].id
            : null,
        selectedSugar:
          this.getListSugarsPicker(tempProduct) &&
          this.getListSugarsPicker(tempProduct)[0]
            ? this.getListSugarsPicker(tempProduct)[0].id
            : null,
        selectedSize: 'M',
        selectedTemperature: 'C',
        dataExtraBonusForProduct: this.updateDataExtra(tempProduct)
      });
    } else {
      this.props.getProductData(product.id, isSuccessGetData => {
        this.props.getImagesAndDescriptionProductInfoData(
          product.haravanUrl,
          isSuccessGetImagesAndDescription => {
            if (isSuccessGetData && isSuccessGetImagesAndDescription) {
              console.log('poi flow detail product is success this.props.detailProductInfo:', this.props.detailProductInfo);
              const tempProduct =
                this.props.detailProductInfo &&
                this.props.detailProductInfo.variants &&
                this.props.detailProductInfo.variants.length
                  ? this.props.detailProductInfo.variants[0]
                  : {};
              this.setState({
                loading: false,
                productVariantsInfo: tempProduct,
                finalPrice:
                  this.props.detailProductInfo &&
                  this.props.detailProductInfo.variants &&
                  this.props.detailProductInfo.variants[0]
                    ? this.props.detailProductInfo.variants[0].price
                    : undefined,
                amount: 1,
                selectedIce:
                  this.getListIcesPicker(tempProduct) &&
                  this.getListIcesPicker(tempProduct)[0]
                    ? this.getListIcesPicker(tempProduct)[0].id
                    : null,
                selectedSugar:
                  this.getListSugarsPicker(tempProduct) &&
                  this.getListSugarsPicker(tempProduct)[0]
                    ? this.getListSugarsPicker(tempProduct)[0].id
                    : null,
                selectedSize: 'M',
                selectedTemperature: 'C',
                dataExtraBonusForProduct: this.updateDataExtra(tempProduct)
              });
            } else {
              alert(strings.alert, strings.network_require_fail, () =>
                this.props.navigation.goBack()
              );
            }
          }
        );
      });
    }
  }

  onSelectedItemPicker = (
    action,
    selectedItem,
    onDoneFunc = isSuccess => {}
  ) => {
    // const { token } = this.props;
    switch (action) {
      case ACTION_TYPE.SELECT_SIZE:
        const tempPrice = this.state.finalPrice;
        // const price = selectedItem && selectedItem === 'L' ? 6000 : 0;
        const price = 6000;

        this.temp = this.chooseProductToShowDependOnInput(
          this.props.detailProductInfo.variants,
          selectedItem,
          this.state.selectedTemperature
        );
        if (this.temp) {
          this.setState({ productVariantsInfo: this.temp });
        }
        if (selectedItem === 'M') {
          this.setState({
            selectedSize: selectedItem,
            finalPrice: tempPrice - price
          });
        } else if (selectedItem === 'L') {
          this.setState({
            selectedSize: selectedItem,
            finalPrice: tempPrice + price
          });
        }
        onDoneFunc(true);
        break;
      case ACTION_TYPE.SELECT_TEMPERATURE:
        this.temp = this.chooseProductToShowDependOnInput(
          this.props.detailProductInfo.variants,
          this.state.selectedSize,
          selectedItem
        );
        if (this.temp) {
          this.setState({ productVariantsInfo: this.temp });
        }
        this.setState(
          {
            selectedTemperature: selectedItem
          },
          () => {
            onDoneFunc(true);
          }
        );
        break;
      case ACTION_TYPE.SELECT_ICE:
        this.setState({ selectedIce: selectedItem });
        onDoneFunc(true);
        break;
      case ACTION_TYPE.SELECT_SUGAR:
        this.setState({ selectedSugar: selectedItem });
        onDoneFunc(true);
        break;
      default:
        return;
    }
  };

  getListSugarsPicker = product => {
    const temp = product.customizations.find(e => e.id === 'YCK');
    if (temp) {
      return temp.options
        .filter(e => e.name.includes('đường'))
        .filter(e => !e.hidden);
    }
    return undefined;
  };

  getListIcesPicker = product => {
    const temp = product.customizations.find(e => e.id === 'YCK');
    if (temp) {
      return temp.options
        .filter(e => e.name.includes('đá'))
        .filter(e => !e.hidden);
    }
    return undefined;
  };

  getListToppingPicker = product => {
    const temp = product.customizations.find(e => e.id === 'TP');
    if (temp) {
      return temp.options.filter(e => !e.hidden);
    }
    return undefined;
  };

  chooseProductToShowDependOnInput = (products, size, type) => {
    const filteredSize = products.filter(e => e.size === size); // tìm ra theo size
    if (type === 'N') {
      const temp = filteredSize.find(e => e.name.includes('nóng'));
      this.updateDataExtra(temp);
      return temp;
    } else {
      const temp = filteredSize.find(e => !e.name.includes('nóng'));
      this.updateDataExtra(temp);
      return temp;
    }
  };

  updateDataExtra = product => {
    MySpinner.show();
    const listSugars = this.getListSugarsPicker(product);
    const listIces = this.getListIcesPicker(product);
    const listTopping = this.getListToppingPicker(product);
    const sizePicker = {
      index: 0,
      name: 'Size',
      type: 'radio_button',
      data: [
        { label: 'Size M(475ml)', value: 'M' },
        {
          label: 'Size L(600ml)',
          value: 'L',
          extraData: '+6,000đ',
          price: 6000
        }
      ],
      onPress: value =>
        this.onSelectedItemPicker(ACTION_TYPE.SELECT_SIZE, value)
    };
    const temperaturePicker = {
      index: 1,
      name: 'Temperature',
      type: 'radio_button',
      data: [
        { label: 'Lạnh', value: 'C' },
        {
          label: 'Nóng',
          value: 'N'
        }
      ],
      onPress: value =>
        this.onSelectedItemPicker(ACTION_TYPE.SELECT_TEMPERATURE, value)
    };
    const temp = [];
    temp.push(sizePicker);
    temp.push(temperaturePicker);
    let index = 2;
    if (listSugars) {
      temp.push({
        index,
        name: 'Sugar',
        type: 'radio_button',
        data: listSugars.map(e => ({ label: e.name, value: e.id })),
        onPress: value =>
          this.onSelectedItemPicker(ACTION_TYPE.SELECT_SUGAR, value)
      });
      index++;
    }
    if (listIces) {
      temp.push({
        index,
        name: 'Ice',
        type: 'radio_button',
        data: listIces.map(e => ({ label: e.name, value: e.id })),
        onPress: value =>
          this.onSelectedItemPicker(ACTION_TYPE.SELECT_ICE, value)
      });
      index++;
    }
    if (listTopping) {
      temp.push({
        index: 4,
        name: 'Topping',
        type: 'check_box',
        data: listTopping.map(e => ({
          name: e.name,
          id: e.id,
          price: e.price,
          price_str: getPriceWithCommas(e.price),
          isChecked: false
        }))
      });
      index++;
    }
    setTimeout(() => {
      MySpinner.hide();
      this.setState({ dataExtraBonusForProduct: temp });
    }, 1000);
  };

  updateSections = sections => {
    this.setState({
      activeSections: sections.includes(undefined) ? [] : sections
    });
  };

  renderGalleryItem = ({ item, index }) => {
    // if (item.type === 'image') {
    return (
      <View
        style={[
          { flex: 1, padding: 3 * SCALE_RATIO_WIDTH_BASIS, elevation: 3 },
          style.shadow
        ]}
      >
        <View
          style={{
            borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            overflow: 'hidden',
            flex: 1
          }}
        >
          <Image
            style={{ width: '100%', height: '100%' }}
            source={{ uri: item }}
            resizeMode='cover'
          />
        </View>
      </View>
    );
    // }
    // return (
    //   <View style={{ flex: 1 }}>
    //     <TouchableOpacity onPress={() => this.renderYoutubeForAndroid()}>
    //       <Image
    //         style={{ width: '100%', height: '100%' }}
    //         source={{ uri: item }}
    //         resizeMode='cover'
    //       />
    //       {/* playYoutubeVideoImg */}
    //       <View
    //         style={{
    //           position: 'absolute',
    //           width: '100%',
    //           height: '100%',
    //           justifyContent: 'center',
    //           alignItems: 'center'
    //         }}
    //       >
    //         <View style={{ width: 50, height: 50 }}>
    //           <Image
    //             style={{ width: '100%', height: '100%' }}
    //             source={playYoutubeVideoImg}
    //             resizeMode='cover'
    //           />
    //         </View>
    //       </View>
    //     </TouchableOpacity>
    //   </View>
    // );
  };

  renderHeader = (section, index, isActive) => (
    <View
      style={{
        width: '100%',
        flexDirection: 'row',
        marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS,
        marginTop: 10 * SCALE_RATIO_WIDTH_BASIS
      }}
    >
      <View style={{ flex: 1 }}>
        <Text style={{ color: '#373737', fontSize: FS(17) }}>
          {section.name}
        </Text>
      </View>
      <View
        style={{
          width: 30 * SCALE_RATIO_WIDTH_BASIS,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        {isActive ? (
          <Feather
            name='chevron-down'
            size={20 * SCALE_RATIO_WIDTH_BASIS}
            color='#ABABAB'
          />
        ) : (
          <Feather
            name='chevron-right'
            size={20 * SCALE_RATIO_WIDTH_BASIS}
            color='#ABABAB'
          />
        )}
      </View>
    </View>
  );

  renderContent = (section, index, isActive) => {
    if (section.type === 'radio_button') {
      return (
        <View style={[style.shadow, { backgroundColor: '#ffffff' }]}>
          <RadioForm
            radio_props={section.data}
            initial={0}
            labelHorizontal
            buttonColor={APP_COLOR_TEXT}
            selectedButtonColor={APP_COLOR}
            animation
            buttonOuterSize={11 * SCALE_RATIO_WIDTH_BASIS}
            onPress={value2 => section.onPress(value2)}
            buttonSize={11 * SCALE_RATIO_WIDTH_BASIS}
            // labelStyle={[style.text, {
            //     fontSize: FS(14),
            // }]}
            style={{
              height: 38 * SCALE_RATIO_WIDTH_BASIS * section.data.length
            }}
            renderLabel={obj => (
              <View
                style={{
                  paddingLeft: 14 * SCALE_RATIO_WIDTH_BASIS,
                  flex: 1,
                  alignItems: 'center',
                  flexDirection: 'row'
                }}
              >
                <View style={{ flex: 1 }}>
                  <Text
                    style={[style.text, { fontSize: FS(14) }]}
                    numberOfLines={1}
                  >
                    {obj.label}
                  </Text>
                </View>
                <View
                  style={{
                    width: 100 * SCALE_RATIO_WIDTH_BASIS,
                    alignItems: 'flex-end'
                  }}
                >
                  <Text
                    style={[style.text, { fontSize: FS(14) }]}
                    numberOfLines={1}
                  >
                    {getPriceWithCommas(obj.price)}
                  </Text>
                </View>
              </View>
            )}
            radioStyle={{
              height: 35 * SCALE_RATIO_WIDTH_BASIS,
              alignItems: 'center',
              paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
              borderBottomWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
              borderColor: '#f0f0f0'
            }}
          />
        </View>
      );
    }
    return (
      <View style={[style.shadow, { backgroundColor: '#ffffff' }]}>
        {section.data.map(e => (
          <View
            style={{
              justifyContent: 'center',
              height: 35 * SCALE_RATIO_WIDTH_BASIS,
              paddingHorizontal: 5 * SCALE_RATIO_WIDTH_BASIS,
              borderBottomWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
              borderColor: '#f0f0f0'
            }}
          >
            <CheckBox
              onClick={() => {
                e.isChecked = !e.isChecked;
                const tempPrice = this.state.finalPrice;
                const tempDataExceptThisSection = this.state.dataExtraBonusForProduct.filter(
                  e2 => section.index !== e2.index
                );
                this.setState({
                  dataExtraBonusForProduct: [
                    ...tempDataExceptThisSection,
                    { ...section }
                  ],
                  finalPrice: e.isChecked
                    ? tempPrice + e.price
                    : tempPrice - e.price
                });
              }}
              isChecked={e.isChecked}
              checkedImage={
                <View
                  style={{
                    width: 15 * SCALE_RATIO_WIDTH_BASIS,
                    height: 15 * SCALE_RATIO_WIDTH_BASIS,
                    borderRadius: 2 * SCALE_RATIO_WIDTH_BASIS,
                    backgroundColor: APP_COLOR
                  }}
                />
              }
              unCheckedImage={
                <View
                  style={{
                    width: 15 * SCALE_RATIO_WIDTH_BASIS,
                    height: 15 * SCALE_RATIO_WIDTH_BASIS,
                    borderRadius: 2 * SCALE_RATIO_WIDTH_BASIS,
                    borderWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
                    borderColor: '#808080'
                  }}
                />
              }
              checkedCheckBoxColor={APP_COLOR_TEXT}
              rightTextView={
                <View style={{ flex: 1, marginLeft: 10 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                      <Text
                        style={[style.text, { fontSize: FS(14) }]}
                        numberOfLines={1}
                      >
                        {e.name}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: 100 * SCALE_RATIO_WIDTH_BASIS,
                        alignItems: 'flex-end'
                      }}
                    >
                      <Text
                        style={[style.text, { fontSize: FS(14) }]}
                        numberOfLines={1}
                      >
                        {getPriceWithCommas(e.price)}
                      </Text>
                    </View>
                  </View>
                </View>
              }
            />
          </View>
        ))}
      </View>
    );
  };

  renderYoutubeForAndroid = () => {
    if (Platform.OS === 'android') {
      YouTubeStandaloneAndroid.playVideo({
        apiKey: YOUTUBE_API, // Your YouTube Developer API Key
        videoId: 'KVZ-P-ZI6W4', // YouTube video ID
        autoplay: true, // Autoplay the video
        startTime: 0 // Starting point of video (in seconds)
      })
        .then(() => console.log('Standalone Player Exited'))
        .catch(errorMessage => console.error(errorMessage));
    } else {
      YouTubeStandaloneIOS.playVideo('KVZ-P-ZI6W4')
        .then(() => console.log('Standalone Player Exited'))
        .catch(errorMessage => console.error(errorMessage));
    }
  };

  render() {
    if (this.state.loading) {
      return <LazyLoadComponent />;
    }
    // const { title, price, compare_at_price, sale, images, id, description } = this.props.detailProductInfo;
    const { description, images } = this.props.detailProductInfo;
    console.log(
      'poi flow detail product this.props.detailProductInfo:',
      this.props.detailProductInfo
    );
    console.log(
      'poi flow product detail productVariantsInfo:',
      this.state.productVariantsInfo
    );
    const { productVariantsInfo } = this.state;
    const title =
      productVariantsInfo && productVariantsInfo.name
        ? productVariantsInfo.name
        : '';
    const price =
      productVariantsInfo && productVariantsInfo.price
        ? productVariantsInfo.price
        : 0;
    const minPrice = 0;
    const maxPrice = 0;
    return (
      <View style={{ flex: 1, backgroundColor: '#FBFBFB' }}>
        <ScrollView style={{ flex: 1 }}>
          <BaseHeader
            children={<Text />}
            leftIcon='x'
            leftIconType='Feather'
            onLeftPress={() => this.props.navigation.goBack()}
            rightIcon='heart'
            rightIconType='Feather'
            onRightPress={() =>
              alert(strings.alert, strings.this_feature_is_in_development)
            }
          />
          <View
            style={{
              height: 300 * SCALE_RATIO_WIDTH_BASIS,
              paddingTop: 20 * SCALE_RATIO_WIDTH_BASIS,
              paddingHorizontal: 30 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Carousel
                ref={c => {
                  this._carousel = c;
                }}
                autoplay
                autoplayInterval={5000}
                // data={this.productInfo.gallery}
                data={images}
                sliderWidth={DEVICE_WIDTH}
                layout='tinder'
                layoutCardOffset={0}
                itemWidth={300 * SCALE_RATIO_WIDTH_BASIS}
                renderItem={this.renderGalleryItem}
                onSnapToItem={index => this.setState({ activeSlide: index })}
              />
              <Pagination
                tappableDots
                carouselRef={this._carousel}
                dotsLength={images.length}
                activeDotIndex={this.state.activeSlide}
                containerStyle={{
                  // backgroundColor: 'rgba(0, 0, 0, 0.75)',
                  position: 'absolute',
                  left: 0 * SCALE_RATIO_WIDTH_BASIS
                }}
                dotStyle={{
                  width: 7 * SCALE_RATIO_WIDTH_BASIS,
                  height: 7 * SCALE_RATIO_WIDTH_BASIS,
                  borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
                  marginVertical: 2 * SCALE_RATIO_WIDTH_BASIS,
                  backgroundColor: 'rgba(255, 255, 255, 0.92)'
                }}
                vertical
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
              />
            </View>
          </View>
          <View
            style={{
              flex: 1,
              padding: 10 * SCALE_RATIO_WIDTH_BASIS,
              justifyContent: 'space-around'
            }}
          >
            <View style={{ marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS }}>
              <Text
                adjustsFontSizeToFit
                numberOfLines={1}
                style={[
                  style.textHeader,
                  {
                    fontWeight: '500',
                    fontSize: FS(20),
                    color: APP_COLOR_TEXT,
                    textAlign: 'justify'
                  }
                ]}
              >
                {title}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text
                  style={[
                    style.text,
                    {
                      fontWeight: '600',
                      color: APP_COLOR,
                      fontSize: FS(16)
                    }
                  ]}
                >
                  {getPriceWithCommas(price)}
                  {/* <Text
                    style={[
                      style.text,
                      {
                        fontWeight: 'normal',
                        color: '#525252',
                        fontSize: FS(14),
                        textDecorationLine: 'line-through'
                      }
                    ]}
                  >
                    {getPriceWithCommas(maxPrice)}
                  </Text> */}
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <Text
                style={[
                  style.text,
                  { color: APP_COLOR_TEXT, fontSize: FS(14) }
                ]}
              >
                {description}
              </Text>
            </View>
            {/* <WebView
              // source={{ html }}
              style={{
                height: 300 * SCALE_RATIO_WIDTH_BASIS,
                // width: DEVICE_WIDTH,
                flex: 1,
                // marginTop: 48 * SCALE_RATIO_HEIGHT_BASIS + getStatusBarHeight(true)
              }}
              javaScriptEnabled
              scalesPageToFit
            /> */}
            <Accordion
              containerStyle={{ marginBottom: 10 * SCALE_RATIO_WIDTH_BASIS }}
              duration={1000}
              underlayColor={'#F7F7F7'}
              activeSections={this.state.activeSections}
              expandMultiple
              sections={
                this.state.dataExtraBonusForProduct
                  ? this.state.dataExtraBonusForProduct.filter(
                      e => e && e.data && e.data.length > 0
                    )
                  : []
              }
              renderHeader={this.renderHeader}
              renderContent={this.renderContent}
              onChange={this.updateSections}
            />
          </View>
        </ScrollView>
        <View
          style={[
            style.shadow,
            {
              height: 100 * SCALE_RATIO_WIDTH_BASIS,
              paddingTop: 10 * SCALE_RATIO_WIDTH_BASIS,
              paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS
            }
          ]}
        >
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flexDirection: 'row', flex: 1 }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start'
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    if (this.state.amount >= 1) {
                      const amount = this.state.amount;
                      this.setState({ amount: amount + 1 });
                    }
                  }}
                >
                  <Feather
                    name='plus-circle'
                    size={22 * SCALE_RATIO_WIDTH_BASIS}
                    color='#799DD7'
                  />
                </TouchableOpacity>
                <View
                  style={{ marginHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS }}
                >
                  <Text
                    style={[
                      style.textCaption,
                      { color: '#282828', fontSize: FS(16) }
                    ]}
                  >
                    {getNumberWithCommas(this.state.amount)}
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    if (this.state.amount > 1) {
                      const amount = this.state.amount;
                      this.setState({ amount: amount - 1 });
                    }
                  }}
                >
                  <Feather
                    name='minus-circle'
                    size={22 * SCALE_RATIO_WIDTH_BASIS}
                    color='#799DD7'
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ alignItems: 'flex-end' }}>
              <TouchableOpacity
                onPress={() =>
                  alert(strings.alert, strings.this_feature_is_in_development)
                }
              >
                <View
                  style={{
                    backgroundColor: APP_COLOR,
                    borderRadius: 40 * SCALE_RATIO_WIDTH_BASIS,
                    width: 160 * SCALE_RATIO_WIDTH_BASIS,
                    height: 30 * SCALE_RATIO_WIDTH_BASIS,
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  <Text
                    style={[
                      style.textHeader,
                      {
                        fontSize: FS(16),
                        textAlign: 'center',
                        color: '#FFFFFF'
                      }
                    ]}
                  >
                    +{' '}
                    {getPriceWithCommas(
                      this.state.finalPrice * this.state.amount
                    )}
                    đ
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
// getImagesAndDescriptionProductInfoData
function mapStateToProps(state) {
  return {
    listCollections: state.menu.listCollections,
    detailProductInfo: state.menu.detailProductInfo
  };
}

const mapActionCreators = {
  getImagesAndDescriptionProductInfoData,
  getProductData
};

export default connect(
  mapStateToProps,
  mapActionCreators,
  null,
  { withRef: true }
)(DetailProductComponent);

const styles = StyleSheet.create({
  bodyContainer: {
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
