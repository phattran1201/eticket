import React, { Component } from 'react';
import { ActivityIndicator, View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import {
  ROUTE_KEY,
  SCALE_RATIO_WIDTH_BASIS,
  IS_IOS,
  SCALE_RATIO_HEIGHT_BASIS,
  DEVICE_WIDTH,
  BASE_URL_TOCOTOCO,
  DEVICE_HEIGHT
} from '../../../constants/Constants';
import LottieView from "lottie-react-native";
import { connect } from 'react-redux';
import style, { FONT } from '../../../constants/style';
import { DATA_TEST } from '../../../constants/dataTest';
import BaseHeader from '../../view/BaseHeader';
import { getDetailArticle } from './NewsAction';
import strings from '../../../constants/Strings';
import { alert } from '../../../utils/alert';
import STouchableOpacity from '../../view/STouchableOpacity';
import { loadListNewsArticleData, loadMoreListNewsArticleData } from '../store/StoreActions';

class NewsComponent extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
     this.state = {
      loading: false,
      refreshing: false,
    };
    this.title = params && params.title ? params.title : '';
    this.data = params && params.data ? params.data : [];
  }
  
  renderNewsItem = ({ item, index }) => (
    <STouchableOpacity
      onPress={() => {
        if (this.props.detailArticle.title === item.title) { //if user click the same item with previous clicked item, then we will get content of html and send to webview
          this.props.navigation.navigate(ROUTE_KEY.WEB_VIEW, {
            name: item.title,
            html: this.props.detailArticle.content
          });
        } else { 
          //user click different item with previous clicked item then we will get data and stored in redux and show it on webview
          //to reduces api call number AMAP.
          this.props.getDetailArticle(item.url, (isSuccess, response) => {
            this.props.navigation.navigate(ROUTE_KEY.WEB_VIEW, {
              name: item.title,
              html: response.content
            });
          });
        }
      }}
    >
      <View
        style={[
          style.shadow,
          {
            overflow: 'visible',
            borderRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            marginBottom: 15 * SCALE_RATIO_WIDTH_BASIS,
            flex: 1,
            backgroundColor: 'white',
            marginHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS
          }
        ]}
      >
        <View
          style={{
            borderTopLeftRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            borderTopRightRadius: 5 * SCALE_RATIO_WIDTH_BASIS,
            overflow: 'hidden'
          }}
        >
          <Image
            style={{
              width: '100%',
              height: 140 * SCALE_RATIO_WIDTH_BASIS
            }}
            source={{
              uri: item.image ? item.image : 'https://www.fancyhands.com/images/default-avatar-250x250.png'
            }}
          />
        </View>
        <View style={{ paddingHorizontal: 10 * SCALE_RATIO_WIDTH_BASIS, paddingBottom: 5 * SCALE_RATIO_WIDTH_BASIS }}>
          <Text
            style={[
              style.textHeader,
              {
                fontFamily: FONT.SemiBold,
                padding: 5 * SCALE_RATIO_WIDTH_BASIS,
                paddingBottom: 0
              }
            ]}
            numberOfLines={2}
          >
            {item.title}
          </Text>
          <Text
            style={[
              style.text,
              {
                padding: 5 * SCALE_RATIO_WIDTH_BASIS
              }
            ]}
            numberOfLines={3}
          >
            {item.note}
          </Text>
        </View>
      </View>
    </STouchableOpacity>
  );

  componentDidMount() {
    this.setState({ loading: true });
    this.props.loadListNewsArticleData(this.props.regionData.home_screen.section_article.blog_handle, (isSuccess) => {
      this.setState({ loading: false });
    });
}

handleLoadmore = () => {
  if (this.state.loading || this.props.newsArticlesPaginate.outOfData) return;
  this.setState({ loading: true });
  this.props.loadMoreListNewsArticleData(this.props.regionData.home_screen.section_article.blog_handle, this.props.newsArticlesPaginate.page, (isSuccess, newData, paginate) => {
      this.setState({ loading: false });  
  });
};

onRefresh = () => {
  this.setState({ refreshing: true });
  this.props.loadListNewsArticleData(this.props.regionData.home_screen.section_article.blog_handle, (isSuccess) => {
      this.setState({ refreshing: false });
  });
}

renderFooter = () => {
  if (this.props.newsArticlesPaginate.outOfData) {
      return null;
    }
    if (this.props.listNewsArticles.length === 0) {
      return (
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            alignContent: "center",
            width: DEVICE_WIDTH,
            height: (DEVICE_HEIGHT * 60) / 100
          }}
        >
          <LottieView
            source={require("../../../assets/isempty.json")}
            autoPlay
            loop
            hardwareAccelerationAndroid
            style={{
              width: 211 * SCALE_RATIO_WIDTH_BASIS,
              height: 206 * SCALE_RATIO_WIDTH_BASIS,
              alignSelf: "center"
            }}
            resizeMode="cover"
          />
        </View>
      );
    }

    return (
      !this.props.newsArticlesPaginate.outOfData &&
        this.props.listNewsArticles.length !== 0 &&
        this.state.loading ? (
          <View
            style={{
              paddingTop: 8 * SCALE_RATIO_WIDTH_BASIS,
              paddingBottom: 15 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <ActivityIndicator animating size="small" />
          </View>
        ) : <View />
    );
  }
  
  render() {
    return (
      <View style={{ backgroundColor: '#FBFBFB', flex: 1 }}>
        <BaseHeader
          styleContent={{ backgroundColor: '#fff' }}
          children={<Text style={style.titleHeader}>{this.title}</Text>}
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
        />
        <FlatList
          style={{
            paddingTop: 10 * SCALE_RATIO_WIDTH_BASIS,
          }}
          data={this.props.listNewsArticles}
          renderItem={this.renderNewsItem}
          onEndReachedThreshold={0.005}
          onEndReached={this.handleLoadmore}
        />
      </View>
    );
  }
}

const mapActionCreators = {
  getDetailArticle,
  loadListNewsArticleData, 
  loadMoreListNewsArticleData
};

const mapStateToProps = state => ({
  token: state.user.token,
  userData: state.user.userData,
  regionData: state.setting.regionData,
  listNewsArticles: state.setting.listNewsArticles,
  listPromotionArticles: state.setting.listPromotionArticles,
  newsArticlesPaginate: state.setting.newsArticlesPaginate,
  currentLocation: state.user.currentLocation,
  detailArticle: state.setting.detailArticle
});

export default connect(
  mapStateToProps,
  mapActionCreators
)(NewsComponent);
