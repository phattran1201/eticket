import request from "../../../utils/request";
import { UPDATE_DETAIL_ARTICLE, BASE_URL_TOCOTOCO, BLOGS, NEWS, THEMEID } from "../../../constants/Constants";

export const getDetailArticle = (handle, onDoneFunc = (isSuccess, response) => {}) => {
    return (dispatch, store) => {
      console.log('poi getDetailArticle: handle ', handle);
        const url = `${BASE_URL_TOCOTOCO}${handle}?view=detail.json&themeid=${THEMEID}`;
        console.log('poi getDetailArticle url: ', url);
        request.get(url)
        .set('X-Tocotoco-Region', store().user.currentLocation.region)
        .finish((err, res) => {
            const response = JSON.parse(res && res.text ? res.text : undefined);
            console.log('poi loadListArticleData response :', response);
            if (!err && response && response.article) {
               //NEWS:
                dispatch({
                  type: UPDATE_DETAIL_ARTICLE,
                  payload: response.article
                });
              onDoneFunc(true, response.article);
            } else {
              onDoneFunc(false, null);
            }
          });
    };
};
