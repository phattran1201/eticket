import React from 'react';
import {
  Animated,
  Dimensions,
  Easing,
  Image,
  PanResponder,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  StatusBar,
  View,
  Geolocation,
  FlatList
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MapView, { Circle, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Modal from 'react-native-modal';
// import * as svg from 'react-native-svg';
import { connect } from 'react-redux';
import {
  SCALE_RATIO_WIDTH_BASIS,
  DEVICE_WIDTH,
  ROUTE_KEY,
  FS,
  DEVICE_HEIGHT
} from '../../../constants/Constants';
import style, { FONT } from '../../../constants/style';
import strings from '../../../constants/Strings';
import MyComponent from '../../view/MyComponent';
import PickerComponent from '../../view/PickerComponent';
import Feather from 'react-native-vector-icons/dist/Feather';
import BaseHeader from '../../view/BaseHeader';
import { loadCurrentLocation } from '../main/MainActions';

const markerIcon = require('../../..//assets/imgs/icons/logo.png');

export class MapComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.currentRadius = 1;
    const locationDelta = this.getLocationDelta(
      10.7725451,
      106.6980413,
      this.currentRadius * 1000
    );
    console.log('poi this.props.listStores:', this.props.listStores);
    const temp = this.props.listStores ? this.props.listStores : [];
    this.state = {
      searchText: '',
      currentPosition: null,
      region: {
        latitude: 10.7725451,
        longitude: 106.6980413,
        latitudeDelta: locationDelta.latitudeDelta,
        longitudeDelta: locationDelta.longitudeDelta
      },
      selectedStore: {
        id: null,
        name: 'Bạn muốn đến cửa hàng nào ?'
      },
      markersList: temp,
      // markersList: [
      //   {
      //     id: 0,
      //     name: 'TocoToco Ngô Thời Nhiệm',
      //     address: '87 Ngô Thời Nhiệm',
      //     district: 6,
      //     ward: 3,
      //     coordinate: {
      //       latitude: 10.784768,
      //       longitude: 106.6934272
      //     },
      //     backgroundImage:
      //       'https://lh5.googleusercontent.com/p/AF1QipP7nllayHsKIwgQHBWa53wDjndONOOh7PVgelfb=w284-h160-k-no',
      //     distance: 0.4
      //   },
      //   {
      //     id: 1,
      //     name: 'Trà Sữa TocoToco Cao Thắng',
      //     address: '58 Cao Thắng',
      //     district: 5,
      //     ward: 3,
      //     coordinate: {
      //       latitude: 10.7772204,
      //       longitude: 106.6934272
      //     },
      //     backgroundImage:
      //       'https://lh5.googleusercontent.com/p/AF1QipNXV_MSB2uSL1ZusYN5Mf3CkrXOo47fuLqZYc1s=w90-h90-n-k-no',
      //     distance: 1.6
      //   },
      //   {
      //     id: 2,
      //     name: 'Trà Sữa TocoToco Nguyễn Tri Phương',
      //     address: '306 Nguyễn Tri Phương',
      //     district: 4,
      //     ward: 10,
      //     coordinate: {
      //       latitude: 10.762822,
      //       longitude: 106.66842
      //     },
      //     backgroundImage:
      //       'https://lh5.googleusercontent.com/p/AF1QipP7nllayHsKIwgQHBWa53wDjndONOOh7PVgelfb=w284-h160-k-no',
      //     distance: 2.3
      //   }
      // ],
      listSearched: []
    };
    this.onRegionChangeComplete = this.onRegionChangeComplete.bind(this);
  }

  getLocationDelta(lat, lon, distance) {
    const MAGIC_NUMBER = 0.4;
    distance = (MAGIC_NUMBER * distance) / 2; //eslint-disable-line
    const circumference = 40075;
    const oneDegreeOfLatitudeInMeters = 111.32 * 1000;
    const angularDistance = distance / circumference;

    const latitudeDelta = distance / oneDegreeOfLatitudeInMeters;
    const longitudeDelta = Math.abs(
      Math.atan2(
        Math.sin(angularDistance) * Math.cos(lat),
        Math.cos(angularDistance) - Math.sin(lat) * Math.sin(lat)
      )
    );

    return {
      latitudeDelta,
      longitudeDelta
    };
  }

  onLocationPress = (coordinate, index) => {
    this._carousel.snapToItem(index);
    const locationDelta = this.getLocationDelta(
      coordinate.latitude,
      coordinate.longitude,
      this.currentRadius * 1000
    );
    const region = {
      latitude: coordinate.latitude,
      longitude: coordinate.longitude,
      latitudeDelta: locationDelta.latitudeDelta,
      longitudeDelta: locationDelta.longitudeDelta
    };
    if (this.map) {
      this.setState({ region });
      this.map.animateToRegion(region, 200);
    }
  };
  onRegionChangeComplete(region) {
    this.setState({ region });
  }

  distance(lo1, la1, lo2, la2) {
    const dLat = (la2 - la1) * (Math.PI / 180);
    const dLon = (lo2 - lo1) * (Math.PI / 180);
    const la1ToRad = la1 * (Math.PI / 180);
    const la2ToRad = la2 * (Math.PI / 180);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(la1ToRad) * Math.cos(la2ToRad) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return 6371 * c;
  }

  renderItem = ({ item, index }) => {
    item.distance = this.distance(
      item.longitude,
      item.latitude,
      this.props.currentLocation.longitude,
      this.props.currentLocation.latitude
    );
    const addressArray = item && item.address ? item.address.split(',') : [];
    item.district = null;
    if (addressArray) {
      item.district = addressArray[2];
    }
    return (
    <TouchableOpacity
      onPress={() => {
        // this.onLocationPress(item.coordinate, index);
        this.props.navigation.navigate({
          key: 'DETAIL_STORE_001',
          routeName: ROUTE_KEY.DETAIL_STORE_COMPONENT,
          params: {
            detailStore: item
          }
        });
      }}
    >
      <View style={styles.carouselItemContainer}>
        <View style={{ width: '100%', height: '72%' }}>
          <FastImage
            style={{ width: '100%', height: '82%' }}
            resizeMode='cover'
            source={{ uri: item.image }}
          />
          <View
            style={{
              position: 'absolute',
              width: '100%',
              height: 40 * SCALE_RATIO_WIDTH_BASIS,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <View
              style={[
                style.shadow,
                {
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: 0.1,
                  borderRadius: 20 * SCALE_RATIO_WIDTH_BASIS,
                  width: 37 * SCALE_RATIO_WIDTH_BASIS,
                  height: 37 * SCALE_RATIO_WIDTH_BASIS,
                  backgroundColor: 'white'
                }
              ]}
            >
              <MaterialIcons
                name='directions'
                color='#D3B574'
                size={30 * SCALE_RATIO_WIDTH_BASIS}
              />
            </View>
          </View>
        </View>
        <View style={{ width: '100%', height: '28%' }}>
          <View
            style={{
              justifyContent: 'center',
              padding: 2 * SCALE_RATIO_WIDTH_BASIS
            }}
          >
            <Text
              style={{
                textAlign: 'center',
                fontSize: 13 * SCALE_RATIO_WIDTH_BASIS,
                color: '#324856',
                // fontWeight: 'bold'
                fontFamily: FONT.Medium
              }}
              numberOfLines={1}
            >
              {item.address}
            </Text>
            <View
              style={{
                justifyContent: 'space-between',
                flexDirection: 'row',
                paddingHorizontal: 6 * SCALE_RATIO_WIDTH_BASIS
              }}
            >
              <Text numberOfLines={1} style={styles.carouselItemMoreInfo}>
                {item.district}
              </Text>
              <Text style={styles.carouselItemMoreInfo} numberOfLines={1}>
                {parseFloat(item.distance).toFixed(1)} km
              </Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
  }

  componentDidMount() {
    loadCurrentLocation().then((posInfo) => {
      console.log('poi 131 this.state.currentPosition aaaaaa',posInfo);
      const locationDelta = this.getLocationDelta(
        posInfo.latitude,
        posInfo.longitude,
        this.currentRadius * 1000
      );
      this.setState({
        region: {
          latitude: posInfo.latitude,
          longitude: posInfo.longitude,
          latitudeDelta: locationDelta.latitudeDelta,
          longitudeDelta: locationDelta.longitudeDelta
        }
      });
      this.setState({
        currentPosition: {
          latitude: posInfo.latitude,
          longitude: posInfo.longitude
        }
      }, () => {
        console.log('poi 131 this.state.currentPosition did', this.state.currentPosition);
      });
    }).catch((err) => {
      console.log('poi pos err', err);
    });
  }

  onChangeSearchTextAndRequestAPI = text => {
    this.setState({
      searchText: text,
      listSearched: this.state.markersList.filter(e => e.address.includes(text))
    });
  };

  render() {
    console.log('poi 131 this.state.currentPosition:', this.props.currentLocation);
    if (this.state.searchText !== '' && this.state.listSearched.length === 0) {
      //search not found:
      return (
        <View style={{ flex: 1 }}>
        <BaseHeader
          noShadow
          children={
            <TextInput
              style={[
                style.textCaption,
                {
                  width: (DEVICE_WIDTH * 80) / 100,
                  borderRadius: 30 * SCALE_RATIO_WIDTH_BASIS,
                  backgroundColor: '#fbfbfb',
                  paddingHorizontal: 15 * SCALE_RATIO_WIDTH_BASIS,
                  paddingVertical: 10 * SCALE_RATIO_WIDTH_BASIS,
                  fontSize: FS(14),
                  fontFamily: 'FontAwesome'
                }
              ]}
              ref='searchBar'
              clearButtonMode='always'
              placeholder=' Tìm kiếm sản phẩm'
              placeholderTextColor='#00000050'
              underlineColorAndroid='transparent'
              selectionColor='#C7AE6D'
              autoCapitalize='none'
              autoCorrect={false}
              value={this.state.searchText}
              onChangeText={this.onChangeSearchTextAndRequestAPI}
            />
          }
          styleContent={{ zIndex: 99, backgroundColor: '#fff' }}
          leftIcon='arrow-left'
          leftIconType='Feather'
          onLeftPress={() => this.props.navigation.goBack()}
        />
        <View style={{ flex: 1, alignItems: 'center' }}>
          <View style={{ height: '40%', marginTop: DEVICE_HEIGHT / 7 }}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <FastImage
                source={require('../../../assets/imgs/icons/notfound.png')}
                resizeMode='contain'
                style={{ width: 100 * SCALE_RATIO_WIDTH_BASIS, height: 100 * SCALE_RATIO_WIDTH_BASIS }}
              />
            </View>
            <Text style={{ marginTop: 10 * SCALE_RATIO_WIDTH_BASIS, fontFamily: FONT.Regular, fontSize: FS(14), textAlign: 'center' }}>Không tìm thấy kết quả phù hợp nào cho</Text>
            <Text style={{ marginTop: 10 * SCALE_RATIO_WIDTH_BASIS, fontFamily: FONT.Medium, fontSize: FS(16), textAlign: 'center' }}>{this.state.searchText}</Text>
          </View>
        </View>
      </View>
      );
    }
    //not using search func or search found
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
            backgroundColor={'#ffffff60'}
            barStyle="dark-content"
            translucent
        />
        <MapView
          style={{ flex: 1 }}
          // onLayout={this.onLayout}
          provider={PROVIDER_GOOGLE}
          ref={instance => (this.map = instance)}
          region={this.state.region}
          // onRegionChangeComplete={this.onRegionChangeComplete}
        >
          {this.state.searchText === ''
            ? this.state.markersList.map(item => (
                <Marker coordinate={{ latitude: item.latitude, longitude: item.longitude }}>
                  <FastImage
                    style={styles.markerImageStyle}
                    source={markerIcon}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                </Marker>
              ))
            : this.state.listSearched.map(item => (
                <Marker coordinate={{ latitude: item.latitude, longitude: item.longitude }}>
                  <FastImage
                    style={styles.markerImageStyle}
                    source={markerIcon}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                </Marker>
              ))}
          {this.state.currentPosition && (
            <Marker coordinate={this.state.currentPosition} />
          )}
        </MapView>
        <View
          style={[
            styles.searchInputContainer,
            {
              paddingVertical: 5 * SCALE_RATIO_WIDTH_BASIS,
              paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
              justifyContent: 'center',
              alignItems: 'center'
            }
          ]}
        >
          <View
            style={[
              style.viewInput,
              {
                borderWidth: 1 * SCALE_RATIO_WIDTH_BASIS,
                flex: 1,
                borderRadius: 20 * SCALE_RATIO_WIDTH_BASIS,
                flexDirection: 'row',
                backgroundColor: '#fff'
              }
            ]}
          >
            <View style={styles.searchIconContainer}>
              <MaterialIcons
                name='search'
                color='#B9B9B9'
                size={20 * SCALE_RATIO_WIDTH_BASIS}
              />
            </View>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <TextInput
                style={{ height: 60 * SCALE_RATIO_WIDTH_BASIS }}
                fontFamily={FONT.Light}
                textAlign='center'
                fontSize={FS(14)}
                placeholder={'Bạn muốn tìm ToCoToCo ở đâu...'}
                value={this.state.searchText}
                onChangeText={this.onChangeSearchTextAndRequestAPI}
              />
            </View>
          </View>
        </View>
        <View style={styles.listStoreStyle}>
          <Carousel
            ref={c => {
              this._carousel = c;
            }}
            data={
              this.state.searchText === ''
                ? this.state.markersList
                : this.state.listSearched
            }
            renderItem={this.renderItem}
            sliderWidth={DEVICE_WIDTH}
            itemWidth={140 * SCALE_RATIO_WIDTH_BASIS}
            enableSnap
            firstItem={0}
            onSnapToItem={index => {
              const coordinate = { latitude: this.state.markersList[index].latitude, longitude: this.state.markersList[index].longitude };
              this.onLocationPress(
                coordinate,
                index
              );
            }}
          />
        </View>
      </View>
    );
  }
}

const searchBarHeight = 45 * SCALE_RATIO_WIDTH_BASIS;

const styles = StyleSheet.create({
  markerImageStyle: {
    width: 40 * SCALE_RATIO_WIDTH_BASIS,
    height: 40 * SCALE_RATIO_WIDTH_BASIS
  },
  searchInputContainer: {
    paddingHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
    position: 'absolute',
    top: 40 * SCALE_RATIO_WIDTH_BASIS,
    width: DEVICE_WIDTH,
    height: searchBarHeight
  },
  searchInput: {
    borderRadius: 30 * SCALE_RATIO_WIDTH_BASIS,
    // borderRadius: 3 * SCALE_RATIO_WIDTH_BASIS,
    flex: 1,
    height: searchBarHeight,
    flexDirection: 'row',
    backgroundColor: '#FFFFFF'
  },
  searchNameContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchNameTextStyle: {
    fontSize: 14 * SCALE_RATIO_WIDTH_BASIS,
    color: '#767676',
    fontWeight: '500'
  },
  searchIconContainer: {
    // alignItems: 'center',
    justifyContent: 'center',
    width: 20 * SCALE_RATIO_WIDTH_BASIS
  },
  listStoreStyle: {
    position: 'absolute',
    width: '100%',
    height: 170 * SCALE_RATIO_WIDTH_BASIS,
    bottom: 20 * SCALE_RATIO_WIDTH_BASIS
  },
  carouselItemContainer: {
    overflow: 'hidden',
    borderRadius: 10 * SCALE_RATIO_WIDTH_BASIS,
    // marginLeft: 10 * SCALE_RATIO_WIDTH_BASIS,
    width: 140 * SCALE_RATIO_WIDTH_BASIS,
    height: 170 * SCALE_RATIO_WIDTH_BASIS,
    backgroundColor: 'white'
  },
  carouselItemMoreInfo: {
    textAlign: 'center',
    fontSize: 10 * SCALE_RATIO_WIDTH_BASIS,
    fontFamily: FONT.Medium,
    color: '#858585'
  }
});

function mapStateToProps(state) {
    return {
      listStores: state.store.listStores,
      currentLocation: state.user.currentLocation,
    };
}

const mapActionCreators = {

};

export default connect(
    mapStateToProps,
    mapActionCreators,
    null,
    { withRef: true }
)(MapComponent);
