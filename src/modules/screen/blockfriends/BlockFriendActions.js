import { Alert } from 'react-native';
import request from '../../../utils/request';
import {
    BASE_URL, BLOCK_API,
    UPDATE_LIST_BLOCKED,
    REMOVE_ITEM_LIST_BLOCKED,
    RESET_LIST_BLOCKED,
    ADD_ITEM_LIST_BLOCKED,
    REMOVE_ITEM_LIST_PARTNERS,
    REPORT_API
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';

export function loadListBlocked(page, onDone = () => { }, onOutOfData = () => { }) {
    return (dispatch, store) => {
        const { id } = store().user.userData;

        const filter = `filter={"sender_id": "${id}"}&limit=10&page=${page}`;

        request.get(`${BASE_URL}${BLOCK_API}?fields=["$all", {"receiver": ["$all"]}]&${filter}`)
            .finish((err, res) => {
                onDone();
                if (!err && res &&
                    res.body &&
                    res.body.results &&
                    res.body.results.objects &&
                    res.body.results.objects.rows
                ) {
                    if (res.body.results.objects.rows.length === 0) onOutOfData();
                    dispatch({
                        type: UPDATE_LIST_BLOCKED,
                        payload: res.body.results.objects.rows
                    });
                }
            });
    };
}

export function reloadListBlocked(page, onDone = () => { }) {
    return (dispatch, store) => {
        const { id } = store().user.userData;

        const filter = `filter={"sender_id": "${id}"}&limit=10&page=${page}`;

        request.get(`${BASE_URL}${BLOCK_API}?fields=["$all", {"receiver": ["$all"]}]&${filter}`)
            .finish((err, res) => {
                onDone();
                if (!err && res &&
                    res.body &&
                    res.body.results &&
                    res.body.results.objects &&
                    res.body.results.objects.rows
                ) {
                    dispatch({
                        type: RESET_LIST_BLOCKED,
                        payload: res.body.results.objects.rows
                    });
                }
            });
    };
}

export function blockFunction(user, onFinish = () => { }, onSuccess = () => { }) {
    return (dispatch, store) => {
        request.post(`${BASE_URL}${BLOCK_API}`)
            .set('Content-Type', 'application/json')
            .set('Authorization', `Bearer ${store().user.token}`)
            .send({
                sender_id: store().user.userData.id,
                receiver_id: user.id
            })
            .finish((err, res) => {
                onFinish();
                if (!err && res &&
                    res.body &&
                    res.body.results &&
                    res.body.results.object
                ) {
                    onSuccess();
                    dispatch({
                        type: ADD_ITEM_LIST_BLOCKED,
                        payload: {
                            ...res.body.results.object,
                            receiver: user,
                        }
                    });
                } else {
                    setTimeout(() =>
                        Alert.alert(
                            strings.alert,
                            strings.block_fail,
                            [
                                { text: strings.yes, onPress: () => { } },
                            ],
                            { cancelable: true }
                        )
                        , 300);
                }
            });
    };
}

export function unBlock(id, onFinish = () => { }) {
    return (dispatch, store) => {
        request.delete(`${BASE_URL}${BLOCK_API}/${id}`)
            .set('Content-Type', 'application/json')
            .set('Authorization', `Bearer ${store().user.token}`)
            .finish((err, res) => {
                onFinish();
                if (!err && res &&
                    res.body &&
                    res.body.code === 200
                ) {
                    dispatch({
                        type: REMOVE_ITEM_LIST_BLOCKED,
                        payload: id
                    });
                } else {
                    Alert.alert(
                        strings.alert,
                        strings.unblock_fail,
                        [
                            { text: strings.yes, onPress: () => { } },
                        ],
                        { cancelable: true }
                    );
                }
            });
    };
}

export function reportFunction(userId, categoryIndex, onFinish = () => { }) {
    return (dispatch, store) => {
        const listCategory = ['ADVERTISING', 'ABUSE', 'THREAT', 'FRAUD', 'SEXUAL_ACT', 'ETC'];
        request.post(`${BASE_URL}${REPORT_API}`)
            .set('Content-Type', 'application/json')
            .set('Authorization', `Bearer ${store().user.token}`)
            .send({
                reported_id: store().user.userData.id,
                user_id: userId,
                category: listCategory[categoryIndex],
                reported_id_type: 'USER',
            })
            .finish((err, res) => {
                onFinish();
                if (!err && res &&
                    res.body &&
                    res.body.code === 200
                ) {
                    Alert.alert(
                        strings.alert,
                        strings.report_success,
                        [
                            { text: strings.yes, onPress: () => { } },
                        ],
                        { cancelable: true }
                    );
                } else {
                    Alert.alert(
                        strings.alert,
                        strings.report_fail,
                        [
                            { text: strings.yes, onPress: () => { } },
                        ],
                        { cancelable: true }
                    );
                }
            });
    };
}
