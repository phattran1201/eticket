import React from 'react';
import { View, Text, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import LottieView from 'lottie-react-native';
import strings from '../../../constants/Strings';
import HeadedWithBackButtonComponent from '../../view/HeaderWithBackButtonComponent';
import MyComponent from '../../view/MyComponent';
import { SCALE_RATIO_WIDTH_BASIS, FS, SCALE_RATIO_HEIGHT_BASIS, DEVICE_WIDTH, DEVICE_HEIGHT } from '../../../constants/Constants';
import MyButton from '../../../style/MyButton';
import style from '../../../constants/style';
import MyImage from '../../view/MyImage';
import getGenderImage from '../../../utils/getGenderImage';
import { loadListBlocked, unBlock, reloadListBlocked } from './BlockFriendActions';
import MySpinner from '../../view/MySpinner';

class BlockFriendsComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      isLoadMore: false,
    };
    this.page = 1;
    this.outOfData = false;
    this.onRefresh = this.onRefresh.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
  }

  renderItem = ({ item }) => (
    <View style={[styles.elementContainer, style.shadow]}>
      <View style={{ flex: 1, flexDirection: 'row', marginLeft: 19 * SCALE_RATIO_WIDTH_BASIS, alignItems: 'center' }}>
        <MyImage
          size={54 * SCALE_RATIO_WIDTH_BASIS}
          source={{ uri: item.receiver.avatar }}
        />
        <Text style={[style.textNormal, styles.textInfo, { marginHorizontal: 8 * SCALE_RATIO_WIDTH_BASIS }]}>{item.receiver.nickname}</Text>
        {getGenderImage(item.receiver.sex)}
        <Text style={[style.textNormal, styles.textInfo, { marginHorizontal: 8 * SCALE_RATIO_WIDTH_BASIS }]}>{item.receiver.age}</Text>
      </View>
      <View>
        <MyButton
          style={{ marginRight: 18 * SCALE_RATIO_WIDTH_BASIS }}
          onPress={() => {
            MySpinner.show();
            this.props.unBlock(item.id, MySpinner.hide);
          }}
        >
          {strings.blocked}
        </MyButton>
      </View>
    </View>
  );

  onRefresh() {
    this.page = 1;
    this.outOfData = false;
    this.setState({ refreshing: true });
    this.props.reloadListBlocked(1, () => this.setState({ refreshing: false }));
  }

  handleLoadMore() {
    if (this.outOfData || this.state.isLoadMore) return;
    this.page++;
    this.setState({ isLoadMore: true });
    this.props.loadListBlocked(this.page, () => this.setState({ isLoadMore: false }), () => { this.outOfData = true; });
  }

  renderFooter = () => {
    if (!this.outOfData && this.state.isLoadMore) {
      return (
        <View style={{ paddingVertical: 20 }}>
          <ActivityIndicator animating size="small" />
        </View>
      );
    }
    if (this.props.listBlocked.length === 0) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
            width: DEVICE_WIDTH,
            height: (DEVICE_HEIGHT * 75) / 100
          }}
        >
          <LottieView
            source={require('../../../assets/isempty.json')}
            autoPlay
            loop
            hardwareAccelerationAndroid
            style={{
              width: 211 * SCALE_RATIO_WIDTH_BASIS,
              height: 206 * SCALE_RATIO_WIDTH_BASIS,
              alignSelf: 'center'
            }}
            resizeMode="cover"
          />
        </View>
      );
    }
    return null;
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <HeadedWithBackButtonComponent
          bodyTitle={'BLOCK FRIENDS'}
          onPress={() => this.props.navigation.goBack()}
        />
        <FlatList
          style={{ flex: 1 }}
          data={this.props.listBlocked}
          renderItem={this.renderItem}
          onRefresh={this.onRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={0.01}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  listBlocked: state.blocking.listBlocked
});

const mapActionCreators = {
  loadListBlocked,
  unBlock,
  reloadListBlocked
};

export default connect(mapStateToProps, mapActionCreators)(BlockFriendsComponent);

const styles = StyleSheet.create({
  elementContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingVertical: 15 * SCALE_RATIO_HEIGHT_BASIS,
  },
  textInfo: {
    fontSize: FS(14),
  },
});
