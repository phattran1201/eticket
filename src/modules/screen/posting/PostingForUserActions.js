import request from '../../../utils/request';
import {
  BASE_URL,
  POST_API,
  LIST_POST_FOR_USER,
  LOAD_MORE_POST_FOR_USER,
  DELETE_POST_FOR_USER
} from '../../../constants/Constants';

const LIMIT = 6;

export function loadListPostForUser(onDoneFunc = isSuccess => {}) {
  return (dispatch, store) => {
    request
      .get(
        `${BASE_URL}${POST_API}?fields=["$all",{"user":["$all"]}]&filter={"user_id":"${
          store().user.userData.id
        }"}&limit=${LIMIT}&page=1`
      )
      .set('Authorization', `Bearer ${store().user.token}`)
      .finish((err, res) => {
        console.log('​loadListPostForUser -> res', res);
        if (!err) {
          dispatch({
            type: LIST_POST_FOR_USER,
            payload: res.body.results.objects.rows
          });
          onDoneFunc(true);
        } else {
          onDoneFunc(false);
        }
      });
  };
}
export function loadMoreListPostForUser(page, onDoneFunc = (isSuccess, isOutOfData) => {}) {
  return (dispatch, store) => {
    request
      .get(
        `${BASE_URL}${POST_API}?fields=["$all",{"user":["$all"]}]&filter={"user_id":"${
          store().user.userData.id
        }"}&limit=${LIMIT}&page=${page}`
      )
      .set('Authorization', `Bearer ${store().user.token}`)
      .finish((err, res) => {
        if (!err) {
          dispatch({
            type: LOAD_MORE_POST_FOR_USER,
            payload: res.body.results.objects.rows
          });
          if (res.body.results.objects.rows < LIMIT) {
            onDoneFunc(true, true);
          } else {
            onDoneFunc(true, false);
          }
        } else {
          onDoneFunc(false, null);
        }
      });
  };
}

export function deletePostForUser(id, onDoneFunc = isSuccess => {}) {
  return (dispatch, store) => {
    request
      .delete(`${BASE_URL}${POST_API}/${id}`)
      .set('Authorization', `Bearer ${store().user.token}`)
      .finish((err, res) => {
        if (!err) {
          dispatch({
            type: DELETE_POST_FOR_USER,
            payload: store().profilePost.listPostForUser.filter(e => e.id !== id)
          });
          onDoneFunc(true);
        } else {
          onDoneFunc(false);
        }
      });
  };
}

export function refreshListPostForUser(onDoneFunc = isSuccess => {}) {
  return (dispatch, store) => {
    request
      .get(
        `${BASE_URL}${POST_API}?fields=["$all",{"user":["$all"]}]&filter={"user_id":"${
          store().user.userData.id
        }"}&limit=${LIMIT}&page=1`
      )
      .set('Authorization', `Bearer ${store().user.token}`)
      .finish((err, res) => {
        console.log('​refreshListPostForUser -> res', res);
        console.log('​refreshListPostForUser -> err', err);
        if (!err) {
          dispatch({
            type: LIST_POST_FOR_USER,
            payload: res.body.results.objects.rows
          });
          onDoneFunc(true);
        } else {
          onDoneFunc(false);
        }
      });
  };
}
