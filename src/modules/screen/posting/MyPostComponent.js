import LottieView from 'lottie-react-native';
import React from 'react';
import { ActivityIndicator, FlatList, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import { alert } from '../../../utils/alert';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import MySpinner from '../../view/MySpinner';
import PostingListItem from '../../view/PostingListItem';
import {
  deletePostForUser,
  loadListPostForUser,
  loadMoreListPostForUser,
  refreshListPostForUser
} from './PostingForUserActions';

class MyPostComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      onlySeeMyPost: false,
      page: 2,
      loading: false,
      outOfData: false,
      refreshing: false
    };
    this.renderItem = this.renderItem.bind(this);
  }

  componentDidMount() {
    MySpinner.show();
    this.setState({ loading: true });
    this.props.loadListPostForUser(isSuccess => {
      this.setState({ loading: false });
      MySpinner.hide();
    });
    console.log(
      '​MyPostComponent -> componentDidMount -> loadListPostForUser',
      loadListPostForUser
    );
  }

  handleLoadMore = () => {
    if (this.state.outOfData || this.state.loading) return;
    MySpinner.show();
    this.setState({ loading: true });
    this.props.loadMoreListPostForUser(this.state.page, (isSuccess, isOutOfData) => {
      if (isSuccess) {
        if (isOutOfData) {
          this.setState({ outOfData: true });
        } else {
          this.setState(previousState => ({
            loading: false,
            page: previousState.page + 1
          }));
        }
        MySpinner.hide();
      } else {
        this.setState({ loading: false });
        MySpinner.hide();
      }
    });
  };

  onRefresh = () => {
    this.refs.listRef.scrollToOffset({ x: 0, y: 0, animated: true });
    this.setState({ loading: true });
    this.props.refreshListPostForUser(isSuccess => {
      this.setState({
        page: 2,
        outOfData: false,
        refreshing: false,
        loading: false
      });
    });
  };

  renderFooter = () => {
    if (this.state.outOfData) {
      return null;
    }
    if (this.props.listPostForUser.length === 0) {
      return (
        <LottieView
          source={require('../../../assets/isempty.json')}
          autoPlay
          loop
          hardwareAccelerationAndroid
          style={{
            width: 240 * SCALE_RATIO_WIDTH_BASIS,
            height: 300 * SCALE_RATIO_WIDTH_BASIS,
            alignSelf: 'center'
          }}
        />
      );
    }

    return (
      !this.state.outOfData &&
      this.props.listPostForUser.length !== 0 &&
      this.state.loading && (
        <View
          style={{
            paddingTop: 8 * SCALE_RATIO_WIDTH_BASIS,
            paddingBottom: 15 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          <ActivityIndicator animating size="small" />
        </View>
      )
    );
  };

  deletePost = id => {
    MySpinner.show();
    this.setState({ loading: true });
    this.props.deletePostForUser(id, isSuccess => {
      if (isSuccess) {
        alert(strings.alert, 'Xóa bài viết thành công', () => {});
        this.setState({ loading: false });
        MySpinner.hide();
      } else {
        alert(strings.alert, 'Xóa bài viết thất bại', () => {});
        this.setState({ loading: false });
        MySpinner.hide();
      }
    });
  };
  renderItem = ({ item }) => (
    <PostingListItem
      item={item}
      navigation={this.props.navigation}
      onPress={() => this.deletePost(item.id)}
    />
  );

  render() {
    return (
      <View style={styles.container}>
        <BaseHeader
          children={<Text style={style.titleHeader}>{strings.my_post.toUpperCase()}</Text>}
          leftIcon="arrow-left"
          leftIconType="Feather"
          onLeftPress={() => this.props.navigation.goBack()}
        />
        <View style={{ flex: 1 }}>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
              marginTop: 20 * SCALE_RATIO_HEIGHT_BASIS
            }}
          >
            <Text style={style.textCaption}>{strings.posting_description}</Text>
          </View>
          <FlatList
            ref="listRef"
            style={{ flex: 1 }}
            data={this.props.listPostForUser}
            keyExtractor={item => item.id}
            renderItem={this.renderItem}
            ListFooterComponent={this.renderFooter}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.01}
            onRefresh={() => {
              this.setState({ refreshing: true });
              this.onRefresh();
            }}
            refreshing={this.state.refreshing}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  listPostForUser: state.profilePost.listPostForUser,
  userData: state.user.userData
});

const mapActionCreators = {
  deletePostForUser,
  loadListPostForUser,
  loadMoreListPostForUser,
  refreshListPostForUser
};
export default connect(
  mapStateToProps,
  mapActionCreators
)(MyPostComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  titleStyle: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue-Bold',
    fontSize: 18 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  }
});
