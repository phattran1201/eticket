import {
  BASE_URL,
  LIST_POST_ALL,
  LOAD_MORE_POST_ALL,
  POST_API
} from '../../../constants/Constants';
import request from '../../../utils/request';

const LIMIT = 6;

export function loadListPost(onDoneFunc = isSuccess => {}) {
  return (dispatch, store) => {
    request
      .get(
        `${BASE_URL}${POST_API}?fields=["$all", {"user": ["$all"]}]&limit=${LIMIT}&page=1]&filter={"user_id": {"$ne":"${
          store().user.userData.id
        }"} }`
      )
      .set('Authorization', `Bearer ${store().user.token}`)
      .finish((err, res) => {
        if (!err) {
          dispatch({
            type: LIST_POST_ALL,
            payload: res.body.results.objects.rows
          });
          onDoneFunc(true);
        } else {
          onDoneFunc(false);
        }
      });
  };
}
export function loadMoreListPost(page, onDoneFunc = (isSuccess, isOutOfData) => {}) {
  return (dispatch, store) => {
    request
      .get(
        `${BASE_URL}${POST_API}?fields=["$all", {"user": ["$all"]}]&limit=${LIMIT}&page=${page}]&filter={"user_id": {"$ne":"${
          store().user.userData.id
        }"} }`
      )
      .set('Authorization', `Bearer ${store().user.token}`)
      .finish((err, res) => {
        if (!err) {
          dispatch({
            type: LOAD_MORE_POST_ALL,
            payload: res.body.results.objects.rows
          });
          if (res.body.results.objects.rows < LIMIT) {
            onDoneFunc(true, true);
          } else {
            onDoneFunc(true, false);
          }
        } else {
          onDoneFunc(false, null);
        }
      });
  };
}
export function refreshListPost(onDoneFunc = isSuccess => {}) {
  return (dispatch, store) => {
    request
      .get(
        `${BASE_URL}${POST_API}?fields=["$all", {"user": ["$all"]}]&limit=${LIMIT}&page=1]&filter={"user_id": {"$ne":"${
          store().user.userData.id
        }"} }`
      )
      .set('Authorization', `Bearer ${store().user.token}`)
      .finish((err, res) => {
        if (!err) {
          dispatch({
            type: LIST_POST_ALL,
            payload: res.body.results.objects.rows
          });
          onDoneFunc(true);
        } else {
          onDoneFunc(false);
        }
      });
  };
}
