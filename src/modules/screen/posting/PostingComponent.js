import LottieView from 'lottie-react-native';
import React from 'react';
import { ActivityIndicator, FlatList, StyleSheet, Text, View } from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  ROUTE_KEY,
  SCALE_RATIO_HEIGHT_BASIS,
  SCALE_RATIO_WIDTH_BASIS
} from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import style from '../../../constants/style';
import BaseHeader from '../../view/BaseHeader';
import MyComponent from '../../view/MyComponent';
import MySpinner from '../../view/MySpinner';
import PostingListItem from '../../view/PostingListItem';
import { loadListPost, loadMoreListPost, refreshListPost } from './PostingActions';

class PostingComponent extends MyComponent {
  constructor(props) {
    super(props);
    this.state = {
      onlySeeMyPost: false,
      page: 2,
      loading: false,
      outOfData: false,
      refreshing: false
    };
    this.renderItem = this.renderItem.bind(this);
  }

  componentDidMount() {
    this.setState({ loading: true });
    this.props.loadListPost(isSuccess => {
      this.setState({ loading: false });
      MySpinner.hide();
    });
  }

  handleLoadMore = () => {
    if (this.state.outOfData || this.state.loading) return;

    this.setState({ loading: true });
    this.props.loadMoreListPost(this.state.page, (isSuccess, isOutOfData) => {
      if (isSuccess) {
        if (isOutOfData) {
          this.setState({ outOfData: true });
        } else {
          this.setState(previousState => ({
            loading: false,
            page: previousState.page + 1
          }));
        }
      } else {
        this.setState({ loading: false });
      }
    });
  };

  onRefresh = () => {
    this.refs.listRef.scrollToOffset({ x: 0, y: 0, animated: true });
    this.setState({ loading: true });
    this.props.refreshListPost(isSuccess => {
      this.setState({
        page: 2,
        outOfData: false,
        refreshing: false,
        loading: false
      });
    });
  };

  renderFooter = () => {
    if (this.state.outOfData) {
      return null;
    }
    if (this.props.listPost.length === 0) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
            width: DEVICE_WIDTH,
            height: (DEVICE_HEIGHT * 60) / 100
          }}
        >
          <LottieView
            source={require('../../../assets/isempty.json')}
            autoPlay
            loop
            hardwareAccelerationAndroid
            style={{
              width: 211 * SCALE_RATIO_WIDTH_BASIS,
              height: 206 * SCALE_RATIO_WIDTH_BASIS,
              alignSelf: 'center'
            }}
            resizeMode="cover"
          />
        </View>
      );
    }

    return (
      !this.state.outOfData &&
      this.props.listPost.length !== 0 &&
      this.state.loading && (
        <View
          style={{
            paddingTop: 8 * SCALE_RATIO_WIDTH_BASIS,
            paddingBottom: 15 * SCALE_RATIO_WIDTH_BASIS
          }}
        >
          <ActivityIndicator animating size="small" />
        </View>
      )
    );
  };

  renderItem = ({ item }) => <PostingListItem item={item} navigation={this.props.navigation} />;

  render() {
    return (
      <View style={styles.container}>
        <BaseHeader
          children={<Text style={style.titleHeader}>{strings.posting.toUpperCase()}</Text>}
          rightIcon="envelope"
          navigation={this.props.navigation}
        />
        <View style={{ flex: 1 }}>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 20 * SCALE_RATIO_WIDTH_BASIS,
              marginTop: 20 * SCALE_RATIO_HEIGHT_BASIS
            }}
          >
            <Text style={style.textCaption}>{strings.posting_description}</Text>
          </View>
          <FlatList
            ref="listRef"
            style={{ flex: 1 }}
            data={this.props.listPost.filter(e => {
              const isBlocked = this.props.listBlocked.find(i => i.receiver.id === e.user.id);
              return !isBlocked;
            })}
            keyExtractor={item => item.id}
            renderItem={this.renderItem}
            ListFooterComponent={this.renderFooter}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.01}
            onRefresh={() => {
              this.setState({ refreshing: true });
              this.onRefresh();
            }}
            refreshing={this.state.refreshing}
          />

          <ActionButton buttonColor="#282828" size={45 * SCALE_RATIO_WIDTH_BASIS}>
            <ActionButton.Item
              buttonColor="#9b59b6"
              title={this.state.onlySeeMyPost ? strings.see_all_posting : strings.see_my_posting}
              onPress={() => this.props.navigation.navigate(ROUTE_KEY.MY_POST_COMPONENT)}
            >
              <Icon
                name={this.state.onlySeeMyPost ? 'ios-eye-off' : 'ios-eye'}
                size={20 * SCALE_RATIO_WIDTH_BASIS}
                color="#fff"
              />
            </ActionButton.Item>
            <ActionButton.Item
              buttonColor="#C7AE6D"
              title={strings.post_now}
              onPress={() => this.props.navigation.navigate(ROUTE_KEY.CREATE_POST_COMPONENT)}
            >
              <Icon name="ios-create" size={20 * SCALE_RATIO_WIDTH_BASIS} color="#fff" />
            </ActionButton.Item>
          </ActionButton>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  listPost: state.post.listPost,
  userData: state.user.userData,
  listBlocked: state.blocking.listBlocked
});

const mapActionCreators = {
  loadListPost,
  loadMoreListPost,
  refreshListPost
};
export default connect(
  mapStateToProps,
  mapActionCreators
)(PostingComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  titleStyle: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue-Bold',
    fontSize: 18 * SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 540,
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  }
});
