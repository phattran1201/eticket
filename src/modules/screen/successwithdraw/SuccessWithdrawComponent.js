import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { DEVICE_HEIGHT, SCALE_RATIO_WIDTH_BASIS } from '../../../constants/Constants';
import strings from '../../../constants/Strings';
import MyComponent from '../../view/MyComponent';
import STouchableOpacity from '../../view/STouchableOpacity';

export default class SuccessWithdrawComponent extends MyComponent {
  render() {
    return (
      <View style={styles.bodyContainer}>
        <View style={styles.bodyContentContainer}>
          <Text style={styles.titleText}>{strings.congratulations}</Text>
          <Text style={styles.subTitleText}>
            {strings.your_withdraw_request_completed_notification}
          </Text>
          <Text style={styles.notiText}>{strings.withdraw_successfully}</Text>

          <STouchableOpacity
            style={styles.buttonContainer}
            onPress={() => this.props.onButtonClick()}
          >
            <Text style={styles.buttonText}>{strings.done}</Text>
          </STouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    alignSelf: 'center',
    fontFamily: 'helveticaneue',
    fontSize: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    fontWeight: 'bold',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  },
  bodyContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  bodyContentContainer: {
    borderRadius: 1.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#fafafa',
    shadowColor: '#797e87',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 2,
    alignItems: 'center',
    padding: 7.8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    marginHorizontal: 6.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  titleText: {
    fontFamily: 'helveticaneue',
    fontSize: 6.7 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '800',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#555667'
  },
  subTitleText: {
    fontFamily: 'helveticaneue',
    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '600',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#707070',
    marginTop: 5.3 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  notiText: {
    fontFamily: 'helveticaneue',
    fontSize: 4 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: '600',
    fontStyle: 'italic',
    textAlign: 'center',
    color: '#707070',
    marginTop: 4 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224)
  },
  buttonContainer: {
    marginTop: 8 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    borderRadius: 20 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    backgroundColor: '#6d77f7',
    paddingVertical: 4.7 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    width: 93 * (SCALE_RATIO_WIDTH_BASIS - DEVICE_HEIGHT / 224),
    alignSelf: 'center'
  },
  buttonText: {
    fontFamily: 'helveticaneue',
    fontSize: 5.7 * SCALE_RATIO_WIDTH_BASIS - (DEVICE_HEIGHT / 224) * 1.1,
    fontWeight: 'normal',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#ffffff'
  }
});
