import { StackNavigator } from 'react-navigation';
import React from 'react';
import { Animated, Easing, BackHandler, NativeModules } from 'react-native';
import { ROUTE_KEY } from '../constants/Constants';
import EditProfileComponent from '../modules/screen/editprofile/EditProfileComponent';
import ForgotPassComponent from '../modules/screen/forgotpass/ForgotPassComponent';
import PreLoginComponent from '../modules/screen/prelogin/PreLoginComponent';
import EmailSignUpComponent from '../modules/screen/emailsignup/EmailSignUpComponent';
import MainComponent from '../modules/screen/main/MainComponent';
import SplashComponent from '../modules/screen/splash/SplashComponent';
import PartnersDetailComponent from '../modules/screen/partnersDetail/PartnersDetailComponent';
import SignInComponent from '../modules/screen/signin/SignInComponent';
import AddProfileComponent from '../modules/screen/addprofile/AddProfileComponent';
import CreatePostComponent from '../modules/screen/createPost/CreatePostComponent';
import ExchangeComponent from '../modules/screen/exchange/ExchangeComponent';
import PaypalComponent from '../modules/screen/paypal/PaypalComponent';
import SettingsComponent from '../modules/screen/settings/SettingsComponent';
import HistoryComponent from '../modules/screen/history/HistoryComponent';
import MessageComponent from '../modules/screen/message/MessageComponent';
import DetailMessageComponent from '../modules/screen/detailMessage/DetailMessageComponent';
import BlockFriendsComponent from '../modules/screen/blockfriends/BlockFriendsComponent';
import TermsConditionsComponent from '../modules/screen/termsconditions/TermsConditionsComponent';
import TermsConditionsWebView from '../modules/screen/termsconditionwebview/TermsConditionsWebView';
import StoreComponent from '../modules/screen/store/StoreComponent';
import MyPostComponent from '../modules/screen/posting/MyPostComponent';
import ExchangeTab from '../modules/screen/exchangetab/ExchangeTopTab';
import SearchComponent from '../modules/screen/search/SearchComponent';
import FavouriteTopicComponent from '../modules/screen/favouriteTopic/FavouriteTopicComponent';
import MapComponent from '../modules/screen/map/MapComponent';
import global from '../utils/globalUtils';
import CheckOutComponent from '../modules/screen/checkout/CheckOutComponent';
import MenuComponent from '../modules/screen/menu/MenuComponent';
import DeliveryComponent from '../modules/screen/checkout/DeliveryComponent';
import AtStoreComponent from '../modules/screen/checkout/AtStoreComponent';
import DetailStoreComponent from '../modules/screen/detailStore/DetailStoreComponent';
import ProfileComponent from '../modules/screen/profile/ProfileComponent';
import OrderComponent from '../modules/screen/order/OrderComponent';
import DetailProductComponent from '../modules/screen/detailProduct/DetailProductComponent';
import PersonalInfoComponent from '../modules/screen/PersonalInfoComponent/PersonalInfoComponent';
import CompletedOrderComponent from '../modules/screen/checkout/CompletedOrderComponent';
import FavouriteComponent from '../modules/screen/favourite/FavouriteComponent';
import DetailOrderComponent from '../modules/screen/detailorder/DetailOrderComponent';
import CategoryComponent from '../modules/screen/category/CategoryComponent';
import RewardsComponent from '../modules/screen/reward/RewardsComponent';
import RegisterComponent from '../modules/screen/register/RegisterComponent';
import RewardsHistoryComponent from '../modules/screen/reward/RewardsHistoryComponent';
import NewsComponent from '../modules/screen/news/NewsComponent';
import WebViewComponent from '../modules/screen/webview/WebViewComponent';
import PromotionComponent from '../modules/screen/promotion/PromotionComponent';

const routeAppConfiguration = {
  [ROUTE_KEY.MAIN]: {
    screen: MainComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.SPLASH]: {
    screen: SplashComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.PRELOGIN]: {
    screen: PreLoginComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.CHECKOUT]: {
    screen: CheckOutComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.DELIVERY]: {
    screen: DeliveryComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.ATSTORE]: {
    screen: AtStoreComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.EMAILSIGNUP]: {
    screen: EmailSignUpComponent,
    navigationOptions: { header: null }
  },
  [ROUTE_KEY.SIGNIN]: {
    screen: SignInComponent,
    navigationOptions: { header: null }
  },
  [ROUTE_KEY.REGISTER]: {
    screen: RegisterComponent,
    navigationOptions: { header: null }
  },
  [ROUTE_KEY.ADDPROFILE]: {
    screen: AddProfileComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.BLOCKFRIENDS]: {
    screen: BlockFriendsComponent,
    navigationOptions: { header: null }
  },
  [ROUTE_KEY.TERMSCONDITIONS]: {
    screen: TermsConditionsComponent,
    navigationOptions: { header: null }
  },
  [ROUTE_KEY.TERMSCONDITIONSWEBVIEW]: {
    screen: TermsConditionsWebView,
    navigationOptions: { header: null }
  },
  [ROUTE_KEY.SEARCH]: {
    screen: SearchComponent,
    navigationOptions: { header: null }
  },
  [ROUTE_KEY.STORE]: {
    screen: StoreComponent,
    navigationOptions: { header: null }
  },
  [ROUTE_KEY.FORGOT_PASS]: {
    screen: ForgotPassComponent,
    navigationOptions: { header: null }
  },
  [ROUTE_KEY.EDIT_PROFILE]: {
    screen: EditProfileComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.PARTNER_DETAIL_COMPONENT]: {
    screen: PartnersDetailComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.FAVOURITE_TOPIC_COMPONENT]: {
    screen: FavouriteTopicComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.FAVOURITE]: {
    screen: FavouriteComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.CREATE_POST_COMPONENT]: {
    screen: CreatePostComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.MY_POST_COMPONENT]: {
    screen: MyPostComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.EXCHANGE_COMPONENT]: {
    screen: ExchangeComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.PAYPAL_COMPONENT]: {
    screen: PaypalComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.EXCHANGE_TAB]: {
    screen: ExchangeTab,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.SETTINGS_COMPONENT]: {
    screen: SettingsComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.HISTORY_COMPONENT]: {
    screen: HistoryComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.MESSAGE_COMPONENT]: {
    screen: MessageComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.DETAIL_MESSAGE_COMPONENT]: {
    screen: DetailMessageComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.MAP_COMPONENT]: {
    screen: MapComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.PROFILE]: {
    screen: ProfileComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.PERSONALINFO]: {
    screen: PersonalInfoComponent,
    navigationOptions: {
      header: null
    }
  },
  [ROUTE_KEY.MENU_COMPONENT]: {
    screen: MenuComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.DETAIL_STORE_COMPONENT]: {
    screen: DetailStoreComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.ORDER_COMPONENT]: {
    screen: OrderComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.DETAIL_PRODUCT]: {
    screen: DetailProductComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.COMPLETED_ORDER_COMPONENT]: {
    screen: CompletedOrderComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.DETAIL_ORDER]: {
    screen: DetailOrderComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.CHOOSE_CATEGORY]: {
    screen: CategoryComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.REWARDS]: {
    screen: RewardsComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.REWARDS_HISTORY]: {
    screen: RewardsHistoryComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.NEWS]: {
    screen: NewsComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.PROMOTION_COMPONENT]: {
    screen: PromotionComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  [ROUTE_KEY.WEB_VIEW]: {
    screen: WebViewComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  }
};

const CollapseExpand = (index, position) => {
  const inputRange = [index - 1, index, index + 1];
  const opacity = position.interpolate({
    inputRange,
    outputRange: [0, 1, 1]
  });

  const scaleY = position.interpolate({
    inputRange,
    outputRange: [0, 1, 1]
  });

  return {
    opacity,
    transform: [{ scaleY }]
  };
};

const SlideFromRight = (index, position, width) => {
  const translateX = position.interpolate({
    inputRange: [index - 1, index, index + 1],
    outputRange: [width, 0, 0]
  });
  const slideFromRight = { transform: [{ translateX }] };
  return slideFromRight;
};
const slowFade = (index, position) => {
  const opacity = position.interpolate({
    inputRange: [index - 1, index, index + 1],
    outputRange: [0, 1, 1]
  });
  return { opacity };
};

const TransitionConfiguration = () => ({
  transitionSpec: {
    duration: 500,
    easing: Easing.out(Easing.poly(4)),
    timing: Animated.timing,
    useNativeDriver: true
  },
  screenInterpolator: sceneProps => {
    const { layout, position, scene } = sceneProps;
    const width = layout.initWidth;
    const { index, route } = scene;
    const params = route.params || {}; // <- That's new
    const transition = params.transition || 'default'; // <- That's new
    return {
      collapseExpand: CollapseExpand(index, position),
      none: null,
      slowFade: slowFade(index, position),
      slideFromRight: SlideFromRight(index, position, width),
      default: slowFade(index, position)
    }[transition];
  }
});

const stackAppConfiguration = {
  initialRouteName: ROUTE_KEY.SPLASH,
  transitionConfig: TransitionConfiguration
};

const Navigator = StackNavigator(routeAppConfiguration, stackAppConfiguration);
const prevGetStateForAction = Navigator.router.getStateForAction;

Navigator.router.getStateForAction = (action, state) => {
  if (
    action.type === 'Navigation/BACK' &&
    state &&
    (state.routes[state.index].routeName === ROUTE_KEY.MAIN ||
      state.routes[state.index].routeName === ROUTE_KEY.SPLASH ||
      state.routes[state.index].routeName === ROUTE_KEY.PRELOGIN ||
      state.routes[state.index].routeName === ROUTE_KEY.ADDPROFILE)
  ) {
    return state;
  }

  return prevGetStateForAction(action, state);
};

function backHandler() {
  if (global.isBackAndroidButtonPressed) {
    const MyBridge = NativeModules.MyBridge;
    if (MyBridge) {
      MyBridge.navigateToLauncher();
    }
  }
  global.isBackAndroidButtonPressed = true;
  setTimeout(() => {
    global.isBackAndroidButtonPressed = false;
  }, 500);
  return true;
}

function getCurrentRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getCurrentRouteName(route);
  }
  return route.routeName;
}

const AppNavigator = () => {
  BackHandler.addEventListener('hardwareBackPress', backHandler);
  return (
    <Navigator
      ref={nav => {
        rootNavigator = nav;
      }}
      onNavigationStateChange={(prevState, currentState) => {
        const currentScreen = getCurrentRouteName(currentState);
        if (
          currentScreen === ROUTE_KEY.MAIN ||
          currentScreen === ROUTE_KEY.SPLASH ||
          currentScreen === ROUTE_KEY.PRELOGIN ||
          currentScreen === ROUTE_KEY.ADDPROFILE
        ) {
          BackHandler.addEventListener('hardwareBackPress', backHandler);
        } else {
          BackHandler.removeEventListener('hardwareBackPress', backHandler);
        }
      }}
    />
  );
};

export let rootNavigator = null;
export default AppNavigator;
