import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducer';

const middlewares = [thunk];
// create store
const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose;

const enhancers = composeEnhancers(
  applyMiddleware(...middlewares)
  // other store enhancers if any
);
const store = createStore(reducer, enhancers);

// const middlewares = [thunk];

// const enhancers = [];

// // create store
// const store = createStore(
//   reducer,
//   {},
//   compose(
//     applyMiddleware(...middlewares),
//     ...enhancers
//   )
// );
export default store;
