import update from 'react-addons-update';
import {
    LOAD_OTHER_GROUP_LIST,
    LOAD_MY_GROUP_LIST,
    LOAD_SEARCH_GROUP,
    LOAD_LIST_REQUEST_JOIN_GROUP,
    LOAD_LIST_MEMBER,

    UPDATE_MY_GROUP_LIST,
    UPDATE_OTHER_GROUP_LIST,
    UPDATE_LIST_MEMBER,
    UPDATE_GROUP_INFO,
    UPDATE_LIST_SEARCH_GROUP,
    UPDATE_USER_INFO,
    UPDATE_LIST_MEMBERS_REQUEST,

    OUT_OF_DATA_OTHER_GROUPS,
    OUT_OF_DATA_MY_GROUPS,
    OUT_OF_DATA_SEARCH_GROUP,
    OUT_OF_DATA_LIST_MEMBER,

    RESET_DATA_OTHER_GROUPS,
    RESET_DATA_MY_GROUPS,
    RESET_DATA_LIST_MEMBER,
    CLEAR_DATA,

    UPDATE_LIST_SUGGEST_MEMBER,
    OUT_OF_DATA_LIST_SUGGEST_FRIEND,
    REFRESH_LIST_SUGGEST_FRIEND
} from '../../../constants/Constants';
import { mergerTwoArray } from '../../../utils/numberUtils';

const initialState = {
    listOtherGroups: [],
    listMyGroups: [],
    listMembers: [], //this will stored a list of members in the group
    listMembersRequest: [],
    groupInfo: {},
    userInfo: {},
    listSearch: {},
    currentOtherGroupPages: 1,
    currentMyGroupPages: 1,
    currentSearchGroupPages: 1,
    currentListMemberPages: 1,
    outOfDataOtherGroups: false,
    outOfDataMyGroups: false,
    outOfDateSearchGroup: false,
    outOfDataListMember: false,
    listRequestJoinGroup: [],
    listSuggestMember: [],
    listSuggestMemberPages: 0,
    listSuggestMemberOutOfData: false
};

const groupReducer = (state = initialState, action) => {
    switch (action.type) {
        case CLEAR_DATA: {
            return initialState;
        }
        case UPDATE_LIST_MEMBERS_REQUEST:
            return update(state, {
                listRequestJoinGroup: {
                    $set: action.payload
                }
            });
        case UPDATE_USER_INFO:
            return update(state, {
                userInfo: {
                    $set: action.payload
                }
            });
        //load group detail 1st time:
        case UPDATE_GROUP_INFO:
            return update(state, {
                groupInfo: {
                    $set: action.payload
                }
            });
        //load 1st time
        case LOAD_OTHER_GROUP_LIST:
            return update(state, {
                listOtherGroups: {
                    $set: action.payload
                },
                currentOtherGroupPages: {
                    $set: 2,
                },
                outOfDataOtherGroups: {
                    $set: false,
                }
            });
        case LOAD_MY_GROUP_LIST:
            return update(state, {
                listMyGroups: {
                    $set: action.payload.map(e => ({ ...e, isMyGroup: true }))
                },
                currentMyGroupPages: {
                    $set: 2,
                },
                outOfDataMyGroups: {
                    $set: false,
                }
            });
        case LOAD_SEARCH_GROUP:
            return update(state, {
                listSearch: {
                    $set: action.payload
                },
                currentSearchGroupPages: {
                    $set: 2
                },
                outOfDateSearchGroup: {
                    $set: false,
                }
            });
        case LOAD_LIST_MEMBER:
            return update(state, {
                listMembers: {
                    $set: action.payload
                },
                currentListMemberPages: {
                    $set: 2,
                },
                outOfDataListMember: {
                    $set: false,
                }
            });
        //load more
        case UPDATE_MY_GROUP_LIST:
            return update(state, {
                listMyGroups: {
                    $set: mergerTwoArray(state.listMyGroups, action.payload).map(e => ({ ...e, isMyGroup: true }))
                },
                currentMyGroupPages: {
                    $set: state.currentMyGroupPages + 1,
                }
            });
        case UPDATE_OTHER_GROUP_LIST:
            return update(state, {
                listOtherGroups: {
                    $set: mergerTwoArray(state.listOtherGroups, action.payload)
                },
                currentOtherGroupPages: {
                    $set: state.currentOtherGroupPages + 1,
                }
            });
        case UPDATE_LIST_MEMBER:
            return update(state, {
                listMembers: {
                    $set: mergerTwoArray(state.listMembers, action.payload)
                },
                currentListMemberPages: {
                    $set: state.currentListMemberPages + 1,
                }
            });
        //out of data
        case OUT_OF_DATA_LIST_MEMBER:
            return update(state, {
                outOfDataListMember: {
                    $set: true
                }
            });
        case OUT_OF_DATA_OTHER_GROUPS:
            return update(state, {
                outOfDataOtherGroups: {
                    $set: true,
                },
            });
        case OUT_OF_DATA_MY_GROUPS:
            return update(state, {
                outOfDataMyGroups: {
                    $set: true,
                }
            });
        case OUT_OF_DATA_SEARCH_GROUP:
            return update(state, {
                outOfDateSearchGroup: {
                    $set: true,
                }
            });
        //reset (Refresh) list:
        case RESET_DATA_LIST_MEMBER:
            return update(state, {
                listMembers: {
                    $set: action.payload
                },
                outOfDataListMember: {
                    $set: false
                },
                currentListMemberPages: {
                    $set: 2
                }
            });
        case RESET_DATA_OTHER_GROUPS:
            return update(state, {
                listOtherGroups: {
                    $set: action.payload
                },
                currentOtherGroupPages: {
                    $set: 2,
                },
                outOfDataOtherGroups: {
                    $set: false,
                }
            });
        case RESET_DATA_MY_GROUPS:
            return update(state, {
                listMyGroups: {
                    $set: action.payload.map(e => ({ ...e, isMyGroup: true }))
                },
                currentMyGroupPages: {
                    $set: 2,
                },
                outOfDataMyGroups: {
                    $set: false,
                }
            });
        case UPDATE_LIST_SEARCH_GROUP:
            return update(state, {
                listSearch: {
                    $set: mergerTwoArray(state.listSearch, action.payload)
                },
                currentSearchGroupPages: {
                    $set: state.currentSearchGroupPages + 1,
                }
            });
        case LOAD_LIST_REQUEST_JOIN_GROUP:
            return update(state, {
                listRequestJoinGroup: {
                    $set: action.payload,
                }
            });
        //suggest member:
        case UPDATE_LIST_SUGGEST_MEMBER: 
            return update(state, {
                listSuggestMember: {
                    $set: [...state.listSuggestMember, ...action.payload]
                },
                listSuggestMemberPages: {
                    $set: state.listSuggestMemberPages + 1
                }
            });
        case OUT_OF_DATA_LIST_SUGGEST_FRIEND:
            return update(state, {
                listSuggestMemberOutOfData: {
                    $set: true
                }
            });
        case REFRESH_LIST_SUGGEST_FRIEND:
            return update(state, {
                listSuggestMember: {
                    $set: action.payload
                },
                listSuggestMemberPages: {
                    $set: 1
                },
                listSuggestMemberOutOfData: {
                    $set: false
                }
            });
        default:
            return state;
    }
};

export default groupReducer;
