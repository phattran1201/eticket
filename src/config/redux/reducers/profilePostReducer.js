import update from 'react-addons-update';
import moment from 'moment';
import {
  LIST_POST_FOR_USER,
  LOAD_LIST_FAVOURITE_POST_FOR_USER,
  ADD_ITEM_IN_START_POSITION_LIST_USER_POSTS,
  REMOVE_POST,
  CLEAR_DATA,
  SHORTEN_LIST,
  UPDATE_LIST_FAVOURITE_POST_PAGES,
  OUT_OF_DATA_LIST_FAVOURITE_POST_PAGES,
  REFRESH_LIST_FAVOURITE_POST,
  LOAD_MORE_POST_FOR_USER,
  DELETE_POST_FOR_USER
} from '../../../constants/Constants';

const initialState = {
  listPostForUser: [],
  listFavouritePost: [],
  listFavouritePostPages: 1,
  listFavouritePostOutOfData: false
};

const profilePostReducer = (state = initialState, action) => {
  switch (action.type) {
    ///dang dùng
    case LIST_POST_FOR_USER:
      return update(state, {
        listPostForUser: {
          $set: action.payload.sort((a, b) => moment(b.created_at) - moment(a.created_at))
        }
      });
    case LOAD_MORE_POST_FOR_USER:
      return update(state, {
        listPostForUser: {
          $set: [
            ...state.listPostForUser,
            ...action.payload.sort((a, b) => moment(b.created_at) - moment(a.created_at))
          ]
        }
      });
    case DELETE_POST_FOR_USER:
      return update(state, {
        listPostForUser: {
          $set: [...action.payload.sort((a, b) => moment(b.created_at) - moment(a.created_at))]
        }
      });
    ///chua dung

    // case CLEAR_DATA: {
    //   return initialState;
    // }

    // case SHORTEN_LIST: {
    //   return update(state, {
    //     listPost: {
    //       $set: state.listPost.length > 10 ? state.listPost.slice(0, 10) : state.listPost
    //     },
    //     listFavouritePost: {
    //       $set:
    //         state.listFavouritePost.length > 10
    //           ? state.listFavouritePost.slice(0, 10)
    //           : state.listFavouritePost
    //     }
    //   });
    // }
    // case REMOVE_POST:
    //   return update(state, {
    //     listPost: {
    //       $set: state.listPost.filter(e => e.id !== action.payload)
    //     },
    //     listFavouritePost: {
    //       $set: state.listFavouritePost.filter(e => e.id !== action.payload)
    //     }
    //   });
    // case LOAD_LIST_FAVOURITE_POST_FOR_USER:
    //   return update(state, {
    //     listFavouritePost: {
    //       $set: action.payload
    //     }
    //   });
    // case ADD_ITEM_IN_START_POSITION_LIST_USER_POSTS:
    //   return update(state, {
    //     listPost: {
    //       $set: [action.payload, ...state.listPost]
    //     }
    //   });
    // case UPDATE_LIST_FAVOURITE_POST_PAGES:
    //   return update(state, {
    //     listFavouritePostPages: {
    //       $set: state.listFavouritePostPages + 1
    //     }
    //   });
    // case OUT_OF_DATA_LIST_FAVOURITE_POST_PAGES:
    //   return update(state, {
    //     listFavouritePostOutOfData: {
    //       $set: true
    //     }
    //   });
    // case REFRESH_LIST_FAVOURITE_POST: {
    //   return update(state, {
    //     listFavouritePostPages: {
    //       $set: 1
    //     },
    //     listFavouritePostOutOfData: {
    //       $set: false
    //     }
    //   });
    // }
    default:
      return state;
  }
};

export default profilePostReducer;
