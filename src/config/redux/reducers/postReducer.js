import update from 'react-addons-update';
import {
  UPDATE_LIST_POSTS,
  REFRESH_LIST_POSTS,
  ADD_ITEM_IN_START_POSITION_LIST_POSTS,
  REMOVE_POST,
  SHORTEN_LIST,
  UPDATE_POST_LIKE_AMOUNT,
  UPDATE_LIST_POST_AFTER_USER_CREATE_POST,
  CLEAR_DATA,
  LIST_POST_ALL,
  LOAD_MORE_POST_ALL,
  DELETE_POST_ALL
} from '../../../constants/Constants';
import { mergerTwoArray } from '../../../utils/numberUtils';
import moment from 'moment';

const initialState = {
  listPosts: [],
  listPost: [],
  page: 2
};

const PostReducer = (state = initialState, action) => {
  switch (action.type) {
    ///dang dùng
    case LIST_POST_ALL:
      return update(state, {
        listPost: {
          $set: action.payload.sort((a, b) => moment(b.created_at) - moment(a.created_at))
        }
      });
    case LOAD_MORE_POST_ALL:
      return update(state, {
        listPost: {
          $set: [
            ...state.listPost,
            ...action.payload.sort((a, b) => moment(b.created_at) - moment(a.created_at))
          ]
        }
      });
    ///chua dung

    case CLEAR_DATA: {
      return initialState;
    }

    case SHORTEN_LIST:
      return update(state, {
        listPosts: {
          $set: state.listPosts.length > 10 ? state.listPosts.slice(0, 10) : state.listPosts
        }
      });
    case REMOVE_POST:
      return update(state, {
        listPosts: {
          $set: state.listPosts.filter(e => e.id !== action.payload)
        }
      });
    case UPDATE_LIST_POSTS:
      return update(state, {
        listPosts: {
          $set: mergerTwoArray(state.listPosts, action.payload)
        },
        page: {
          $set: state.page + 1
        }
      });
    case UPDATE_POST_LIKE_AMOUNT:
      return update(state, {
        listPosts: {
          $set: state.listPosts.map(e => {
            if (e.id === action.payload.post_id) {
              e.amount_of_like = action.payload.amount_of_like;
            }
            return e;
          })
        }
      });
    case REFRESH_LIST_POSTS:
      return update(state, {
        listPosts: {
          $set: action.payload.listPosts
        },
        page: {
          $set: action.payload.page
        }
      });
    case ADD_ITEM_IN_START_POSITION_LIST_POSTS:
      return update(state, {
        listPosts: {
          $set: [action.payload, ...state.listPosts]
        }
      });
    case UPDATE_LIST_POST_AFTER_USER_CREATE_POST:
      return update(state, {
        listPosts: {
          $set: action.payload
        }
      });
    default:
      return state;
  }
};

export default PostReducer;
