import { combineReducers } from 'redux';
import areaFilterReducer from './filter/areaFilterReducer';
import categoryFilterReducer from './filter/categoryFilterReducer';
import tradeTypeFilterReducer from './filter/tradeTypeFilterReducer';
import radiusFilterReducer from './filter/radiusFilterReducer';
import orderFilterReducer from './filter/orderFilterReducer';
import GenderFilterReducer from './filter/genderFilterReducer';

const reducer = combineReducers({
    area: areaFilterReducer,
    category: categoryFilterReducer,
    trade_type: tradeTypeFilterReducer,
    radius: radiusFilterReducer,
    order: orderFilterReducer,
    gender: GenderFilterReducer
});

export default reducer;
