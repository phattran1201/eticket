import update from 'react-addons-update';
import {
  UPDATE_SETTING_DATA, UPDATE_REGION_DATA, LOAD_LIST_NEWS_ARTICLES_DATA, LOAD_LIST_PROMOTION_ARTICLES_DATA, UPDATE_DETAIL_ARTICLE, LOAD_MORE_LIST_NEWS_ARTICLES_DATA, LOAD_MORE_LIST_PROMOTION_ARTICLES_DATA
} from '../../../constants/Constants';

const initialState = {
  settingData: {},
  regionData: {},
  listArticle: [],
  listNewsArticles: [],
  newsArticlesPaginate: {},
  listPromotionArticles: [],
  promotionArticlesPaginate: {},
  detailArticle: {}
};

const SettingReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_SETTING_DATA:
      return update(state, {
        settingData: {
          $set: action.payload
        }
      });
    case UPDATE_REGION_DATA:
      return update(state, {
        regionData: {
          $set: action.payload
        }
      });
    case LOAD_LIST_NEWS_ARTICLES_DATA:
      return update(state, {
        listNewsArticles: {
          $set: action.payload.listNewsArticles
        },
        newsArticlesPaginate: {
          $set: action.payload.newsArticlesPaginate
        }
      });
    case LOAD_LIST_PROMOTION_ARTICLES_DATA:
      return update(state, {
        listPromotionArticles: {
          $set: action.payload.listPromotionArticles
        },
        promotionArticlesPaginate: {
          $set: action.payload.promotionArticlesPaginate
        }
      });
    case UPDATE_DETAIL_ARTICLE:
      return update(state, {
        detailArticle: {
          $set: action.payload
        }
      });
    case LOAD_MORE_LIST_NEWS_ARTICLES_DATA:
      return update(state, {
        listNewsArticles: {
          $set: [...state.listNewsArticles, ...action.payload.listNewsArticles]
        },
        newsArticlesPaginate: {
          $set: action.payload.newsArticlesPaginate
        }
      });
    case LOAD_MORE_LIST_PROMOTION_ARTICLES_DATA:
      return update(state, {
        listPromotionArticles: {
          $set: [...state.listPromotionArticles, ...action.payload.listPromotionArticles]
        },
        promotionArticlesPaginate: {
          $set: action.payload.promotionArticlesPaginate
        }
      });
    default:
      return state;
  }
};

export default SettingReducer;
