import update from 'react-addons-update';
import {
    GET_LIST_CALL_HISTORY,
    UPDATE_LIST_CALL_HISTORY
} from '../../../constants/Constants';

const initialState = {
    listCallHistory: [],
};

const CallHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_LIST_CALL_HISTORY:
            return update(state, {
                listCallHistory: {
                    $set: action.payload
                }
            });
        case UPDATE_LIST_CALL_HISTORY:
            return update(state, {
                listCallHistory: {
                    $set: [...state.listCallHistory, ...action.payload]
                }
            })
        default:
            return state;
    }
};

export default CallHistoryReducer;
