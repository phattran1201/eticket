import update from 'react-addons-update';
import {
  UPDATE_CURRENT_LOCATION,
  UPDATE_CURRENT_USER_DATA,
  UPDATE_CURRENT_TOKEN,
  UPDATE_USER_SETTINGS,
  LOAD_STORE_INFORMATION_FOR_USER,
  UPDATE_USER_TYPE,
  SET_LOGGED_IN,
  UPDATE_LIST_FRIEND,
  UPDATE_LIST_FOLLOWINGS,
  UPDATE_LIST_LIKE_CMT,
  UPDATE_LIST_SUGGEST_FRIEND,
  UPDATE_LIST_SEARCH_FRIEND,
  OUT_OF_DATA_LIST_SUGGEST_FRIEND,
  REFRESH_LIST_SUGGEST_FRIEND,
  CHANGE_ALWAYS_ACCEPT_INSTANT_PRODUCT,
  LOAD_STORE_MEMBER,
  CLEAR_DATA,
  REPLACE_LIST_SUGGEST_FRIEND,
  UPDATE_CURRENT_USER_IMAGE
} from '../../../constants/Constants';
// import { State } from '../../../../node_modules/react-native-image-zoom-viewer/src/image-viewer.type';

const initialState = {
  currentLocation: {
    latitude: 10.7725451,
    longitude: 106.6980413,
    region: null
  },
  token: '',
  userData: null,
  currentSettings: null,
  storeInfo: null,
  storeMember: [],
  isLoggedIn: false,
  listFriend: [],
  listSuggestFriend: [],
  listFollowings: [],
  listLikeCmt: [],
  listSearch: {},
  alwaysAcceptInstantProduct: false,
  listSuggestFriendPages: 0,
  listSuggestFriendOutOfData: false
};

const UserReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_DATA: {
      return initialState;
    }
    case UPDATE_CURRENT_LOCATION:
      return update(state, {
        currentLocation: {
          $set: action.payload
        }
      });
    case SET_LOGGED_IN:
      return update(state, {
        isLoggedIn: {
          $set: action.payload
        }
      });
    case UPDATE_CURRENT_USER_DATA:
      return update(state, {
        userData: {
          $set: action.payload
        }
      });
    case UPDATE_CURRENT_USER_IMAGE:
      return update(state, {
        userData: {
          ...state.userData,
          avatar: action.payload
        }
      });
    case UPDATE_CURRENT_TOKEN:
      return update(state, {
        token: {
          $set: action.payload
        }
      });
    case UPDATE_USER_SETTINGS:
      return update(state, {
        currentSettings: {
          $set: action.payload
        }
      });
    case LOAD_STORE_INFORMATION_FOR_USER:
      return update(state, {
        storeInfo: {
          $set: action.payload
        }
      });
    case UPDATE_USER_TYPE:
      return update(state, {
        userData: {
          $set: {
            ...state.userData,
            user_type: action.payload
          }
        }
      });
    case UPDATE_LIST_FRIEND:
      return update(state, {
        listFriend: {
          $set: action.payload
        }
      });
    case UPDATE_LIST_SUGGEST_FRIEND:
      return update(state, {
        listSuggestFriend: {
          $set: [...state.listSuggestFriend, ...action.payload]
        },
        listSuggestFriendPages: {
          $set: state.listSuggestFriendPages + 1
        }
      });
    case REPLACE_LIST_SUGGEST_FRIEND:
      return update(state, {
        listSuggestFriend: {
          $set: action.payload
        }
      });
    case OUT_OF_DATA_LIST_SUGGEST_FRIEND:
      return update(state, {
        listSuggestFriendOutOfData: {
          $set: true
        }
      });
    case REFRESH_LIST_SUGGEST_FRIEND:
      return update(state, {
        listSuggestFriend: {
          $set: action.payload
        },
        listSuggestFriendPages: {
          $set: 1
        },
        listSuggestFriendOutOfData: {
          $set: false
        }
      });
    case UPDATE_LIST_FOLLOWINGS:
      return update(state, {
        listFollowings: {
          $set: action.payload
        }
      });
    case UPDATE_LIST_LIKE_CMT:
      return update(state, {
        listLikeCmt: {
          $set: action.payload
        }
      });
    case UPDATE_LIST_SEARCH_FRIEND:
      return update(state, {
        listSearch: {
          $set: action.payload
        }
      });
    case CHANGE_ALWAYS_ACCEPT_INSTANT_PRODUCT:
      return update(state, {
        alwaysAcceptInstantProduct: {
          $set: true
        }
      });
    case LOAD_STORE_MEMBER:
      return update(state, {
        storeMember: {
          $set: action.payload
        }
      });
    default:
      return state;
  }
};

export default UserReducer;
