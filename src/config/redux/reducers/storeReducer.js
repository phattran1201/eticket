import update from 'react-addons-update';
import {
  UPDATE_LIST_STORES,
} from '../../../constants/Constants';

const initialState = {
  listStores: [],
};

const StoreReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_LIST_STORES:
      return update(state, {
        listStores: {
          $set: action.payload
        }
      });
    default:
      return state;
  }
};

export default StoreReducer;
