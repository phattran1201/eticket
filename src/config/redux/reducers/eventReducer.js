import update from 'react-addons-update';
import { CLEAR_DATA, LIST_EVENT } from '../../../constants/Constants';

const initialState = {
  listEvent: []
};

const EventReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_DATA: {
      return initialState;
    }
    case LIST_EVENT:
      return update(state, {
        listEvent: {
          $set: action.payload
        }
      });
    default:
      return state;
  }
};

export default EventReducer;
