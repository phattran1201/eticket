import update from 'react-addons-update';
import { UPDATE_LIST_MEDIA, REFRESH_LIST_MEDIA, SHORTEN_LIST, CLEAR_DATA } from '../../../constants/Constants';

const initialState = {
  listMedia: []
};

const mediaReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_DATA: {
      return initialState;
    } 
    case UPDATE_LIST_MEDIA:
      return update(state, {
        listMedia: {
          $set: [...state.listMedia, ...action.payload]
        }
      });
      case SHORTEN_LIST: {
        return update(state, {
          listMedia: {
          $set: state.listMedia.length > 10 ? state.listMedia.slice(0, 10) : state.listMedia
        }
      });
    }
    case REFRESH_LIST_MEDIA:
      return update(state, {
        listMedia: {
          $set: action.payload
        }
      });
    default:
      return state;
  }
};

export default mediaReducer;
