import update from 'react-addons-update';
import {
  CLEAR_CONNECTION_STATE,
  UPDATE_CONVERSATION_CONNECTION_STATE,
  ADD_A_CONVERSATION,
  REMOVE_A_CONVERSATION,
  UPDATE_LAST_MESSAGE_OF_A_CONVERSATION,
  UPDATE_LIST_CONVERSATION,
  REFRESH_LIST_CONVERSATION,
  CLEAR_DATA,
  SHORTEN_LIST,
  REMOVE_ALL_CONVERSATION,
  READ_ALL_MESSAGE,
  GET_LIST_INTERESTED_CONVERSATION,
  ADD_A_INTERESTED_CONVERSATION,
  DISINTERESTED_CONVERSATION,
  ADD_TO_LIST_INTERESTED_CONVERSATION
} from '../../../constants/Constants';
import { mergerTwoArray } from '../../../utils/numberUtils';

const initialState = {
  list_conversation: [],
  list_interested_conversation: [],
  list_connected_user_id: []
};

const ConversationReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHORTEN_LIST:
      return update(state, {
        list_conversation: {
          $set:
            state.list_conversation.length > 10
              ? state.list_conversation.slice(0, 10)
              : state.list_conversation
        }
      });
    case CLEAR_DATA: {
      return initialState;
    }
    case CLEAR_CONNECTION_STATE: {
      return update(state, {
        list_connected_user_id: {
          $set: []
        }
      });
    }
    case UPDATE_CONVERSATION_CONNECTION_STATE: {
      const newConnectedUserIdList = state.list_connected_user_id;

      action.payload.userIds.forEach(newId => {
        let isExist = false;
        state.list_connected_user_id.forEach(oldId => {
          if (newId === oldId) {
            isExist = true;
          }
        });

        if (action.payload.isConnected && !isExist) {
          newConnectedUserIdList.push(newId);
        } else if (!action.payload.isConnected && isExist) {
          newConnectedUserIdList.splice(newId, 1);
        }
      });

      return update(state, {
        list_connected_user_id: {
          $set: newConnectedUserIdList
        }
      });
    }
    case GET_LIST_INTERESTED_CONVERSATION: {
      return update(state, {
        list_interested_conversation: {
          $set: action.payload
        }
      })
    }
    case ADD_TO_LIST_INTERESTED_CONVERSATION: {
      return update(state, {
        list_interested_conversation: {
          $set: [...state.list_interested_conversation, ...action.payload]
        }
      })
    }
    case ADD_A_INTERESTED_CONVERSATION: {
      let temp = [...state.list_interested_conversation];
      temp.push(action.payload);
      return update(state, {
        list_interested_conversation: {
          $set: temp
        }
      })
    }
    case DISINTERESTED_CONVERSATION: {
      let temp = state.list_interested_conversation;
      return update(state, {
        list_interested_conversation: {
          $set: temp.filter(e => e.id !== action.payload.id)
        }
      })
    }
    case ADD_A_CONVERSATION: {
      return update(state, {
        list_conversation: {
          $set: state.list_conversation
            .filter(item => item.id !== action.payload.conversation.id)
            .concat(action.payload.conversation)
        }
      });
    }
    case REMOVE_A_CONVERSATION: {
      return update(state, {
        list_conversation: {
          $set: state.list_conversation.filter(item => item.id !== action.payload.conversationId)
        }
      });
    }
    case REMOVE_ALL_CONVERSATION: {
      return update(state, {
        list_conversation: {
          $set: []
        }
      });
    }
    case UPDATE_LIST_CONVERSATION:
      return update(state, {
        list_conversation: {
          $set: mergerTwoArray(state.list_conversation, action.payload)
        }
      });
    case READ_ALL_MESSAGE: {
      return update(state, {
        list_conversation: {
          $set: action.payload
        }
      });
    }
    case UPDATE_LAST_MESSAGE_OF_A_CONVERSATION: {
      const listConversation = state.list_conversation;
      listConversation.forEach(item => {
        if (item.id === action.payload.conversationId) {
          item.messages = [action.payload.message];
          item.users_in_conversation = item.users_in_conversation.map(userInConversation => ({
            ...userInConversation,
            status: true
          }));
        }
      });
      return update(state, {
        list_conversation: {
          $set: listConversation
        }
      });
    }
    case REFRESH_LIST_CONVERSATION:
      return update(state, {
        list_conversation: {
          $set: action.payload
        }
      });
    default:
      return state;
  }
};

export default ConversationReducer;
