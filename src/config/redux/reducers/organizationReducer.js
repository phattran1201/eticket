import update from 'react-addons-update';
import { mergerTwoArray } from '../../../utils/numberUtils';
import { 
    REFRESH_LIST_ORGANIZATION, 
    LOAD_MORE_LIST_ORGANIZATION, 
    OUT_OF_DATA_LIST_ORGANIZATION, 
    UPDATE_ORGANIZATION_INFO,

    REFRESH_LIST_GROUP_OF_ORGANIZATION, 
    LOAD_MORE_LIST_GROUP_OF_ORGANIZATION, 
    OUT_OF_DATA_LIST_GROUP_OF_ORGANIZATION,
    UPDATE_ORGANIZATION_DASHBOARD, 
} from '../../../constants/Constants';

const initialState = {
    //list organization
    listOrganization: [],
    listOrganizationPages: 0,
    listOrganizationOutOfData: false,
    //Organization info:
    organizationInfo: {},
    organizationDashBoard: {},
    //list group in organization:
    listGroupOfOrganization: [],
    listGroupOfOrganizationPages: 0,
    listGroupOfOrganizationOutOfData: false,

};

const organizationReducer = (state = initialState, action) => {
    switch (action.type) {
        case REFRESH_LIST_ORGANIZATION:
            return update(state, {
                listOrganization: {
                    $set: action.payload
                },
                listOrganizationPages: {
                    $set: 1
                },
                listOrganizationOutOfData: {
                    $set: false
                }
            });
        case LOAD_MORE_LIST_ORGANIZATION:
            return update(state, {
                listOrganization: {
                    $set: mergerTwoArray(state.listOrganization, action.payload)
                },
                listOrganizationPages: {
                    $set: state.listOrganizationPages + 1
                }
            });
        case OUT_OF_DATA_LIST_ORGANIZATION:
            return update(state, {
                listOrganizationOutOfData: {
                    $set: true
                }
            });
        case UPDATE_ORGANIZATION_INFO:
            return update(state, {
                organizationInfo: {
                    $set: action.payload
                }
            });
        case UPDATE_ORGANIZATION_DASHBOARD:
            return update(state, {
                organizationDashBoard: {
                    $set: action.payload
                }
            });
        case REFRESH_LIST_GROUP_OF_ORGANIZATION:
            return update(state, {
                listGroupOfOrganization: {
                    $set: action.payload
                },
                listGroupOfOrganizationPages: {
                    $set: 1
                },
                listGroupOfOrganizationOutOfData: {
                    $set: false
                }
            });
        case LOAD_MORE_LIST_GROUP_OF_ORGANIZATION:
            return update(state, {
                listGroupOfOrganization: {
                    $set: mergerTwoArray(state.listGroupOfOrganization, action.payload)
                },
                listGroupOfOrganizationPages: {
                    $set: state.listGroupOfOrganizationPages + 1
                }
            });
        case OUT_OF_DATA_LIST_GROUP_OF_ORGANIZATION:
            return update(state, {
                listGroupOfOrganizationOutOfData: {
                    $set: true
                }
            });
        default:
            return state;
    }
};

export default organizationReducer;
