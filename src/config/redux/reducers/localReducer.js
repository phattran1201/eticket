import update from 'react-addons-update';
import {
    UPDTAE_LIST_SAVED_IMAGE,
    UPDTAE_LIST_EMOJI,
} from '../../../constants/Constants';

const initialState = {
    listSavedImage: [],
    listStickerCategory: [],
};

const localReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDTAE_LIST_SAVED_IMAGE:
            return update(state, {
                listSavedImage: {
                    $set: [...state.listSavedImage, action.payload],
                }
            });
        case UPDTAE_LIST_EMOJI:
            return update(state, {
                listStickerCategory: {
                    $set: action.payload,
                }
            });
        default: 
            return state;
    }
};

export default localReducer;
