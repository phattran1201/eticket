import update from 'react-addons-update';
import {
  UPDATE_LIST_BLOCKED,
  REMOVE_ITEM_LIST_BLOCKED,
  RESET_LIST_BLOCKED,
  ADD_ITEM_LIST_BLOCKED
} from '../../../constants/Constants';

const initialState = {
  listBlocked: [],
};

const BlockingReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_LIST_BLOCKED:
      return update(state, {
        listBlocked: {
          $set: [...state.listBlocked, ...action.payload]
        }
      });
    case RESET_LIST_BLOCKED:
      return update(state, {
        listBlocked: {
          $set: action.payload
        }
      });
    case REMOVE_ITEM_LIST_BLOCKED:
      return update(state, {
        listBlocked: {
          $set: state.listBlocked.filter(e => e.id !== action.payload)
        }
      });
    case ADD_ITEM_LIST_BLOCKED:
      return update(state, {
        listBlocked: {
          $set: [...state.listBlocked, action.payload]
        }
      });
    default:
      return state;
  }
};

export default BlockingReducer;
