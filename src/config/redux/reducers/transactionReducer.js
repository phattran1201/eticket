import update from 'react-addons-update';
import {
    LOAD_TRANSACTION_LIST,
    LOAD_WALLET_INFO,
    CLEAR_DATA,
    SHORTEN_LIST,
    LOAD_TRANSACTION_LIST_BUY,
    LOAD_TRANSACTION_LIST_SELL,
    LOAD_MORE_TRANSACTION_LIST_SELL,
    LOAD_MORE_TRANSACTION_LIST_BUY,
    OUT_OF_DATA_TRANSACTION_LIST_SELL,
    OUT_OF_DATA_TRANSACTION_LIST_BUY,
    UPDATE_TRANSACTION_LIST_BUY,
    UPDATE_TRANSACTION_LIST_SELL,
} from '../../../constants/Constants';
import { mergerTwoArray } from '../../../utils/numberUtils';

const initialState = {
    transactionList: [],
    walletInfo: null,
    sellTransactionList: [],
    buyTransactionList: [],
    sellPages: 0,
    buyPages: 0,
    sellOutOfData: false,
    buyOutOfData: false,
};

const TransactionReducer = (state = initialState, action) => {
    switch (action.type) {
        case CLEAR_DATA: {
            return initialState;
        }
        case SHORTEN_LIST: {
            return update(state, {
                transactionList: {
                    $set: state.transactionList.length > 10 ? state.transactionList.slice(0, 10) : state.transactionList
                },
                sellTransactionList: {
                    $set: state.sellTransactionList.length > 10 ? state.sellTransactionList.slice(0, 10) : state.sellTransactionList
                },
                buyTransactionList: {
                    $set: state.buyTransactionList.length > 10 ? state.buyTransactionList.slice(0, 10) : state.buyTransactionList
                }
            });
        }
        case LOAD_TRANSACTION_LIST:
            return update(state, {
                transactionList: {
                    $set: action.payload
                }
            });
        case UPDATE_TRANSACTION_LIST_SELL:
            return update(state, {
                sellTransactionList: {
                    $set: action.payload
                }
            });
        case UPDATE_TRANSACTION_LIST_BUY:
            return update(state, {
                buyTransactionList: {
                    $set: action.payload
                }
            });
        //load and refresh:
        case LOAD_TRANSACTION_LIST_SELL:
            return update(state, {
                sellTransactionList: {
                    $set: action.payload
                },
                sellPages: {
                    $set: 1
                },
                sellOutOfData: {
                    $set: false,
                }
            });
        case LOAD_TRANSACTION_LIST_BUY:
            return update(state, {
                buyTransactionList: {
                    $set: action.payload
                },
                buyPages: {
                    $set: 1
                },
                buyOutOfData: {
                    $set: false,
                }
            });
        //load more :
        case LOAD_MORE_TRANSACTION_LIST_SELL:
            return update(state, {
                sellTransactionList: {
                    $set: mergerTwoArray(state.sellTransactionList, action.payload)
                },
                sellPages: {
                    $set: state.sellPages + 1
                }
            });
        case LOAD_MORE_TRANSACTION_LIST_BUY:
            return update(state, {
                buyTransactionList: {
                    $set: mergerTwoArray(state.buyTransactionList, action.payload)
                },
                buyPages: {
                    $set: state.buyPages + 1
                }
            });
        //out of data
        case OUT_OF_DATA_TRANSACTION_LIST_SELL:
            return update(state, {
                sellOutOfData: {
                    $set: true,
                }
            });
        case OUT_OF_DATA_TRANSACTION_LIST_BUY:
            return update(state, {
                buyOutOfData: {
                    $set: true,
                }
            });
        case LOAD_WALLET_INFO:
            return update(state, {
                walletInfo: {
                    $set: action.payload
                }
            });
        default:
            return state;
    }
};

export default TransactionReducer;
