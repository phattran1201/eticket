import update from 'react-addons-update';
import {
    UPDATE_LIST_PARTNERS,
    ADD_TO_LIST_PARTNERS,
    UPDATE_LIST_RECOMMENDED_PARTNERS,
    REMOVE_ITEM_LIST_PARTNERS,
    UPDATE_LIST_ALL_PARTNERS,
    UPDATE_LIST_SEARCH_PARTNERS,
    ADD_LIST_SEARCH_PARTNERS
} from '../../../constants/Constants';

const initialState = {
    listPartners: [],
    listAllPartners: [],
    listRecommendedPartners: [],
    listSearchPartners: []
};

const PartnersReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_LIST_RECOMMENDED_PARTNERS:
            return update(state, {
                listRecommendedPartners: {
                    $set: action.payload
                }
            });
        case UPDATE_LIST_PARTNERS:
            return update(state, {
                listPartners: {
                    $set: action.payload
                }
            });
        case UPDATE_LIST_ALL_PARTNERS:
            return update(state, {
                listAllPartners: {
                    $set: action.payload
                }
            });
        case ADD_TO_LIST_PARTNERS:
            return update(state, {
                listPartners: {
                    $set: [...state.listPartners, ...action.payload]
                }
            });
        case REMOVE_ITEM_LIST_PARTNERS:
            return update(state, {
                listPartners: {
                    $set: state.listPartners.filter(e => e.id !== action.payload)
                }
            });
        case UPDATE_LIST_SEARCH_PARTNERS:
            return update(state, {
                listSearchPartners: {
                    $set: action.payload
                }
            });
        case ADD_LIST_SEARCH_PARTNERS:
            return update(state, {
                listSearchPartners: {
                    $set: [...state.listSearchPartners, ...action.payload]
                }
            });
        default:
            return state;
    }
};

export default PartnersReducer;
