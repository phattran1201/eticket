import update from 'react-addons-update';
import { LOAD_DRAW_MONEY, REFRESH_LIST_NOTIFICATION, CLEAR_DATA, SHORTEN_LIST } from '../../../constants/Constants';

const initialState = {
  listdrawmoney: []
};

const HistorydrawReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_DATA: {
      return initialState;
    }
    case SHORTEN_LIST: {
    return update(state, {
      listdrawmoney: {
      $set: state.listdrawmoney.length > 10 ? state.listdrawmoney.slice(0, 10) : state.listdrawmoney
    }
  });
}
    case LOAD_DRAW_MONEY:
      return update(state, {
        listdrawmoney: {
          $set: action.payload
        }
      });
    // case REFRESH_LIST_NOTIFICATION:
    //   return update(state, {
    //     listnotitfication: {
    //       $set: action.payload
    //     }
    //   });
    default:
      return state;
  }
};

export default HistorydrawReducer;
