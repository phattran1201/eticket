import update from 'react-addons-update';
import {
  UPDATE_LIST_RATING,
  REMOVE_ITEM_LIST_RATING
} from '../../../constants/Constants';

const initialState = {
  listRating: [],
};

const RatingReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_LIST_RATING:
      return update(state, {
        listRating: {
          $set: action.payload
        }
      });
    case REMOVE_ITEM_LIST_RATING:
      return update(state, {
        listRating: {
          $set: state.listRating.filter(e => e.id !== action.payload)
        }
      });
    default:
      return state;
  }
};

export default RatingReducer;
