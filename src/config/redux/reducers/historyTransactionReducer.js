import update from 'react-addons-update';
import { LOAD_IMPORT_WALLET, LOAD_EXPORT_WALLET, CLEAR_DATA, SHORTEN_LIST } from '../../../constants/Constants';

const initialState = {
  listExport: [],
  listImport: []
};
// const listExportwallet

const historyTransactionReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_DATA: {
      return initialState;
    }
    case SHORTEN_LIST: {
      return update(state, {
        listExport: {
        $set: state.listExport.length > 10 ? state.listExport.slice(0, 10) : state.listExport
      },
      listImport: {
        $set: state.listImport.length > 10 ? state.listImport.slice(0, 10) : state.listImport
      }
    });
  }
    case LOAD_IMPORT_WALLET:
      return update(state, {
        listImport: {
          $set: action.payload
        }
      });
    case LOAD_EXPORT_WALLET:
      return update(state, {
        listExport: {
          $set: action.payload
        }
      });
    default:
      return state;
  }
};

export default historyTransactionReducer;
