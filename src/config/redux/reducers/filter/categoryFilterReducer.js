import update from 'react-addons-update';
import { 
    UPDATE_LIST_CATEGORY_FILTER,
    UPDATE_CATEGORY_FILTER,

    UPDATE_CATEGORY_FILTER_FOR_MAP,
    UPDATE_NEW_CATEGORY_FILTER_FOR_MAP,
} from '../../../../constants/Constants';

const initialState = {
    listCategory: [],
    currentCategory: {},

    newCategoryForMap: {},
    currentCategoryForMap: {},
};

const CategoryFilterReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_LIST_CATEGORY_FILTER:
            return update(state, {
                listCategory: {
                    $set: action.payload
                },
                newCategoryForMap: {
                    $set: action.payload[0]
                },
                currentCategory: {
                    $set: action.payload[0]
                },
                currentCategoryForMap: {
                    $set: action.payload[0]
                }
            });
        case UPDATE_CATEGORY_FILTER:
            return update(state, {
                currentCategory: {
                    $set: action.payload
                }
            });
        case UPDATE_CATEGORY_FILTER_FOR_MAP:
            return update(state, {
                newCategoryForMap: {
                    $set: action.payload
                },
                currentCategoryForMap: {
                    $set: action.payload
                }
            });
        case UPDATE_NEW_CATEGORY_FILTER_FOR_MAP: 
            return update(state, {
                newCategoryForMap: {
                    $set: action.payload
                },
            });
        default: 
            return state;
    }
};

export default CategoryFilterReducer;
