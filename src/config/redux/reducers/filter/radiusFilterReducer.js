import update from 'react-addons-update';
import { 
    UPDATE_RADIUS_FILTER, 
    UPDATE_NEW_RADIUS_FILTER,
    UPDATE_FILTER_BY_RADIUS,
    UPDATE_FILTER_BY_RADIUS_NEW
} from '../../../../constants/Constants';

const initialState = {
    currentRadius: 5,
    newRadius: 5,
    isFilterByRadiusCurrent: true,
    isFilterByRadiusNew: true,
};

const RadiusFilterReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_RADIUS_FILTER:
            return update(state, {
                currentRadius: {
                    $set: action.payload
                },
                newRadius: {
                    $set: action.payload
                }
            });
        case UPDATE_NEW_RADIUS_FILTER:
            return update(state, {
                newRadius: {
                    $set: action.payload
                }
            });
        case UPDATE_FILTER_BY_RADIUS:
            return update(state, {
                isFilterByRadiusCurrent: {
                    $set: action.payload
                },
                isFilterByRadiusNew: {
                    $set: action.payload
                }
            });
        case UPDATE_FILTER_BY_RADIUS_NEW:
            return update(state, {
                isFilterByRadiusNew: {
                    $set: action.payload
                },
            });
        default: 
            return state;
    }
};

export default RadiusFilterReducer;
