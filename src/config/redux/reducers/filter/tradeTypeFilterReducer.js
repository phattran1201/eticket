import update from 'react-addons-update';
import { 
    UPDATE_TRADE_TYPE_FILTER, 
    UPDATE_NEW_TRADE_TYPE_FILTER_FOR_MAP,
    UPDATE_TRADE_TYPE_FILTER_FOR_MAP
} from '../../../../constants/Constants';

const initialState = {
    currentTradeType: 'ALL',

    currentTradeTypeForMap: 'ALL',
    newTradeTypeForMap: 'ALL',
};

const TradeTypeFilterReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_TRADE_TYPE_FILTER:
            return update(state, {
                currentTradeType: {
                    $set: action.payload
                },
            });
        case UPDATE_TRADE_TYPE_FILTER_FOR_MAP:
            return update(state, {
                currentTradeTypeForMap: {
                    $set: action.payload
                },
                newTradeTypeForMap: {
                    $set: action.payload
                }
            });
        case UPDATE_NEW_TRADE_TYPE_FILTER_FOR_MAP:
            return update(state, {
                newTradeTypeForMap: {
                    $set: action.payload
                },
            });
        default: 
            return state;
    }
};

export default TradeTypeFilterReducer;
