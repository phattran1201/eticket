import update from 'react-addons-update';
import { 
    UPDATE_CURRENT_AREA_FILTER,
    LOAD_AREA_FILTER,
} from '../../../../constants/Constants';

const initialState = {
    currentArea: {},
};

const AreaFilterReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_AREA_FILTER:
            return update(state, {
                currentArea: {
                    $set: action.payload
                },
            });
        case UPDATE_CURRENT_AREA_FILTER:
            return update(state, {
                currentArea: {
                    $set: action.payload
                },
            });
        default: 
            return state;
    }
};

export default AreaFilterReducer;
