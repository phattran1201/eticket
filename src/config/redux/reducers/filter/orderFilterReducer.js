import update from 'react-addons-update';
import { UPDATE_ORDER_FILTER, } from '../../../../constants/Constants';
import strings from '../../../../constants/Strings';

const listOrder = [
    {
        id: `["sign_in_time", "DESC"]`,
        name: strings.recently_visited
    },
    {
        id: `["created_at", "DESC"]`,
        name: strings.new_user
    },
    {
        id: `"age"`,
        name: strings.age
    }
];

const initialState = {
    listOrder,
    currentOrder: listOrder[0],
};

const OrderFilterReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_ORDER_FILTER:
            return update(state, {
                currentOrder: {
                    $set: action.payload
                },
            });
        default:
            return state;
    }
};

export default OrderFilterReducer;
