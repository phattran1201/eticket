import update from 'react-addons-update';
import { UPDATE_GENDER_FILTER } from '../../../../constants/Constants';
import strings from '../../../../constants/Strings';

export const listGender = [
    {
        name: strings.male,
        id: 'MALE',
    },
    {
        name: strings.female,
        id: 'FEMALE',
    },
    {
        name: strings.all,
        id: 'ALL',
    },
];

const initialState = {
    listGender,
    currentGender: listGender[0]
};

const GenderFilterReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_GENDER_FILTER:
            return update(state, {
                currentGender: {
                    $set: action.payload
                },
            });
        default:
            return state;
    }
};

export default GenderFilterReducer;
