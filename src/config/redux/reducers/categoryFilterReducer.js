import update from 'react-addons-update';
import {
    LOAD_LIST_GLOBAL_CATEGORY_FILTER,
    UPDATE_GLOBAL_CATEGORY_FILTER,
} from '../../../constants/Constants';

const initialState = {
    listGlobalCategory: [],
    currentObject: {},
};

const CategoryFilterReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_LIST_GLOBAL_CATEGORY_FILTER:
            return update(state, {
                listGlobalCategory: {
                    $set: action.payload
                },
                currentObject: {
                    $set: action.payload[0]
                }
            });
        case UPDATE_GLOBAL_CATEGORY_FILTER:
            return update(state, {
                currentObject: {
                    $set: action.payload
                }
            });
        default:
            return state;
    }
};

export default CategoryFilterReducer;
