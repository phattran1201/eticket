import update from 'react-addons-update';


import { 
    UPDATE_LIST_PRODUCTS, 
    SHORTEN_LIST,
    UPDATE_DETAIL_NEW_PRODUCT,
    OUTOFDATA_LIST_PRODUCTS,
    RESET_LIST_PRODUCTS,
    LOAD_AREA_FOR_NEW_PRODUCT_DETAIL,
    UPDATE_AREA_FOR_NEW_PRODUCT_DETAIL,
    RESET_LIST_PRODUCTS_ON_MAP
} from '../../../constants/Constants';
import { mergerTwoArray } from '../../../utils/numberUtils';
import moment from 'moment';

const initialState = {
    listProducts: [],
    currentPage: 0,
    outOfData: false,
    listProductsOnMap: [],
    detailNewProduct: {
        name: '',
        price: 0,
        short_description: '',
        created_at: moment(),
        type: {
            key: 'SELL',
            name: 'Cần bán',
        },
        category: undefined,
        is_from_store: {
            key: false,
            name: 'Cá nhân',
        },
        list_image: [],
        currentArea: {},
    }
};

const getLocationFromProduct = (product) => {
    let location = {};
    location.latitude = product.is_limit_duration ? product.latitude : product.latitude_temp;
    location.longitude = product.is_limit_duration ? product.longitude : product.longitude_temp;
    return location;
};

const getProductWithANewLocation = (product, newLocation) => {
    if (product.is_limit_duration) {
        product.latitude = newLocation.latitude;
        product.longitude = newLocation.longitude;
    } else {
        product.latitude_temp = newLocation.latitude;
        product.longitude_temp = newLocation.longitude;
    }
    return product;
};

const getProductWithAUniqueLocation = (product, listProducts) => {
    let isLocationUnique = true;
    let productLocation = getLocationFromProduct(product);
    listProducts.forEach(e => {
        if (product.id !== e.id && isTwoLocationsBeingOverlapped(
            productLocation,
            getLocationFromProduct(e))) {
                isLocationUnique = false;
            return;
        }
    });

    while (!isLocationUnique) {
        isLocationUnique = true;
        // const nearByRandomLocation = getRandomNearByLocationFromALocation(productLocation);
        // console.log('bambi sap tim random location', productLocation);
        const nearByRandomLocation = getRandomLocation(productLocation, 50);
        // console.log('bambi da tim xong random location', nearByRandomLocation);
        listProducts.forEach(e => {
            if (product.id !== e.id && isTwoLocationsBeingOverlapped(
                nearByRandomLocation,
                getLocationFromProduct(e))) {
                    isLocationUnique = false;
                return;
            }
        });

        if (isLocationUnique) { 
            product = getProductWithANewLocation(product, nearByRandomLocation);
        }
    }

    return product;
};

//Radius in meter
const getRandomLocation = (point, radius) => {
    const myLocation = {};
    myLocation.latitude = point.latitude;
    myLocation.longitude = point.longitude;
    const x0 = point.latitude;
    const y0 = point.longitude;

    // Convert radius from meters to degrees
    const radiusInDegrees = radius / 111000;

    const u = Math.random();
    const v = Math.random();
    const w = radiusInDegrees * Math.sqrt(u);
    const t = 2 * Math.PI * v;
    const x = w * Math.cos(t);
    const y = w * Math.sin(t);

    // Adjust the x-coordinate for the shrinking of the east-west distances
    const newX = x / Math.cos(y0);

    const foundLatitude = newX + x0;
    const foundLongitude = y + y0;
    const randomLatLng = { latitude: foundLatitude, longitude: foundLongitude };
    return measure(randomLatLng.latitude, randomLatLng.longitude, point.latitude, point.longitude)
     > 30 ? randomLatLng : getRandomLocation(point, radius);
};

const measure = (lat1, lon1, lat2, lon2) => {  // generally used geo measurement function
    const R = 6378.137; // Radius of earth in KM
    const dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
    const dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;
    return d * 1000; // meters
};

const getRandomNearByLocationFromALocation = (location) => {
    const randomRange = 1;
    const minRandom = 0.5;
    const randomLatitude = (minRandom + Math.random() * randomRange) * (Math.random * 100 < 50 ? -1 : 1);
    const randomLongitude = (minRandom + Math.random() * randomRange) * (Math.random * 100 < 50 ? -1 : 1);

    location.latitude = location.latitude + randomLatitude;
    location.longitude = location.latitude + randomLongitude;
    return location;
};

const isTwoLocationsBeingOverlapped = (locationA, locationB) => {
    if (locationA.latitude === locationB.latitude
            && locationA.longitude === locationB.longitude) {
        return true;
    }
    return false;
};

const ProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case SHORTEN_LIST:
        return update(state, {
            listProducts: {
            $set: state.listProducts.length > 10 ? state.listProducts.slice(0, 10) : state.listProducts
          }
        });
        case UPDATE_LIST_PRODUCTS:
            return update(state, {
                listProducts: {
                    $set: mergerTwoArray(state.listProducts, action.payload)
                },
                currentPage: {
                    $set: state.currentPage + 1,
                }
            });
        case RESET_LIST_PRODUCTS_ON_MAP:
            return update(state, {
                listProductsOnMap: {
                    $set: action.payload.map(e => getProductWithAUniqueLocation(e, action.payload))
                },
            });
        case RESET_LIST_PRODUCTS:
            return update(state, {
                listProducts: {
                    $set: action.payload
                },
                currentPage: {
                    $set: 1,
                },
                outOfData: {
                    $set: false,
                }
            });
        case OUTOFDATA_LIST_PRODUCTS:
            return update(state, {
                outOfData: {
                    $set: true,
                }
            });
        case UPDATE_DETAIL_NEW_PRODUCT:
            return update(state, {
                detailNewProduct: {
                    $set: action.payload
                }
            });
        case LOAD_AREA_FOR_NEW_PRODUCT_DETAIL:
            return update(state, {
                detailNewProduct: {
                    $set: {
                        ...state.detailNewProduct,
                        currentArea: action.payload,
                    }
                }
            });
        case UPDATE_AREA_FOR_NEW_PRODUCT_DETAIL:
            return update(state, {
                detailNewProduct: {
                    $set: {
                        ...state.detailNewProduct,
                        currentArea: action.payload,
                    }
                },
            });
        default:
            return state;
    }
};

export default ProductReducer;
