import update from 'react-addons-update';
import { UPDATE_LIST_NOTIFICATION, REFRESH_LIST_NOTIFICATION, CLEAR_DATA, SHORTEN_LIST } from '../../../constants/Constants';
import { mergerTwoArray } from '../../../utils/numberUtils';

const initialState = {
  listnotitfication: []
};

const NotificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_DATA: {
      return initialState;
    }
    case SHORTEN_LIST: {
      return update(state, {
        listnotitfication: {
        $set: state.listnotitfication.length > 10 ? state.listnotitfication.slice(0, 10) : state.listnotitfication
      }
    });
  }
    case UPDATE_LIST_NOTIFICATION:
      return update(state, {
        listnotitfication: {
          $set: mergerTwoArray(state.listnotitfication, action.payload)
        }
      });
    case REFRESH_LIST_NOTIFICATION:
      return update(state, {
        listnotitfication: {
          $set: action.payload
        }
      });
    default:
      return state;
  }
};

export default NotificationReducer;
