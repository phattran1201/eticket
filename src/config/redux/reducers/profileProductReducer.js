import update from 'react-addons-update';
import {
    LIST_PRODUCT_FOR_USER,
    LOAD_LIST_FAVOURITE_PRODUCTS_FOR_USER,
    ADD_ITEM_IN_START_POSITION_LIST_REVIEW_PRODUCT,
    UPDATE_LIST_FAVOURITE_PRODUCT,
    CLEAR_DATA,
    SHORTEN_LIST,
    UPDATE_LIST_FAVOURITE_PRODUCT_PAGES,
    OUT_OF_DATA_LIST_FAVOURITE_PRODUCT_PAGES,
    REFRESH_LIST_FAVOURITE_PRODUCT
} from '../../../constants/Constants';

const initialState = {
    listProductForUser: [],
    listFavouriteProduct: [],
    listFavouriteProductPages: 1,
    listFavouriteProductOutOfData: false,
};

const profileProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case CLEAR_DATA: {
            return initialState;
        }
        case SHORTEN_LIST: {
            return update(state, {
                listFavouriteProduct: {
                    $set: state.listFavouriteProduct.length > 10 ? state.listFavouriteProduct.slice(0, 10) : state.listFavouriteProduct
                },
                listProductForUser: {
                    $set: state.listProductForUser.length > 10 ? state.listProductForUser.slice(0, 10) : state.listProductForUser
                }
            });
        }
        case LIST_PRODUCT_FOR_USER:
            return update(state, {
                listProductForUser: {
                    $set: action.payload,
                }
            });
        case LOAD_LIST_FAVOURITE_PRODUCTS_FOR_USER:
            return update(state, {
                listFavouriteProduct: {
                    $set: action.payload,
                }
            });
        case UPDATE_LIST_FAVOURITE_PRODUCT:
            return update(state, {
                listFavouriteProduct: {
                    $set: action.payload,
                }
            });
        case ADD_ITEM_IN_START_POSITION_LIST_REVIEW_PRODUCT:
            return update(state, {
                listProductForUser: {
                    $set: [action.payload, ...state.listProductForUser]
                }
            });
        case UPDATE_LIST_FAVOURITE_PRODUCT_PAGES:
            return update(state, {
                listFavouriteProductPages: {
                    $set: state.listFavouriteProductPages + 1
                }
            });
        case OUT_OF_DATA_LIST_FAVOURITE_PRODUCT_PAGES:
            return update(state, {
                listFavouriteProductOutOfData: {
                    $set: true
                }
            });
        case REFRESH_LIST_FAVOURITE_PRODUCT: {
            return update(state, {
                listFavouriteProductPages: {
                    $set: 1
                },
                listFavouriteProductOutOfData: {
                    $set: false
                }
            });
        }
        default:
            return state;
    }
};

export default profileProductReducer;
