package com.itc.eticket;

import android.content.Intent;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import static com.itc.eticket.MainApplication.openningIncommingCallActivity;

/**
 * Created by macbook on 1/15/19.
 */

public class MyBridge extends ReactContextBaseJavaModule {

    public MyBridge(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @ReactMethod
    public void isOpenningIncommingCallActivity(Callback stringCallback) {
        stringCallback.invoke(openningIncommingCallActivity);
    }

    @ReactMethod
    public void navigateToLauncher() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getReactApplicationContext().startActivity(startMain);
    }

    @Override
    public String getName() {
        return "MyBridge";
    }
}