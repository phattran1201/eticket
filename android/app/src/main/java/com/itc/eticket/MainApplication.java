package com.itc.eticket;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.facebook.common.logging.FLog;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.react.ReactApplication;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.inprogress.reactnativeyoutube.ReactNativeYouTube;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.horcrux.svg.SvgPackage;
import com.airbnb.android.react.maps.MapsPackage;
import me.listenzz.modal.TranslucentModalReactPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.dooboolab.kakaologins.RNKakaoLoginsPackage;
import com.microsoft.codepush.react.CodePush;
import com.dooboolab.RNIap.RNIapPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import ca.jaysoo.extradimensions.ExtraDimensionsPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.avishayil.rnrestart.ReactNativeRestartPackage;
import com.facebook.soloader.SoLoader;
import com.nikolaiwarner.RNTextInputReset.RNTextInputResetPackage;
import com.levelasquez.androidopensettings.AndroidOpenSettingsPackage;
import com.zmxv.RNSound.RNSoundPackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import fr.bamlab.rnimageresizer.ImageResizerPackage;

import io.fabric.sdk.android.Fabric;
import io.invertase.firebase.RNFirebasePackage;

import com.imagepicker.ImagePickerPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;

import io.underscope.react.fbak.RNAccountKitPackage;

import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;

import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;

import com.facebook.CallbackManager;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.facebook.appevents.AppEventsLogger;


import java.util.Arrays;
import java.util.List;

import com.rndetectnavbarandroid.RNDetectNavbarAndroidPackage;

public class MainApplication extends MultiDexApplication implements ReactApplication {
    private static CallbackManager mCallbackManager = CallbackManager.Factory.create();
    protected static CallbackManager getCallbackManager() {
        return mCallbackManager;
    }

    public static boolean openningIncommingCallActivity = false;
    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

        @Override
        protected String getJSBundleFile() {
        return CodePush.getJSBundleFile();
        }
    
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(new MainReactPackage(),
            new RNCWebViewPackage(),
            new ReactNativeYouTube(),
            new SplashScreenReactPackage(),
            new SvgPackage(),
            new MapsPackage(),
            new TranslucentModalReactPackage(),
            new ReactVideoPackage(),
            new RNKakaoLoginsPackage(),
            new CodePush(getResources().getString(R.string.reactNativeCodePush_androidDeploymentKey), getApplicationContext(), BuildConfig.DEBUG),
            new LinearGradientPackage(),
            new RNIapPackage(),
            new MyBridgePackage(),
            new ExtraDimensionsPackage(), new RNGoogleSigninPackage(),
                    new ReactNativeRestartPackage(), new RNDetectNavbarAndroidPackage(),
                    new RNTextInputResetPackage(), new AndroidOpenSettingsPackage(), new RNSoundPackage(),
                    new LottiePackage(), new FastImageViewPackage(),
                    new RNFetchBlobPackage(), new ImageResizerPackage(),
                    new RNFirebasePackage(), new ImagePickerPackage(), new RNDeviceInfo(),
                    new ReactNativeLocalizationPackage(), new RNAccountKitPackage(),
                    new FBSDKPackage(mCallbackManager), new VectorIconsPackage(),
                    new io.invertase.firebase.messaging.RNFirebaseMessagingPackage(),
                    new RNFirebaseNotificationsPackage());
        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        Fabric.with(this, new Crashlytics());
        FLog.setLoggingDelegate(ReactNativeFabricLogger.getInstance());
        SoLoader.init(this, /* native exopackage */ false);
        AppEventsLogger.activateApp(this);
    }
}
