package com.itc.eticket;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.facebook.react.HeadlessJsTaskService;
import com.google.firebase.messaging.RemoteMessage;

import io.invertase.firebase.Utils;
import io.invertase.firebase.messaging.RNFirebaseBackgroundMessagingService;
import io.invertase.firebase.messaging.RNFirebaseMessagingService;

/**
 * Created by macbook on 1/8/19.
 */

public class MyRNFirebaseMessagingService extends RNFirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage message) {
        if (message.getData() != null && !android.text.TextUtils.isEmpty(message.getData().get("type")) && message.getData().get("type").equals("background_call")) {
            if (!Utils.isAppInForeground(this.getApplicationContext())) {
                // Finish all current activity (important)
                Intent intent = new Intent("background_call");
                LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(intent);

                // Start a new activity
                MainApplication.openningIncommingCallActivity = true;
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                i.putExtra("caller", message.getData().get("caller"));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
            }
        } else if (message.getData() != null && !android.text.TextUtils.isEmpty(message.getData().get("type")) && message.getData().get("type").equals("background_call_cancel")) {
          Intent intent = new Intent("background_call");
          intent.putExtra("isCancelType", true);
          LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(intent);
        } else {
            super.onMessageReceived(message);
        }
    }
}
