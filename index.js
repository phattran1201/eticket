/* eslint-disable max-line-length */
import { AppRegistry } from 'react-native';
import App from './src/modules/App';

console.disableYellowBox = true;

AppRegistry.registerComponent('App', () => App);
